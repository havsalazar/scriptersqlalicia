import { GenerateJsons } from './api/createJsons';
import {CreateScripts} from "./api/createScripts" 
import CreateScriptsV2  from './api/createScriptsv2'
//newJsonCreate();
//  CreateJsonSoat()
const createScripts=new CreateScripts 
//createScripts.createCupsSql()
//createScripts.createProceduresSoatql() 
// createScripts.createNewScripts() 
//createScripts.createERP();
// createScripts.createInstitutions();
function init() {}
 
// createScripts.createNewSqlSoat();
//CreateJsonEcopetrol();
//CreateJsonColmedica();
// CreateJsonSuraEps();
//CreateJsonERP()
// CreateJsonInstitutions()
// generateAllRAtes();
CreateScriptsV2.init();
function newJsonCreate(){
  const principalFuctions = new GenerateJsons() 
  var fs = require('fs');
  var dir = 'src/files/converted/cups';
  
  if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
      principalFuctions.convertXlsx({
        input: "src/files/homologacion.xlsx",
        output: dir+"/newCups.json",
        sheet: "CUPS 2017"
      });
      console.log('finiquitado')
  } 
}
function CreateJsonERP(){
  const principalFuctions = new GenerateJsons() 
  principalFuctions.convertXlsx({
    input: "src/files/erps.xlsx",
    output: "src/files/converted/erp.json",
    sheet: "Hoja1"
  });
  console.log('finiquitado')
}
function CreateJsonInstitutions(){
  const principalFuctions = new GenerateJsons() 
  principalFuctions.convertXlsx({
    input: "src/files/instituciones.xlsx",
    output: "src/files/converted/instituciones.json",
    sheet: "Cali"
  });
  console.log('finiquitado')
}
function CreateJsonSoat(){
  const principalFuctions = new GenerateJsons() 
  principalFuctions.convertXlsx({
    input: "src/files/soat.xlsx",
    output: "src/files/converted/soat.json",
    sheet: "soat1"
  });
  console.log('finiquitado')
}

function CreateJsonEcopetrol(){
  const principalFuctions = new GenerateJsons() 
  principalFuctions.convertXlsx({
    input: "src/files/ecopetrol.xlsx",
    output: "src/files/converted/ecopetrol.json",
    sheet: "FULL_HENRY"
  });
  console.log('finiquitado')
}

function CreateJsonColmedica(){
  const principalFuctions = new GenerateJsons() 
  principalFuctions.convertXlsx({
    input: "src/files/colmedica.xlsx",
    output: "src/files/converted/colmedica.json",
    sheet: "colmedica_vs_colmedica"
  });
  console.log('finiquitado')

}
function CreateJsonSuraEps(){
  const principalFuctions = new GenerateJsons() 
  principalFuctions.convertXlsx({
    input: "src/files/suraeps.xlsx",
    output: "src/files/converted/suraEps.json",
    sheet: "sura"
  });
  console.log('finiquitado')
  CreateJsonSuraPoliza()
}
function CreateJsonSuraPoliza(){
  const principalFuctions = new GenerateJsons() 
  principalFuctions.convertXlsx({
    input: "src/files/surapoliza.xlsx",
    output: "src/files/converted/suraPoliza.json",
    sheet: "Hoja1"
  });
  console.log('finiquitado')

}
function generateAllRAtes(){
  const principalFuctions = new GenerateJsons() 
  principalFuctions.convertXlsx({
    input: "src/files/tarifarios.xlsx",
    output: "src/files/converted/soat.json",
    sheet: "soat"
  });
  principalFuctions.convertXlsx({
    input: "src/files/tarifarios.xlsx",
    output: "src/files/converted/isso.json",
    sheet: "isso"
  });
  principalFuctions.convertXlsx({
    input: "src/files/tarifarios.xlsx",
    output: "src/files/converted/ecopetrol.json",
    sheet: "ecopetrol"
  });
  principalFuctions.convertXlsx({
    input: "src/files/tarifarios.xlsx",
    output: "src/files/converted/colmedica.json",
    sheet: "colmedica"
  });
  principalFuctions.convertXlsx({
    input: "src/files/tarifarios.xlsx",
    output: "src/files/converted/allianz.json",
    sheet: "allianz"
  });
  principalFuctions.convertXlsx({
    input: "src/files/tarifarios.xlsx",
    output: "src/files/converted/surapoliza.json",
    sheet: "sura poliza"
  });
  principalFuctions.convertXlsx({
    input: "src/files/tarifarios.xlsx",
    output: "src/files/converted/suraeps.json",
    sheet: "sura mp y eps"
  });
}