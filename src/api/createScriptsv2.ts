import { uniqBy, filter, map } from 'ramda'
class CreateScriptsV2 {

    private fs = require('fs');

    constructor() {
    }
    init() {
        this.CreateSoat();
        this.CreateIsso();
        this.CreateEcopetrol();
        this.CreateColmedica();
        this.CreateAllianz();
        this.CreateSuraPoliza();
        this.CreateSuraEMP();
    }
    CreateSoat() {
        const dir = __dirname + './../files/converted/soat';
        if (!this.fs.existsSync(dir)) {
            this.fs.mkdirSync(dir);
        }
        const rate = require('./../files/converted/soat.json');
        const unique = uniqBy(p => p['Cod SOAT'], rate);
        let sqlrates = '';
        let sqlCups = '';
        let sqlValues = '';
        unique.map(obj => {
            let newText = obj['Descripción SOAT'];
            newText = newText.replace("'", "''");
            sqlrates = sqlrates.concat(`\n INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '${obj['Cod SOAT']}', '${newText}' , 1, now(), now(), 'f', 1);\n`);
            map((y) => {
                sqlCups = sqlCups.concat(`\n INSERT INTO "rate_cups_procedures"("procedure_id","rate_procedure_id", "created_at", "updated_at") 
                VALUES (get_cups_id('${y['Codigo cups']}'),get_rate_procedure_id('${obj['Cod SOAT']}',1), now(),now() );\n`);
            }, filter((x) => x['Cod SOAT'] === obj['Cod SOAT'], rate));
            const group = filter((x) => {
                return x['Cod SOAT'] === obj['Cod SOAT'] &&
                    `${x['grupo_soat']}` != '0' && `${x['grupo_soat']}` != ''
            }, rate) || [];
            if (group.length > 0) {
                if (`${group[0]['grupo_soat']}` != '0' && `${group[0]['grupo_soat']}` != '') {
                    sqlValues = sqlValues.concat(`\n INSERT INTO "procedure_surgical_groups"( "rate_procedure_id", "surgical_group_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES (get_rate_procedure_id('${obj['Cod SOAT']}',1), ${group[0]['grupo_soat']}, now(), now(), 'f', 1); \n`);
                }
            }
        });
        const data = {
            rate: '/sqlrates.sql',
            rateData: sqlrates,
            rate_cups: '/sqlCups.sql',
            rate_cups_data: sqlCups,
            values: '/sqlValues.sql',
            values_data: sqlValues
        }
        this.createFiles(dir, data);
    }
    CreateIsso() {
        const dir = __dirname + './../files/converted/isso';
        if (!this.fs.existsSync(dir)) {
            this.fs.mkdirSync(dir);
        }
        const rate = require('./../files/converted/isso.json');
        const unique = uniqBy(p => p['cod ISSO'], rate);
        let sqlrates = '';
        let sqlCups = '';
        let sqlValues = '';
        unique.map(obj => {
            let newText = obj['Descripción ISSO'];
            newText = newText.replace("'", "''");
            sqlrates = sqlrates.concat(`\n INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '${obj['cod ISSO']}', '${newText}' ,4,  now(),  now(), 'f', 1);\n`);
            map((y) => {
                sqlCups = sqlCups.concat(`\n INSERT INTO "rate_cups_procedures"("procedure_id","rate_procedure_id", "created_at", "updated_at") 
                VALUES (get_cups_id('${y['Codigo cups']}'), get_rate_procedure_id('${obj['cod ISSO']}',8),  now(),  now());\n`);
            }, filter((x) => x['cod ISSO'] === obj['cod ISSO'], rate));
            sqlValues = sqlValues.concat(`INSERT INTO procedure_uvr_numbers ("rate_procedure_id","uvr_number", "created_at", "updated_at", "erased", "user_id")
                VALUES (get_rate_procedure_id('${obj['cod ISSO']}',4), ${obj['UVR']} , now() , now(),'f',1);\n`);
        }); 
        const data = {
            rate: '/sqlrates.sql',
            rateData: sqlrates,
            rate_cups: '/sqlCups.sql',
            rate_cups_data: sqlCups,
            values: '/sqlValues.sql',
            values_data: sqlValues
        }
        this.createFiles(dir, data); 
    }
    CreateEcopetrol() {
        const dir = __dirname + './../files/converted/ecopetrol'; 
        if (!this.fs.existsSync(dir)) {
            this.fs.mkdirSync(dir); 
        }
        const rate = require('./../files/converted/ecopetrol.json');
        const unique = uniqBy(p => p['Cod ECOP'], rate);
        let sqlrates = '';
        let sqlCups = '';
        let sqlValues = '';
        unique.map(obj => {
            let newText = obj['Descripción ECOPETROL'];
            newText = newText.replace("'", "''");
            sqlrates = sqlrates.concat(`\n INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ('${obj['Cod ECOP']}','${newText}' ,2, now(), now(), 'f', 1);\n`);
            map((y) => {
                sqlCups = sqlCups.concat(`\n INSERT INTO "rate_cups_procedures"("procedure_id","rate_procedure_id", "created_at", "updated_at") 
                VALUES (get_cups_id('${obj['Codigo cups']}'), get_rate_procedure_id('${obj['Cod ECOP']}',2), now(), now());\n`);
            }, filter((x) => x['Cod ECOP'] === obj['Cod ECOP'], rate));
            sqlValues = sqlValues.concat(`INSERT INTO "procedure_static_values"( "rate_procedure_id", "value", "created_at", "updated_at", "erased", "user_id")
                   VALUES (get_rate_procedure_id('${obj['Cod ECOP']}',2), ${obj['tarifa_fija']}, now(), now(), 'f', 1); \n`); 
        });
        const data = {
            rate: '/sqlrates.sql',
            rateData: sqlrates,
            rate_cups: '/sqlCups.sql',
            rate_cups_data: sqlCups,
            values: '/sqlValues.sql',
            values_data: sqlValues
        }
        this.createFiles(dir, data); 
    } 
    CreateColmedica(){
        const dir = __dirname + './../files/converted/colmedica'; 
        if (!this.fs.existsSync(dir)) {
            this.fs.mkdirSync(dir); 
        }
        const rate = require('./../files/converted/colmedica.json');
        const unique = uniqBy(p => p['cod COLME'], rate);
        let sqlrates = '';
        let sqlCups = '';
        let sqlValues = '';
        unique.map(obj => {
            let newText = obj['Descripción COLMEDICA'];
            newText = newText.replace("'", "''");
            sqlrates = sqlrates.concat(`\n INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('${obj['cod COLME']}', '${newText}' ,8, now(), now(), 'f', 1);`);
            map((y) => {
                sqlCups = sqlCups.concat(`\n INSERT INTO "rate_cups_procedures"("procedure_id","rate_procedure_id", "created_at", "updated_at") 
                VALUES (get_cups_id('${obj['Codigo cups']}'), get_rate_procedure_id('${obj['cod COLME']}',8), now(), now());`);
            }, filter((x) => x['cod COLME'] === obj['cod COLME'], rate));
            sqlValues = sqlValues.concat(`\n INSERT INTO procedure_uvr_numbers ("rate_procedure_id","uvr_number", "created_at", "updated_at", "erased", "user_id")
            VALUES (get_rate_procedure_id('${obj['cod COLME']}',8), ${obj['UVR']} , now() , now(),'f',1); `); 
        });
        const data = {
            rate: '/sqlrates.sql',
            rateData: sqlrates,
            rate_cups: '/sqlCups.sql',
            rate_cups_data: sqlCups,
            values: '/sqlValues.sql',
            values_data: sqlValues
        }
        this.createFiles(dir, data); 
    }
    CreateAllianz(){
        const dir = __dirname + './../files/converted/allianz'; 
        if (!this.fs.existsSync(dir)) {
            this.fs.mkdirSync(dir); 
        }
        const rate = require('./../files/converted/allianz.json');
        const unique = uniqBy(p => p['Cod ALLIA'], rate);
        let sqlrates = '';
        let sqlCups = '';
        let sqlValues = '';
        unique.map(obj => {
            let newText = obj['Descripción ALLIANZ'];
            newText = newText.replace("'", "''");
            sqlrates = sqlrates.concat(`\n INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('${obj['Cod ALLIA']}', '${newText}' ,13, now(), now(), 'f', 1);`);
            map((y) => {
                sqlCups = sqlCups.concat(`\n INSERT INTO "rate_cups_procedures"("procedure_id","rate_procedure_id", "created_at", "updated_at") 
                VALUES (get_cups_id('${obj['Codigo cups']}'), get_rate_procedure_id('${obj['Cod ALLIA']}',13), now(), now());`);
            }, filter((x) => x['Cod ALLIA'] === obj['Cod ALLIA'], rate));
            sqlValues = sqlValues.concat(`\n INSERT INTO procedure_uvr_numbers ("rate_procedure_id","uvr_number", "created_at", "updated_at", "erased", "user_id")
            VALUES (get_rate_procedure_id('${obj['Cod ALLIA']}',13), ${obj['UVR']} , now() , now(),'f',1); `); 
        });
        const data = {
            rate: '/sqlrates.sql',
            rateData: sqlrates,
            rate_cups: '/sqlCups.sql',
            rate_cups_data: sqlCups,
            values: '/sqlValues.sql',
            values_data: sqlValues
        }
        this.createFiles(dir, data); 
    }


    CreateSuraPoliza(){
        const dir = __dirname + './../files/converted/surapoliza'; 
        if (!this.fs.existsSync(dir)) {
            this.fs.mkdirSync(dir); 
        }
        const rate = require('./../files/converted/surapoliza.json');
        const unique = uniqBy(p => p['Cod POLIZA'], rate);
        let sqlrates = '';
        let sqlCups = '';
        let sqlValues = '';
        unique.map(obj => {
            let newText = obj['Descripción SURA POLIZA'];
            newText = newText.replace("'", "''");
            sqlrates = sqlrates.concat(`\n INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('${obj['Cod POLIZA']}', '${newText}' ,9, now(), now(), 'f', 1);`);
            map((y) => {
                sqlCups = sqlCups.concat(`\n INSERT INTO "rate_cups_procedures"("procedure_id","rate_procedure_id", "created_at", "updated_at") 
                VALUES (get_cups_id('${obj['Codigo cups']}'), get_rate_procedure_id('${obj['Cod POLIZA']}',9), now(), now());`);
            }, filter((x) => x['Cod POLIZA'] === obj['Cod POLIZA'], rate));
            sqlValues = sqlValues.concat(`\n INSERT INTO procedure_uvr_numbers ("rate_procedure_id","uvr_number", "created_at", "updated_at", "erased", "user_id")
            VALUES (get_rate_procedure_id('${obj['Cod POLIZA']}',9), ${obj['UVR']} , now() , now(),'f',1); `); 
        }); 
        const data = {
            rate: '/sqlrates.sql',
            rateData: sqlrates,
            rate_cups: '/sqlCups.sql',
            rate_cups_data: sqlCups,
            values: '/sqlValues.sql',
            values_data: sqlValues
        }
        this.createFiles(dir, data); 
    }
    CreateSuraEMP(){
        const dir = __dirname + './../files/converted/suraeps'; 
        if (!this.fs.existsSync(dir)) {
            this.fs.mkdirSync(dir); 
        }
        const rate = require('./../files/converted/suraeps.json');
        const unique = uniqBy(p => p['Cod MP'], rate);
        let sqlrates = '';
        let sqlCups = '';
        let sqlValues = '';
        unique.map(obj => {
            let newText = obj['Descripción SURA MP Y EPS'];
            newText = newText.replace("'", "''");
            sqlrates = sqlrates.concat(`\n INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('${obj['Cod MP']}', '${newText}' ,10, now(), now(), 'f', 1);`);
            map((y) => {
                sqlCups = sqlCups.concat(`\n INSERT INTO "rate_cups_procedures"("procedure_id","rate_procedure_id", "created_at", "updated_at") 
                VALUES (get_cups_id('${obj['Codigo cups']}'), get_rate_procedure_id('${obj['Cod MP']}',9), now(), now());`);
            }, filter((x) => x['Cod MP'] === obj['Cod MP'], rate));
            sqlValues = sqlValues.concat(`\n INSERT INTO procedure_uvr_numbers ("rate_procedure_id","uvr_number", "created_at", "updated_at", "erased", "user_id")
            VALUES (get_rate_procedure_id('${obj['Cod MP']}',10), ${obj['UVR']} , now() , now(),'f',1); `); 
        }); 
        const data = {
            rate: '/sqlrates.sql',
            rateData: sqlrates,
            rate_cups: '/sqlCups.sql',
            rate_cups_data: sqlCups,
            values: '/sqlValues.sql',
            values_data: sqlValues
        }
        this.createFiles(dir, data); 

    }

    private createFiles(dir, data) {
        this.fs.writeFile(dir + data.rate, data.rateData, (err) => {
            if (err) return console.error(err);
            console.log(`${data.rate} created!`);
        });
        this.fs.writeFile(dir + data.rate_cups, data.rate_cups_data, (err) => {
            if (err) return console.error(err);
            console.log(`${data.rate_cups} created!`);
        });
        this.fs.writeFile(dir + data.values, data.values_data, (err) => {
            if (err) return console.error(err);
            console.log(`${data.values} created!`);
        });
    }
}
export default new CreateScriptsV2();