
// import * as cupsFile from "../files/converted/cups.json";
// import * as soatFile from "../files/converted/soat.json"
import { uniqBy, filter, map } from 'ramda'
export class CreateScripts {

    private fs = require('fs');

    constructor() { 
    }

    createERP() {
        const dir = __dirname + './../files/converted/erp';

        if (!this.fs.existsSync(dir)) {
            this.fs.mkdirSync(dir);
        }
        const erps = require('./../files/converted/erp.json');
        let sqlERP = '';

        erps.map(obj => {
            sqlERP = sqlERP.concat(`\n
            INSERT INTO "entity_responsible_payments"("name", "created_at", "updated_at", "erased", "created_by_id", "updated_by_id", "code")
             VALUES ('${obj['NOMBRE ENTIDAD']}', now(), noW(), 'f', 1, 1, '${obj['CODIGO']}');

            `);
        });
        this.fs.writeFile(dir + '/sqlERP.sql', sqlERP, (err) => {
            if (err) {
                return console.error(err);
            }
            console.log("sqlERP created!");
        });
    }
    createInstitutions(){
        const dir = __dirname + './../files/converted/institutions';

        if (!this.fs.existsSync(dir)) {
            this.fs.mkdirSync(dir);
        }
        const institutions = require('./../files/converted/instituciones.json');
        let sqlInstitutions = '';

        institutions.map(obj => {
            sqlInstitutions = sqlInstitutions.concat(`\n
            INSERT INTO "institutions" ( "nit", "name", "business_name", "business_type", "address", "phone", "email", "manager", "erased", "city_id", "user_id", "created_at", "updated_at")
             VALUES ( '${obj['nits_nit']}', '${obj['nombre_prestador']}', '${obj['razon_social']}', NULL, '${obj['direccion']}', '${obj['telefono']}', '${obj['email']}', '${obj['gerente']}',
             'f', 48324, 1, now(), now());

            `);
        });
        this.fs.writeFile(dir + '/sqlInstitutions.sql', sqlInstitutions, (err) => {
            if (err) {
                return console.error(err);
            }
            console.log("sqlInstitutions created!");
        });
    }

    createNewScripts() {
        let cupsArray = [];
        let sqlProcedures = '';
        const principalJson = require(__dirname + './../files/converted/newCups.json');
        for (let index = 0; index < principalJson.length; index++) {
            const obj = principalJson[index]
            cupsArray.push({
                id: obj['Id Alicia'],
                code: obj['Codigo'],
                description: obj['Descripcion Procedimiento'],
            });

            sqlProcedures = sqlProcedures.concat(`\n INSERT INTO "procedures"("id", "code", "description", "created_at", "updated_at", "erased", "user_id") 
                VALUES (${obj['Id Alicia']}, '${obj['Codigo']}', '${obj['Descripcion Procedimiento']}', '2018-11-08 12:53:00', '2018-11-08 12:53:00', 'f', 1); \n`);
            /*if (obj['Codigo SOAT']) {
                soatArray.push({
                    id: obj['id_tarifa_soat'],
                    custom_code: obj['Codigo SOAT'],
                    gruop: obj['grupo soat'],
                    uvr: obj['UVR']
                })
                sqlSOAT = sqlSOAT.concat(`\n INSERT INTO "rate_procedures"("id", "custom_code", "procedure_id", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES (${obj['id_tarifa_soat']}, '${obj['Codigo SOAT']}', ${obj['Id Alicia']}, 1, '2019-04-03 14:29:24', '2019-04-03 14:29:28', 'f', 1);\n`);

                if (obj['grupo soat'] && obj['grupo soat'] > 0) {
                    sqlSOAT = sqlSOAT.concat(`\n INSERT INTO "procedure_surgical_groups"( "rate_procedure_id", "surgical_group_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES (${obj['id_tarifa_soat']}, ${obj['grupo soat']}, '2018-09-25 09:27:00', '2018-09-25 09:27:00', 'f', 1); \n`);
                }

                if (obj['UVR'] && obj['UVR'] > 0) {
                    sqlSOAT = sqlSOAT.concat(`\n  INSERT INTO "procedure_uvr_numbers"( "rate_procedure_id", "uvr_number", "created_at", "updated_at", "erased", "user_id") 
                    VALUES (${obj['id_tarifa_soat']}, ${obj['UVR']}, '2019-04-04 16:58:12', '2019-04-04 16:58:19', 'f', 1); \n`);
                }
            }
            if (obj['Codigo Ecopetrol 1']) {
                ecopetrolArray.push({
                    custom_code: obj['Codigo Ecopetrol 1'],
                    id: obj['id_tarifa'],
                    value: obj['valor_tarifa_fija'],
                })
                sqlEcopetrol = sqlEcopetrol.concat(`\n INSERT INTO "rate_procedures"("id", "custom_code", "procedure_id", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES (${obj['id_tarifa']}, '${obj['Codigo Ecopetrol 1']}', ${obj['Id Alicia']}, 1, '2019-04-03 14:29:24', '2019-04-03 14:29:28', 'f', 1);\n`);
                if(obj['valor_tarifa_fija'] && obj['valor_tarifa_fija']>0){
                    sqlEcopetrol = sqlEcopetrol.concat(`\n 
                    INSERT INTO "public"."procedure_static_values"( "rate_procedure_id", "value", "created_at", "updated_at", "erased", "user_id")
                     VALUES (${obj['id_tarifa']}, ${obj['valor_tarifa_fija']}, '2019-04-04 17:20:07', '2019-04-04 17:20:10', 'f', 1);

                    \n`);
                }

            }*/


        }
        // this.fs.writeFile(__dirname + './../files/converted/jsonCups.json', JSON.stringify(cupsArray), (err) => {
        //     if (err) {
        //         return console.error(err);
        //     }
        //     console.log("jsonCups created!");
        // });
        // this.fs.writeFile(__dirname + './../files/converted/jsonSura.json', JSON.stringify(soatArray), (err) => {
        //     if (err) {
        //         return console.error(err);
        //     }
        //     console.log("jsonSura created!");
        // });
        // this.fs.writeFile(__dirname + './../files/converted/jsonEcopetrol.json', JSON.stringify(ecopetrolArray), (err) => {
        //     if (err) {
        //         return console.error(err);
        //     }
        //     console.log("jsonEcopetrol created!");
        // });
        // //SqlGeneration
        // this.fs.writeFile(__dirname + './../files/converted/sqlSoatRate.sql', sqlSOAT, (err) => {
        //     if (err) {
        //         return console.error(err);
        //     }
        //     console.log("soatRatesql created!");
        // });
        this.fs.writeFile(__dirname + './../files/converted/sqlProcedures.sql', sqlProcedures, (err) => {
            if (err) {
                return console.error(err);
            }
            console.log("sqlProcedures created!");
        });
        // this.fs.writeFile(__dirname + './../files/converted/sqlEcopetrolRate.sql', sqlEcopetrol, (err) => {
        //     if (err) {
        //         return console.error(err);
        //     }
        //     console.log("sqlEcopetrol created!");
        // });


    }


    createNewSqlSoat() {
        const dir = __dirname + './../files/converted/soat';

        if (!this.fs.existsSync(dir)) {
            this.fs.mkdirSync(dir);

        }
        const soat = require('./../files/converted/soat.json');
        // {"valor":"0","Codigo CUPS":"010101","DESC CUPS":"PUNCIoN CISTERNAL, ViA LATERAL","Codigo SOAT":"1250","grupo soat":"4","DESCRIPCIÓN DEL SOAT":"Punción cisternal"}
        const uniqueSoat = uniqBy(p => p['Codigo SOAT'], soat);
        let sqlSoat = '';
        let sqlCupsSoat = '';
        let sqlGroupsSoat = '';
        this.fs.writeFile(dir + '/jsonSOATunique.json', JSON.stringify(uniqueSoat), (err) => {
            if (err) {
                return console.error(err);
            }
            let id = 0;
            const newSoat = uniqueSoat.map((x) => {
                id += 1;
                x.id = id;
                return x;
            });
            newSoat.map(obj => {

                let newText = obj['DESCRIPCIÓN DEL SOAT'];
                newText = newText.replace("'", "''");
                sqlSoat = sqlSoat.concat(`\n INSERT INTO "rate_procedures"("id", "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES (${obj['id']}, '${obj['Codigo SOAT']}', '${newText}' , 1, '2019-04-03 14:29:24', '2019-04-03 14:29:28', 'f', 1);\n`);
                map((y) => {
                    sqlCupsSoat = sqlCupsSoat.concat(`\n INSERT INTO "rate_cups_procedures"("procedure_id","rate_procedure_id", "created_at", "updated_at") 
                    VALUES (get_cups_id('${obj['Codigo CUPS']}'), '${obj['id']}', '2019-04-03 14:29:24', '2019-04-03 14:29:28');\n`);
                }, filter((x) => x['Codigo SOAT'] === obj['Codigo SOAT'], soat))
                // if (obj['grupo soat'] && obj['grupo soat'] > 0) {

                const group = filter((x) => {
                    return x['Codigo SOAT'] === obj['Codigo SOAT'] &&
                        `${x['grupo soat']}` != '0' && `${x['grupo soat']}` != ''
                }, soat) || [];
                const valor = filter((x) => {
                    return x['Codigo SOAT'] === obj['Codigo SOAT'] &&
                        `${x['valor']}` != '0' && `${x['valor']}` != ''
                }, soat) || [];
                if (group.length > 0) {
                    if (`${group[0]['grupo soat']}` != '0' && `${group[0]['grupo soat']}` != '') {
                        sqlGroupsSoat = sqlGroupsSoat.concat(`\n INSERT INTO "procedure_surgical_groups"( "rate_procedure_id", "surgical_group_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES (get_rate_procedure_id('${obj['Codigo SOAT']}',1), ${group[0]['grupo soat']}, '2018-09-25 09:27:00', '2018-09-25 09:27:00', 'f', 1); \n`);
                    }
                }
                if (valor.length > 0) {
                    if (`${valor[0]['valor']}` != '0' && `${valor[0]['valor']}` != '') {
                        sqlGroupsSoat = sqlGroupsSoat.concat(`\n 
                    INSERT INTO "public"."procedure_static_values"( "rate_procedure_id", "value", "created_at", "updated_at", "erased", "user_id")
                     VALUES (get_rate_procedure_id('${obj['Codigo SOAT']}',1), ${valor[0]['valor']}, '2019-04-04 17:20:07', '2019-04-04 17:20:10', 'f', 1); 
                    \n`);
                    }
                }
                // }
            });

            this.fs.writeFile(dir + '/sqlSoatNew.sql', sqlSoat, (err) => {
                if (err) {
                    return console.error(err);
                }
                console.log("sqlSoatNew created!");
            });
            this.fs.writeFile(dir + '/sqlRateCups.sql', sqlCupsSoat, (err) => {
                if (err) {
                    return console.error(err);
                }
                console.log("sqlCupsSoat created!");
            });
            this.fs.writeFile(dir + '/sqlGroupsSoat.sql', sqlGroupsSoat, (err) => {
                if (err) {
                    return console.error(err);
                }
                console.log("sqlGroupsSoat created!");
            });
            this.createNewSqlEcopetrol(id)

            //console.log("jsonSOATunique created!");
        });

    }
    createNewSqlEcopetrol(id) {
        const dir = __dirname + './../files/converted/ecopetrol';

        if (!this.fs.existsSync(dir)) {
            this.fs.mkdirSync(dir);

        }
        const ecopetrol = require('./../files/converted/ecopetrol.json');

        let sqlEcopetrol = '';
        let sqlRateCupsEcopetrol = '';
        let sqlValuesEcopetrol = '';

        const newEcopetrol = ecopetrol.map((x) => {
            id += 1;
            return { ...x, id };
        });
        newEcopetrol.map(obj => {
            sqlEcopetrol = sqlEcopetrol.concat(`\n INSERT INTO "rate_procedures"("id", "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES (${obj['id']}, '${obj['Codigo Ecopetrol 1']}', '${obj['Descripción Ecopetrol 1']}' ,2, '2019-04-03 14:29:24', '2019-04-03 14:29:28', 'f', 1);\n`);

            sqlRateCupsEcopetrol = sqlRateCupsEcopetrol.concat(`\n INSERT INTO "rate_cups_procedures"("procedure_id","rate_procedure_id", "created_at", "updated_at") 
                VALUES (get_cups_id('${obj['Codigo']}'), get_rate_procedure_id('${obj['Codigo Ecopetrol 1']}',2), '2019-04-03 14:29:24', '2019-04-03 14:29:28');\n`);
            if (obj['valor_tarifa_fija'] != 'na') {
                sqlValuesEcopetrol = sqlValuesEcopetrol.concat(`\n 
                    INSERT INTO "public"."procedure_static_values"( "rate_procedure_id", "value", "created_at", "updated_at", "erased", "user_id")
                     VALUES (get_rate_procedure_id('${obj['Codigo Ecopetrol 1']}',2), ${obj['valor_tarifa_fija']}, '2019-04-04 17:20:07', '2019-04-04 17:20:10', 'f', 1); 
                    \n`);
            }
        })
        this.fs.writeFile(dir + '/sqlEcopetrol.sql', sqlEcopetrol, (err) => {
            if (err) {
                return console.error(err);
            }
            console.log("sqlProcedures created!");
        });
        this.fs.writeFile(dir + '/sqlRateCupsEcopetrol.sql', sqlRateCupsEcopetrol, (err) => {
            if (err) {
                return console.error(err);
            }
            console.log("sqlProcedures created!");
        });
        this.fs.writeFile(dir + '/sqlValuesEcopetrol.sql', sqlValuesEcopetrol, (err) => {
            if (err) {
                return console.error(err);
            }
            console.log("sqlProcedures created!");
        });
        this.createNewSqlColmedica(id);
    }
    createNewSqlColmedica(id) {
        const dir = __dirname + './../files/converted/colmedica';

        if (!this.fs.existsSync(dir)) {
            this.fs.mkdirSync(dir);

        }
        const colmedica = require('./../files/converted/colmedica.json');

        let sqlcolmedica = '';
        let sqlRateCupscolmedica = '';
        let sqlValuescolmedica = '';
        const newColmedica = colmedica.map((x) => {
            id += 1;
            return { ...x, id };
        });
        newColmedica.map(obj => {
            sqlcolmedica = sqlcolmedica.concat(`\n INSERT INTO "rate_procedures"("id", "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES (${obj.id}, '${obj['Codigo colmedica']}', '${obj['Descripcion colmedica']}' ,8, '2019-04-03 14:29:24', '2019-04-03 14:29:28', 'f', 1);\n`);

            sqlRateCupscolmedica = sqlRateCupscolmedica.concat(`\n INSERT INTO "rate_cups_procedures"("procedure_id","rate_procedure_id", "created_at", "updated_at") 
                VALUES (get_cups_id('${obj['Codigo']}'), get_rate_procedure_id('${obj['Codigo colmedica']}',8), '2019-04-03 14:29:24', '2019-04-03 14:29:28');\n`);
            if (obj['valor_tarifa_fija'] != 'na') {
                sqlValuescolmedica = sqlValuescolmedica.concat(`\n 
                    INSERT INTO procedure_uvr_numbers ("rate_procedure_id","uvr_number", "created_at", "updated_at", "erased", "user_id")
                    VALUES (get_rate_procedure_id('${obj['Codigo colmedica']}',8), ${obj['GUPO/UVR']} , now() , now(),'f',1); 
                    \n`);
            }
        })


        this.fs.writeFile(dir + '/sqlColmedica.sql', sqlcolmedica, (err) => {
            if (err) {
                return console.error(err);
            }
            console.log("sqlcolmedica created!");
        });
        this.fs.writeFile(dir + '/sqlRateCupsColmedica.sql', sqlRateCupscolmedica, (err) => {
            if (err) {
                return console.error(err);
            }
            console.log("sqlRateCupscolmedica created!");
        });
        this.fs.writeFile(dir + '/sqlValuesColmedica.sql', sqlValuescolmedica, (err) => {
            if (err) {
                return console.error(err);
            }
            console.log("sqlValuescolmedica created!");
        });

    }
    createNewSqlSuraEPS(id) {
        const dir = __dirname + './../files/converted/suraEPS';

        if (!this.fs.existsSync(dir)) {
            this.fs.mkdirSync(dir);
        }
        const sura = require('./../files/converted/suraeps.json');

        let sqlsura = '';
        let sqlRateCupssura = '';
        let sqlValuessura = '';
        const newSura = sura.map((x) => {
            id += 1;
            return { ...x, id };
        });
        newSura.map(obj => {
            sqlsura = sqlsura.concat(`\n INSERT INTO "rate_procedures"("id", "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES (${obj.id}, '${obj['Codigo colmedica']}', '${obj['Descripcion colmedica']}' ,8, '2019-04-03 14:29:24', '2019-04-03 14:29:28', 'f', 1);\n`);

            sqlRateCupssura = sqlRateCupssura.concat(`\n INSERT INTO "rate_cups_procedures"("procedure_id","rate_procedure_id", "created_at", "updated_at") 
                VALUES (get_cups_id('${obj['Codigo']}'), get_rate_procedure_id('${obj['Codigo colmedica']}',8), '2019-04-03 14:29:24', '2019-04-03 14:29:28');\n`);
            if (obj['valor_tarifa_fija'] != 'na') {
                sqlValuessura = sqlValuessura.concat(`\n 
                    INSERT INTO procedure_uvr_numbers ("rate_procedure_id","uvr_number", "created_at", "updated_at", "erased", "user_id")
                    VALUES (get_rate_procedure_id('${obj['Codigo colmedica']}',8), ${obj['GUPO/UVR']} , now() , now(),'f',1); 
                    \n`);
            }
        })


        this.fs.writeFile(dir + '/sqlsuraEPS.sql', sqlsura, (err) => {
            if (err) {
                return console.error(err);
            }
            console.log("sqlsura created!");
        });
        this.fs.writeFile(dir + '/sqlRateCupsSuraEPS.sql', sqlRateCupssura, (err) => {
            if (err) {
                return console.error(err);
            }
            console.log("sqlRateCupssura created!");
        });
        this.fs.writeFile(dir + '/sqlValuessura.sql', sqlValuessura, (err) => {
            if (err) {
                return console.error(err);
            }
            console.log("sqlValuessura created!");
        });
    }
    createNewSqlSuraPoliza(id) {
        const fs = require('fs');
        const dir = __dirname + './../files/converted/suraEPS';

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
    }

}