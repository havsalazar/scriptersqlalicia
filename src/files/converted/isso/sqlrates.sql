
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '10101', 'PUNCION CISTERNAL VIA LATERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '10102', 'PUNCION CISTERNAL VIA MEDIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '10201', 'PUNCION ASPIRACION DE LIQUIDO VENTRICULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '10202', 'PUNCION ASPIRACION DE LIQUIDO VENTRICULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '10203', 'PUNCION ASPIRACION DE LIQUIDO VENTRICULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '10901', 'PUNCION SUBDURAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '11101', 'BIOPSIA OSEA EN CRANEO POR CRANEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '11201', 'BIOPSIA DE MENINGE POR CRANEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '11302', 'BIOPSIA ABIERTA CRANEOTOMIA DE CEREBRO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '11303', 'BIOPSIA DE CEREBRO POR TREPANACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '11304', 'BIOPSIA ESTEREOTAXICA DE CEREBRO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '12101', 'CRANEALIZACION DE SENO FRONTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '12401', 'DRENAJE DE ESPACIO EPIDURAL SUPRATENTORI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '12402', 'DRENAJE DE ESPACIO EPIDURAL FOSA POSTERI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '12410', 'EXTRACCION DE CUERPO EXTRAÑO INTRACRANEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '12502', 'DRENAJE DE COLECCION EPIDURAL SUPRATENTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '12503', 'DRENAJE DE COLECCION EPIDURAL FOSA POSTE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '13101', 'DRENAJE DE ESPACIO SUBDURAL POR CRANEOTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '13102', 'DRENAJE DE ESPACIO SUBDURAL POR TREPANAC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '13103', 'DRENAJE DE ESPACIO SUBDURAL EN FOSA POST' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '13104', 'DRENAJE DE ESPACIO SUBDURAL POR DRENAJE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '13105', 'DRENAJE DE ESPACIO SUBARACNOIDEO POR DER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '13106', 'DRENAJE DE ESPACIO SUBDURAL POR DERIVACI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '13110', 'DRENAJE DE ESPACIO SUBDURAL CON REPARO D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '13201', 'SECCION DE TEJIDO CEREBRAL TRACTOS CEREB' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '13202', 'SECCION DE TEJIDO CEREBRAL TRACTOS CEREB' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '14101', 'TALAMOTOMIA POR ESTEREOTAXIA ESTIMULACIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '14201', 'PALIDOTOMIA POR ESTEREOTAXIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15101', 'RESECCION TUMOR OSEO POR CRANEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15102', 'RESECCION TUMOR OSEO POR CRANIECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15104', 'CORRECCION DE DISPLASIA FIBROSA DEL CRAN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15201', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15202', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15203', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15204', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15301', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15302', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15304', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15305', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15306', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15307', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15308', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15309', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15310', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15311', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15401', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15402', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15403', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15404', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15405', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15406', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15407', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15408', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15409', 'RESECCION DE TUMOR DE LA BASE DEL CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '15501', 'TOMA DE INJERTO OSEO DE CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '16101', 'RESECCION TUMOR DE MENINGE CEREBRAL POR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '16102', 'RESECCION TUMOR DE MENINGE CEREBRAL POR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '16201', 'RESECCION TUMOR DE LA HOZ POR CRANEOTOMI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '16301', 'RESECCION TUMOR DEL TENTORIO POR CRANEOT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '16401', 'DRENAJE DE QUISTE ARACNOIDEO GUIADO POR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17001', 'DRENAJE DE COLECCIONES INTRACEREBRALES P' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17002', 'DRENAJE DE COLECCIONES INTRACEREBRALES G' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17004', 'DRENAJE DE COLECCIONES INTRACEREBRALES D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17005', 'DRENAJE DE COLECCIONES INTRACEREBRALES D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17201', 'RESECCION DE TUMOR SUPRATENTORIAL HEMISF' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17202', 'RESECCION DE TUMOR SUPRATENTORIAL HEMISF' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17204', 'RESECCION DE TUMOR SUPRATENTORIAL HEMISF' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17209', 'DRENAJE DE QUISTE TUMORAL SUPRATENTORIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17301', 'RESECCION TUMOR INTRACEREBELOSO POR CRAN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17303', 'DRENAJE DE QUISTE TUMORAL INFRATENTORIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17401', 'RESECCION DE TUMOR DE LINEA MEDIA SUPRAT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17501', 'RESECCION DE TUMOR DE LINEA MEDIA INFRAT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17504', 'RESECCION DE LESIONES EXOFITICAS SOLIDAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17505', 'RESECCION DE LESIONES EXOFITICAS SOLIDAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17508', 'RESECCION DE LESIONES SOLIDAS O QUISTICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17509', 'RESECCION DE LESIONES SOLIDAS O QUISTICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17601', 'RESECCION DE LESIONES VENTRICULARES SUPR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17701', 'RESECCION DE LESIONES VENTRICULARES INFR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17801', 'RESECCION DE LESIONES INTRAVENTICULARES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '17805', 'DRENAJE DE QUISTE TUMORAL DE LINEA MEDIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '18101', 'HEMISFERECTOMIA CEREBRAL POR CRANEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '18201', 'HEMISFERECTOMIA CEREBELOSA POR CRANEOTOM' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '19100', 'LOBECTOMIA POR CRANEOTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '19200', 'LOBECTOMIA POR CRANIECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '20101', 'CORRECCION DE CRANEO SINOSTOSIS POR CRAN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '20102', 'CORRECCION DE CRANEO SINOSTOSIS POR CRAN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '20103', 'CORRECCION DE CRANEO ESTENOSIS MULTIPLE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '20104', 'CORRECCION DE CRANEO ESTENOSIS MULTIPLE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '20105', 'CORRECCION DE CRANEO ESTENOSIS CON ALTER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '20106', 'CORRECCION DE CRANEO ESTENOSIS CON ALTER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '20107', 'CORRECCION DE CRANEO ESTENOSIS CON ALTER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '20108', 'CORRECCION DE CRANEO ESTENOSIS CON ALTER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '20201', 'ESQUIRLECTOMIA CRANEAL A TRAVES DE TREPA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '20202', 'DESBRIDAMIENTO DE FRACTURA COMPUESTA CON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '20203', 'REDUCCION DE FRACTURA CRANEAL HUNDIMIENT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '20204', 'REDUCCION DE FRACTURA COMPUESTA CONMINUT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '20401', 'CORRECCION DE DEFECTO OSEO PRE-EXISTENTE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '20500', 'INSERCION O SUSTITUCION DE PLACA O MALLA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '20601', 'CORRECCION DISPLASIA FIBROSA POR CRANEOP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '21101', 'CORRECCION DE DESGARRO DURAL POST TRAUMA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '21102', 'CORRECCION DE DESGARRO DURAL POST TRAUMA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '21103', 'CORRECCION DE DESGARRO DURAL EN BASE DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '21104', 'CORRECCION DE DESGARRO DURAL EN BASE DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '21201', 'CORRECCION FISTULA LCR EN BOVEDA CRANEAN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '21202', 'CORRECCION FISTULA LCR EN BOVEDA CRANEAN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '21203', 'CORRECCION FISTULA LCR EN BASE DE CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '21204', 'CORRECCION FISTULA LCR EN BASE DE CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '21205', 'CORRECCION FISTULA LCR EN BASE DE CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '21206', 'CORRECCION FISTULA LCR EN BASE DE CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '21207', 'CORRECCION FISTULA LCR EN BASE DE CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '21208', 'CORRECCION FISTULA LCR EN BASE DE CRANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '21209', 'CORRECCION DE MENINGOCELE POR CRANIECTOM' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '21210', 'CORRECCION DE MENINGOCELE CON CRANEOPLAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '21211', 'CORRECCION DE MENINGOCELE DE FOSA ANTERI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '21212', 'CORRECCION DE MENINGOCELE DE FOSA ANTERI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '21213', 'CORRECCION DE MENINGOCELE DE FOSA ANTERI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '21214', 'CORRECCION DE MENINGOENCEFALOCELE POR CR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '22101', 'DERIVACION DE VENTRICULO A CISTERNA MAGN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '22201', 'COLOCACION DE CATETER VENTRICULAR AL EXT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '22202', 'DERIVACION VENTRICULAR A ESPACIO SUBARAC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '22300', 'COLOCACION DE CATETER VENTRICULO PERITON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '23201', 'DERIVACION VENTRICULOATRIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '23401', 'VENTRICULOPERITONEOSTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '23402', 'DERIVACION CISTO PERITONEAL QUISTE VENTR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '23500', 'DERIVACION VENTRICULAR AL APARATO URINAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '24100', 'IRRIGACION DE DERIVACION VENTRICULAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '24201', 'REEMPLAZO PARCIAL DE DERIVACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '24202', 'REEMPLAZO TOTAL DE DERIVACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '24300', 'RETIRO DE DERIVACION SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '25000', 'IMPLANTE DE CATETER INTRAVENTRICULAR INT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '28201', 'IMPLANTACION DE CATETER INTRACEREBRAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '28202', 'IMPLANTACION DE DISPOSITIVO EXTRADURAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '28203', 'IMPLANTACION DE DISPOSITIVO INTRACEREBRA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '28301', 'IMPLANTACION DE NEUROESTIMULADOR POR CRA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '28302', 'COLOCACION EPIDURAL DEL ELECTRODO DE NEU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '28303', 'IMPLANTACION PARENQUIMATOSA DEL ELECTROD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '28304', 'IMPLANTACION DE RECEPTOR ELECTROENCEFALO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '28601', 'INJERTO INTRACEREBRAL DE TEJIDO SUPRARRE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '30101', 'EXTRACCION DE CUERPO EXTRAÑO DEL CANAL R' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '30102', 'EXTRACCION DE CUERPO EXTRAÑO DEL CANAL R' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '30103', 'EXTRACCION DE CUERPO EXTRAÑO DEL CANAL R' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '30201', 'EXPLORACION Y DESCOMPRESION DEL CANAL RA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '30202', 'EXPLORACION Y DESCOMPRESION DEL CANAL RA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '30203', 'EXPLORACION Y DESCOMPRESION DEL CANAL RA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '30207', 'EXPLORACION Y DESCOMPRESION DEL CANAL RA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '30401', 'DRENAJE DE COLECCION EPIDURAL POR LAMINO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '30402', 'DRENAJE DE COLECCION EPIDURAL POR LAMINE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '32100', 'CORDOTOMIA PERCUTANEA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '32200', 'CORDOTOMIA ABIERTA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '33100', 'PUNCION LUMBAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '33201', 'BIOPSIA DE TUMOR INTRADURALES INTRAMEDUL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '34201', 'RESECCION DE TUMOR EXTRADURAL EPIDURAL V' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '34202', 'RESECCION DE TUMOR EXTRADURAL EPIDURAL V' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '34203', 'RESECCION DE TUMOR EXTRADURAL EPIDURAL V' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '34204', 'RESECCION DE TUMOR EXTRADURAL EPIDURAL V' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '34205', 'RESECCION DE TUMOR EXTRADURAL EPIDURAL V' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '34206', 'RESECCION DE TUMOR EXTRADURAL EPIDURAL C' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '34301', 'RESECCION DE TUMOR INTRADURAL EXTRAMEDUL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '34302', 'RESECCION DE TUMOR INTRADURAL EXTRAMEDUL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '34303', 'RESECCION DE TUMOR INTRADURAL EXTRAMEDUL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '34304', 'RESECCION DE TUMOR INTRADURAL EXTRAMEDUL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '34305', 'RESECCION DE TUMOR INTRADURAL EXTRAMEDUL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '34401', 'RESECCION DE TUMOR INTRADURAL INTRAMEDUL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '34402', 'RESECCION DE TUMOR INTRADURAL INTRAMEDUL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '35101', 'CORRECCION DE MALFORMACION DE MEDULA ESP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '35102', 'CORRECCION DE MALFORMACION DE MEDULA ESP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '35103', 'CORRECCION DE MALFORMACION DE MEDULA ESP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '35104', 'CORRECCION DE MALFORMACION DE MEDULA ESP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '35105', 'CORRECCION DE MALFORMACION DE MEDULA ESP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '35106', 'CORRECCION DE MALFORMACION DE MEDULA ESP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '35107', 'CORRECCION DE MALFORMACION DE MEDULA ESP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '35108', 'CIERRE O LIGADURA DE COMUNICACION PERSIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '35201', 'CORRECCION DE ANOMALIA DE MEDULA ESPINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '35202', 'CORRECCION DE  MEDULA ESPINAL EN UNION C' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '35401', 'PLASTIA O INJERTO DE MENINGE ESPINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '35402', 'ESQUIRLECTOMIA CON PLASTIA O INJERTO DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '37100', 'DERIVACION SIRINGO PERITONEAL ESPINAL SO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '37200', 'DERIVACION SIRINGO SUBDURAL ESPINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '37400', 'DERIVACION LUMBAR EXTERNA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '37500', 'DERIVACION SIRINGO PLEURAL ESPINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '38200', 'NEUROLISIS DE RAICES ESPINALES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '39001', 'INSERCION DE CATETER EPIDURAL EN CANAL E' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '39002', 'INSERCION DE CATETER EPIDURAL EN CANAL E' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '39301', 'IMPLANTACION DE ELECTRODOS O RECEPTOR DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '39302', 'IMPLANTACION DE ELECTRODOS O RECEPTOR DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '39700', 'REEMPLAZO IRRIGACION O REVISION DE DERIV' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '39800', 'RETIRO DE DERIVACION ESPINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '40200', 'SECCION DE NERVIO TRIGEMINO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '40301', 'SECCION DE NERVIO LARINGEO RECURRENTE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '40705', 'NEURECTOMIA DE NERVIO PERIFERICO EN CABE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '40706', 'RESECCION DE TUMOR DE NERVIO EN BRAZO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '40707', 'RESECCION DE TUMOR DE NERVIO EN ANTEBRAZ' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '40708', 'RESECCION DE TUMOR DE NERVIO EN MANO O D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '40709', 'RESECCION DE TUMOR DE NERVIO EN MUSLO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '40710', 'RESECCION DE TUMOR DE NERVIO EN PIERNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '40711', 'RESECCION DE TUMOR DE NERVIO EN PIE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '40712', 'RESECCION DE TUMOR DE NERVIO EN MUÑON DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '40730', 'TOMA DE INJERTO DE NERVIO PERIFERICO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '41101', 'BIOPSIA NERVIO PERIFERICO SUPERFICIAL O' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '41200', 'BIOPSIA ABIERTA DE NERVIO O GANGLIO PERI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '42101', 'RIZOTOMIA DE NERVIO TRIGEMINAL POR CRANE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '42102', 'RIZOTOMIA DE NERVIO XI POR CRANEOTOMIA S' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '42201', 'NEUROLISIS DE NERVIO XI POR AMIGDALECTOM' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '42202', 'NEUROLISIS DE NERVIO VIDIANO POR ELECTRO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '42301', 'NEUROLISIS DE NERVIO EN BRAZO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '42302', 'NEUROLISIS DE NERVIO EN ANTEBRAZO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '42303', 'NEUROLISIS EXTERNA EN NERVIO DE MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '42304', 'NEUROLISIS INTERNA EN NERVIO DE MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '42305', 'NEUROLISIS DE NERVIOS EN DEDOS DE MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '42306', 'NEUROLISIS DE NERVIO EN MUSLO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '42307', 'NEUROLISIS DE NERVIO EN PIERNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '42308', 'NEUROLISIS DE NERVIO EN PIE O DEDOS DE P' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '43102', 'NEURORRAFIA DE NERVIO DENTARIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '43103', 'NEURORRAFIA DE NERVIO EN BRAZO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '43104', 'NEURORRAFIA DE NERVIO EN ANTEBRAZO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '43105', 'NEURORRAFIA DE NERVIO EN MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '43106', 'NEURORRAFIA DE NERVIO COLATERAL EN DEDO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '43107', 'NEURORRAFIA DE NERVIO EN MUSLO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '43108', 'NEURORRAFIA DE NERVIO EN PIERNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '43109', 'NEURORRAFIA DE NERVIO EN PIE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '44101', 'DESCOMPRESION NEUROVASCULAR DE NERVIO TR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '44203', 'DESCOMPRESION NEUROVASCULAR DE NERVIOS I' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '44204', 'DESCOMPRESION NEUROVASCULAR DE NERVIO AC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '44205', 'DESCOMPRESION INTRACANALICULAR DE NERVIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '44206', 'DESCOMPRESION NEUROVASCULAR DE NERVIO F' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '44207', 'DESCOMPRESION DE NERVIO FACIAL INTRATEMP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '44208', 'DESCOMPRESION DE NERVIO FACIAL INTRATEMP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '44301', 'DESCOMPRESION ENDOSCOPICA DE NERVIO EN T' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '44311', 'DESCOMPRESION DE NERVIO EN TUNEL DEL CAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '44400', 'DESCOMPRESION DE NERVIO EN TUNEL DEL TAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '44501', 'DESCOMPRESION DE NERVIO EN BRAZO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '44502', 'DESCOMPRESION DE NERVIO EN ANTEBRAZO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '44503', 'DESCOMPRESION DE NERVIO EN MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '44504', 'DESCOMPRESION DE NERVIO EN DEDO DE LA MA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '44506', 'DESCOMPRESION DE NERVIO EN MUSLO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '44507', 'DESCOMPRESION DE NERVIO EN PIERNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '44508', 'DESCOMPRESION DE NERVIO EN PIE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '46101', 'TRANSPOSICION DE NERVIO EN MIEMBRO SUPER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '47102', 'REPARACION DE NERVIO FACIAL POR ANASTOMO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '51100', 'BIOPSIA DE NERVIO O GANGLIO SIMPATICO SO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '52101', 'GANGLIONECTOMIA ESFENOPALATINA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '52200', 'SIMPATECTOMIA CERVICAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '52300', 'SIMPATECTOMIA LUMBAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '52401', 'SIMPATECTOMIA PRESACRA POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '52601', 'RESECCION DE TUMOR EN NERVIO O GANGLIO S' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '52602', 'RESECCION DE TUMOR EN NERVIO O GANGLIO S' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '52603', 'SIMPATECTOMIA DIGITAL DEDO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '53201', 'NEUROLISIS DE PLEJO BRAQUIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '53202', 'NEUROLISIS DE PLEJO LUMBAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '53203', 'NEUROLISIS DE PLEJO CERVICAL SUPERFICIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '53204', 'NEUROLISIS DE PLEJO TORACICO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '53205', 'NEUROLISIS DE PLEJO CELIACO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '53206', 'NEUROLISIS DE PLEJO HIPOGASTRICO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '53207', 'NEUROLISIS DEL GANGLIO SIMPATICO PRESACR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '54101', 'NEURORRAFIA DE TRONCO DE PLEJO BRAQUIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '54102', 'NEURORRAFIA DE TRONCO DE PLEJO BRAQUIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '54103', 'INJERTO NEUROVASCULAR EN NERVIO O GANGLI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '54201', 'RECONSTRUCCION DE PLEJO POR NEURORRAFIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '54202', 'RECONSTRUCCION DE PLEJO POR INJERTO DE N' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '54203', 'RECONSTRUCCION DE PLEJO POR NEUROTIZACIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '55101', 'EXPLORACION SUPRA E INFRACLAVICULAR DE P' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '55200', 'DESCOMPRESION DE PLEJO O TRONCO CERVICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '60901', 'DRENAJE DE COLECCION EN AREA TIROIDEA PO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '60902', 'EXTRACCION DE CUERPO EXTRAÑO POR INCISIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '60903', 'EXPLORACION DE CUELLO O AREA TIROIDEA PO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '60904', 'DRENAJE EN CUELLO EXCEPTO AREA TIROIDEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '71300', 'BIOPSIA DE HIPOFISIS VIA TRANSFRONTAL SO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '71600', 'BIOPSIA DE TIMO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '71700', 'BIOPSIA DE GLANDULA PINEAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '75300', 'ESCISION PARCIAL DE GLANDULA PINEAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '75400', 'ESCISION TOTAL DE GLANDULA PINEAL PINEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '76100', 'ESCISION PARCIAL DE HIPOFISIS VIA TRANSF' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '76200', 'ESCISION PARCIAL DE HIPOFISIS VIA TRANSE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '76400', 'ESCISION TOTAL DE HIPOFISIS VIA TRANSFRO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '76500', 'ESCISION TOTAL DE HIPOFISIS VIA TRANSESF' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '80100', 'DRENAJE DE COLECCION POR BLEFAROTOMIA SO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '81100', 'BIOPSIA DE PARPADO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '82000', 'ESCISION O ABLACION DE LESION O TEJIDO D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '82100', 'RESECCION DE CHALAZION SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '82301', 'RESECCION DE TUMOR BENIGNO O MALIGNO DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '82302', 'RESECCION DE TUMOR BENIGNO O MALIGNO DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '82401', 'RESECCION TOTAL DE PARPADO Y RECONSTRUCC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '83100', 'CORRECCION DE PTOSIS PALPEBRAL POR SUSPE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '83200', 'CORRECCION DE PTOSIS PALPEBRAL POR SUSPE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '83300', 'CORRECCION DE PTOSIS PALPEBRAL POR RESEC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '83400', 'CORRECCION DE PTOSIS PALPEBRAL POR RESEC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '84100', 'CORRECCION DE ENTROPION POR TECNICA DE S' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '84200', 'CORRECCION DE ENTROPION CON INJERTO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '84300', 'CORRECCION DE ECTROPION CON INJERTO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '84400', 'CORRECCION DE ECTROPION POR TECNICA DE S' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '85100', 'CANTOTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '85200', 'CANTORRAFIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '85300', 'CANTOPLASTIA FIJACION DE CANTO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '86101', 'BLEFAROPLASTIA SUPERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '86102', 'BLEFAROPLASTIA INFERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '86200', 'RECONSTRUCCION DE PARPADO CON COLGAJO O' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '86300', 'RECONSTRUCCION DE PARPADO CON INJERTO DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '86400', 'RECONSTRUCCION DE PARPADO CON COLGAJO TA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '87100', 'RECONSTRUCCION DE PLIEGUES POR ACORTAMIE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '87300', 'RECONSTRUCCION DE PLIEGUES EN CORRECCION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '87401', 'RECONSTRUCCION DE PLIEGUES EN CORRECCION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '88100', 'REPARACION LINEAL DE LACERACION DE PARPA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '88201', 'SUTURA SUPERFICIAL DE HERIDA UNICA DE PA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '88202', 'SUTURA SUPERFICIAL DE HERIDA MULTIPLE DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '88401', 'SUTURA PROFUNDA DE HERIDA UNICA DE PARPA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '88402', 'SUTURA PROFUNDA DE HERIDA MULTIPLE DE PA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '88403', 'SUTURA DE PARPADO Y RECONSTRUCCION CON I' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '91100', 'BIOPSIA DE GLANDULA LAGRIMAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '91200', 'BIOPSIA DE SACO O CONDUCTO LAGRIMAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '92100', 'DACRIOADENECTOMIA PARCIAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '92200', 'DACRIOADENECTOMIA TOTAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '94100', 'EXTRACCION DE CUERPO EXTRAÑO DE SACO LAG' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '95300', 'DRENAJE DE SACO LAGRIMAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '96100', 'DACRIOCISTECTOMIA SACO LAGRIMAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '97100', 'PLASTIA DE CANALICULOS LAGRIMALES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '97200', 'PLASTIA DE PUNTO LAGRIMAL CIRUGIA DE WEB' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '97300', 'PLASTIA DE PUNTO LAGRIMAL MODIFICADA CON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '98101', 'DACRIOCISTORRINOSTOMIA VIA EXTERNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '98201', 'CONJUNTIVODACRIOCISTORRINOSTOMIA SIMPLE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '98301', 'CONJUNTIVODACRIOCISTORRINOSTOMIA CON INT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '100100', 'EXTRACCION DE CUERPO EXTRAÑO INCRUSTADO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '103101', 'RESECCION DE QUISTE O TUMOR BENIGNO DE C' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '103102', 'RESECCION DE QUISTE O TUMOR BENIGNO DE C' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '103103', 'RESECCION DE PTERIGION SIMPLE NASAL O TE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '103104', 'RESECCION DE PTERIGION SIMPLE NASAL O TE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '103105', 'RESECCION DE PTERIGION REPRODUCIDO NASAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '103106', 'RESECCION DE TUMOR MALIGNO DE CONJUNTIVA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '103107', 'RESECCION DE TUMOR MALIGNO DE CONJUNTIVA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '103108', 'PERITOMIA TOTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '104100', 'REPARACION DE SIMBLEFARON CON INJERTO LI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '104400', 'REPARACION DE SIMBLEFARON CON INJERTO DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '105100', 'DIVISION DE SIMBLEFARON SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '106100', 'SUTURA DE LA CONJUNTIVA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '111100', 'EXTRACCION DE CUERPO EXTRAÑO PROFUNDO EN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '114100', 'RESECCION SIMPLE DE TUMOR DE CORNEA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '114200', 'CAUTERIZACION DE CORNEA TERMO QUIMIO O C' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '115101', 'CORNOESCLERORRAFIA REPARACION DE HERIDA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '115200', 'REPARACION DE DESHISCENCIA DE HERIDA POS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '115301', 'REPARACION DE LACERACION O HERIDA CORNEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '115302', 'REPARACION DE LACERACION O HERIDA CORNEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '115800', 'RETIRO DE SUTURA EN CORNEA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '116100', 'QUERATOPLASTIA LAMELAR O SUPERFICIAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '116200', 'QUERATOPLASTIA PENETRANTE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '116300', 'QUERATOPLASTIA PENETRANTE COMBINADA CON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '116400', 'ESCLEROQUERATOPLASTIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '117300', 'IMPLANTE DE PROTESIS CORNEANA QUERATOPRO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '117400', 'QUERATOTOMIA RADIAL MIOPICA O ASTIGMATIC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '117500', 'QUERATOTOMIA FOTORREFRACTIVA CON LASER M' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '117600', 'QUERATECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '118100', 'QUERATOPIGMENTACION TATUAJE DE LA CORNEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '120000', 'EXTRACCION DE CUERPO EXTRAÑO INTRAOCULAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '121301', 'REDUCCION DE HERNIA DE IRIS POR SUTURA D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '121400', 'IRIDECTOMIA BASAL PERIFERICA Y TOTAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '123001', 'IRIDOPLASTIA CON SUTURA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '123400', 'REPARACION O SUTURA DE IRIDODIALISIS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '123500', 'COREOPLASTIA PUPILOPLASTIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '123701', 'REPARACION DE COLOBOMA DEL IRIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '124201', 'RESECCION DE TUMOR DE IRIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '124401', 'RESECCION DE TUMOR DE CUERPO CILIAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '125100', 'GONIOTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '125400', 'TRABECULOTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '125500', 'CICLODIALISIS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '126401', 'TRABECULECTOMIA SECUNDARIA CON CIRUGIA O' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '126601', 'REVISION DE AMPOLLA FILTRANTE CON AGUJA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '126705', 'REVISION ANTERIOR DE TUBO DE IMPLANTE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '127501', 'TRABECULOPLASTIA CON LASER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '128100', 'SUTURA DE LA ESCLERA ESCLERORRAFIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '128401', 'RESECCION DE TUMOR DE LA ESCLEROTICA VIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '128402', 'RESECCION DE TUMOR DE LA ESCLEROTICA POR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '128800', 'PLASTIAS EN ESCLERA ESCLEROPLASTIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '131100', 'EXTRACCION INTRACAPSULAR DE CRISTALINO S' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '132200', 'EXTRACCION EXTRACAPSULAR DE CRISTALINO P' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '132300', 'EXTRACCION EXTRACAPSULAR DE CRISTALINO P' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '132400', 'EXTRACCION EXTRACAPSULAR DE CRISTALINO E' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '136501', 'CAPSULOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '137100', 'EXTRACCION EXTRACAPSULAR DE CRISTALINO C' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '138100', 'EXTRACCION DE LENTE INTRAOCULAR PSEUDOCR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '147401', 'VITRECTOMIA POSTERIOR CON INSERCION DE S' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '147402', 'VITRECTOMIA POSTERIOR CON RETINOPEXIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '152100', 'PROCEDIMIENTO DE ALARGAMIENTO EN UN MUSC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '152200', 'PROCEDIMIENTO DE ACORTAMIENTO EN UN MUSC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '154101', 'REINSERCION O RETROINSERCION DE MUSCULOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '154102', 'REINSERCION O RETROINSERCION DE MUSCULOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '154103', 'REINSERCION O RETROINSERCION DE MUSCULOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '161100', 'EXTRACCION DE CUERPO EXTRAÑO DE ORBITA S' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '163100', 'EVISCERACION DEL GLOBO OCULAR CON IMPLAN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '164100', 'ENUCLEACION CON O SIN IMPLANTE PROTESICO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '164200', 'ENUCLEACION CON INJERTO DERMOGRASO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '165100', 'EXENTERACION DE ORBITA CON ESCISION DE E' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '165200', 'EXENTERACION DE ORBITA CON EXTRACCION TE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '166101', 'INSERCION SECUNDARIA DE PROTESIS CON FOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '168100', 'REPARACION DE HERIDA DE ORBITA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '168301', 'PLASTIA DE ORBITA CON RECONSTRUCCION DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '168401', 'DESCOMPRESION DE ORBITA VIA TECHO DE ORB' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '168402', 'DESCOMPRESION DE ORBITA VIA LATERAL TECN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '168403', 'DESCOMPRESION DE ORBITA VIA INFERIOR Y M' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '168404', 'DESCOMPRESION DE ORBITA VIA INFERIOR Y M' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '168405', 'DESCOMPRESION DE ORBITA VIA INFERIOR Y M' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '169201', 'RESECCION DE TUMOR MALIGNO DE ORBITA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '169202', 'RESECCION DE TUMOR BENIGNO DE ORBITA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '169203', 'DRENAJE DE COLECCION ANTERIOR DE ORBITA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '169204', 'DRENAJE DE COLECCION POSTERIOR DE ORBITA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '180100', 'DRENAJE DE  COLECCION DE PABELLON AURICU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '180200', 'DRENAJE DE  COLECCION DE CONDUCTO AUDITI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '182100', 'RESECCION DE FISTULA O QUISTE PREAURICUL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '182200', 'RESECCION DE APENDICE PREAURICULAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '182300', 'RESECCION DE QUISTE DE PABELLON AURICULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '183101', 'RESECCION DE TUMOR BENIGNO DE CONDUCTO A' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '184100', 'SUTURA DE LACERACION DE PABELLON AURICUL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '185101', 'OTOPLASTIA SIN REDUCCION DE TAMAÑO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '187101', 'RECONSTRUCCION PROTESICA DE AURICULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '187103', 'RECONSTRUCCION POR AGENESIA DE LA AURICU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '187104', 'RECONSTRUCCION POR AGENESIA DE LA AURICU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '187105', 'RECONSTRUCCION POR AGENESIA DE LA AURICU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '187106', 'RECONSTRUCCION POR AGENESIA DE LA AURICU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '187200', 'REIMPLANTE DE AURICULA PABELLON AURICULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '202101', 'DRENAJE DE COLECCION DE MASTOIDES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '202301', 'TIMPANOTOMIA EXPLORATORIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '207301', 'DESCOMPRESION DE SACO ENDOLINFATICO CON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '207501', 'LABERINTECTOMIA Y VESTIBULOTOMIA VIA TRA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '209100', 'REVISION DE MASTOIDECTOMIAS O MASTOIDOPL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '211301', 'DRENAJE DE LESION COLECCION EN PIRAMIDE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '218301', 'RECONSTRUCCION NASAL TOTAL CON INJERTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '218302', 'RECONSTRUCCION NASAL TOTAL CON COLGAJO F' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '218304', 'RECONSTRUCCION PROTESICA DE NARIZ CON MI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '218901', 'REIMPLANTACION DE NARIZ AMPUTADA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '221401', 'NASOSINUSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '224102', 'SINUSOTOMIA FRONTAL EXPLORATORIA O TERAP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '224103', 'SINUSOTOMIA FRONTAL VIA CORONAL CON COLG' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '224104', 'SINUSOTOMIA FRONTAL VIA CORONAL CON FRON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '224105', 'SINUSOTOMIA FRONTAL VIA CORONAL CON CRAN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '224106', 'SINUSOTOMIA FRONTAL VIA CILIAR CON TREPA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '224107', 'SINUSOTOMIA FRONTAL VIA CILIAR CON TREPA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '226001', 'RESECCION DE TUMOR MALIGNO DE SENO PARAN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '226301', 'FRONTO ETMOIDECTOMIA EXTERNA OPERACION D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '226302', 'ETMOIDECTOMIA EXTERNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '226303', 'ETMOIDECTOMIA ANTERIOR TRANSNASAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '226304', 'ETMOIDECTOMIA ANTERIOR VIA ENDOSCOPICA T' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '226305', 'ETMOIDECTOMIA ANTERIOR Y POSTERIOR VIA E' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '236100', 'IMPLANTE ALOPLASTICO CERAMICO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '236200', 'IMPLANTE ALOPLASTICO METALICO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '236300', 'IMPLANTE DENTAL ALOPLASTICO OSEOINTEGRAC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '237902', 'EXPLORACION Y MOVILIZACION DE NERVIO DEN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '243101', 'ESCISION DE LESION BENIGNA ENCAPSULADA E' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '243102', 'ESCISION DE LESION BENIGNA ENCAPSULADA E' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '243103', 'ESCISION DE LESION BENIGNA NO ENCAPSULAD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '243104', 'ESCISION DE LESION BENIGNA NO ENCAPSULAD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '243105', 'ESCISION DE LESION MALIGNA DE ENCIA SIN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '243106', 'ESCISION DE LESION MALIGNA DE ENCIA CON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '243107', 'ESCISION DE LESION MALIGNA DE ENCIA CON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '243108', 'ESCISION DE LESION MALIGNA DE ENCIA CON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '243109', 'ESCISION DE LESION MALIGNA DE ENCIA CON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '243201', 'SUTURA DE LACERACION DE ENCIA MENOR DE T' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '243202', 'SUTURA DE LACERACION DE ENCIA MAYOR DE T' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '243301', 'ENUCLEACION DE QUISTE EPIDERMOIDE VIA IN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '243302', 'ENUCLEACION DE QUISTE EPIDERMOIDE VIA EX' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '244101', 'ENUCLEACION DE QUISTE ODONTOGENICO HASTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '244102', 'ENUCLEACION DE QUISTE ODONTOGENICO DE MA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '244103', 'RESECCION DE TUMOR BENIGNO O MALIGNO ODO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '244104', 'RESECCION DE TUMOR BENIGNO O MALIGNO ODO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '244105', 'RESECCION DE TUMOR BENIGNO O MALIGNO ODO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '244106', 'RESECCION DE TUMOR BENIGNO O MALIGNO ODO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '244107', 'RESECCION DE TUMOR BENIGNO O MALIGNO ODO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '244108', 'MARSUPIALIZACION DE QUISTE ODONTOGENICO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '252501', 'HEMIGLOSECTOMIA CON CIERRE PRIMARIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '263101', 'PAROTIDECTOMIA DEL LOBULO SUPERFICIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '263201', 'PAROTIDECTOMIA TOTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '263203', 'SIALOADENECTOMIA DE GLANDULA SUBLINGUAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '263204', 'SIALOADENECTOMIA DE GLANDULA SUBMAXILAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '270101', 'INCISION Y DRENAJE INTRAORAL EN CAVIDAD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '270102', 'INCISION Y DRENAJE EXTRAORAL EN CAVIDAD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '273201', 'ESCISION DE LESION PROFUNDA DE PALADAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '273202', 'RESECCION EN BLOQUE DE APOFISIS ALVEOLAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '273203', 'PALATECTOMIA PARCIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '273204', 'PALATECTOMIA TOTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '274201', 'RESECCION PARCIAL DE LABIO POR TUMOR MAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '274202', 'RESECCION PARCIAL DE LABIO POR TUMOR MAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '274203', 'RESECCION TOTAL DE LABIO POR TUMOR MALIG' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '274301', 'RESECCION DE LESION BENIGNA DE LA MUCOSA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '274302', 'RESECCION DE LESION BENIGNA DE LA MUCOSA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '274303', 'RESECCION DE TUMOR MALIGNO DE MUCOSA ORA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '274304', 'RESECCION DE TUMOR MALIGNO DE MUCOSA ORA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '274400', 'RESECCION DE FOSETAS LABIALES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '274901', 'REMOCION DE CUERPO EXTRAÑO EN TEJIDOS BL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275101', 'SUTURA O REPARACION DE LACERACION HERIDA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275102', 'SUTURA O REPARACION DE LACERACION HERIDA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275201', 'ESTOMATORRAFIA SUTURA DE HERIDA EN MUCOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275202', 'ESTOMATORRAFIA SUTURA DE HERIDA EN MUCOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275301', 'RESECCION INTRAORAL DE FISTULA DE BOCA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275302', 'RESECCION EXTRAORAL DE FISTULA DE BOCA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275401', 'CORRECCION PARCIAL DE LABIO FISURADO POR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275402', 'CORRECCION PRIMARIA DE LABIO FISURADO UN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275403', 'CORRECCION SECUNDARIA DE LABIO FISURADO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275404', 'CORRECCION DE LABIO FISURADO BILATERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275500', 'INJERTO DE PIEL DE GROSOR TOTAL APLICADO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275601', 'LIPOINJERTO EN CARA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275701', 'INJERTO DE PIEL EN LABIOS CON ADHESION D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275801', 'PROFUNDIZACION O DESCENSO DE PISO DE BOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275901', 'PROFUNDIZACION DE SURCO VESTIBULAR CON I' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275902', 'PROFUNDIZACION DE SURCO VESTIBULAR CON I' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '276101', 'PALATORRAFIA EN Z FURLOW' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '276201', 'CORRECCION DE HENDIDURA ALVEOLOPALATINA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '276204', 'RECONSTRUCCION DE BOVEDA PALATINA MEDIAN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '276205', 'CORRECCION DE FISURA PALATINA CON COLGAJ' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '276206', 'INJERTO OSEO DE PALADAR O ALVEOLAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '276207', 'UVULO-PALATO-FARINGOPLASTIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '277201', 'RESECCION PARCIAL DE UVULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '290301', 'EXTRACCION DE CUERPO EXTRAÑO ENCLAVADO E' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '295101', 'FARINGOPLASTIA CON COLGAJO FARINGEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '295601', 'FARINGOPLASTIA POR IMPLANTE FARINGEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '295602', 'FARINGOPLASTIA POR COLGAJO FARINGEO DE B' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '295603', 'FARINGOPLASTIA POR ENTRECRUZAMIENTO DE P' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '295604', 'FARINGOPLASTIA CON COLGAJO FARINGEO POST' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '300101', 'RESECCION DE QUISTE VENTRICULAR VIA EXTE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '300103', 'RESECCION ENDOSCOPICA DE QUISTE VENTRICU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '300201', 'RESECCION DE LESION EN LARINGE VIA ABIER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '300202', 'RESECCION ENDOSCOPICA DE LESION EN LARIN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '300401', 'RESECCION O LISIS DE ADHERENCIAS DE LARI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '300402', 'RESECCION ENDOSCOPICA DE ADHERENCIAS DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '313101', 'EXTRACCION DE CUERPO EXTRAÑO DE TRAQUEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '319201', 'DILATACION ENDOSCOPICA DE LA TRAQUEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '320001', 'RESECCION O ABLACION DE LESION O TEJIDO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '320201', 'RESECCION ENDOSCOPICA DE LESION EN BRONQ' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '341101', 'EXPLORACION Y DRENAJE DE MEDIASTINO POR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '348202', 'SUTURA DE LACERACION DIAFRAGMATICA VIA A' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '348301', 'FISTULECTOMIA TORACICOABDOMINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '348302', 'FISTULECTOMIA TORACICOGASTRICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '348303', 'FISTULECTOMIA TORACICOINTESTINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '356101', 'REPARACION DE DEFECTO INTERAURICULAR CON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '356201', 'REPARACION DE DEFECTO INTERVENTRICULAR C' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '356202', 'REPARACION DE DEFECTO INTERVENTRICULAR C' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358001', 'REPARACION DE CANAL ATRIO-VENTRICULAR PA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358002', 'REPARACION DE CANAL ATRIO-VENTRICULAR CO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358101', 'REPARACION DE TETRALOGIA DE FALLOT CON E' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358102', 'REPARACION TRANSATRIAL DE LA TETRALOGIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358103', 'REPARACION TRANSVENTRICULAR DE LA TETRAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358104', 'REPARACION DE TETRALOGIA DE FALLOT CON E' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358105', 'REPARACION DE TETRALOGIA DE FALLOT CON C' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358106', 'REPARACION DE TETRALOGIA DE FALLOT CON F' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358201', 'REPARACION COMPLETA DE DRENAJE VENOSO PU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358202', 'REPARACION COMPLETA DE DRENAJE VENOSO PU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358203', 'REPARACION COMPLETA DE DRENAJE VENOSO PU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358204', 'REPARACION COMPLETA DE DRENAJE VENOSO PU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358205', 'REPARACION COMPLETA DE DRENAJE VENOSO PU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358206', 'REPARACION COMPLETA DE DRENAJE VENOSO PU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358207', 'REPARACION DE DRENAJE VENOSO PULMONAR AN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358208', 'REPARACION DE DRENAJE VENOSO PULMONAR AN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358209', 'REPARACION DE DRENAJE VENOSO PULMONAR AN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358306', 'REPARO DEL TRUNCUS ARTERIOSO CON HOMOINJ' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358307', 'REPARO DEL TRUNCUS ARTERIOSO CON CONDUCT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358308', 'REPARO DEL TRUNCUS ARTERIOSO CON PARCHE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358401', 'REPARO DE LA DOBLE SALIDA DEL VENTRICULO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358402', 'REPARO DE LA DOBLE SALIDA DEL VENTRICULO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358403', 'REPARO DE LA DOBLE SALIDA DEL VENTRICULO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358501', 'REPARO DEL DOBLE TRACTO DE SALIDA DEL VE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358502', 'REPARO DE DEFECTO VENTRICULAR POR TRANSP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358503', 'REPARO DEL DOBLE TRACTO DE SALIDA DEL VE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358504', 'REPARO DEL DOBLE TRACTO DE SALIDA DEL VE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358505', 'REPARO DEL DOBLE TRACTO DE SALIDA DEL VE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358506', 'REPARO DEL DOBLE TRACTO DE SALIDA DEL VE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358507', 'REPARO DEL DOBLE TRACTO DE SALIDA DEL VE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358508', 'REPARO DEL DOBLE TRACTO DE SALIDA DEL VE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358509', 'REPARO DEL DOBLE TRACTO DE SALIDA DEL VE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358510', 'REPARO INTRAVENTRICULAR DEL DOBLE TRACTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358701', 'REPARACION DE COARTACION AORTICA CON RES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358702', 'REPARACION DE COARTACION AORTICA CON RES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358703', 'REPARACION DE COARTACION AORTICA CON INT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358801', 'TRANSPOSICION DEL RETORNO VENOSO CON PAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358802', 'TRANSPOSICION ARTERIAL CON REIMPLANTE DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358803', 'REPARO DE VENTANA AORTOPULMONAR CON PARC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '359501', 'REINTERVENCION POR SANGRADO DESPUES DE C' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '359502', 'REINTERVENCION POR CARDIOPATIAS CONGENIT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '373200', 'ESCISION DE ANEURISMA DE CORAZON SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '373303', 'RESECCION ENDOMIOCARDICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '373600', 'EXTRACCION DE CUERPO EXTRAÑO INTRACARDIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '373700', 'EXTRACCION DE CUERPO EXTRAÑO INTRAPERICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '374200', 'REPARACION DE CORAZON POR RUPTURA POSTIN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '374300', 'PERICARDIORRAFIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '375100', 'TRASPLANTE CARDIACO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '375200', 'OBTENCION DEL CORAZON DONANTE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '376100', 'IMPLANTACION DE BALON CONTRAPULSACION SO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380101', 'TROMBOEMBOLECTOMIA DE CEREBRAL MEDIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380102', 'TROMBOEMBOLECTOMIA DE COMUNICANTE ANTERI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380103', 'TROMBOEMBOLECTOMIA DE CAROTIDA INTERNA S' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380104', 'TROMBOEMBOLECTOMIA DE OFTALMICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380201', 'TROMBOEMBOLECTOMIA ARTERIAL DE ZONA I Y' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380202', 'TROMBOEMBOLECTOMIA ARTERAL DE ZONA II DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380203', 'TROMBOEMBOLECTOMIA VENOSA DE ZONA I Y II' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380204', 'TROMBOEMBOLECTOMIA VENOSA DE ZONA II DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380301', 'TROMBOLECTOMIA DE ARTERIA SUBCLAVIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380302', 'TROMBOEMBOLECTOMIA ARTERIAL AXILAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380303', 'TROMBOEMBOLECTOMIA ARTERIAL DE BRAZO O A' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380304', 'TROMBOLECTOMIA DE VENA SUBCLAVIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380305', 'TROMBOEMBOLECTOMIA VENOSA AXILAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380306', 'TROMBOEMBOLECTOMIA VENOSA DE BRAZO O ANT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380500', 'TROMBOEMBOLECTOMIA EN VASOS ESPINALES SO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380601', 'TROMBOEMBOLECTOMIA DE ARTERIAS ABDOMINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380701', 'TROMBOEMBOLECTOMIA DE VENAS ABDOMINALES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380801', 'TROMBOEMBOLECTOMIA SUPRAPATELAR DE ARTER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380802', 'TROMBOEMBOLECTOMIA INFRAPATELAR DE ARTER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380901', 'TROMBOEMBOLECTOMIA SUPRAPATELAR VENOSA P' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380902', 'TROMBOEMBOLECTOMIA INFRAPATELAR VENOSA P' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380903', 'TROMBOEMBOLECTOMIA VENOSA SUPERFICIAL EN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '381101', 'ENDARTERECTOMIA DE LA CAROTIDA INTERNA P' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '381201', 'ENDARTERECTOMIA DE LA CAROTIDA COMUN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '381202', 'ENDARTERECTOMIA DE ARTERIA CAROTIDA EXTE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '381203', 'ENDARTERECTOMIA DE CAROTIDA INTERNA PORC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '381301', 'ENDARTERECTOMIA DE SUBCLAVIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '381303', 'ENDARTERECTOMIA DE VASOS DE BRAZO O ANTE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '381501', 'TROMBOENDARTERECTOMIA PULMONAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383101', 'RESECCION DE MALFORMACION ARTERIOVENOSA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383102', 'RESECCION DE MALFORMACION ARTERIOVENOSA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383103', 'RESECCION DE MALFORMACIONES ARTERIO VENO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383104', 'RESECCION DE MALFORMACION ARTERIOVENOSA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383105', 'RESECCION DE MALFORMACION ARTERIOVENOSA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383201', 'RESECCION DE MALFORMACION ARTERIOVENOSA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383202', 'RESECCION ARTERIAL CON ANASTOMOSIS TERMI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383203', 'RESECCION ARTERIAL CON ANASTOMOSIS TERMI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383204', 'RESECCION VENOSA CON ANASTOMOSIS TERMINO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383205', 'RESECCION VENOSA CON ANASTOMOSIS TERMINO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383301', 'RESECCION CON ANASTOMOSIS PRIMARIA DE SU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383302', 'RESECCION CON ANASTOMOSIS PRIMARIA AXILA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383303', 'RESECCION CON ANASTOMOSIS TERMINO-TERMIN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383405', 'RECONSTRUCION AORTICA TORACO-ABDOMINAL (' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383501', 'RESECCION CON ANASTOMOSIS EN VASOS ESPIN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383502', 'RESECCION CON ANASTOMOSIS EN VASOS ESPIN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383503', 'RESECCION CON ANASTOMOSIS EN VASOS ESPIN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383601', 'RESECCION CON ANASTOMOSIS TERMINO-TERMIN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383701', 'RESECCION CON ANASTOMOSIS TERMINO-TERMIN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '384201', 'RESECCION CON INJERTO AUTOLOGO O PROTESI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '384202', 'RESECCION CON INJERTO AUTOLOGO O PROTESI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '384301', 'RESECCION DE SUBCLAVIA CON INTERPOSICION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '384302', 'RESECCION AXILAR CON INTERPOSICION DE IN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '384303', 'RESECCION EN BRAZO O ANTEBRAZO CON INTER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '384400', 'RESECCION CON SUSTITUCION DE AORTA ABDOM' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '384500', 'RESECCION CON SUSTITUCION DE VASOS TORAC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '384600', 'RESECCION CON SUSTITUCION DE ARTERIAS AB' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '384700', 'RESECCION CON SUSTITUCION DE VENAS ABDOM' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '384801', 'RESECCION ARTERIAL SUPRAPATELAR CON INJE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '384802', 'RESECCION ARTERIAL INFRAPATELAR CON INJE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '384901', 'TRANSPOSICION VENOSA CON SEGMENTO VALVUL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385101', 'OCLUSION PINZAMIENTO O LIGADURA DE ARTER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385102', 'OCLUSION PINZAMIENTO O LIGADURA DE ARTER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385103', 'OCLUSION PINZAMIENTO O LIGADURA DE CAROT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385104', 'OCLUSION PINZAMIENTO O LIGADURA DE ARTER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385105', 'OCLUSION PINZAMIENTO O LIGADURA DE VASOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385110', 'OCLUSION PINZAMIENTO O LIGADURA DE ARTER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385111', 'OCLUSION PINZAMIENTO O LIGADURA DE LESIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385201', 'OCLUSION PINZAMIENTO O LIGADURA ARTERIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385202', 'OCLUSION PINZAMIENTO O LIGADURA ARTERIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385203', 'OCLUSION PINZAMIENTO O LIGADURA VENOSA E' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385204', 'OCLUSION PINZAMIENTO O LIGADURA VENOSA E' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385301', 'OCLUSION PINZAMIENTO O LIGADURA DE SUBCL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385303', 'OCLUSION PINZAMIENTO O LIGADURA DE VASOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385601', 'OCLUSION PINZAMIENTO O LIGADURA DE ARTER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385701', 'OCLUSION PINZAMIENTO O LIGADURA DE VENAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385801', 'OCLUSION PINZAMIENTO O LIGADURA ARTERIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385802', 'OCLUSION PINZAMIENTO O LIGADURA ARTERIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385901', 'OCLUSION PINZAMIENTO O LIGADURA VENOSA P' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385902', 'OCLUSION PINZAMIENTO O LIGADURA VENOSA P' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385903', 'LIGADURA DE PERFORANTES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '386100', 'TOMA DE INJERTO ARTERIAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '386200', 'TOMA DE INJERTO VENOSO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '388700', 'LIGADURA Y ESCISION DE VENAS VARICOSAS A' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '388901', 'LIGADURA Y ESCISION SUPRAPATELAR DE VENA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '388902', 'LIGADURA Y ESCISION INFRAPATELAR DE VENA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '388903', 'LIGADURA Y ESCISION DE SAFENA EXTERNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '388904', 'LIGADURA Y ESCISION DE SAFENA INTERNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '389101', 'IMPLANTACION DE CATETER VENOSO SUBCLAVIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '392701', 'FORMACION DE FISTULA AV PERIFERICA PARA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '392702', 'FORMACION DE FISTULA AV PERIFERICA PARA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '393201', 'SUTURA DE ARTERIAS EN ZONA I Y III DEL C' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '393202', 'SUTURA DE ARTERIAS EN ZONA II DEL CUELLO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '393203', 'SUTURA DE VENAS ZONA I Y III DE CUELLO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '393204', 'SUTURA DE VENAS ZONA II DE CUELLO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '393306', 'RECONSTRUCCION DE VASOS PERIFERICOS EN M' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '395301', 'CIERRE DE FISTULA ARTERIOVENOSA VIA ABIE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397201', 'EXPLORACION DE ARTERIAS EN CUERO CABELLU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397202', 'EXPLORACION DE ARTERIAS ZONA I Y III DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397203', 'EXPLORACION DE ARTERIAS EN ZONA II DE CU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397204', 'EXPLORACION DE VENAS EN CUERO CABELLUDO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397205', 'EXPLORACION DE VENA EN ZONA I Y III DEL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397206', 'EXPLORACION DE VENA EN ZONA II DEL CUELL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397301', 'EXPLORACION DE ARTERIA SUBCLAVIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397302', 'EXPLORACION DE ARTERIA AXILAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397303', 'EXPLORACION DE ARTERIA DE BRAZO O ANTEBR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397304', 'EXPLORACION DE VENA SUBCLAVIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397305', 'EXPLORACION DE VENA AXILAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397306', 'EXPLORACION DE VENA DE BRAZO O ANTEBRAZO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397400', 'EXPLORACION DE AORTA ABDOMINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397601', 'EXPLORACION DE ARTERIAS ABDOMINALES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397700', 'EXPLORACION DE VENAS ABDOMINALES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397801', 'EXPLORACION ARTERIAL SUPRAPATELAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397802', 'EXPLORACION ARTERIAL INFRAPATELAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397901', 'EXPLORACION VENOSA SUPRAPATELAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397902', 'EXPLORACION VENOSA INFRAPATELAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '398001', 'RESECCION DE TUMOR DE CUERPO CAROTIDEO Q' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '398002', 'RESECCION DE TUMOR DE CUERPO CAROTIDEO Q' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '399400', 'SUSTITUCION O REVISION DE CANULA VASO A' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '406600', 'LIGADURA OBLITERACION EN EL AREA ILIACA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '407200', 'LINFANGIORRAFIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '407300', 'LINFANGIOPLASTIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '407400', 'TRASPLANTE DE LINFATICOS AUTOGENOS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '410100', 'TRASPLANTE AUTOLOGO DE MEDULA OSEA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '410200', 'TRASPLANTE ALOGENICO DE MEDULA OSEA CON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '410300', 'TRASPLANTE ALOGENICO DE MEDULA OSEA SIN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '410400', 'TRASPLANTE AUTOLOGO DE CELULAS MADRES HE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '423301', 'POLIPECTOMIA ENDOSCOPICA DE ESOFAGO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '423304', 'INYECCION ESCLEROSIS ENDOSCOPICA DE VARI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '423305', 'LIGADURA ENDOSCOPICA DE VARICES ESOFAGIC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '426101', 'RECONSTRUCCION ESOFAGICA ANTE-ESTERNAL C' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '426102', 'RECONSTRUCCION ESOFAGICA ANTE-ESTERNAL C' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '426103', 'RECONSTRUCCION ESOFAGICA ANTE  ESTERNAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '428500', 'REPARACION DE ESTENOSIS ESOFAGICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '428700', 'REPARACION DE ATRESIA ESOFAGICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '429101', 'LIGADURA DE VARICES ESOFAGICAS VIA TRANS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '429102', 'LIGADURA DE VARICES ESOFAGICAS POR TRANS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '429300', 'INSERCION ENDOSCOPICA DE PROTESIS STENT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '429401', 'EXTRACCION DE CUERPO EXTRAÑO O LESION LO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '429402', 'EXTRACCION DE CUERPO EXTRAÑO O LESION LO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '429405', 'EXTRACCION ENDOSCOPICA DE CUERPO EXTRAÑO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '430101', 'EXTRACCION DE CUERPO EXTRAÑO MULTIPLE BE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '434101', 'LIGADURA ENDOSCOPICA DE VARICES GASTRICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '434200', 'EXTRACCION ENDOSCOPICA DE TUMOR SUBMUCOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '434500', 'MUCOSECTOMIA ENDOSCOPICA GASTRICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '441200', 'GASTROSCOPIA A TRAVES DE ESTOMA ARTIFICI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '446603', 'REINTERVENCION EN ANTIRREFLUJO GASTRESOF' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '452401', 'SIGMOIDOSCOPIA FLEXIBLE O RIGIDA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '456202', 'YEYUNECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '456203', 'ILECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '456400', 'RESECCION INTESTINAL CONDUCTO ONFALOMESE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '463200', 'YEYUNOSTOMIA PERCUTANEA ENDOSCOPICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '464001', 'REMODELACION DE ENTEROSTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '466200', 'PLICATURA INTESTINAL (OPERACION DE NOBLE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '467901', 'RESECCION INTESTINAL Y DE QUISTE POR PER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '467902', 'RESECCION DE DUPLICCION INTESTINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '468011', 'REDUCCION INTESTINAL SIN RESECCION INTES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '468012', 'REDUCCION INTESTINAL CON RESECCION INTES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '480100', 'PROCTOTOMIA VIA ABDOMINAL O PERINEAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '482100', 'PROCTOSIGMOIDOSCOPIA TRANSABDOMINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '482200', 'PROCTOSIGMOIDOSCOPIA A TRAVES DE ESTOMA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '482301', 'PROCTOSIGMOIDOSCOPIA RIGIDA O FLEXIBLE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '486500', 'RESECCION DE MUÑON RECTAL POST-DUHAMEL S' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '487901', 'REPARACION DE LESION OBSTETRICA ANTIGUA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '487902', 'REPARACION DE RECTO PROLAPSADO POR INFIL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '487905', 'REPARACION DE LA ESTENOSIS RECTAL CON RE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '489100', 'INCISION DE ESTENOSIS RECTAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '489200', 'MIOMECTOMIA ANO-RECTAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '497401', 'TRANSPOSICION DEL MUSCULO RECTO INTERNO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '497502', 'RECONSTRUCCION DE ANO POR ATRESIA ANAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '499100', 'INCISION DE TABIQUE ANAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '499400', 'REDUCCION DE PROLAPSO ANAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '499600', 'RETIRO DE MATERIAL DE CERCLAJE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '504000', 'HEPATECTOMIA TOTAL OBTENCION DE ORGANO S' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '505100', 'TRASPLANTE AUXILIAR HETEROTOPICO DE HIGA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '511500', 'MEDICION DE LA PRESION DEL ESFINTER DE O' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '512501', 'RESECCION DE QUISTES DEL COLEDOCO CON DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '518600', 'INSERCION ENDOSCOPICA DE TUBO DE DRENAJE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '521300', 'PANCREATOGRAFIA RETROGRADA ENDOSCOPICA E' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '526200', 'PANCREATECTOMIA TOTAL OBTENCION DEL ORGA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '528200', 'HOMOTRASPLANTE DE PANCREAS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '529300', 'INSERCION ENDOSCOPICA DE TUBO TUTOR PROT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '529700', 'INSERCION ENDOSCOPICA DE TUBO DE DRENAJE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '541801', 'MARSUPIALIZACION ABDOMINAL POR PANCREATI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '544200', 'ONFALECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '546100', 'NUEVO CIERRE DE DISRUPCION  POSTOPERATOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '547600', 'RESECCION DE QUISTE VITELINO O SENO UMBI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '549002', 'INSERCION DE CATETER PERMANENTE PARA HEM' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '549012', 'RETIRO DE CATETER  PERMANENTE PARA HEMOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '549013', 'RETIRO DE OTRO CATETER PERITONEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '549501', 'PLICATURA DE PERITONEO NOBLE MODIFICADA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '550101', 'MARSUPIALIZACION DE QUISTE RENAL POR NEF' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '550103', 'NEFROLITOTOMIA O EXTRACCION DE CALCULO O' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '550200', 'NEFROSTOMIA VIA ABIERTA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '551210', 'PIELOSTOMIA O INSERCION DE TUBO PARA DRE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '552100', 'NEFROSCOPIA DIAGNOSTICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '552200', 'PIELOSCOPIA DIAGNOSTICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '553102', 'RESECCION ENDOSCOPICA DE LESION PIELICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '554200', 'RESECCION DE POLO RENAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '554400', 'RESECCION EN CUÑA DE RIÑON SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '555101', 'NEFRO URETERECTOMIA CON SEGMENTO DE VEJI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '555102', 'NEFRO URETERECTOMIA TOTAL UNILATERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '555200', 'NEFRECTOMIA DE RIÑON RESIDUAL O UNICO SO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '555300', 'REMOCION DE RIÑON TRANSPLANTADO O RECHAZ' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '555601', 'NEFRECTOMIA OBTENCION DE ORGANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '556200', 'TRASPLANTE DE RIÑON DE DONANTE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '558101', 'NEFRORRAFIA O SUTURA DE LACERACION RENAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '558500', 'SINFISIOTOMIA DE RIÑON EN HERRADURA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '558640', 'NEFROENTEROSTOMIA CUTANEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '558701', 'PIELOPLASTIA VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '558720', 'PIELOPLASTIA ENDOSCOPICA  POR ENDOPIELOT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '560100', 'REMOCION TRANSURETRAL (ENDOSCOPICA) DE C' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '561101', 'MEATOTOMIA URETERAL VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '561102', 'MEATOTOMIA URETERAL VIA ENDOSCOPICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '562201', 'URETEROLITOTOMIA POR URETEROTOMIA VIA AB' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '563100', 'URETEROSCOPIA DIAGNOSTICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '565101', 'URETEROILEOSTOMIA CUTANEA [CIRUGIA DE BR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '565201', 'URETEROSIGMOIDOSTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '565410', 'URETERONEOCECOCISTOPLASTIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '565610', 'URETEROCOLOSTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '565710', 'URETERONEOPROCTOSTOMIA ANASTOMOSIS DE UR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '566000', 'URETEROSTOMIA CUTANEA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '567440', 'URETERONEOCISTOSTOMIA POR ANASTOMOSIS O' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '567441', 'URETERONEOCISTOSTOMIA CON TECNICA DE ALA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '567460', 'REANASTOMOSIS URETERO-VESICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '567500', 'TRANSURETERO-URETEROSTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '568200', 'SUTURA DE LACERACION DE URETER O URETERO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '568941', 'RESECCION DE URETEROCELE Y REIMPLANTE DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '568942', 'RESECCION ENDOSCOPICA DE URETEROCELE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '568970', 'REEMPLAZO DE URETER CON SEGMENTO ILEAL I' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '570100', 'LIMPIEZA Y DRENAJE TRANSURETRAL DE VEJIG' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '570200', 'REMOCION TRANSURETRAL ENDOSCOPICA DE CAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '572101', 'VESICOSTOMIA CUTANEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '574201', 'RESECCION ENDOSCOPICA DE LESION VESICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '575101', 'RESECCION DE SENO URACAL DE VEJIGA URACO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '575102', 'RESECCION DE FISTULA URACAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '575202', 'RESECCION O FULGURACION SUPRAPUBICA DE L' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '576060', 'RESECCION DE CUELLO VESICAL TRANSVESICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '576061', 'RESECCION TRANSURETRAL ENDOSCOPICA DE CU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '578501', 'PLICATURA DE ESFINTER VESICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '578600', 'REPARACION DE EXTROFIA VESICAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '579950', 'PROCEDIMIENTO ANTI-INCONTINENCIA URINARI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '580030', 'REMOCION DE CALCULO O CUERPO EXTRAÑO URE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '580050', 'URETROTOMIA INTERNA ENDOSCOPICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '580110', 'URETROSTOMIA PERINEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '581010', 'MEATOTOMIA URETRAL EXTERNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '582100', 'URETROSCOPIA PERINEAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '583230', 'URETRECTOMIA SIMPLE VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '583240', 'URETRECTOMIA RADICAL VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '584101', 'URETRORRAFIA FEMENINA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '584102', 'URETRORRAFIA PENEANA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '584103', 'URETRORRAFIA PERINEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '584303', 'RESECCION DE FISTULA URETROCUTANEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '584402', 'REVISION DE ANASTOMOSIS DE URETRA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '584530', 'MAGPI MEATOPLASTIA GLANDULOPLASTIA AVANZ' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '584601', 'URETROPLASTIA TRANSPUBICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '584602', 'URETROPLASTIA CON OTROS TEJIDOS (CON INJ' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '584603', 'URETROPLASTIA PERINEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '584700', 'MEATOPLASTIA URETRAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '584901', 'MARSUPIALIZACION DE DIVERTICULO URETRAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '585010', 'MEATOTOMIA URETRAL INTERNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '587010', 'URETROLITOTOMIA ENDOSCOPICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '590200', 'URETEROLISIS CON LIBERACION O REPOSICION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '595101', 'SUSPENSION URETRO VESICAL RETROPUBICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '595103', 'URETROCISTOPEXIA POR LAPAROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '597910', 'URETROPEXIA ANTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '597920', 'URETROPLASTIA DE AMPLIACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '597940', 'URETROCOLPOPEXIA VIA VAGINAL O ABDOMINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '597941', 'URETROCOLPOPEXIA REPRODUCIDA VIA VAGINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '600200', 'PROSTATOLITOTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '602901', 'RESECCCION O ENUCLEACION TRANSURETRAL DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '602902', 'PROSTATECTOMIA TRANSURETRAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '603100', 'PROSTATECTOMIA TRANSVESICAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '604100', 'PROSTATECTOMIA TRANSVESICOCAPSULAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '607301', 'VESICULECTOMIA O ESPERMATOCISTECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '609301', 'REVISION Y REPARACION DE CAPSULA VIA TRA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '612100', 'REPARACION O ESCISION DE HIDROCELE [HIDR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '613101', 'RESECCION DE QUISTE SEBACEO EN ESCROTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '613401', 'RESECCION PARCIAL DEL ESCROTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '613402', 'RESECCION TOTAL DEL ESCROTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '614100', 'SUTURA DE LACERACION DE ESCROTO Y TUNICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '614910', 'RECONSTRUCCION DE ESCROTO CON COLGAJO O' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '622100', 'RESECCION DE LESION TESTICULAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '623001', 'ORQUIECTOMIA CON EPIDIDIDECTOMIA RADICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '625101', 'ORQUIDOPEXIA CON DESTORSION DE TESTICULO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '625202', 'ORQUIDOPEXIA TRANSABDOMINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '625210', 'ORQUIDOPEXIA CON RECONSTRUCCION DE CANAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '625220', 'ORQUIDOPEXIA CON TRANSPOSICION O MOVILIZ' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '626100', 'ORQUIDORRAFIA O SUTURA DE TESTICULO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '631010', 'VARICOCELECTOMIA CON LIGADURA ALTA DE VE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '631011', 'VARICOCELECTOMIA CON PRESERVACION DE ART' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '633200', 'RESECCION DE HEMATOCELE DE CORDON ESPERM' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '635200', 'REDUCCION DE TORSION TESTICULAR O CORDON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '636100', 'VASOTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '637300', 'VASECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '638200', 'RECONSTRUCCION DEL CONDUCTO DEFERENTE SE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '644100', 'SUTURA DE LACERACION O HERIDA EN PENE SO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '644400', 'RECONSTRUCCION PENEANA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '644500', 'REIMPLANTE O RECOLOCACION DE PENE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '644920', 'PLASTIA DE FRENILLO PENEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '649300', 'SECCION O CORTE DE ADHERENCIAS PENEANAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '649600', 'RETIRO DE PROTESIS PENEANA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '652200', 'RESECCION CUNEIFORME EN OVARIO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '652301', 'RESECCION DE TUMOR DE OVARIO POR LAPAROT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '652302', 'RESECCION DE TUMOR DE OVARIO POR LAPAROS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '652401', 'PUNCION Y DRENAJE DE LESION DE OVARIO PO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '652402', 'PUNCION Y DRENAJE DE LESION DE OVARIO PO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '652410', 'OFOROSTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '652801', 'RESECCION DE QUISTE PARA OVARICO POR LAP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '652802', 'RESECCION DE QUISTE PARA OVARICO POR LAP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '653101', 'OOFORECTOMIA UNILATERAL POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '653102', 'OOFORECTOMIA UNILATERAL POR LAPAROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '653103', 'OOFORECTOMIA UNILATERAL CON OMENTECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '655101', 'OOFORECTOMIA BILATERAL POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '655102', 'OOFORECTOMIA BILATERAL POR LAPAROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '655103', 'OOFORECTOMIA BILATERAL CON OMENTECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '657000', 'OFOROPLASTIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '657100', 'OOFORORRAFIA SIMPLE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '657801', 'OOFOROPEXIA UNILATERAL POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '657802', 'OOFOROPEXIA UNILATERAL POR LAPAROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '657803', 'OOFOROPEXIA BILATERAL POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '660101', 'SALPINGOSTOMIA Y DRENAJE TROMPA DE FALOP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '660102', 'SALPINGOSTOMIA Y DRENAJE TROMPA DE FALOP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '660201', 'SALPINGOSTOMIA POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '660203', 'SALPINGOSTOMIA Y SALPINGOPASTIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '663100', 'SECCION O LIGADURA DE TROMPAS DE FALOPIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '664001', 'SALPINGECTOMIA UNILATERAL TOTAL POR LAPA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '665001', 'SALPINGECTOMIA BILATERAL TOTAL POR LAPAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '666210', 'RESECCION DE LESION EN MESOSALPINX POR L' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '666220', 'RESECCION DE LESION EN MESOSALPINX POR L' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '667101', 'SUTURA SIMPLE DE TROMPA DE FALOPIO POR L' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '667302', 'SALPINGO-SALPINGOSTOMIA POR LAPAROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '667400', 'SALPINGOHISTEROTOMIA (SALPINGO-UTEROSTOM' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '667601', 'SALPINGOLISIS POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '667610', 'SALPINGOLISIS DE ADHERENCIAS LEVES MODER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '667901', 'SALPINGOPLASTIA (FIMBRIOPLASTIA) POR LAP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '667902', 'SALPINGOPLASTIA (FIMBROPLASTIA) POR LAPA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '669110', 'SALPINGO-OOFORECTOMIA UNILATERAL POR LAP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '669120', 'SALPINGO OOFORECTOMIA UNILATERAL POR LAP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '669210', 'SALPINGO-OOFORECTOMIA BILATERAL POR LAPA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '669220', 'SALPINGO OOFORECTOMIA BILATERAL POR LAPA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '669410', 'SALPINGO-OOFOROPLASTIA [OPERACION DE EST' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '676100', 'SUTURA DE LACERACION O DESGARRO DE CUELL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '676920', 'TRAQUELOPLASTIA CON TRAQUELORRAFIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '682302', 'RESECCION DE POLIPO ENDOMETRIAL POR HIST' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '691110', 'RESECCION DE TUMOR DE LIGAMENTO ANCHO PO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '691130', 'SECCION DE LIGAMENTO UTERO SACRO POR LAP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '691230', 'SECCION DE LIGAMENTO UTERO SACRO POR LAP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '691301', 'SECCION DE ADHERENCIAS UTERINAS A PARED' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '691302', 'SECCION DE ADHERENCIAS UTERINAS A PARED' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '699600', 'RETIRO DE MATERIAL DE CERCLAJE DE CUELLO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '699700', 'RETIRO DE OTRO CUERPO EXTRAÑO PENETRANTE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '701410', 'SECCION O INCISION DE TABIQUE VAGINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '701430', 'VAGINOPERINEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '702110', 'VAGINOSCOPIA CON INSTRUMENTO OPTICO EN N' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '703310', 'RESECCION DEL TABIQUE VAGINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '703320', 'RESECCION DE LESION BENIGNA EN TERCIO ME' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '703321', 'RESECCION DE LESION BENIGNA EN TERCIO SU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '703330', 'RESECCION PARCIAL DE VAGINA O ESCISION D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '703340', 'RESECCION DE TUMOR MALIGNO DE VAGINA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '704100', 'VAGINECTOMIA O COLPECTOMIA TOTAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '706000', 'RECONSTRUCCION DE VAGINA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '706101', 'VAGINOPLASTIA VIA ABDOMINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '706102', 'VAGINOPLASTIA VIA PERINEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '706103', 'VAGINOPLASTIA VIA ABDOMINOPERINEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '707120', 'REPARACION DE DESGARRO VAGINAL NO OBSTET' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '707130', 'REPARACION DE DESGARRO VAGINAL NO OBSTET' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '709220', 'REPARACION DE ENTEROCELE VIA VAGINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '709230', 'REPARACION DE ENTEROCELE VIA ABDOMINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '712401', 'RESECCION DE GLANDULA DE BARTHOLIN BARTH' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '713100', 'RESECCION DE GLANDULA DE SKENE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '713400', 'RESECCION DE ENDOMETRIOMA PERINEAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '713500', 'RESECCION GRANULOMA VULVO PERINEAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '715100', 'VULVECTOMIA RADICAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '715200', 'VULVECTOMIA TOTAL O COMPLETA BILATERAL S' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '716120', 'VULVECTOMIA PARCIAL O UNILATERAL MENOS D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '716200', 'VULVECTOMIA BILATERAL SIMPLE O PARCIAL S' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '717102', 'SUTURA DE DESGARRO O LACERACION OBSTETRI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '717920', 'SUTURA DE DESGARRO O LACERACION NO OBSTE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '743100', 'REMOCION DE EMBARAZO ECTOPICO ABDOMINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '743200', 'REMOCION DE FETO EN CAVIDAD PERITONEAL S' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '754101', 'REMOCION MANUAL DE PLACENTA RETENIDA O R' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '755100', 'REPARACION DE DESGARRO OBSTETRICO ACTUAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '755200', 'REPARACION DE DESGARRO OBSTETRICO ACTUAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '756100', 'REPARACION DE LACERACIONES O DESGARROS O' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '756200', 'REPARACION DE LACERACIONES O DESGARROS O' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '756910', 'REPARO SECUNDARIO DE EPISIOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '760101', 'SECUESTRECTOMIA INTRAORAL CON FIJACION I' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '760102', 'SECUESTRECTOMIA EXTRAORAL CON FIJACION I' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '762201', 'RESECCION DE TUMOR MALIGNO MAXILAR O MAN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '762202', 'RESECCION RADICAL DE TUMOR MALIGNO MAXIL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '763901', 'RESECCION PARCIAL MAXILAR SIN RECONSTRUC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '764301', 'RECONSTRUCCION MANDIBULAR TOTAL O PARCIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '764302', 'RECONSTRUCCION MANDIBULAR (TOTAL O PARCI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '764303', 'RECONSTRUCCION MANDIBULAR (TOTAL O PARCI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '764304', 'RECONSTRUCCION MANDIBULAR (TOTAL O PARCI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '764305', 'RECONSTRUCCION MANDIBULAR (TOTAL O PARCI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '764401', 'RESECCION PARCIAL MAXILAR CON RECONSTRUC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '764402', 'RESECCION TOTAL DE MAXILAR CON RECONSTRU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '764601', 'RETROPOSICION QUIRURGICA DE LA PREMAXILA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '765301', 'REEMPLAZO TOTAL DE ARTICULACION TEMPOROM' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '765302', 'REEMPLAZO TOTAL DE ARTICULACION TEMPOROM' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '766901', 'SUSPENSION ESQUELETICA EN FRACTURAS U OS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767201', 'REDUCCION ABIERTA DE FRACTURA DE ARCO CI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767203', 'REDUCCION ABIERTA DE FRACTURA MALAR CON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767301', 'REDUCCION CERRADA DE FRACTURA HEMI LEFOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767302', 'REDUCCION CERRADA DE FRACTURA LEFORT I C' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767303', 'REDUCCION CERRADA DE FRACTURA LEFORT II' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767304', 'REDUCCION CERRADA DE FRACTURA LEFORT III' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767401', 'REDUCCION ABIERTA DE FRACTURA HEMI LEFOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767402', 'REDUCCION ABIERTA DE FRACTURA LEFORT I.' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767403', 'REDUCCION ABIERTA DE FRACTURA LEFORT II.' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767404', 'REDUCCION ABIERTA DE FRACTURA LEFORT III' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767501', 'REDUCCION CERRADA FRACTURA DE CONDILO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767502', 'REDUCCION CERRADA DE FRACTURA SIMPLE DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767503', 'REDUCCION CERRADA DE FRACTURA MULTIPLE D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767601', 'REDUCCION ABIERTA DE FRACTURA DE CONDILO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767602', 'REDUCCION ABIERTA DE FRACTURA SIMPLE DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767603', 'REDUCCION ABIERTA DE FRACTURA MULTIPLE D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767701', 'REDUCCION ABIERTA DE FRACTURAS DENTOALVE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767702', 'REDUCCION ABIERTA DE FRACTURAS DENTOALVE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767703', 'REDUCCION ABIERTA DE FRACTURAS DENTOALVE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767705', 'REDUCCION Y FIJACION DE LUXACION DENTO A' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767706', 'REDUCCION Y FIJACION DE LUXACION DENTO A' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767801', 'REDUCCION CERRADA DE FRACTURA ORBITAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767802', 'REDUCCION CERRADA DE FRACTURAS ALVEOLARE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767901', 'REDUCCION ABIERTA DE BORDE O PARED ORBIT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767902', 'REDUCCION ABIERTA DE FRACTURA DE UNA PAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767903', 'REDUCCION ABIERTA DE FRACTURA DE DOS O M' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767904', 'REDUCCION ABIERTA DE FRACTURA DE PISO DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767905', 'REDUCCION ABIERTA DE FRACTURA DE PARED M' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767907', 'REDUCCION ABIERTA DE FRACTURA NASO-ORBIT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767908', 'REDUCCION ABIERTA DE FRACTURAS MULTIPLES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '768302', 'REDUCCION CERRADA DE LUXACION ARTICULACI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '768401', 'REDUCCION ABIERTA DE LUXACION TEMPOROMAN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '768701', 'RETIRO DE MATERIAL DE FIJACION INTERNA D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '768702', 'RETIRO DE CERCLAJE INTER O INTRA MAXILAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '770100', 'SECUESTRECTOMIA DRENAJE DESBRIDAMIENTO O' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '770200', 'SECUESTRECTOMIA DRENAJE DESBRIDAMIENTO D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '770301', 'SECUESTRECTOMIA DRENAJE DESBRIDAMIENTO D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '770302', 'SECUESTRECTOMIA DRENAJE DESBRIDAMIENTO D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '770500', 'SECUESTRECTOMIA DRENAJE DESBRIDAMIENTO D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '770600', 'SECUESTRECTOMIA DRENAJE DESBRIDAMIENTO D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '770701', 'SECUESTRECTOMIA DRENAJE DESBRIDAMIENTO D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '770702', 'SECUESTRECTOMIA DRENAJE DESBRIDAMIENTO D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '770801', 'SECUESTRECTOMIA DRENAJE DESBRIDAMIENTO D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '770802', 'SECUESTRECTOMIA DRENAJE DESBRIDAMIENTO D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '770901', 'SECUESTRECTOMIA DRENAJE DESBRIDAMIENTO D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '770902', 'SECUESTRECTOMIA DRENAJE DESBRIDAMIENTO D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '770920', 'SECUESTRECTOMIA DRENAJE DESBRIDAMIENTO D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '775600', 'REPARACION DE DEDO DE PIE EN MARTILLO FA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '775701', 'REPARACION DE DEDO DE PIE EN GARRA CON A' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '775702', 'REPARACION DEL QUINTO DEDO DE PIE EN GAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776001', 'RESECCION DE EXOSTOSIS EN HUESO NO ESPEC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776109', 'RESECCION TUMOR BENIGNO DE TORAX REJA CO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776110', 'RESECCION TUMOR MALIGNO DE TORAX (REJA C' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776801', 'RESECCION DE LESION MALIGNA OSEA EN TARS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776802', 'RESECCION DE TUMOR BENIGNO EN TARSIANOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776803', 'RESECCION DEL ESPOLON CALCANEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776804', 'RESECCION DE OSTEOFITOS TIBIALES YO TALA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776805', 'RESECCION DE EXOSTOSIS NO ARTICULAR DE P' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '777103', 'TOMA DE INJERTO OSEO DE COSTILLA COSTOCO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '777700', 'TOMA DE INJERTO DE TIBIA O PERONE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '777902', 'TOMA DE INJERTO DE HUESO ILIACO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778101', 'RESECCION PARCIAL DE ESCAPULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778102', 'RESECCION PARCIAL DE CLAVICULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778103', 'RESECCION PARCIAL DE CLAVICULA VIA ENDOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778104', 'RESECCION PARCIAL DE COSTILLAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778105', 'RESECCION PARCIAL DE ESTERNON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778201', 'RESECCION DE EPICONDILO O EPITROCLEA HUM' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778203', 'RESECCION DE CABEZA HUMERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778301', 'RESECCION PARCIAL DE DIAFISIS EN CUBITO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778302', 'RESECCION DE EPIFISIS DE CUBITO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778303', 'RESECCION PARCIAL DE DIAFISIS EN RADIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778304', 'RESECCION DE CUPULA DE RADIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778305', 'RESECCION DE OLECRANON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778306', 'RESECCION DE CABEZA DE RADIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778402', 'RESECCION CABEZA DE METACARPIANOS UNO O' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778600', 'RESECCION PARCIAL DE ROTULA O HEMIPATELE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778701', 'RESECCION PARCIAL DE LA TIBIA HEMIDIAFIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778702', 'RESECCION PARCIAL DE PERONE HEMIDIAFISEC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778800', 'RESECCION PARCIAL DE TARSIANOS O METATAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778902', 'RESECCION CABEZA DE FALANGE DE MANO UNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778912', 'RESECCION CABEZA DE FALANGE DE PIE UNA O' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778921', 'RESECCION PARCIAL DE HUESOS PELVIANOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778931', 'RESECCION DE APOFISIS ODONTOIDES POR ABO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779101', 'RESECCION TOTAL DE ESCAPULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779102', 'RESECCION TOTAL DE CLAVICULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779105', 'RESECCION TOTAL DE ESTERNON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779131', 'RESECCION TOTAL DE COSTILLA O COSTOCONDR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779134', 'RESECCION DE COSTILLA CERVICAL O SUPERNU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779201', 'RESECCION DE HUMERO PROXIMAL O DISTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779202', 'RESECCION RADICAL DE HUMERO SIN INJERTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779203', 'RESECCION TOTAL O RADICAL DE HUMERO CON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779301', 'RESECCION TOTAL O RADICAL DE CUBITO O RA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779500', 'RESECCION TOTAL DE FEMUR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779600', 'RESECCION TOTAL DE ROTULA O PATELECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779701', 'RESECCION TOTAL DE LA TIBIA O PERONE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779801', 'RESECCION TOTAL RADICAL DEL TARSO O META' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779901', 'RESECCION TOTAL DE FALANGES DE MANO UNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779902', 'RESECCION TOTAL DE FALANGES DE PIE UNA O' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779932', 'VERTEBRECTOMIA TOTAL CERVICAL CON REEMPL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779940', 'RESECCION TOTAL DE SACRO (VERTEBRECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779941', 'RESECCION TOTAL DE COCCIX COCCIGECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782341', 'RESECCIONOSTEOTOMIA DE CARPIANOS O METAC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '790100', 'REDUCCION CERRADA DE FRACTURA SIN FIJACI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '790200', 'REDUCCION CERRADA DE FRACTURA SIN FIJACI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '790301', 'REDUCCION CERRADA SIN FIJACION DE FRACTU' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '790401', 'REDUCCION CERRADA DE FRACTURA SIN FIJACI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '790402', 'REDUCCION CERRADA DE FRACTURA SIN FIJACI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '790500', 'REDUCCION CERRADA DE FRACTURADE FEMUR SI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '790600', 'REDUCCION CERRADA DE FRACTURA SIN FIJACI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '790701', 'REDUCCION CERRADA SIN FIJACION INTERNA D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '790702', 'REDUCCION CERRADA DE FRACTURA SIN FIJACI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '790800', 'REDUCCION CERRADA SIN FIJACION INTERNA F' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '790901', 'REDUCCION CERRADA DE FRACTURA SIN FIJACI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '790902', 'REDUCCION CERRADA DE FRACTURA SIN FIJACI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '791100', 'REDUCCION CERRADA DE FRACTURA CON FIJACI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '791201', 'REDUCCION CERRADA DE FRACTURA CON FIJACI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '791402', 'REDUCCION CERRADA DE FRACTURA DE METACAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '791403', 'REDUCCION DE FRACTURA DE HUESOS DE CARPO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '791502', 'REDUCCION CERRADA DE FRACTURA DE FEMUR S' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '791503', 'REDUCCION CERRADA DE FRACTURA DE FEMUR S' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '791701', 'REDUCCION CERRADA DE PILON CON FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '791702', 'REDUCCION CERRADA DE FRACTURAS OSTEOCOND' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '791703', 'REDUCCION CERRADA DE FRACTURAS DEL TERCI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '804304', 'RESECCION DE LESION EN MUÑECA POR ARTROS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807001', 'RESECCCION COMPLETA O PARCIAL DE MEMBRAN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807101', 'SINOVECTOMIA DE HOMBRO PARCIAL VIA ABIER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807102', 'SINOVECTOMIA DE HOMBRO TOTAL VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807103', 'SINOVECTOMIA DE HOMBRO PARCIAL POR ARTRO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807104', 'SINOVECTOMIA DE HOMBRO TOTAL POR ARTROSC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807201', 'SINOVECTOMIA DE CODO PARCIAL VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807202', 'SINOVECTOMIA DE CODO TOTAL VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807203', 'SINOVECTOMIA DE CODO PARCIAL POR ARTROSC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807204', 'SINOVECTOMIA DE CODO TOTAL POR ARTROSCOP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807301', 'SINOVECTOMIA DE MUÑECA PARCIAL VIA ABIER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807302', 'SINOVECTOMIA DE MUÑECA TOTAL VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807303', 'SINOVECTOMIA DE MUÑECA PARCIAL POR ARTRO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807304', 'SINOVECTOMIA DE MUÑECA TOTAL POR ARTROSC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807401', 'SINOVECTOMIA INTERFALANGICA UNA O MAS VI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807402', 'SINOVECTOMIA EN CARPO VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807403', 'SINOVECTOMIA METACARPOFALANGICA UNA O MA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807404', 'SINOVECTOMIA DE FALANGES UNA O MAS POR A' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807501', 'SINOVECTOMIA DE CADERA PARCIAL VIA ABIER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807502', 'SINOVECTOMIA DE CADERA TOTAL VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807503', 'SINOVECTOMIA  PARCIAL DE CADERA POR ARTR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807504', 'SINOVECTOMIA TOTAL DE CADERA POR ARTROSC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807601', 'SINOVECTOMIA DE RODILLA PARCIAL VIA ABIE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807602', 'SINOVECTOMIA DE RODILLA TOTAL VIA ABIERT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807603', 'SINOVECTOMIA DE RODILLA PARCIAL POR ARTR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807604', 'SINOVECTOMIA DE RODILLA TOTAL POR ARTROS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807701', 'SINOVECTOMIA DE TOBILLO PARCIAL VIA ABIE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807702', 'SINOVECTOMIA DE TOBILLO TOTAL VIA ABIERT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807703', 'SINOVECTOMIA DE TOBILLO PARCIAL POR ARTR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807704', 'SINOVECTOMIA DE TOBILLO TOTAL POR ARTROS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '807800', 'SINOVECTOMIA DE PIE O ARTEJOS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808111', 'RESECCION DE HIGROMA DE HOMBRO VIA ABIER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808201', 'RESECCION DE HIGROMA DE CODO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808601', 'RESECCION DE HIGROMA DE RODILLA VIA ABIE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808604', 'RESECCION DE PLICAS DE RODILLA POR ARTRO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808701', 'RESECCION DE LESION OSTEOCONDRAL EN TOBI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814703', 'RETINACULOPLASTIA PARA LIBERACION DE LA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814712', 'SUTURA DE MENISCO MEDIAL O LATERAL POR A' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814722', 'SUTURA DE MENISCO MEDIAL Y LATERAL POR A' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '815301', 'REVISION REEMPLAZO PROTESICO PARCIAL DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '815302', 'REVISION REEMPLAZO TOTAL DE CADERA CON R' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '815501', 'REVISION REEMPLAZO PROTESICO EN RODILLA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '815502', 'REVISION REEMPLAZO PROTESICO TOTAL EN RO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '815810', 'REVISION REEMPLAZO PROTESICO TOTAL DE TO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '817302', 'REVISION DE REEMPLAZO TOTAL DE MUÑECA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '819330', 'SUTURA DEL FIBROCARTILAGO TRIANGULAR O D' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '819410', 'SUTURA SIMPLE DEL TENDON DE AQUILES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '819701', 'REVISION DE ARTROPLASTIA DE HOMBRO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '819702', 'REVISION REEMPLAZO PROTESICO TOTAL DE HO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '819703', 'REVISION REEMPLAZO PROTESICO DE HOMBRO C' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '819704', 'REVISION REEMPLAZO PROTESICO TOTAL DE HO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '819706', 'REVISION REEMPLAZO TOTAL DE CODO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '821101', 'TENOTOMIA DE MANO PALMAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '821102', 'TENOTOMIA DE MANO DORSAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '823301', 'TENOSINOVECTOMIA EN EXTENSORES DE MANO U' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '823303', 'TENOSINOVECTOMIA FLEXORES MANO UNO O MAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '823307', 'TENOSINOVECTOMIA TIPO ENFERMEDAD DE QUER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '823311', 'TENOSINOVECTOMIA EN DEDOS DE MANO UNO O' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '824100', 'SUTURA DE ENVOLTURA O VAINA DE TENDON DE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '824201', 'TENORRAFIA DE FLEXORES DE DEDOS CADA UNO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '824202', 'TENORRAFIA DE FLEXORES DE DEDOS CADA UNO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '824203', 'TENORRAFIA DE FLEXORES DE DEDOS CADA UNO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '824211', 'TENORRAFIA DE FLEXORES DE MANO UNO O MAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '824213', 'TENORRAFIA DE FLEXORES DE MANO UNO O MAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '824215', 'TENORRAFIA DE FLEXORES DE MANO UNO O MAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '824301', 'TENORRAFIA DE EXTENSORES DE DEDOS CADA U' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '824321', 'TENORRAFIA DE EXTENSORES DE MANO UNO O M' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '825301', 'TRANSFERENCIA TENDON MANO Y PUÑO UNO O M' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '825303', 'TRANSFERENCIA DE PRONADOR A SUPINADOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '825304', 'TRANSFERENCIA DE FLEXORES A EXTENSORES E' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '825305', 'TRANSFERENCIA DE EXTENSOR DEL PULGAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '825307', 'TRASFERENCIA DE TENDON EN MANO O MUÑECA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '828101', 'TRASPOSICION DE DEDO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '828102', 'TRASPLANTE DE ARTEJO A MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '828501', 'TENODESIS EN MANO UNO O MAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '829111', 'TENOLISIS EN EXTENSORES DE DEDO UNO O MA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '829115', 'TENOLISIS EN FLEXORES DE DEDOS UNO O MAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '829121', 'TENOLISIS EN EXTENSORES DE MANO UNO O MA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '829125', 'TENOLISIS EN FLEXORES DE MANO UNO O MAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831101', 'TENOTOMIAS EN PIE UNA O MAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831201', 'TENOTOMIAS ABIERTAS UNILATERALES DE CADE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831302', 'TENOTOMIAS EN HOMBRO UNA O MAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831303', 'TENOTOMIAS EN BRAZO UNA O MAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831304', 'TENOTOMIAS EN ANTEBRAZO UNA O MAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831305', 'TENOTOMIA SIMPLE EN CUELLO TORTICOLIS CO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831306', 'TENOTOMIA MULTIPLE EN CUELLO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831307', 'TENOTOMIA DE MUSCULOS ESPASTICOS  EXCEPT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831308', 'TENOTOMIA EN PIERNA UNA O MAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831309', 'TENOTOMIAS DE ISQUIOTIBIALES UNA O MAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831910', 'SECCION DE ESTERNOCLEIDOMASTOIDEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '836010', 'SUTURA DE MUSCULO Y O TENDON Y O FASCIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '836201', 'TENORRAFIA DE FLEXORES DE ANTEBRAZO UNO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '836202', 'TENORRAFIA DE EXTENSORES DE ANTEBRAZO UN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '836305', 'SUTURA DEL MANGUITO ROTADOR POR ENDOSCOP' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '836405', 'SUTURA DEL TENDON BICIPITAL TENODESIS PO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '837501', 'TRANSFERENCIAS DEL PRONADOR REDONDO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '837502', 'TRANSFERENCIAS DE TENDON EN PARALISIS RA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '837503', 'TRANSFERENCIA TENDINOSA O MIOTENDINOSA C' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '837601', 'TRANSFERENCIAS MIOTENDINOSAS DE HOMBRO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '837602', 'TRANSFERENCIAS MIOTENDINOSAS DE ANTEBRAZ' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '837603', 'TRANSFERENCIAS MIOTENDINOSAS DE CODO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '837604', 'TRANSFERENCIAS MIOTENDINOSAS DE MUÑECA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '837605', 'TRANSFERENCIAS MIOTENDINOSAS DE CADERA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '837606', 'TRANSFERENCIAS MIOTENDINOSAS DE MUSLO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '837607', 'TRANSFERENCIAS MIOTENDINOSAS DE RODILLA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '837608', 'TRANSFERENCIAS MIOTENDINOSAS DE PIERNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '837609', 'TRANSFERENCIAS MIOTENDINOSAS DE PIE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '838830', 'TENODESIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '861410', 'TATUAJE INTRADERMICO O INYECCION DE PIGM' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '865101', 'SUTURA DE HERIDA UNICA EN AREA GENERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '865102', 'SUTURA DE HERIDA MULTIPLE EN AREA GENERA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '865202', 'SUTURA DE HERIDA UNICA DE CARA SIN COMPR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '865203', 'SUTURA DE HERIDA UNICA DE PLIEGUES DE FL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '865204', 'SUTURA DE HERIDA MULTIPLE DE CARA NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '865207', 'SUTURA DE HERIDA PARCIAL DE CUERO CABELL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '865210', 'SUTURA DE MATRIZ UNGUEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '72300', 'SUPRARRENALECTOMIA (ADRENALECTOMIA), PARCIAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '72301', 'SUPRARRENALECTOMIA (ADRENALECTOMIA), PARCIAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '72200', 'SUPRARRENALECTOMIA (ADRENALECTOMIA) UNILATERAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '72201', 'SUPRARRENALECTOMIA (ADRENALECTOMIA) UNILATERAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '78200', 'ESCISION TOTAL DE TIMO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '98202', 'CONJUNTIVODACRIOCISTORRINOSTOMIA SIMPLE VIA EXTERNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '102100', 'BIOPSIA DE CONJUNTIVA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '142101', 'ABLACION  DE LESION CORIORETINAL, POR DIATERMIA O CRIOTERAPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '138101', 'EXTRACCION DE LENTE INTRAOCULAR (PSEUDOCRISTALINO) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '218802', 'SEPTOPLASTIA CON CIERRE  DE PERFORACION SEPTAL    (180)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '218803', 'SEPTOPLASTIA CON CIERRE  DE PERFORACION SEPTAL    (180)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '218804', 'SEPTOPLASTIA CON CIERRE  DE PERFORACION SEPTAL    (180)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '253000', 'GLOSECTOMIA TOTAL SIN RESECCION MANDIBULAR Y RECONSTRUCCION CON COLGAJO PEDICULADO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '260200', 'EXPLORACION DE GLANDULA SALIVAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '291200', 'BIOPSIA FARINGEA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '334201', 'CIERRE DE FISTULA  BRONCOCUTANEA O BRONCOPLEURAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '334202', 'CIERRE DE BRONCOSTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '334400', 'RECONSTRUCCION DE BRONQUIO (BRONCOPLASTIA) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '322100', 'RESECCION O PLICATURA DE BULAS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '322200', 'REDUCCION  QUIRURGICA DE VOLUMEN PULMONAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '322201', 'REDUCCION  QUIRURGICA DE VOLUMEN PULMONAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '323100', 'LOBECTOMIA SEGMENTARIA ( LOBECTOMIA PARCIAL O RESECCION EN CUÑA) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '502201', 'RESECCION EN CUÑA DE HIGADO   (81)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '324200', 'LOBECTOMIA TOTAL PULMONAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '325100', 'NEUMONECTOMIA SIMPLE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '325200', 'NEUMONECTOMIA RADICAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '330101', 'EXTRACCION DE CUERPO EXTRAÑO EN BRONQUIO O PULMON, VIA ABIERTA    (345)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '344101', 'ESCISION O ABLACION DE LESION DE PARED TORACICA POR TORACOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '345100', 'PLEURECTOMIA PARIETAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '349201', 'PLEURODESIS QUIMICA    (346)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '349202', 'PLEURODESIS QUIMICA    (346)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '345300', 'DECORTICACION PULMONAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '348201', 'SUTURA DE LACERACION DIAFRAGMATICA VIA  TRANSTORACICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '348500', 'IMPLANTACION DE MARCAPASOS DIAFRAGMATICO SOD *' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '348600', 'PLICATURA DE DIAFRAGMA POR EVENTRACION SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '349400', 'CONTROL DE HEMORRAGIA DESPUES DE INTERVENCIONES INTRATORACICAS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '352100', 'REEMPLAZO DE LA VALVULA AORTICA CON PROTESIS MECANICA O BIOPROTESIS (AUTOLOGA O HETEROLOGA) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '352300', 'REEMPLAZO DE VALVULA TRICUSPIDE CON PROTESIS MECANICA O BIOPROTESIS (AUTOLOGA O HETEROLOGA) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '369200', 'REPARACION O CIERRE DE FISTULA AORTO-CORONARIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '379402', 'IMPLANTACION DE CARDIOVERSOR/DESFIBRILADOR  POR  VIA SUBCUTANEA (SUBPECTORAL)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '379403', 'IMPLANTACION DE CARDIOVERSOR/DESFIBRILADOR  POR  VIA SUBCUTANEA (SUBPECTORAL)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '379404', 'IMPLANTACION DE CARDIOVERSOR/DESFIBRILADOR  POR  VIA SUBCUTANEA (SUBPECTORAL)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383401', 'RECONSTRUCION  DE AORTA TORACICA ASCENDENTE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383402', 'RECONSTRUCCION DEL CAYADO AORTICO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '392204', 'DERIVACION O PUENTE AORTO- SUBCLAVIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '397500', 'EXPLORACION DE VASOS TORACICOS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '401101', 'BIOPSIA DE GANGLIO LINFATICO SUPERFICIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '401102', 'BIOPSIA DE GANGLIO LINFATICO SUPERFICIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '405100', 'VACIAMIENTO RADICAL LINFATICO AXILAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '405301', 'LINFADENECTOMIA RADICAL INGUINOFEMORAL, UNILATERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '405302', 'LINFADENECTOMIA RADICAL INGUINOFEMORAL O ILIACA  BILATERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '405401', 'LINFADENECTOMIA RADICAL PELVICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '405402', 'LINFADENECTOMIA RADICAL EXTRAPERITONEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '406400', 'LIGADURA DEL CONDUCTO TORACICO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '414200', 'ESCISION DE LESION O TEJIDO DE BAZO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '414300', 'ESPLENECTOMIA PARCIAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '414400', 'EXTRACCION  DE CUERPO EXTRAÑO DEL BAZO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '414500', 'ESCISION DE BAZO ACCESORIO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '420100', 'DRENAJE DE COLECCION DE ESOFAGO POR ESOFAGOTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '420101', 'DRENAJE DE COLECCION DE ESOFAGO POR ESOFAGOTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '423101', 'DIVERTICULECTOMIA DE ESOFAGO VIA CERVICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '423102', 'DIVERTICULECTOMIA DE ESOFAGO VIA TRANSTORACICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '423103', 'DIVERTICULECTOMIA DE ESOFAGO VIA TRANSTORACICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '423202', 'RESECCION DE TUMOR  DE ESOFAGO  POR TORACOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '423203', 'RESECCION DE TUMOR  DE ESOFAGO  POR TORACOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '424100', 'ESOFAGECTOMIA PARCIAL SOD    (191)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '424101', 'ESOFAGECTOMIA PARCIAL SOD    (191)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '424200', 'ESOFAGECTOMIA TOTAL SOD    (191) (202)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '424201', 'ESOFAGECTOMIA TOTAL SOD    (191) (202)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '425100', 'ESOFAGOESOFAGOSTOMIA VIA INTRATORACICA Y/O CERVICAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '425101', 'ESOFAGOESOFAGOSTOMIA VIA INTRATORACICA Y/O CERVICAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '425000', 'ANASTOMOSIS  DE ESOFAGO  VIA INTRATORACICA Y/O CERVICAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '425001', 'ANASTOMOSIS  DE ESOFAGO  VIA INTRATORACICA Y/O CERVICAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '425002', 'ANASTOMOSIS  DE ESOFAGO  VIA INTRATORACICA Y/O CERVICAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '425003', 'ANASTOMOSIS  DE ESOFAGO  VIA INTRATORACICA Y/O CERVICAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '427401', 'ESOFAGOCARDIO MIOTOMIA ABDOMINAL O TORACICA [HELLER] VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '429205', 'DILATACION ESOFAGICA ENDOSCOPICA MEDIANTE LASER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '431200', 'GASTROSTOMIA  POR LAPAROTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '433100', 'PILOROMIOTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '437100', 'GASTROYEYUNOSTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '438100', 'GASTRECTOMIA SUBTOTAL RADICAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '438300', 'GASTROENTEROANASTOMOSIS DERIVATIVA (DUODENO O YEYUNO) CON EXCLUSION PILORICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '438301', 'GASTROENTEROANASTOMOSIS DERIVATIVA (DUODENO O YEYUNO) CON EXCLUSION PILORICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '439300', 'ESOFAGOGASTRECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '440100', 'VAGOTOMIA  TRONCULAR Y PILOROPLASTIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '440200', 'VAGOTOMIA SELECTIVA O SUPRASELECTIVA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '441100', 'GASTROSCOPIA TRANSABDOMINAL (INTRAQUIRURGICA) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '442100', 'DILATACION DE PILORO MEDIANTE INCISION SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '442000', 'PILOROPLASTIA-PILORECTOMIA ANTERIOR SOD    (208)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '444000', 'SUTURA DE ULCERA PERFORADA CON VAGOTOMIA Y EPIPLOPLASTIA SOD   (18)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '444100', 'SUTURA DE ULCERA GASTRICA SOD    (18)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '444200', 'SUTURA DE ULCERA DUODENAL SOD    (18)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '444300', 'CONTROL ENDOSCOPICO DE HEMORRAGIA GASTRICA O DUODENAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '445100', 'REANASTOMOSIS DEL  ESTOMAGO POR DESHISCENCIA DE LA SUTURA SOD    (207)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '446100', 'SUTURA DE DESGARRO O HERIDA DE ESTOMAGO (GASTRORRAFIA) SOD   (17)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '446200', 'CIERRE DE GASTROSTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '446300', 'CIERRE DE OTRA FISTULA GASTRICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '446500', 'ESOFAGOGASTROPLASTIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '449100', 'LIGADURA DE VARICES GASTRICAS VIA ABIERTA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '449200', 'MANIPULACION INTRAOPERATORIA DE ESTOMAGO (REDUCCION DE VOLVULO ) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '449500', 'BAYPASS O DERIVACION O PUENTE DUODENAL PARA REFLUJO DUODENOGASTRICO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '449501', 'BAYPASS O DERIVACION O PUENTE DUODENAL PARA REFLUJO DUODENOGASTRICO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '449502', 'BAYPASS O DERIVACION O PUENTE DUODENAL PARA REFLUJO DUODENOGASTRICO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '450001', 'EXTRACCION DE CUERPO EXTRAÑO INTESTINAL POR ENTEROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '451100', 'ENDOSCOPIA TRANSABDOMINAL DE INTESTINO DELGADO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '452301', 'COLONOSCOPIA TOTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '460200', 'RESECCION DE SEGMENTO EXTERIORIZADO DE INTESTINO DELGADO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '454100', 'RESECCION DE LESION O TEJIDO DE INTESTINO GRUESO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '456300', 'RESECCION TOTAL DE INTESTINO DELGADO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '457101', 'COLECTOMIA PARCIAL  CON COLOSTOMIA O ILEOSTOMIA Y FISTULA MUCOSA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '457000', 'COLECTOMIA PARCIAL CON COLOSTOMIA Y CIERRE DE SEGMENTO DISTAL  [HARTMAN] SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '457200', 'CECECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '457300', 'HEMICOLECTOMIA DERECHA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '457400', 'RESECCION DE COLON TRANSVERSO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '457500', 'HEMICOLECTOMIA IZQUIERDA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '457600', 'SIGMOIDECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '458100', 'COLECTOMIA TOTAL CON ILEOSTOMIA Y PROTECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '458200', 'COLECTOMIA TOTAL MAS RESERVORIO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '459100', 'ANASTOMOSIS DE INTESTINO DELGADO A INTESTINO DELGADO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '459200', 'ANASTOMOSIS DE INTESTINO DELGADO AL MUÑON RECTAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '459400', 'ANASTOMOSIS DE INTESTINO GRUESO A INTESTINO GRUESO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '459501', 'ANASTOMOSIS DE INTESTINO DELGADO AL ANO, CON FORMACION DE RESERVORIO (EN "J", " H" O "S")' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '461100', 'COLOSTOMIA TEMPORAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '465100', 'CIERRE DE ESTOMA DE INTESTINO DELGADO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '465201', 'CIERRE DE ESTOMA DE INTESTINO GRUESO POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '467301', 'ENTERORRAFIA (UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '467200', 'CIERRE DE FISTULA DE DUODENO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '467400', 'CIERRE DE FISTULA DE INTESTINO DELGADO, SALVO DUODENO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '467601', 'CIERRE DE FISTULA  ENTEROCOLICA ( UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '467700', 'CIERRE DE FISTULA ENTEROCUTANEA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '469400', 'REINTERVENCION DE ANASTOMOSIS INTESTINAL SOD    (207)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '485400', 'PROCTECTOMIA CON DESCENSO ABDOMINO-PERINEAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '486200', 'RESECCION ANTERIOR DE RECTO CON COLOSTOMIA SIMULTANEA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '486700', 'RESECCION DE TUMOR RECTAL POR VIA TRANS-ANAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '487100', 'SUTURA DE LACERACION DE RECTO (PROCTORRAFIA) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '487500', 'PROCTOPEXIA ABDOMINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '487601', 'PROCTOSIGMOIDOPEXIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '489300', 'REPARACION DE FISTULA PERIRRECTAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '494700', 'EVACUACION DE HEMORROIDES TROMBOSADAS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '500100', 'EXTRACCION DE CUERPO EXTRAÑO INTRAHEPATICO POR INCISION SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '502202', 'HEPATECTOMIA DE DOS SEGMENTOS    (81)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '502204', 'HEPATECTOMIA TRISEGMENTARIA    (81)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '502402', 'ENUCLEACION DE LESION HEPATICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '506101', 'HEPATORRAFIA SIMPLE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '506102', 'HEPATORRAFIA MULTIPLE; CON DESBRIDAMIENTO Y HEMOSTASIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '510100', 'HEPATICOTOMIA O HEPATICOSTOMIA CON DRENAJE O EXTRACCION DE CALCULOS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '511100', 'COLANGIOGRAFIA ENDOSCOPICA RETROGRADA (TRANSDUODENAL) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '512600', 'RESECCION DE TUMOR MALIGNO DE VIAS BILIARES BILIOENTERICAS PROXIMALES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '513200', 'ANASTOMOSIS DE VESICULA BILIAR A INTESTINO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '513600', 'COLEDOCODUODENOSTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '514201', 'EXPLORACION POR ATRESIA CONGENITA DE VIAS BILIARES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '514300', 'RE EXPLORACION  DE VIAS BILIARES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '514400', 'INSERCION DE TUBO COLEDOCOHEPATICO PARA DESCOMPRESION SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '517100', 'SUTURA SIMPLE DE COLEDOCO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '517200', 'COLEDOCOPLASTIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '517300', 'RECONSTRUCCION DE VIAS BILIARES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '518300', 'ESFINTEROPLASTIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '519400', 'REVISION DE ANASTOMOSIS DE LAS VIAS BILIARES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '520100', 'DRENAJE DE ABSCESO DE PANCREAS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '520200', 'MARSUPIALIZACION DE QUISTE DEL PANCREAS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '522200', 'RESECCION DE LESION O TEJIDO DE PANCREAS SOD    (204)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '523100', 'EXTRACCION DE CUERPO EXTRAÑO DE PANCREAS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '524400', 'DRENAJE INTERNO DE QUISTE PANCREATICO POR CISTOGASTROSTOMIA ABIERTA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '525100', 'PANCREATECTOMIA PROXIMAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '525101', 'PANCREATECTOMIA PROXIMAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '525201', 'PANCREATECTOMIA DISTAL CON  ESPLENECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '525202', 'PANCREATECTOMIA DISTAL CON  ESPLENECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '525300', 'PANCREATECTOMIA SUBTOTAL  [OPERACION DE CHILD] SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '527100', 'PANCREATICODUODENECTOMIA TOTAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '527200', 'PANCREATICODUODENECTOMIA  PROXIMAL [WHIPPLE] SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '529502', 'SUTURA SIMPLE DE PANCREAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '529602', 'ANASTOMOSIS DEL PANCREAS POR LAPAROTOMIA    (83)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '529603', 'ANASTOMOSIS DEL PANCREAS POR LAPAROTOMIA    (83)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '529604', 'PANCREATO-YEYUNOSTOMIA TERMINO LATERAL (OPERACION DE PUESTOW)    (83)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '530300', 'HERNIORRAFIA INGUINAL ENCARCELADA SOD    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '530400', 'HERNIORRAFIA INGUINAL REPRODUCIDA SOD    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '534100', 'HERNIORRAFIA UMBILICAL REPRODUCIDA SOD    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '534000', 'HERNIORRAFIA UMBILICAL SOD    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '535200', 'HERNIORRAFIA EPIGASTRICA SOD    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '536000', 'HERNIORRAFIA LUMBAR SOD    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '536100', 'HERNIORRAFIA OBTURADORA SOD    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '537100', 'REPARACION DE HERNIA DIAFRAGMATICA VIA TORACICA SOD    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '537101', 'REPARACION DE HERNIA DIAFRAGMATICA VIA TORACICA SOD    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '537200', 'REPARACION DE HERNIA DIAFRAGMATICA VIA TORACOABDOMINAL SOD    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '537201', 'REPARACION DE HERNIA DIAFRAGMATICA VIA TORACOABDOMINAL SOD    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '540001', 'DRENAJE DE COLECCION EXTRAPERITONEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '540002', 'DRENAJE DE COLECCION EXTRAPERITONEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '541503', 'RESECCION DE TUMOR RETROPERITONEAL CON DISECCION DE ESTRUCTURAS VASCULARES U ORGANOS RETROPERITONEALES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '541504', 'RESECCION DE TUMOR RETROPERITONEAL CON DISECCION DE ESTRUCTURAS VASCULARES U ORGANOS RETROPERITONEALES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '541600', 'RESECCION DE LESION BENIGNA O MALIGNA EN EPIPLON O EN MESENTERIO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '541400', 'LAVADO PERITONEAL TERAPEUTICO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '691910', 'CITORREDUCCION DE TUMOR DE LIGAMENTO ANCHO O DE LIGAMENTO UTERO SACRO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '543200', 'RESECCION DE TUMOR MALIGNO DE LA PARED ABDOMINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '543201', 'RESECCION DE TUMOR MALIGNO DE LA PARED ABDOMINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '547400', 'EVENTRORRAFIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '551120', 'EXTRACCION DE CUERPO EXTRAÑO Y/O CALCULO POR PIELOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '553101', 'ESCISION LOCAL O ABLACION DE LESION RENAL VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '555600', 'NEFRECTOMIA SIMPLE ( UNILATERAL TOTAL) SOD    (210)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'M09143', 'NEFRECTOMIA RADICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '556100', 'AUTOTRANSPLANTE RENAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '557000', 'NEFROPEXIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '558630', 'ANASTOMOSIS URETERO CALICIAL O NEFROCALICOSTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '558800', 'LIBERACION DE ADHERENCIAS PIELICAS O URETEROPIELICAS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '564200', 'URETERECTOMIA TOTAL O RESIDUAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '564130', 'ACORTAMIENTO O REMODELACION DE URETER CON REIMPLANTACION URETEROVESICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '568700', 'URETEROPLASTIA O ANASTOMOSIS TERMINO-TERMINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '576000', 'CISTECTOMIA  PARCIAL  POR VIA ABIERTA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '578801', 'ANASTOMOSIS DE VEJIGA CON SEGMENTO INTESTINAL NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '588202', 'INSERCION DE PROTESIS  (STENT) URETRAL VIA ENDOSCOPICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '590100', 'EXPLORACION RETROPERITONEAL (LUMBOTOMIA EXPLORADORA) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '590300', 'URETEROLISIS O  PIELOURETEROLISIS (LIBERACION DE ADHERENCIAS PERIURETERALES Y PERICALICIALES) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '604000', 'ADENOMECTOMIA RETROPUBICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '607200', 'VESICULOTOMIA SEMINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '607201', 'VESICULOTOMIA SEMINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '607302', 'VESICULECTOMIA  O ESPERMATOCISTECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '623000', 'ORQUIECTOMIA (TESTICULO) SOD    (85)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '631001', 'LIGADURA ALTA DE VENA ESPERMATICA VIA RETROPERITONEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '652702', 'FULGURACION EN OVARIO  POR LAPAROSCOPIA    (24)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '672001', 'CONIZACION NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '674000', 'AMPUTACION DEL CUELLO UTERINO O TRAQUELECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '674001', 'AMPUTACION DEL CUELLO UTERINO O TRAQUELECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '578401', 'FISTULECTOMIA CERVICO-VESICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '686100', 'HISTERECTOMIA RADICAL MODIFICADA [OPERACION DE WERTHEIM] SOD    (384)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '686101', 'HISTERECTOMIA RADICAL MODIFICADA [OPERACION DE WERTHEIM] SOD    (384)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '768100', 'INJERTO OSEO EN HUESO FACIAL SOD    (224)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '774001', 'BIOPSIA DE HUESO EN SITIO NO ESPECIFICADO, POR VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '774911', 'BIOPSIA DE VERTEBRA, POR VIA  PERCUTANEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778932', 'RESECCION DE APOFISIS ODONTOIDES  POR ABORDAJE TRANSORAL (94)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779933', 'VERTEBRECTOMIA TOTAL CERVICAL CON REEMPLAZO CORPORAL ARTIFICIAL    (174)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779942', 'RESECCION TOTAL DE SACRO [VERTEBRECTOMIA TOTAL SACRA] CON ARTRODESIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779934', 'VERTEBRECTOMIA TOTAL CERVICAL CON REEMPLAZO CORPORAL ARTIFICIAL    (174)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781501', 'APLICACION TUTOR EXTERNO EN CADERA NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782401', 'EPIFISIODESIS ABIERTA DE FEMUR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782402', 'EPIFISIODESIS ABIERTA DE FEMUR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782403', 'GRAPADO EPIFISIARIO  DE FEMUR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782523', 'EPIFISIODESIS PERCUTANEA  DE TIBIA Y PERONE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782524', 'EPIFISIODESIS PERCUTANEA  DE TIBIA Y PERONE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782721', 'EPIFISIODESIS PERCUTANEA DE FALANGES DE MANO (UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782722', 'EPIFISIODESIS PERCUTANEA DE FALANGES DE MANO (UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782723', 'EPIFISIODESIS PERCUTANEA DE FALANGES DE MANO (UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783202', 'ALARGAMIENTO DE HUMERO POR INJERTO CON DISPOSITIVOS INTERNOS DE FIJACION U OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783203', 'ALARGAMIENTO DE HUMERO POR INJERTO CON DISPOSITIVOS INTERNOS DE FIJACION U OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783204', 'ALARGAMIENTO DE HUMERO POR INJERTO CON DISPOSITIVOS INTERNOS DE FIJACION U OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783402', 'ALARGAMIENTO DE  METACARPIANOS (UNO O MAS) POR INJERTO CON DISPOSITIVOS INTERNOS DE FIJACION O OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783403', 'ALARGAMIENTO DE  METACARPIANOS (UNO O MAS) POR INJERTO CON DISPOSITIVOS INTERNOS DE FIJACION O OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783901', 'ALARGAMIENTO DE FALANGES DE MANO CON INJERTO SIN DISPOSITIVOS INTERNOS DE FIJACION Y OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783902', 'ALARGAMIENTO DE FALANGES DE MANO CON INJERTO SIN DISPOSITIVOS INTERNOS DE FIJACION Y OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783903', 'ALARGAMIENTO DE FALANGES DE MANO CON INJERTO SIN DISPOSITIVOS INTERNOS DE FIJACION Y OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783904', 'ALARGAMIENTO DE FALANGES DE MANO CON INJERTO SIN DISPOSITIVOS INTERNOS DE FIJACION Y OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '785400', 'FIJACION INTERNA SIN REDUCCION DE FRACTURA DE CARPIANOS O METACARPIANOS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '787902', 'OSTEOCLASTIA DE FALANGES (UNO O MAS) DE PIE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793950', 'REDUCCION ABIERTA DE FRACTURA DE COLUMNA VERTEBRAL [TORACICA, LUMBAR O SACRA] VIA ANTERIOR  CON INSTRUMENTACION SIMPLE    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '804101', 'DIVISION DE CAPSULA, LIGAMENTO O CARTILAGO ARTICULAR DE HOMBRO POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '804701', 'DIVISION DE CAPSULA, LIGAMENTO O CARTILAGO ARTICULAR DE TOBILLO  POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '804702', 'DIVISION DE CAPSULA, LIGAMENTO O CARTILAGO ARTICULAR DE TOBILLO  POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '804703', 'DIVISION DE CAPSULA, LIGAMENTO O CARTILAGO ARTICULAR DE TOBILLO  POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814220', 'REPARACION TRIADA DE RODILLA: MENISCOPLASTIAL CON REPARACION DE LIGAMENTO CRUZADO ANTERIOR Y LIGAMENTO MEDIAL COLATERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814713', 'SUTURA DE MENISCO MEDIAL O LATERAL, POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '825400', 'REFIJACION DE MUSCULO DE MANO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '825401', 'REFIJACION DE MUSCULO DE MANO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '497402', 'TRANSPOSICION DEL MUSCULO RECTO INTERNO PARA INCONTINENCIA ANAL, VIA SAGITAL POSTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '827901', 'INJERTO DE TENDON EXTENSOR DE MANO  O DEDOS (UNO O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '840701', 'AMPUTACION DE BRAZO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '549003', 'COLOCACION DE CATETERES PARA DERIVACION VENTRICULOPERITONEAL Y PERITONEOVENTRICULAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793930', 'REDUCCION ABIERTA DE FRACTURA DE ODONTOIDES VIA ANTERIOR CON INSTRUMENTACION SIMPLE (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793931', 'REDUCCION ABIERTA  DE FRACTURA DE ODONTOIDES VIA ANTERIOR CON INSTRUMENTACION MODULAR    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792932', 'REDUCCION ABIERTA FRACTURA  SIN FIJACION DE COLUMNA CERVICAL VIA POSTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793932', 'REDUCCION ABIERTA DE FRACTURA DE ODONTOIDES VIA POSTERIOR CON INSTRUMENTACION SIMPLE (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793933', 'REDUCCION ABIERTA DE FRACTURA DE ODONTOIDES VIA POSTERIOR CON INSTRUMENTACION MODULAR    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793937', 'REDUCCION DE FRACTURA OCCIPITOCERVICAL VIA POSTERIOR CON INSTRUMENTACION SIMPLE    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793938', 'REDUCCION DE FRACTURA OCCIPITOCERVICAL VIA POSTERIOR CON INSTRUMENTACION MODULAR    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793942', 'REDUCCION DE FRACTURA COLUMNA CERVICAL EN C1 VIA POSTERIOR CON INSTRUMENTACION SIMPLE    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793943', 'REDUCCION DE FRACTURA COLUMNA CERVICAL EN C1 VIA POSTERIOR CON INSTRUMENTACION MODULAR    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793946', 'REDUCCION DE FRACTURA COLUMNA CERVICAL POR DEBAJO DE C2 VIA POSTERIOR CON INSTRUMENTACION SIMPLE     (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793947', 'REDUCCION DE FRACTURA COLUMNA CERVICAL POR DEBAJO DE C2 VIA POSTERIOR CON INSTRUMENTACION MODULAR    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792931', 'REDUCCION ABIERTA FRACTURA SIN FIJACION DE COLUMNA CERVICAL  VIA ANTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792935', 'REDUCCION ABIERTA  FRACTURA SIN FIJACION DE COLUMNA TORACICA O LUMBAR   VIA ANTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793935', 'REDUCCION DE FRACTURA OCCIPITOCERVICAL VIA ANTERIOR CON INSTRUMENTACION SIMPLE    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793936', 'REDUCCION  DE FRACTURA OCCIPITOCERVICAL VIA ANTERIOR CON INSTRUMENTACION MODULAR    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793940', 'REDUCCION DE FRACTURA COLUMNA CERVICAL EN C1 VIA ANTERIOR CON INSTRUMENTACION SIMPLE    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793941', 'REDUCCION DE FRACTURA COLUMNA CERVICAL EN C1 VIA ANTERIOR CON INSTRUMENTACION MODULAR    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793944', 'REDUCCION DE FRACTURA COLUMNA CERVICAL POR DEBAJO DE C2 VIA ANTERIOR CON INSTRUMENTACION SIMPLE    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793945', 'REDUCCION DE FRACTURA COLUMNA CERVICAL POR DEBAJO DE C2 VIA ANTERIOR CON INSTRUMENTACION MODULAR    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793951', 'REDUCCION ABIERTA DE FRACTURA DE COLUMNA VERTEBRAL [TORACICA, LUMBAR O SACRA] VIA ANTERIOR CON INSTRUMENTACION MODULAR    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776931', 'ESCISION DE TUMOR (BENIGNO O MALIGNO)  EN  COLUMNA VERTEBRAL VIA POSTERIOR O POSTEROLATERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776932', 'ESCISION DE TUMOR (BENIGNO O MALIGNO)  SACROCOCCIGEO VIA ANTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776933', 'ESCISION DE TUMOR (BENIGNO O MALIGNO)  SACROCOCCIGEO VIA POSTERIOR O POSTEROLATERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776930', 'ESCISION DE TUMOR (BENIGNO O MALIGNO)  EN  COLUMNA VERTEBRAL VIA ANTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '805101', 'ESCISION DE DISCO INTERVERTEBRAL EN SEGMENTO CERVICAL VIA ANTERIOR   (100)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '805102', 'ESCISION DE DISCO INTERVERTEBRAL  EN SEGMENTO CERVICAL VIA POSTERIOR    (100)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '805105', 'DISCECTOMIA CERVICAL, VIA ANTERIOR CON INJERTO OSEO AUTOLOGO O HETEROLOGO [CLOWARD, SMITH ROBINSON, SIMMONS]    (100)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '805121', 'ESCISION DE DISCO INTERVERTEBRAL  EN SEGMENTO TORACICO VIA ANTERIOR    (100)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '805122', 'ESCISION DE DISCO INTERVERTEBRAL  EN SEGMENTO TORACICO VIA POSTERIOR    (100)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '805131', 'ESCISION DE DISCO INTERVERTEBRAL  EN SEGMENTO LUMBAR VIA ANTERIOR     (100)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '805132', 'ESCISION DE DISCO INTERVERTEBRAL  EN SEGMENTO LUMBAR VIA POSTERIOR    (100)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '805134', 'DISCECTOMIA  LUMBAR, VIA  POSTEROLATERAL  CON O SIN FACECTOMIA [EN DESCOMPRESION]    (100)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780931', 'INJERTO  OSEO EN  COLUMNA VERTEBRAL VIA ANTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780932', 'INJERTO  OSEO EN  COLUMNA VERTEBRAL VIA POSTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '805110', 'NUCLEOTOMIA PERCUTANEA  CERVICAL [EN DESCOMPRESION]    (100)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '805123', 'NUCLEOTOMIA PERCUTANEA  TORACICA [ EN DESCOMPRESION]    (100)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '805133', 'NUCLEOTOMIA PERCUTANEA LUMBAR [ EN DESCOMPRESION]    (100)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '805200', 'QUIMIONUCLEOLISIS O DISCOLISIS INTERVERTEBRAL SOD *    (100)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '43001', 'ANASTOMOSIS DE NERVIO FACIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '89101', 'ABLACION DE PESTAÑAS, POR ELECTROLISIS    (175)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '103201', 'ABLACION DE LESION O TEJIDO DE CONJUNTIVA, POR DIATERMIA O CRIOCOAGULACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '183102', 'AURICULECTOMIA PARCIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '183103', 'AURICULECTOMIA TOTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '223901', 'ANTROTOMIA MAXILAR EXPLORATORIA VIA MEATO INFERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '223902', 'ANTROTOMIA MAXILAR EXPLORATORIA VIA FOSA CANINA CON RESECCION DE MUCOSA DEL ANTRO MAXILAR Y ANTROTOMIA INFERIOR  [OPERACION DE CALDWELL-LUC]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '227101', 'CIERRE DE FISTULA OROANTRAL     (183)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '317202', 'CIERRE DE FISTULA TRAQUEO CUTANEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '295201', 'CIERRE DE FISTULA BRANQUIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '295202', 'CIERRE DE QUISTE BRANQUIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '402500', 'ESCISION DE HIGROMA  QUISTICO DE CUELLO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '402600', 'ESCISION DE LINFANGIOMA DE CUELLO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '381302', 'ENDARTERECTOMIA AXILAR    (76)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '381801', 'ENDARTERECTOMIA SUPRAPATELAR    (76)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '381802', 'ENDARTERECTOMIA INFRAPATELAR    (76)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383801', 'ANASTOMOSIS ARTERIAL PRIMARIA SUPRAPATELAR    (8)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383802', 'ANASTOMOSIS ARTERIAL PRIMARIA INFRAPATELAR    (8)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383901', 'ANASTOMOSIS VENOSA TERMINO-TERMINAL SUPRAPATELAR    (8)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383902', 'ANASTOMOSIS VENOSA TERMINO-TERMINAL INFRAPATELAR    (8)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '394100', 'CONTROL DE HEMORRAGIA DESPUES DE CIRUGIA VASCULAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '392802', 'DERIVACION (INJERTO) O PUENTE FEMORO-PERONEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '392803', 'DERIVACION (INJERTO) O PUENTE FEMORO-POPLITEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '392804', 'DERIVACION (INJERTO) O PUENTE FEMORO-TIBIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '392601', 'DERIVACION O PUENTE FEMORO- FEMORAL [ CRUZADO]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '392202', 'DERIVACION O PUENTE AXILO- AXILAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '392602', 'DERIVACION O  PUENTE AXILO- FEMORAL UNILATERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '392603', 'DERIVACION O PUENTE AXILO- FEMORAL BIFEMORAL [CRUZADO]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '407101', 'ANASTOMOSIS DE VASOS LINFATICOS DE GRUESO CALIBRE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '406500', 'DERIVACION LINFOVENOSA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '406300', 'CIERRE DE FISTULA DEL CONDUCTO TORACICO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '402400', 'ESCISION DE GANGLIO LINFATICO INGUINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '381603', 'ENDARTERECTOMIA AORTOILIACA    (76)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '381400', 'ENDARTERECTOMIA DE AORTA SOD    (76)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '392604', 'DERIVACION AORTICO-MESENTERICO SUPERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '392400', 'DERIVACION AORTA-RENAL SOD    (12)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '392501', 'DERIVACION AORTO-FEMORAL    (12)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '392502', 'DERIVACION AORTO-ILIACA    (12)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '381601', 'ENDARTERECTOMIA RENAL   (76)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '381602', 'ENDARTERECTOMIA  CELIACA Y/O MESENTERICA    (76)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '390200', 'ANASTOMOSIS (INJERTO) SUBCLAVIA-PULMONAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '391201', 'DERIVACION YUGULO-CAVA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '391202', 'DERIVACION YUGULO- ATRIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '392100', 'ANASTOMOSIS DE VENA CAVA-ARTERIA PULMONAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '392201', 'DERIVACION DE AORTA A CAROTIDA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '392203', 'DERIVACION O PUENTE SUBCLAVIO- SUBCLAVIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '390100', 'ANASTOMOSIS (INJERTO) AORTA ASCENDENTE-ARTERIA PULMONAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '359200', 'CREACION DE CONDUCTO ENTRE EL VENTRICULO DERECHO Y LA ARTERIA PULMONAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '359300', 'CREACION DE CONDUCTO ENTRE EL VENTRICULO IZQUIERDO Y LA AORTA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '359401', 'CREACION DE CONDUCTO ENTRE AURICULA Y ARTERIA PULMONAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '359402', 'CREACION DE FISTULAS SISTEMICO-PULMONARES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '359403', 'DERIVACION CAVO- PULMONAR TOTAL [FONTAN]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '359404', 'DERIVACION CAVO SUPERIOR A ARTERIA PULMONAR [GLENN CLASICO O BIDIRECCIONAL]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '359405', 'DERIVACION CAVO SUPERIOR- PULMONAR,  DEJANDO FLUJO ANTEROGRADO (REPARO UNO Y MEDIO VENTRICULAR)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '374100', 'CARDIORRAFIA SOD *' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '360300', 'ANGIOPLASTIA DE ARTERIA CORONARIA CON TORAX ABIERTO SOD *' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '974300', 'RETIRO DE SUTURAS DE TORAX ( RETIRO DE PUNTOS) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '427200', 'ESOFAGOTOMIA TRANSTORACICA CON MIOTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '427300', 'ESOFAGOTOMIA CON MIOTOMIA EN ESPIRAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '428300', 'CIERRE DE ESOFAGOSTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '317301', 'CIERRE DE FISTULA TRAQUEOESOFAGICA CON ANASTOMOSIS ESOFAGICA E INTERPOSICION DE TEJIDO MEDIASTINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '428201', 'ESOFAGORRAFIA POR CERVICOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '428202', 'ESOFAGORRAFIA POR TORACOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '543302', 'ESCISION DE LESION AMPLIA EN LA PARED ABDOMINAL CON PROTESIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '547401', 'EVENTRORRAFIA CON COLOCACION DE MALLA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '591910', 'EXPLORACION DE TEJIDO PERIVESICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '547200', 'CORRECCION DE ONFALOCELE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '547100', 'CORRECCION TOTAL DE EVISCERACION PRENATAL ( GASTROSQUISIS) SOD    (158)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '547300', 'CIERRE DE PIEL CON INCISIONES DE RELAJACION EN ONFALOCELE O GASTROSQUISIS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '599110', 'ESCISION DE TUMOR RETROPERITONEAL  CON DISECCION DE GRANDES VASOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '549201', 'EXTRACCION CUERPO EXTRAÑO INTRAPERITONEAL (O DIU PERDIDO), POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '502101', 'DRENAJE Y/O MARSUPIALIZACION DE LESION HEPATICA POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '391701', 'DERIVACION PORTO- CAVA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '391702', 'DERIVACION MESENTERICO- CAVA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '391703', 'DERIVACION ESPLENO-RENAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '512101', 'COLECISTECTOMIA POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '528300', 'HETEROTRASPLANTE DE PANCREAS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '446602', 'CIRUGIA ANTIRREFLUJO GASTROESOEFAGICO CON RECONSTRUCCION DEL ESFINTER ESOFAGICO INFERIOR POR VIA ABDOMINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '446601', 'CIRUGIA ANTIRREFLUJO GASTROESOFAGICO CON RECONSTRUCCION DEL ESFINTER ESOFAGICO POR VIA INFERIOR TRANSTORACICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '456201', 'DUODENECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '467903', 'ANASTOMOSIS ILEO-COLICA LATEROLATERAL POR AGANGLIOSIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '459301', 'ANASTOMOSIS DE ILEO A COLON TRANVERSO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '465101', 'CIERRE DE ESTOMA DE INTESTINO DELGADO POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '578301', 'FISTULECTOMIA RECTO-VESICAL O RECTO-VESICO-VAGINAL    (370)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '467801', 'CORRECCION DE ATRESIA DE INTESTINO CON PLASTIA PROXIMAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '467804', 'CORRECCION DE ATRESIA DE COLON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '591100', 'LIBERACION O LISIS DE ADHERENCIAS PERIVESICALES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '467802', 'CORRECCION DE ATRESIA DE DUODENO,YEYUNO E ILEON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '467803', 'CORRECCION DE ATRESIAS INTESTINALES MULTIPLES NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '488101', 'DRENAJE DE COLECCION RECTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '488102', 'DRENAJE DE COLECCION PERIRRECTAL, RETRORECTAL O PELVICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '483801', 'EXTRACCION DE CUERPO EXTRAÑO EN RECTO  POR VIA RECTAL ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '483802', 'EXTRACCION DE CUERPO EXTRAÑO EN RECTO  POR VIA ABDOMINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '963800', 'EXTRACCION DIGITAL O MANUAL DE HECES IMPACTADAS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '487301', 'FISTULECTOMIA RECTO-VAGINAL CON COLOSTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '487302', 'FISTULECTOMIA RECTO-VESICAL CON COLOSTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '487303', 'FISTULECTOMIA RECTO-URETRAL CON COLOSTOMIA.' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '487602', 'CORRECCION DE PROLAPSO POR RESECCION DE PROCIDENCIA RECTAL CON ANASTOMOSIS, VIA PERINEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '487904', 'CORRECCION DE LA ESTENOSIS RECTAL VIA SAGITAL POSTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '487701', 'DESCENSO RECTAL VIA SAGITAL POSTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '487702', 'DESCENSO RECTAL VIA  ANTERIOR Y POSTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '487703', 'DESCENSO RECTAL ABDOMINOPERINEAL POR AGANGLIOSIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '490100', 'DRENAJE DE ABSCESO ISQUIORRECTAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '499500', 'CONTROL DE HEMORRAGIA (POSOPERATORIA) DE ANO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '497504', 'ANOPLASTIA POR ESTENOSIS    (80)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '497503', 'ESFINTEROPLASTIA ANAL    (80)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '487903', 'CORRECCION DE ATRESIA RECTAL, VIA SAGITAL POSTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '497501', 'CONSTRUCCION DE ANO, POR AGENESIA CONGENITA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '497505', 'CORRECCION DE ANO IMPERFORADO Y FISTULA RECTO-VAGINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '497506', 'CORRECCION DE ANO IMPERFORADO Y FISTULA RECTO-VESICAL, VIA SAGITAL POSTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '497507', 'CORRECCION DE ANO IMPERFORADO Y FISTULA RECTO-VESICAL, VIA COMBINADA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '497508', 'CORRECCION DE ANO IMPERFORADO Y FISTULA RECTO-URETRAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '497301', 'FISTULECTOMIA ANO-VESTIBULAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '497303', 'FISTULECTOMIA ANO-VAGINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '497302', 'FISTULECTOMIA ANO-PERINEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862102', 'MARSUPIALIZACION DE QUISTE PILONIDAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862103', 'RESECCION QUISTE PILONIDAL ( CIERRE PARCIAL O ESCISION ABIERTA)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '550102', 'EXPLORACION DE RIÑON POR NEFROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '558201', 'CIERRE DE  NEFROSTOMIA O  PIELOSTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '551110', 'EXPLORACION DE PELVIS RENAL POR PIELOTOMIA VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '551140', 'EXTRACCION DE CALCULO CORALIFORME POR PIELOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '550104', 'DRENAJE DE COLECCION RENAL POR NEFROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '590400', 'INCISION Y DRENAJE DE COLECCION  PERIRENAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '541501', 'EXPLORACION DE ESPACIO RETROPERITONEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '553120', 'DIVERTICULECTOMIA U OBLITERACION DE DIVERTICULO DE CALIZ' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '558620', 'ANASTOMOSIS PIELO-URETERO-VESICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '558310', 'CIERRE DE FISTULA NEFROVISCERAL NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '549001', 'COLOCACION DE CATETER PARA DIALISIS PERITONEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '389500', 'CATETERIZACION VENOSA PARA DIALISIS RENAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '562101', 'EXPLORACION DE URETER POR URETEROTOMIA (VIA ABIERTA)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '564120', 'ESCISION DE LESION  URETERAL Y/O PARA URETERAL    (212)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '568300', 'CIERRE DE URETEROSTOMIA  SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '568410', 'CIERRE DE FISTULA URETERO-ENTERICA O URETEROVISCERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '571101', 'EXTRACCION DE CUERPO EXTRAÑO O  CALCULO  EN VEJIGA  POR CISTOTOMIA (VIA ABIERTA)    (155)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '571210', 'CISTOSTOMIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '575201', 'ENDOMETRECTOMIA DE VEJIGA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '579930', 'LIBERACION DE ADHERENCIAS EN VEJIGA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '577005', 'CISTECTOMIA TOTAL CON URETRECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '577150', 'ESCISION O REMOCION DE VEJIGA, URETRA Y TEJIDO GRASO EN MUJER    (361)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '578802', 'ANASTOMOSIS CISTOCOLICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '577120', 'EXENTERACION PELVICA MASCULINA  (CON RECTO)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '688101', 'EXENTERACION O EVISCERACION PELVICA  FEMENINA TOTAL O COMPLETA    (156)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '688200', 'EXENTERACION ANTERIOR: UTERO Y VEJIGA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '688300', 'EXENTERACION POSTERIOR: UTERO Y RECTO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '578701', 'AMPLIACION DE VEJIGA CON SEGMENTO AISLADO DE ILEON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '578702', 'AMPLIACION DE VEJIGA CON SEGMENTO DE COLON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '578704', 'ILEO-CECO-CISTOPLASTIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '597101', 'CISTOURETROPEXIA CON CABESTRILLO (SUSPENSION DEL MUSCULO ELEVADOR)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '597104', 'CISTOURETROPEXIA VAGINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '579301', 'CONTROL DE HEMORRAGIA (POSTQUIRURGICA) DE VEJIGA VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '578302', 'FISTULECTOMIA VESICO-SIGMOIDO-VAGINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '578450', 'FISTULECTOMIA URETRO-PERINEO-VESICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '578402', 'FISTULECTOMIA VESICO-VAGINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '578403', 'FISTULECTOMIA UTERO-VESICAL (VESICOUTERINA)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '591920', 'INCISION Y DRENAJE DE COLECCION EN TEJIDO PERIVESICAL Y ESPACIO DE RETZIUS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '578201', 'CIERRE DE CISTOSTOMIA [FISTULECTOMIA VESICO-CUTANEA]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '578202', 'CIERRE DE VESICOSTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '489500', 'CORRECCION DE CLOACA O DE EXTROFIA DE CLOACA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '580010', 'ESCISION O ESCISION DE TABIQUE URETRAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '583201', 'FULGURACION  DE LESIONES URETRALES POR VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '586101', 'DILATACION DE URETRA POR URETROTOMIA EXTERNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '586102', 'DILATACION DE URETRA POR URETROTOMIA  INTERNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '584302', 'CIERRE DE FISTULA URETRO-PERINEO-ESCROTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '584304', 'CIERRE DE FISTULA URETRO-VAGINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '584305', 'CIERRE DE FISTULA DE  NEOURETRA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '584301', 'CIERRE DE FISTULA URETRORECTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '584200', 'CIERRE DE URETROSTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '584401', 'ANASTOMOSIS DE URETRA-URETRA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '589110', 'DRENAJE DE GLANDULA BULBOURETRAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '589120', 'INCISION Y DRENAJE DE COLECCIONES PERIURETRALES O  URINOSOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '589200', 'ESCISION DE TEJIDO PERIURETRAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '583202', 'ESCISION DE VALVA CONGENITA DE URETRA, VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '600110', 'DRENAJE DE COLECCION EN PROSTATA VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '608101', 'DRENAJE DE COLECCION PERIPROSTATICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '608201', 'ESCISION DE LESION DE TEJIDO PERIPROSTATICO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '577130', 'ESCISION O REMOCION DE VEJIGA, PROSTATA, VESICULAS SEMINALES Y TEJIDO GRASO (CISTOPROSTATECTOMIA)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '609401', 'CONTROL DE HEMORRAGIA (POSTQUIRURGICA) DE PROSTATA VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '601400', 'BIOPSIA  ABIERTA DE VESICULAS SEMINALES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '631301', 'HIDROCELECTOMIA DE CORDON ESPERMATICO VIA INGUINAL    (159)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '609100', 'ASPIRACION PERCUTANEA DE PROSTATA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '619100', 'ASPIRACION PERCUTANEA DE TUNICA VAGINAL (HIDROCELE) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '619201', 'DRENAJE DE COLECCION  DE  TUNICA VAGINALIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '620100', 'DRENAJE POR INCISION  EN TESTICULO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '629100', 'ASPIRACION DE TESTICULO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '639300', 'INCISION Y DRENAJE DE CORDON ESPERMATICO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '610101', 'INCISION Y DRENAJE  DE ESCROTO Y TUNICA VAGINALIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '639100', 'ASPIRACION DE ESPERMATOCELE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '644300', 'CONSTRUCCION ( DE NOVO) DE PENE  SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '645100', 'CIRUGIA DE GENITALES AMBIGUOS  SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '614200', 'FISTULECTOMIA DEL ESCROTO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '613102', 'FULGURACION DE LESION ESCROTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '627100', 'IMPLANTE DE PROTESIS TESTICULAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '625201', 'IMPLANTACION DEL TESTICULO EN TEJIDOS VECINOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '625104', 'FIJACION TESTICULAR PROFILACTICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '619202', 'EXTRACCION DE CUERPO EXTRAÑO DEL ESCROTO POR INCISION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '629300', 'EXTRACCION DE CUERPO EXTRAÑO DEL TESTICULO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '639600', 'EXTRACCION DE CUERPO EXTRAÑO DE CORDON ESPERMATICO Y EPIDIDIMO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '637100', 'LIGADURA O SECCION DE CONDUCTO DEFERENTE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '637200', 'LIGADURA DE CORDON ESPERMATICO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '634000', 'EPIDIDIMECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '638300', 'EPIDIDIMOVASOSTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '632100', 'ESPERMATOCELECTOMIA  O RESECCION QUISTE DEL EPIDIDIMO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '639200', 'INCISION (EPIDIDIMOTOMIA)Y DRENAJE DEL EPIDIDIMO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '649100', 'CORTE DORSAL O LATERAL EN PREPUCIO SOD    (215)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '642100', 'FULGURACION O RESECCION DE LESIONES EN PENE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '640000', 'CIRCUNCISION SOD    (216)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '643100', 'AMPUTACION PARCIAL DEL PENE O PENECTOMIA PARCIAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '643200', 'AMPUTACION TOTAL DEL PENE O PENECTOMIA TOTAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '644910', 'ESCISION DE NODULOS DE ENFERMEDAD DE PEYRONIE  (23)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '649500', 'INSERCION O REEMPLAZO DE PROTESIS INTERNA DE PENE NO INFLABLE (RIGIDA Y SEMIRIGIDA) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '649700', 'INSERCION O SUSTITUCION DE PROTESIS DE PENE INFLABLE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '649804', 'CORRECCION DE ANGULACION PENEANA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '644200', 'LIBERACION DE CORDEE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '649805', 'INCISION Y DRENAJE DE FLEGMON PENEANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '649803', 'IRRIGACION, PUNCION O DRENAJE DE CUERPO CAVERNOSO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '649801', 'DERIVACION CUERPOCAVERNOSA-CUERPOESPONGIOSA O BULBO-CAVERNOSA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '649802', 'DERIVACION CUERPO-SAFENA O SAFENO-CAVERNOSA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '644930', 'INYECCION EN PLACAS DE FIBROSIS DE PENE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '850100', 'DRENAJE EN MAMA DE COLECCION POR MASTOTOMIA O MAMOTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '850201', 'EXTRACCION DE CUERPO EXTRAÑO DE MAMA POR MASTOTOMIA    (217)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '852002', 'ESCISION SELECTIVA DE CANAL GALACTOFORO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '852003', 'ESCISION EN BLOQUE DE CONDUCTOS GALACTOFOROS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '852100', 'RESECCION LOCAL DE LESION DE MAMA SOD    (218)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '852401', 'ESCISION DE PEZON ACCESORIO O SUPERNUMERARIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '854301', 'MASTECTOMIA SIMPLE CON ESCISION DE GANGLIOS LINFATICOS REGIONALES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '854501', 'ESCISION DE MAMA, MUSCULOS PECTORALES Y GANGLIO LINFATICO REGIONALES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '854701', 'ESCISION DE MAMA, MUSCULOS, GANGLIOS LINFATICOS (AXILARES,CLAVICULARES,SUPRACLAVICULARES,MAMARIOS INTERNOS Y MEDIASTINICOS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '854502', 'MASTECTOMIA RADICAL MODIFICADA UNILATERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '852300', 'MASTECTOMIA SUBTOTAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '652101', 'CISTECTOMIA DE OVARIO POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '655200', 'ESCISION DE OVARIO REMANENTE SOLITARIO O UNICO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '652910', 'CITOREDUCCION DE TUMOR DE OVARIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '665301', 'ESCISION DE TROMPA DE FALOPIO REMANENTE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '666110', 'ESCISION DE LESION CON SALPINGECTOMIA PARCIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '669130', 'ESCISION DE UN OVARIO (REMANENTE, SOLITARIO O UNICO) CON RESECCION DE TROMPA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '652901', 'LIBERACION O LISIS DE ADHERENCIAS (LEVES, MODERADAS O SEVERAS) DE OVARIO POR LAPAROTOMIA  (24)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '669901', 'LIBERACION O LISIS DE ADHERENCIAS DE OVARIO Y TROMPAS DE FALOPIO POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '691901', 'DRENAJE DE  COLECCION  DE LIGAMENTO ANCHO POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '692210', 'HISTEROPEXIA POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '692211', 'HISTEROPEXIA POR LAPAROTOMIA CON SIMPATECTOMIA PRESACRA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '692110', 'INTERPOSICION TIPO WATKINS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '682402', 'MIOMECTOMIA UTERINA Y ESCISION DE TUMOR FIBROIDE ( UNICO O MULTIPLE) VIA VAGINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '682401', 'MIOMECTOMIA UTERINA Y ESCISION DE TUMOR FIBROIDE (UNICO O MULTIPLE) POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '690101', 'LEGRADO UTERINO GINECOLOGICO DIAGNOSTICO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '690102', 'LEGRADO UTERINO GINECOLOGICO TERAPEUTICO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '674100', 'ESCISION DE MUÑON CERVICAL VIA VAGINAL O ABDOMINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '674200', 'ESCISION DE MUÑON CERVICAL CON REPARACION DEL PISO PELVICO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '674300', 'ESCISION DE MUÑON CERVICAL CON COLPORRAFIA ANTERIOR Y POSTERIOR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '674400', 'ESCISION DE MUÑON CERVICAL CON CORRECCION DE ENTEROCELE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '670100', 'DILATACION Y CURETAJE DE MUÑON CERVICAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '673401', 'CONIZACION  CON RADIOCIRUGIA (LETZ) BAJO COLPOSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '675100', 'CERCLAJE DE ISTMO UTERINO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '673101', 'ESCISION DE POLIPO EN CUELLO UTERINO (CERVIX) NCOC **' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '694101', 'HISTERORRAFIA POR LAPAROTOMIA    (220)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '682201', 'INCISION O ESCISION DE TABIQUE CONGENITO UTERINO POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '694910', 'CORRECION DE DESGARRO O LACERACION OBSTETRICA ANTIGUAS EN UTERO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '694920', 'HISTEROPLASTIA [OPERACION DE STRASMAN]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '676910', 'CORRECCION DE DESGARRO OBSTETRICO ANTIGUO DE CUELLO UTERINO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '684101', 'HISTERECTOMIA  TOTAL ABDOMINAL AMPLIADA  CON VAGINECTOMIA PARCIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '684010', 'HISTERECTOMIA  TOTAL, CON CERVICECTOMIA, REMOCION DE VEJIGA, TRANSPLANTE URETERAL Y/O RESECCION ABDOMINOPERINEAL DE COLON Y RECTO Y COLOSTOMIA O CUALQUIER COMBINACION ANTERIOR.' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '685130', 'HISTERECTOMIA VAGINAL CON REPARACION PLASTICA DE VAGINA Y COLPORRAFIA ANTERIOR Y POSTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '685110', 'HISTERECTOMIA VAGINAL CON COLPOURETROCISTOPEXIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '685120', 'HISTERECTOMIA VAGINAL CON REPARACION DE ENTEROCELE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '687000', 'HISTERECTOMIA RADICAL VAGINAL [ OPERACION DE SCHAUTA] SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '699120', 'IMPLANTACION INTRAUTERINA DE PLATINAS RADIOACTIVAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '701201', 'COLPOTOMIA CON EXPLORACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '701420', 'DRENAJE DE COLECCION  DE FONDO DE SACO (CUPULA VAGINAL)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '703200', 'ESCISION O ABLACION DE LESION O TEJIDO EN  FONDO DE SACO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '701202', 'COLPOTOMIA CON DRENAJE DE  COLECCION PELVICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '701300', 'LIBERACION-LISIS DE ADHERENCIAS INTRALUMINALES EN VAGINA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '710100', 'LISIS DE ADHERENCIAS EN VULVA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '703100', 'HIMENECTOMIA O HIMENOTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '707600', 'HIMENORRAFIA O HIMENOPLASTIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '568440', 'FISTULECTOMIA VESICO-URETERO-VAGINAL Y REIMPLANTE URETERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '707510', 'CIERRE DE FISTULA URETROVAGINAL O VESICO VAGINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '709100', 'EXTRACCION DE CUERPO EXTRAÑO EN VAGINA CON INCISION SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '707930', 'CORECCION DE SENO UROGENITAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '705110', 'COLPORRAFIA ANTERIOR CON PLASTIA O REPARACION DE URETROCELE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '705210', 'COLPORRAFIA POSTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '705301', 'COLPORRAFIA ANTERIOR Y POSTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '705303', 'COLPORRAFIA ANTERIOR Y POSTERIOR CON AMPUTACION DE CUELLO [MANCHESTER-FOTHERGILL]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '705302', 'COLPORRAFIA ANTERIOR Y POSTERIOR CON REPARACION DE ENTEROCELE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '707701', 'COLPOPEXIA POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '707702', 'COLPOPEXIA VIA VAGINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '707200', 'CORRECCION DE FISTULA COLOVAGINAL (CECOVAGINAL) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '707300', 'CORRECCION DE FISTULA RECTOVAGINAL Y/O PERINEAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '707400', 'CORRECCION DE OTRAS FISTULAS  VAGINOINTESTINALES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '717200', 'CORRECCION DE FISTULA DE VULVA O PERINE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '710921', 'INCISION Y DRENAJE DE COLECCION DE VULVA O DE GLANDULA DE SKENE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '712100', 'ASPIRACION CON AGUJA DE LAS GLANDULAS DE BARTHOLIN(QUISTE) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '712200', 'INCISION Y DRENAJE DE LA GLANDULA DE BARTHOLIN (QUISTE) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '718100', 'EXTRACCION DE CUERPO EXTRAÑO DE VULVA Y/O PERINE CON INCISION SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '717300', 'DRENAJE DE COLECCION VULVOPERINEAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '863103', 'RESECCION DE LESIONES CUTANEAS POR CAUTERIZACION, FULGURACION O CRIOTERAPIA EN AREA ESPECIAL, HASTA TRES LESIONES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '863104', 'RESECCION DE LESIONES CUTANEAS POR CAUTERIZACION, FULGURACION O CRIOTERAPIA EN AREA ESPECIAL, ENTRE TRES A DIEZ LESIONES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '863105', 'RESECCION DE LESIONES CUTANEAS POR CAUTERIZACION, FULGURACION O CRIOTERAPIA EN AREA ESPECIAL, MAS DE DIEZ LESIONES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '712300', 'MARZUPIALIZACION Y/O  DRENAJE EN LA GLANDULA DE  BARTHOLIN O DE SKENE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '714100', 'CLITORIDECTOMIA O AMPUTACION TOTAL DE CLITORIS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '714200', 'CLITORIDOTOMIA O ESCISION PARCIAL DE CLITORIS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '707920', 'CORRECCION DE LACERACION OBSTETRICA ANTIGUA EN VAGINA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '717910', 'CORRECCION DE LACERACION OBSTETRICA ANTIGUA EN  VULVA Y PERINE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '717930', 'PERINEOPLASTIA POR DESGARRO ANTIGUO DE PERINE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '735300', 'ASISTENCIA DEL PARTO NORMAL CON EPISIORRAFIA Y/O PERINEORRAFIA SOD    (27)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '735930', 'ASISTENCIA DEL PARTO ESPONTANEO GEMELAR O MULTIPLE    (27)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '735931', 'ASISTENCIA DEL PARTO INTERVENIDO GEMELAR O MULTIPLE    (27)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '721001', 'PARTO INSTRUMENTADO CON FORCEPS ESPATULAS DE VELASCO BAJOS   (27)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '721002', 'PARTO INSTRUMENTADO CON FORCEPS ESPATULAS DE VELASCO MEDIOS    (27)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '725100', 'EXTRACCION ( TOTAL O PARCIAL) INSTRUMENTADA EN PODALICA SOD    (27)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '732201', 'PARTO INTERVENIDO CON MANIOBRA DE VERSION FETAL INTERNA Y COMBINADA CON EXTRACION    (27)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '740100', 'CESAREA SEGMENTARIA TRANSPERITONEAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '740200', 'CESAREA CORPORAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '740300', 'CESAREA  EXTRAPERITONEAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '750101', 'LEGRADO UTERINO OBSTETRICO POSTPARTO O POSTABORTO POR  DILATACION Y CURETAJE  (89)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '750105', 'LEGRADO UTERINO OBSTETRICO POSTPARTO O POSTABORTO POR ASPIRACION AL VACIO    (89)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '751100', 'AMNIOCENTESIS  DIAGNOSTICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '751200', 'AMNIOCENTESIS TERAPEUTICA (NO DELIBERADAMENTE ABORTIVA) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '680100', 'HISTEROTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '659300', 'ESCISION DE EMBARAZO ECTOPICO OVARICO SIN OOFORECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '691920', 'ESCISION DE EMBARAZO ECTOPICO INTRALIGAMENTOSO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '753600', 'CORDOCENTESIS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '684001', 'HISTERECTOMIA  TOTAL ABDOMINAL CON REMOCION DE MOLA O FETO MUERTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776101', 'ESCISION TUMOR BENIGNO  DE CLAVICULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776104', 'ESCISION TUMOR BENIGNO DE  ESCAPULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776201', 'ESCISION TUMOR BENIGNO EN HUMERO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776202', 'ESCISION TUMOR BENIGNO EN HUMERO CON INJERTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776203', 'ESCISION TUMOR MALIGNO EN HUMERO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '796100', 'LAVADO Y DESBRIDAMIENTO DE FRACTURA ABIERTA DE HUMERO SOD  (98)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808011', 'DESBRIDAMIENTO, LAVADO Y LIMPIEZA DE ARTICULACION DE HOMBRO VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '834910', 'LIMPIEZA Y DESBRIDAMIENTO QUIRURGICOS DE MUSCULOS , TENDONES Y FASCIA EN  BRAZO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786101', 'EXTRACCION DE DISPOSITIVO IMPLANTADO EN  ESCAPULA, CLAVICULA O TORAX [COSTILLAS Y ESTERNON]   (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786201', 'EXTRACCION DE DISPOSITIVO IMPLANTADO  EN HUMERO    (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786202', 'EXTRACCION DE CUERPO EXTRAÑO  EN HUMERO POR VIA ABIERTA    (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '839901', 'EXTRACCION DE CUERPO EXTRAÑO EN TEJIDOS BLANDOS DE HOMBRO (MUSCULOS, TENDON O SINOVIAL)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '839902', 'EXTRACCION DE CUERPO EXTRAÑO EN TEJIDOS BLANDOS DE BRAZO (MUSCULOS, TENDON O SINOVIAL)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786102', 'EXTRACCION DE CUERPO EXTRAÑO  EN  ESCAPULA, CLAVICULA O TORAX [COSTILLAS Y ESTERNON] POR VIA ABIERTA    (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772104', 'OSTEOTOMIA DE ESCAPULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772101', 'OSTEOTOMIA DE CLAVICULA CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772201', 'OSTEOTOMIA DE HUMERO CON FIJACION INTERNA O EXTERNA  [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782104', 'ACORTAMIENTO DE HUMERO MEDIANTE RESECCION/OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778202', 'HEMI O DIAFISECTOMIA DE HUMERO    (94)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780101', 'INJERTO OSEO EN CLAVICULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '843100', 'REMODELACION [REVISION] [ RECONSTRUCCION] DEL MUÑON DE AMPUTACION DE HOMBRO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '843300', 'REMODELACION [REVISION] [ RECONSTRUCCION] DEL MUÑON DE AMPUTACION DE BRAZO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776102', 'ESCISION TUMOR MALIGNO  DE CLAVICULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776105', 'ESCISION TUMOR MALIGNO DE  ESCAPULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '798106', 'ESCAPULOPEXIA [TRATAMIENTO DE LA LUXACION CONGENITA DE ESCAPULA ALADA]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '818301', 'ACROMIOPLASTIA VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '787100', 'OSTEOCLASTIA DE  ESCAPULA , CLAVICULA O TORAX [COSTILLAS Y ESTERNON] SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792102', 'REDUCCION ABIERTA SIN FIJACION INTERNA FRACTURA DE  ESCAPULA O CLAVICULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792103', 'REDUCCION ABIERTA SIN FIJACION INTERNA FRACTURA CUELLO  Y GLENOIDES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '794101', 'REDUCCION CERRADA DE EPIFISIS SEPARADA EN HUMERO SIN FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '794102', 'REDUCCION CERRADA DE EPIFISIS SEPARADA EN HUMERO CON FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '785100', 'FIJACION INTERNA SIN REDUCCION DE FRACTURA EN ESCAPULA, CLAVICULA O TORAX(COSTILLAS Y ESTERNON) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793101', 'REDUCCION ABIERTA DE FRACTURA CON FIJACION INTERNA (DISPOSITIVOS DE FIJACION U OSTEOSINTESIS) DE CLAVICULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793202', 'REDUCCION ABIERTA DE FRACTURA  DE TUBEROSIDAD PROXIMAL DE HUMERO CON FIJACION INTERNA (DISPOSITIVOS DE FIJACION U OSTEOSINTESIS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793203', 'REDUCCION ABIERTA DE FRACTURA  CONMINUTA DE TERCIO PROXIMAL HUMERO CON FIJACION INTERNA (DISPOSITIVOS DE FIJACION [OSTEOSINTESIS])' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '795101', 'REDUCCION ABIERTA SIN FIJACION INTERNA DE EPIFISIS SEPARADA DE HUMERO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '795102', 'REDUCCION ABIERTA CON FIJACION INTERNA DE EPIFISIS SEPARADA DE HUMERO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792200', 'REDUCCION ABIERTA DE FRACTURA SIN FIJACION INTERNA DE HUMERO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '785200', 'FIJACION INTERNA SIN REDUCCION DE FRACTURA DE HUMERO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793210', 'REDUCCION ABIERTA DE FRACTURA EN DIAFISIS DE HUMERO CON FIJACION INTERNA (DISPOSITIVOS DE FIJACION U OSTEOSINTESIS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781201', 'APLICACION DE TUTORES EXTERNOS EN HUMERO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793204', 'REDUCCION ABIERTA DE FRACTURA SUPRACONDILEA DE  HUMERO CON FIJACION INTERNA (DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793205', 'REDUCCION ABIERTA DE FRACTURA SUPRACONDILEA E INTERCONDILEA DE HUMERO CON FIJACION INTERNA (DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793201', 'REDUCCION ABIERTA DE FRACTURA SUBCAPITAL DE HUMERO CON FIJACION INTERNA (DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793206', 'REDUCCION ABIERTA DE FRACTURA DE EPICONDILO O EPITROCLEA DE HUMERO CON FIJACION INTERNA (DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781202', 'COLOCACION DE DISPOSITIVO DE FIJACION EN CODO    (95)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '840900', 'AMPUTACION INTERTORACO ESCAPULAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '840001', 'AMPUTACION  CON COLGAJO CERRADO DE MIEMBRO SUPERIOR SITIO NO ESPECIFICADO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '840800', 'DESARTICULACION DE HOMBRO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '842401', 'REIMPLANTE DE MIEMBRO SUPERIOR A NIVEL DEL BRAZO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776301', 'ESCISION TUMOR BENIGNO EN RADIO O CUBITO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786301', 'EXTRACCION DE DISPOSITIVO IMPLANTADO  EN RADIO O CUBITO  (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786302', 'EXTRACCION DE CUERPO EXTRAÑO  EN RADIO O CUBITO POR VIA ABIERTA    (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '800201', 'EXTRACCION DE DISPOSITIVO IMPLANTADO EN CODO POR ARTROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '839903', 'EXTRACCION DE CUERPO EXTRAÑO EN TEJIDOS BLANDOS DE ANTEBRAZO (MUSCULO, TENDON, SINOVIAL)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '796201', 'LAVADO Y DESBRIDAMIENTO DE FRACTURA ABIERTA DE CUBITO O RADIO  (98)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808031', 'DESBRIDAMIENTO, LAVADO Y LIMPIEZA DE ARTICULACION DE MUÑECA O PUÑO VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808021', 'DESBRIDAMIENTO, LAVADO Y LIMPIEZA DE ARTICULACION DE CODO VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '834920', 'LIMPIEZA Y DESBRIDAMIENTO QUIRURGICOS  DE MUSCULOS , TENDONES Y FASCIA EN ANTEBRAZO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772301', 'OSTEOTOMIA  EN RADIO O CUBITO CON FIJACION INTERNA O EXTERNA [DISPOSITIVOS DE FIJACION  U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772302', 'OSTEOTOMIA EN RADIO Y CUBITO CON FIJACION (INTERNA O EXTERNA) [DISPOSITIVOS DE FIJACION  U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782241', 'ACORTAMIENTO DE CUBITO  O RADIO MEDIANTE RESECCION/OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782243', 'ACORTAMIENTO DE RADIO Y CUBITO MEDIANTE RESECCION/OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780300', 'INJERTO  OSEO EN CUBITO O RADIO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782211', 'EPIFISIODESIS ABIERTA DE CUBITO O RADIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782213', 'EPIFISIODESIS ABIERTA DE CUBITO Y RADIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782221', 'EPIFISIODESIS PERCUTANEA DE  CUBITO O RADIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782223', 'EPIFISIODESIS PERCUTANEA DE RADIO Y  CUBITO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '843200', 'REMODELACION [REVISION] [ RECONSTRUCCION] DEL MUÑON DE AMPUTACION DE ANTEBRAZO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776302', 'ESCISION TUMOR MALIGNO EN RADIO O CUBITO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '799202', 'REDUCCION CERRADA DE FRACTURA DE CODO NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '794201', 'REDUCCION CERRADA DE EPIFISIS SEPARADA EN CUBITO O RADIO SIN FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '794202', 'REDUCCION CERRADA DE EPIFISIS SEPARADA  CUBITO O RADIO CON FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '794203', 'REDUCCION CERRADA DE EPIFISIS SEPARADA EN RADIO Y CUBITO SIN FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '794204', 'REDUCCION CERRADA DE EPIFISIS SEPARADA EN RADIO Y CUBITO CON FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '799201', 'REDUCCCION CERRADA DE LUXOFRACTURA RADIOCUBITAL (MONTEGGIA-GALLEAZI)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '798301', 'REDUCCION ABIERTA  DE LUXACION RADIOCUBITAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '785300', 'FIJACION INTERNA SIN REDUCCION DE FRACTURA DE RADIO O CUBITO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792301', 'REDUCCION ABIERTA FRACTURA SIN FIJACION INTERNA DE RADIO O CUBITO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793301', 'REDUCCION ABIERTA DE FRACTURA EN DIAFISIS DE CUBITO O RADIO CON FIJACION INTERNA (DISPOSITIVOS DE FIJACION [OSTEOSINTESIS])' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793304', 'REDUCCION ABIERTA DE FRACTURA EN  SEGMENTO PROXIMAL DE RADIO(CUPULA RADIAL) CON FIJACION INTERNA (DISPOSITIVOS DE FIJACION [OSTEOSINTESIS])' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '795201', 'REDUCCION ABIERTA DE EPIFISIS SEPARADA DE RADIO O CUBITO SIN FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '795202', 'REDUCCION ABIERTA  DE EPIFISIS SEPARADA DE RADIO O CUBITO CON FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792302', 'REDUCCION ABIERTA DE FRACTURA SIN FIJACION INTERNA DE RADIO Y CUBITO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793305', 'REDUCCION ABIERTA DE FRACTURA EN SEGMENTO PROXIMAL DE RADIO Y CUBITO CON FIJACION INTERNA (DISPOSITIVOS DE FIJACION [OSTEOSINTESIS])' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793306', 'REDUCCION ABIERTA DE FRACTURA EN SEGMENTO DISTAL DE RADIO Y CUBITO CON FIJACION INTERNA (DISPOSITIVOS DE FIJACION [OSTEOSINTESIS])' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793307', 'REDUCCION ABIERTA DE FRACTURA EN DIAFISIS DE CUBITO Y RADIO CON FIJACION INTERNA (DISPOSITIVOS DE FIJACION [OSTEOSINTESIS])' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '795203', 'REDUCCION ABIERTA  DE EPIFISIS SEPARADA DE RADIO Y CUBITO SIN FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '795204', 'REDUCCION ABIERTA  DE EPIFISIS SEPARADA DE RADIO Y CUBITO CON FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793303', 'REDUCCION ABIERTA DE FRACTURA EN SEGMENTO DISTAL DE CUBITO O RADIO(COLLES, OTROS) CON FIJACION INTERNA (DISPOSITIVOS DE FIJACION [OSTEOSINTESIS])' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '799203', 'REDUCCION ABIERTA FRACTURA O LUXOFRACTURA CODO SIN FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '799204', 'REDUCCION ABIERTA FRACTURA O LUXOFRACTURA CODO CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793302', 'REDUCCION ABIERTA DE FRACTURA EN SEGMENTO PROXIMAL DE CUBITO O DE OLECRANON CON FIJACION INTERNA (DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '791301', 'REDUCCION CERRADA DE FRACTURA  DE CUBITO O RADIO (COLLES, OTROS) CON FIJACION PERCUTANEA CON PINES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781301', 'APLICACION TUTOR EXTERNO EN RADIO O CUBITO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781302', 'APLICACION DE TUTORES EXTERNOS EN RADIO Y CUBITO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781304', 'APLICACION DE TUTORES EXTERNOS EN  PUÑO O MUÑECA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781401', 'APLICACION TUTOR EXTERNO EN MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '840500', 'AMPUTACION A TRAVES DE ANTEBRAZO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '840600', 'DESARTICULACION DE CODO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '840400', 'DESARTICULACION DE MUÑECA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '842301', 'REIMPLANTE DEL MIEMBRO SUPERIOR A NIVEL DEL ANTEBRAZO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776920', 'ESCISION DE TUMOR BENIGNO  EN  HUESOS PELVIANOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '796903', 'LAVADO Y DESBRIDAMIENTO DE FRACTURA EXPUESTA DE PELVIS    (98)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808051', 'DESBRIDAMIENTO, LAVADO Y LIMPIEZA DE ARTICULACION DE PELVIS VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786921', 'EXTRACCION DE CUERPO EXTRAÑO  EN HUESOS PELVIANOS, POR VIA ABIERTA   (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '839906', 'EXTRACCION DE CUERPO EXTRAÑO EN TEJIDOS BLANDOS DE PELVIS (MUSCULO, TENDON O SINOVIAL)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786920', 'EXTRACCION DE DISPOSITIVO IMPLANTADO  EN HUESOS PELVIANOS    (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772920', 'OSTEOTOMIAS SIMPLES EN PELVIS  [PEMBERTON-SALTER- CHIARI- DEGA]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772921', 'OSTEOTOMIAS COMPLEJAS EN PELVIS  CON FIJACION [GANZ-DOBLE- TRIPLE]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772502', 'OSTEOTOMIA SUPRA E INTERCONDILEA DE FEMUR CON FIJACION INTERNA O EXTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772501', 'OSTEOTOMIA FEMORAL DIAFISIARIA CON FIJACION INTERNA O EXTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772520', 'OSTEOTOMIA CON  DESCENSO DEL TROCANTER MAYOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780920', 'INJERTO OSEO EN PELVIS O CADERA NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776921', 'ESCISION DE TUMOR MALIGNO  EN  HUESOS PELVIANOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '843900', 'REMODELACION [REVISION] [ RECONSTRUCCION] DEL MUÑON DE AMPUTACION EN PELVIS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '799500', 'REDUCCION DE FRACTURAS INTRAARTICULARES Y LUXOFRACTURAS EN CADERA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793910', 'REDUCCION ABIERTA DE FRACTURA DEL ILIACO  CON FIJACION INTERNA (DISPOSITIVOS DE FIJACION U OSTEOSINTESIS)  (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793920', 'REDUCCION ABIERTA DE FRACTURA EN PELVIS [ ACETABULO, REBORDE ANTERIOR O POSTERIOR] CON  FIJACION INTERNA (DISPOSITIVOS DE FIJACION U OSTEOSINTESIS)    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793921', 'REDUCCION ABIERTA DE FRACTURA COMPLEJA  EN PELVIS [ACETABULO, REBORDE ANTERIOR, POSTERIOR Y SUPERIOR] CON  FIJACION INTERNA (DISPOSITIVOS DE FIJACION U OSTEOSINTESIS)     (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781920', 'APLICACION DE TUTORES EXTERNOS EN PELVIS, POR VIA ANTERIOR O POSTERIOR    (95)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793911', 'REDUCCION ABIERTA DE FRACTURA EN  RAMAS PUBIS CON  FIJACION INTERNA (DISPOSITIVOS DE FIJACION U OSTEOSINTESIS)     (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793912', 'REDUCCION ABIERTA DE FRACTURA EN SINFISIS PUBICA CON  FIJACION INTERNA (DISPOSITIVOS DE FIJACION U OSTEOSINTESIS)    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778923', 'HEMIPELVECTOMIA   (94)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778922', 'HEMI-HEMIPELVECTOMIA    (94)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '841800', 'DESARTICULACION DE PELVIS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776501', 'ESCISION TUMOR BENIGNO EN FEMUR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776601', 'ESCISION TUMOR BENIGNO EN ROTULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '796500', 'LAVADO Y DESBRIDAMIENTO DE FRACTURA ABIERTA DE  FEMUR SOD   (98)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '796902', 'LAVADO Y DESBRIDAMIENTO  DE FRACTURA EXPUESTA DE ROTULA    (98)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808061', 'DESBRIDAMIENTO, LAVADO Y LIMPIEZA DE RODILLA VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '834930', 'LIMPIEZA Y DESBRIDAMIENTO QUIRURGICOS  DE MUSCULOS , TENDONES Y FASCIA EN MUSLO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786502', 'EXTRACCION DE CUERPO EXTRAÑO  EN  FEMUR, POR VIA ABIERTA    (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786602', 'EXTRACCION DE CUERPO EXTRAÑO EN ROTULA, VIA ABIERTA    (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786501', 'EXTRACCION DE DISPOSITIVO IMPLANTADO EN FEMUR    (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786601', 'EXTRACCION DE DISPOSITIVO IMPLANTADO  EN  ROTULA     (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772503', 'OSTEOTOMIA SUPRACONDILEA O INTERCONDILEA DE FEMUR, CON FIJACION INTERNA O EXTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782404', 'ACORTAMIENTO DE FEMUR MEDIANTE RESECCION/OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772504', 'OSTEOTOMIA MULTIPLE DE FEMUR, CON FIJACION (INTERNA O EXTERNA) [DISPOSITIVOS DE FIJACION  U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772505', 'OSTEOTOMIA VALGUIZANTE O VARIZANTE DE CUELLO DE FEMUR CON FIJACION INTERNA O EXTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778501', 'HEMIDIAFISECTOMIA EN FEMUR   (94)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772600', 'OSTEOTOMIA  EN ROTULA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776602', 'ESCISION TUMOR MALIGNO EN ROTULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '843600', 'REMODELACION [REVISION] [ RECONSTRUCCION] DEL MUÑON DE AMPUTACION DE MUSLO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776502', 'ESCISION TUMOR BENIGNO EN FEMUR CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776503', 'ESCISION TUMOR MALIGNO EN FEMUR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783501', 'ALARGAMIENTO DE FEMUR POR INJERTO SIN DISPOSITIVOS INTERNOS DE FIJACION Y OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783503', 'ALARGAMIENTO DE FEMUR POR TECNICA DE DISTRACCION  SIN CORTICOTOMIA/OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783502', 'ALARGAMIENTO DE FEMUR POR INJERTO CON DISPOSITIVOS INTERNOS DE FIJACION Y OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783504', 'ALARGAMIENTO DE FEMUR POR TECNICA DE DISTRACCION CON CORTICOTOMIA/OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '839907', 'EXTRACCION DE CUERPO EXTRAÑO EN TEJIDOS BLANDOS DE  MUSLO (MUSCULO, TENDON O SINOVIAL)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '791501', 'REDUCCION CERRADA CON FIJACION INTERNA DE CUELLO DE FEMUR O INTERTROCANTERICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '794501', 'REDUCCION CERRADA DE EPIFISIS SEPARADA DE FEMUR SIN FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '794502', 'REDUCCION CERRADA DE EPIFISIS SEPARADA DE FEMUR CON FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '785600', 'FIJACION INTERNA SIN REDUCCION DE FRACTURA DE ROTULA  SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '791600', 'REDUCCION CERRADA DE FRACTURA CON FIJACION INTERNA DE ROTULA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792600', 'REDUCCION ABIERTA DE FRACTURA DE ROTULA SIN FIJACION INTERNA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793600', 'REDUCCION ABIERTA DE FRACTURA EN ROTULA CON FIJACION INTERNA(DISPOSITIVOS DE FIJACION U OSTEOSINTESIS] SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792500', 'REDUCCION ABIERTA DE FRACTURA  DE FEMUR SIN FIJACION INTERNA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '795501', 'REDUCCION ABIERTA SIN FIJACION INTERNA DE EPIFISIS SEPARADA DE FEMUR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '795502', 'REDUCCION ABIERTA CON FIJACION INTERNA DE EPIFISIS SEPARADA DE FEMUR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793502', 'REDUCCION ABIERTA DE FRACTURA EN DIAFISIS DE FEMUR CON FIJACION INTERNA (DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '785500', 'FIJACION INTERNA SIN REDUCCION DE FRACTURA DE FEMUR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793501', 'REDUCCION ABIERTA DE FRACTURA EN FEMUR (CUELLO, INTERTROCANTERICA, SUPRACONDILEA) CON FIJACION INTERNA (DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781502', 'APLICACION DE TUTORES EXTERNOS EN MUSLO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781601', 'APLICACION TUTOR EXTERNO RODILLA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781503', 'COLOCACION QUIRURGICA DE DISPOSITIVO PARA TRACCION ESQUELETICA EN MUSLO (TRANSCONDILEA)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '841700', 'AMPUTACION POR ENCIMA DE RODILLA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '841600', 'DESARTICULACION DE RODILLA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776701', 'ESCISION TUMOR BENIGNO EN TIBIA O PERONE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776702', 'ESCISION TUMOR MALIGNO EN TIBIA O PERONE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776901', 'ESCISION DE TUMOR BENIGNO  EN FALANGES DE PIE O DE MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '796600', 'LAVADO Y DESBRIDAMIENTO DE FRACTURA ABIERTA DE TIBIA O  PERONE SOD  (98)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '796700', 'LAVADO Y DESBRIDAMIENTO DE FRACTURA ABIERTA DE TARSIANOS O  METATARSIANOS SOD    (98)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '796800', 'LAVADO Y DESBRIDAMIENTO DE FRACTURA ABIERTA DE DEDOS DE PIE SOD     (98)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808071', 'DESBRIDAMIENTO, LAVADO Y LIMPIEZA DE TOBILLO VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808081', 'DESBRIDAMIENTO, LAVADO Y LIMPIEZA DE ARTICULACION DE PIE Y/O ARTEJOS VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '834940', 'LIMPIEZA Y DESBRIDAMIENTO QUIRURGICOS DE  MUSCULOS , TENDONES Y FASCIA EN PIERNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '834950', 'LIMPIEZA Y DESBRIDAMIENTO QUIRURGICOS DE  MUSCULOS , TENDONES Y FASCIA EN PIE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786702', 'EXTRACCION DE CUERPO EXTRAÑO EN TIBIA O PERONE, VIA ABIERTA   (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786802', 'EXTRACCION DE CUERPO EXTRAÑO EN TARSIANOS O METATARSIANOS (UNO O MAS), VIA ABIERTA    (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786902', 'EXTRACCION DE CUERPO EXTRAÑO EN FALANGES (UNO O MAS) DE MANO, VIA ABIERTA    (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786701', 'EXTRACCION DE DISPOSITIVO IMPLANTADO  EN  TIBIA O PERONE    (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '839908', 'EXTRACCION DE CUERPO EXTRAÑO EN TEJIDOS BLANDOS DE PIERNA (MUSCULO, TENDON, SINOVIAL)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '800801', 'EXTRACCION DE DISPOSITIVO IMPLANTADO  EN PIE Y DEDOS DEL PIE POR ARTROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786801', 'EXTRACCION DE DISPOSITIVO IMPLANTADO EN TARSIANOS O METATARSIANOS (UNO O MAS)    (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786901', 'EXTRACCION DE DISPOSITIVO IMPLANTADO EN FALANGES (UNO O MAS) DE MANO    (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786910', 'EXTRACCION DE DISPOSITIVO IMPLANTADO  EN FALANGES (UNO O MAS) DE PIE    (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772701', 'OSTEOTOMIA DE TIBIA CON FIJACION INTERNA O EXTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782541', 'ACORTAMIENTO DE TIBIA O PERONE MEDIANTE RESECCION/OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782543', 'ACORTAMIENTO DE TIBIA Y PERONE MEDIANTE RESECCION/OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772702', 'OSTEOTOMIA DE PERONE CON FIJACION INTERNA O EXTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772801', 'OSTEOTOMIA DE HUESO DEL TARSO O METATARSO (UNO O MAS HUESOS) CON FIJACION INTERNA O EXTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]   (93)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772910', 'OSTEOTOMIA EN FALANGES DEL PIE (EXCEPTO GRUESO ARTEJO) (UNO O MAS HUESOS) NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782641', 'ACORTAMIENTO DE TARSIANOS  O METATARSIANOS MEDIANTE RESECCION/OSTEOTOMIA (UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772802', 'OSTEOTOMIA DE HUESO DEL TARSO Y METATARSO (UNO O MAS HUESOS) CON FIJACION INTERNA O EXTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]]    (93)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772911', 'OSTEOTOMIA EN FALANGES DEL PIE (UNO O MAS HUESOS) (EXCEPTO GRUESO ARTEJO) CON FIJACION INTERNA O EXTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782781', 'ACORTAMIENTO DE FALANGES DE PIE MEDIANTE RESECCION/ OSTEOTOMIA (UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '841001', 'AMPUTACION  CON COLGAJO CERRADO DE MIEMBRO INFERIOR SITIO NO ESPECIFICADO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '841500', 'AMPUTACION O DESARTICULACION DE PIERNA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '841200', 'AMPUTACION O DESARTICULACION DE PIE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '841300', 'DESARTICULACION DE TOBILLO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '841400', 'AMPUTACION DE TOBILLO A TRAVES DEL MALEOLO DE TIBIA Y PERONE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '841100', 'AMPUTACION  O DESARTICULACION  DEDOS DEL PIE (UNO O MAS) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '843700', 'REMODELACION [REVISION] [ RECONSTRUCCION] DEL MUÑON DE AMPUTACION DE LA PIERNA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '843800', 'REMODELACION [REVISION] [ RECONSTRUCCION] DEL MUÑON DE AMPUTACION DEL PIE Y/O ARTEJOS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782511', 'EPIFISIODESIS ABIERTA DE TIBIA O PERONE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782521', 'EPIFISIODESIS PERCUTANEA  DE TIBIA O PERONE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782513', 'EPIFISIODESIS ABIERTA DE TIBIA Y PERONE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778703', 'HEMIDIAFISECTOMIA EN TIBIA Y PERONE    (94)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779802', 'ASTRAGALECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780600', 'INJERTO  OSEO EN ROTULA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780800', 'INJERTO OSEO EN HUESOS TARSIANOS O  METATARSIANOS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780902', 'INJERTO OSEO EN FALANGES DEL PIE  (UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '775103', 'CORRECCION DE HALLUX VALGUS CON BUNIECTOMIA SIMPLE Y CAPSULOPLASTIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '775101', 'CORRECCION DE HALLUX VALGUS CON OSTEOTOMIA PROXIMAL O DISTAL METATARSIANA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '775102', 'CORRECCION DE HALLUX VALGUS CON OSTEOTOMIA DISTAL Y PROXIMAL METATARSIANA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '804802', 'CORRECCION DE VARO METATARSIANO O PIE ADUCTO [HEYMAN-HERNDON-STRONG]  (99)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '849001', 'CORRECCION  DE MALFORMACION CONGENITA DE PIE CON LIBERACION DE PARTES BLANDAS POSTERIORES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '849002', 'CORRECCION  DE MALFORMACION CONGENITA DE PIE CON LIBERACION DE PARTES BLANDAS POSTERIORES Y MEDIALES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '849003', 'CORRECCION DE MALFORMACION CONGENITA DE PIE CON LIBERACION DE PARTES BLANDAS Y PARTES OSEAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783701', 'ALARGAMIENTO DE TIBIA  POR INJERTO  SIN DISPOSITIVOS INTERNOS DE FIJACION U OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783703', 'ALARGAMIENTO DE  PERONE POR INJERTO  SIN DISPOSITIVOS INTERNOS DE FIJACION U OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783705', 'ALARGAMIENTO DE TIBIA  POR TECNICA DE DISTRACCION  SIN CORTICOTOMIA/OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783707', 'ALARGAMIENTO DE PERONE POR TECNICA DE DISTRACCION SIN CORTICOTOMIA/OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783702', 'ALARGAMIENTO DE TIBIA  POR INJERTO CON DISPOSITIVOS INTERNOS DE FIJACION U OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783704', 'ALARGAMIENTO DE  PERONE POR INJERTO CON  DISPOSITIVOS INTERNOS DE FIJACION U OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783706', 'ALARGAMIENTO DE TIBIA  POR TECNICA DE DISTRACCION CON CORTICOTOMIA/OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783708', 'ALARGAMIENTO DE PERONE POR TECNICA DE DISTRACCION CON CORTICOTOMIA/OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '842700', 'REIMPLANTE DE PIERNA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '842600', 'REIMPLANTE DE PIE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '794601', 'REDUCCION CERRADA DE EPIFISIS SEPARADA DE TIBIA O PERONE SIN FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '794602', 'REDUCCION CERRADA DE EPIFISIS SEPARADA DE TIBIA O PERONE CON FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '794603', 'REDUCCION CERRADA DE EPIFISIS SEPARADA DE TIBIA Y  PERONE SIN FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '794604', 'REDUCCION CERRADA DE EPIFISIS SEPARADA DE TIBIA Y PERONE CON FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '799702', 'REDUCCION CERRADA DE LUXOFRACTURA DE CUELLO DE PIE O TOBILLO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797701', 'REDUCCION CERRADA DE LUXACION TRAUMATICA TOBILLO (CUELLO DE PIE)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797801', 'REDUCCION CERRADA DE LUXACION TARSO-METARSIANOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797802', 'REDUCCION CERRADA DE LUXACION TARSO-METARSIANOS  CON FIJACION PERCUTANEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797803', 'REDUCCION CERRADA DE LUXACIONES METATARSO-FALANGICAS O INTERFALANGICAS EN PIE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793801', 'REDUCCION ABIERTA DE FRACTURA DE TARSO CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '785700', 'FIJACION INTERNA SIN REDUCCION DE FRACTURA DE TIBIA O PERONE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792701', 'REDUCCION ABIERTA, SIN FIJACION INTERNA, DE FRACTURA DE TIBIA O PERONE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793706', 'REDUCCION ABIERTA DE FRACTURA EN PILON CON FIJACION INTERNA Y EXTERNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793701', 'REDUCCION ABIERTA DE FRACTURA EN PERONE CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '795604', 'REDUCCION ABIERTA DE EPIFISIS SEPARADA DE TIBIA Y PERONE CON FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793702', 'REDUCCION ABIERTA DE FRACTURA EN TIBIA CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793704', 'REDUCCION ABIERTA DE FRACTURA EN PLATILLOS TIBIALES Y EXTENSION DISFISIARIA CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]] SIN INJERTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '795601', 'REDUCCION ABIERTA DE EPIFISIS SEPARADA DE TIBIA O PERONE  SIN  FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '795602', 'REDUCCION ABIERTA DE EPIFISIS SEPARADA DE  TIBIA O PERONE CON FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '795603', 'REDUCCION ABIERTA DE EPIFISIS SEPARADA DE TIBIA Y PERONE SIN FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793705', 'REDUCCION ABIERTA DE FRACTURA EN PLATILLOS TIBIALES O PLAFONT CON FIJACION INTERNA E INJERTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793802', 'REDUCCION ABIERTA DE FRACTURA DE METATARSIANOS  (UNO O MAS) CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '798801', 'REDUCCION ABIERTA DE LUXACION TARSO-METARSIANOS (UNO O MAS)  CON DISPOSITIVO DE FIJACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '799801', 'REDUCCION ABIERTA DE LUXOFRACTURA SIN FIJACION DE HUESOS DEL TARSO O METATARSO (UNO O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '799802', 'REDUCCION ABIERTA DE LUXOFRACTURA CON FIJACION DE HUESOS DEL TARSO O METATARSO (UNO O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792702', 'REDUCCION ABIERTA,  SIN FIJACION INTERNA,  DE FRACTURA DE  PILON Y MALEOLO LATERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792703', 'REDUCCION ABIERTA,  SIN FIJACION INTERNA,  DE FRACTURA DE PILON Y MALEOLO INTERNO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792710', 'REDUCCION ABIERTA,  SIN FIJACION INTERNA,  DE FRACTURA BIMALEOLAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '799703', 'REDUCCION ABIERTA CON FIJACION  DE LUXO FRACTURA O FRACTURA (UNI O BIMALEOLAR) DE TOBILLO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '799704', 'REDUCCION ABIERTA CON FIJACION  DE FRACTURA O LUXO FRACTURA TRIMALEOLAR DE TOBILLO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781701', 'APLICACION DE TUTORES EXTERNOS EN TIBIA O PERONE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781702', 'APLICACION DE TUTORES EXTERNOS EN TIBIA Y PERONE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781703', 'APLICACION TUTOR EXTERNO EN CUELLO DE PIE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781801', 'APLICACION TUTOR EXTERNO PIE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781704', 'COLOCACION DE DISPOSITIVO EXTERNO DE FIJACION TRANSTIBIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781802', 'COLOCACION DE DISPOSITIVO EXTERNO EN PIE (CALCANEO)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781901', 'APLICACION DE DISPOSITIVO EXTERNO DE FIJACION DE FALANGES  DE MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '781902', 'APLICACION DE DISPOSITIVO EXTERNO DE FIJACION DE FALANGES DE PIE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '785800', 'FIJACION INTERNA SIN REDUCCION DE FRACTURA DE TARSIANOS O METATARSIANOS  SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792801', 'REDUCCION ABIERTA FRACTURA SIN FIJACION INTERNA, DE HUESOS DEL TARSO O METATARSO (UNO O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793804', 'REDUCCION ABIERTA DE FRACTURA CALCANEO CON FIJACION INTERNA Y ARTRODESIS SUBASTRAGALINA    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793803', 'REDUCCION ABIERTA FRACTURA DE TARSO Y METATARSO  (UNO O MAS HUESOS) CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793902', 'REDUCCION ABIERTA DE FRACTURA EN FALANGES DE PIE (UNA O MAS) CON FIJACION INTERNA    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '839909', 'EXTRACCION DE CUERPO EXTRAÑO EN TEJIDOS BLANDOS DE PIE (MUSCULO, TENDON O  SINOVIAL)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792902', 'REDUCCION ABIERTA DE FRACTURA SIN FIJACION INTERNA DE FALANGES DE PIE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '787800', 'OSTEOCLASTIA DE TARSIANOS O METATARSIANOS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '770930', 'DRENAJE, CURETAJE, SECUESTRECTOMIA DE COLUMNA VERTEBRAL, VIA ANTERIOR    (91)  (92)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '770931', 'DRENAJE, CURETAJE, SECUESTRECTOMIA, DE COLUMNA VERTEBRAL VIA POSTERIOR O POSTEROLATERAL    (91)  (92)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '796905', 'LAVADO Y DESBRIDAMIENTO DE FRACTURA ABIERTA DE RAQUIS O COLUMNA  (98)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786930', 'EXTRACCION DE DISPOSITIVO IMPLANTADO EN COLUMNA VERTEBRAL, POR VIA ANTERIOR  (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786935', 'EXTRACCION DE DISPOSITIVO IMPLANTADO  EN COLUMNA VERTEBRAL, VIA POSTERIOR     (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786931', 'EXTRACCION DE CUERPO EXTRAÑO  EN COLUMNA VERTEBRAL, VIA ANTERIOR        (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786936', 'EXTRACCION DE CUERPO EXTRAÑO  EN COLUMNA VERTEBRAL, VIA POSTERIOR     (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772103', 'OSTEOTOMIA DE COSTILLA (UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772105', 'OSTEOTOMIA DE ESTERNON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797901', 'REDUCCION CERRADA DE LUXACION DE COLUMNA TORACICA O LUMBAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797902', 'REDUCCION CERRADA DE LUXACION DE SACRO Y COCCIX' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792101', 'REDUCCION ABIERTA SIN FIJACION INTERNA DE FRACTURA DE UNA O MAS COSTILLAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793103', 'REDUCCION ABIERTA DE FRACTURA CON FIJACION INTERNA DE COSTILLA O ESTERNON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792936', 'REDUCCION ABIERTA FRACTURA SIN FIJACION DE COLUMNA TORACICA O LUMBAR   VIA POSTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793952', 'REDUCCION ABIERTA DE FRACTURA DE COLUMNA VERTEBRAL [TORACICA, LUMBAR O SACRA] VIA POSTERIOR O POSTEROLATERAL CON INSTRUMENTACION SIMPLE   (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793953', 'REDUCCION ABIERTA DE FRACTURA DE COLUMNA VERTEBRAL [TORACICA, LUMBAR O SACRA] VIA POSTERIOR O POSTEROLATERAL  CON INSTRUMENTACION MODULAR    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '798901', 'REDUCCION ABIERTA DE LUXACION CERVICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810109', 'ARTRODESIS  C1-C2 MEDIANTE VIA POSTERIOR  CON  INSTRUMENTACION SIMPLE    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810110', 'ARTRODESIS  C1-C2 MEDIANTE VIA POSTERIOR  CON  INSTRUMENTACION MODULAR    (102)  (103)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810302', 'ARTRODESIS DE NIVEL C2 O POR DEBAJO,  TECNICA POSTERIOR  O POSTEROLATERAL CON INSTRUMENTACION SIMPLE    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810303', 'ARTRODESIS DE NIVEL C2 O POR DEBAJO,  TECNICA POSTERIOR  O POSTEROLATERAL CON INSTRUMENTACION MODULAR    (102)  (103)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810812', 'ARTRODESIS POSTEROLATERAL INTERCORPORAL (PLIF) DE COLUMNA VERTEBRAL CON INSTRUMENTACION    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810914', 'REFUSION DE COLUMNA TORACICA, VIA POSTERIOR O POSTEROLATERAL , CON INJERTO E INSTRUMENTACION    (102)  (104)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810923', 'REFUSION DE COLUMNA LUMBAR, VIA POSTERIOR O POSTEROLATERAL, CON INJERTO      (104)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810924', 'REFUSION DE COLUMNA LUMBAR, VIA POSTERIOR O POSTEROLATERAL, CON INJERTO E INSTRUMENTACION      (104)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810933', 'REFUSION DE COLUMNA SACRA, VIA POSTERIOR O POSTEROLATERAL, CON INJERTO      (104)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810934', 'REFUSION  DE COLUMNA SACRA, VIA POSTERIOR O POSTEROLATERAL, CON INJERTO E INSTRUMENTACION      (104)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810102', 'ARTRODESIS OCCIPITOCERVICAL MEDIANTE TECNICA TRANSORAL  CON  INSTRUMENTACION    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810107', 'ARTRODESIS C1-C2 MEDIANTE TECNICA TRANSORAL ANTERIOR CON  INSTRUMENTACION    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810401', 'ARTRODESIS DE LA REGION TORACICA O TORACOLUMBAR,  TECNICA ANTERIOR O ANTEROLATERAL (INTERSOMATICA) SIN INSTRUMENTACION    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810601', 'ARTRODESIS DE LA REGION LUMBAR O LUMBOSACRA,  TECNICA ANTERIOR O ANTEROLATERAL (INTERSOMATICA) SIN INSTRUMENTACION   (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810611', 'FUSION INTERCORPORAL ANTEROLATERAL (ALIF)  SIN INSTRUMENTACION    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810612', 'FUSION INTERCORPORAL ANTEROLATERAL (ALIF)  CON INSTRUMENTACION    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810701', 'ARTRODESIS O FUSION ESPINAL LUMBAR Y/O  LUMBOSACRA, TECNICA LATERAL INTERTRANSVERSA SIN INSTRUMENTACION    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810911', 'REFUSION  DE COLUMNA TORACICA , VIA ANTERIOR, CON INJERTO     (102)  (104)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810912', 'REFUSION DE COLUMNA TORACICA, VIA ANTERIOR, CON INJERTO E INSTRUMENTACION     (102)  (104)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810921', 'REFUSION DE COLUMNA LUMBAR, VIA ANTERIOR, CON INJERTO     (102)  (104)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810922', 'REFUSION DE COLUMNA LUMBAR, VIA ANTERIOR, CON INJERTO E INSTRUMENTACION    (102)  (104)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810931', 'REFUSION DE COLUMNA SACRA, VIA ANTERIOR, CON INJERTO     (102)  (104)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810932', 'REFUSION DE COLUMNA SACRA, VIA ANTERIOR, CON INJERTO E INSTRUMENTACION    (102)  (104)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810807', 'ARTRODESIS CON INSTRUMENTACION TRANSLAMINAR     (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '801101', 'ARTROTOMIA DE HOMBRO CON EXPLORACION DE ARTICULACION ACROMIOCLAVICULAR O EXTERNO CLAVICULAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '801200', 'ARTROTOMIA  DE CODO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '801300', 'ARTROTOMIA  DE MUÑECA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '801500', 'ARTROTOMIA  DE PELVIS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '799601', 'REDUCCION DE FRACTURAS INTRAARTICULARES Y LUXOFRACTURAS EN RODILLA POR ARTROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '801600', 'ARTROTOMIA  DE RODILLA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '801700', 'ARTROTOMIA  DE TOBILLO O CUELLO DE PIE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '800101', 'EXTRACCION  DE DISPOSITIVO IMPLANTADO EN  HOMBRO POR ARTROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '800102', 'EXTRACCION DE CUERPO EXTRAÑO INTRAARTICULAR EN HOMBRO POR ARTROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '800202', 'EXTRACCION DE CUERPO EXTRAÑO INTRAARTICULAR EN CODO POR ARTROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '800301', 'EXTRACCION DE DISPOSITIVO IMPLANTADO EN MUÑECA POR ARTROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '800302', 'EXTRACCION DE CUERPO EXTRAÑO INTRAARTICULAR EN MUÑECA POR ARTROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '800501', 'EXTRACCION DE DISPOSITIVO IMPLANTADO EN PELVIS POR ARTROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '800502', 'EXTRACCION DE CUERPO EXTRAÑO INTRAARTICULAR EN PELVIS POR ARTROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '800601', 'EXTRACCION DE DISPOSITIVO IMPLANTADO  EN RODILLA POR ARTROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '800602', 'EXTRACCION DE CUERPO EXTRAÑO INTRAARTICULAR EN RODILLA POR ARTROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '800701', 'EXTRACCION DE DISPOSITIVO IMPLANTADO EN TOBILLO POR ARTROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '800702', 'EXTRACCION DE CUERPO EXTRAÑO INTRAARTICULAR EN TOBILLO POR ARTROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '800802', 'EXTRACCION DE CUERPO EXTRAÑO EN PIE O ARTEJOS POR ARTROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '830301', 'EXTRACCION DE DEPOSITOS CALCANEOS O BURSA-SUBDELTOIDEOS O INTRATENDINOSOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '936800', 'INMOVILIZACION O MANIPULACION ARTICULAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '806101', 'MENISCECTOMIA SIMPLE MEDIAL O LATERAL DE RODILLA  VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '806102', 'MENISECTOMIA MEDIAL Y LATERAL DE RODILLA VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814410', 'REALINEAMIENTO DISTAL DE ROTULA CON CIRUGIA DE TEJIDOS BLANDOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814420', 'REALINEAMIENTO DISTAL DE ROTULA CON OSTEOTOMIA DE TUBEROSIDAD ANTERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814501', 'REPARACION DE LIGAMENTO CRUZADO NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814601', 'CORRECCION QUIRURGICA LIGAMENTARIA MEDIAL O LATERAL Y/O CAPSULAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814727', 'REPARACION AGUDA DE LIGAMENTO CRUZADO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814901', 'REPARACION AGUDA DE LIGAMENTOS DEL TOBILLO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '818604', 'REPARACION AGUDA DE LIGAMIENTO COLATERAL CODO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814502', 'RECONSTRUCCION DE LIGAMENTO CRUZADO ANTERIOR  CON AUTOINJERTO O ALOINJERTO VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814602', 'RECONSTRUCCION O TRANSFERENCIAS PARA LIGAMENTOS MEDIAL O LATERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814902', 'RECONSTRUCCION SECUNDARIA DE LIGAMENTOS DE TOBILLO CON AUTO O ALOINJERTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '818602', 'RECONSTRUCCION SECUNDARIA DE LIGAMENTOS DE CODO CON AUTO O ALOINJERTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814503', 'RECONSTRUCCION DE LIGAMENTO CRUZADO POSTERIOR  CON AUTOINJERTO O ALOINJERTO VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '818100', 'REEMPLAZO PROTESICO PARCIAL DE HOMBRO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '818010', 'REEMPLAZO PROTESICO PRIMARIO TOTAL DE HOMBRO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '818020', 'REEMPLAZO PROTESICO SECUNDARIO TOTAL DE HOMBRO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '818500', 'REEMPLAZO PARCIAL PROTESICO  DE CODO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '818400', 'REEMPLAZO TOTAL PROTESICO  DE CODO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '815200', 'REEMPLAZO PARCIAL DE  CADERA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '815101', 'REEMPLAZO PROTESICO TOTAL PRIMARIO DE CADERA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '815102', 'REEMPLAZO PROTESICO TOTAL CON ARTRODESIS DE CADERA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '815411', 'REEMPLAZO PROTESICO  PRIMARIO PARCIAL DE RODILLA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '815401', 'REEMPLAZO  TOTAL DE RODILLA BICOMPARTIMENTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '815402', 'REEMPLAZO  TOTAL DE RODILLA TRICOMPARTIMENTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '815403', 'REEMPLAZO  TOTAL DE RODILLA UNICOMPARTIMENTAL (HEMIARTICULACION)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '815600', 'REEMPLAZO PROTESICO TOTAL DE TOBILLO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '817201', 'ARTROPLASTIA POR INTERPOSICION O RESECCION  MUÑECA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '817208', 'ARTROPLASTIA RADIOCARPIANA (MUÑECA O PUÑO) NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '818305', 'ARTROPLASTIA ACROMIO- CLAVICULAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '818306', 'ARTROPLASTIA POR INTERPOSICION O RESECCION HOMBRO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '818601', 'ARTROPLASTIA POR INTERPOSICION O RESECCION DEL CODO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '775301', 'CORRECCION DE HALLUX VALGUS CON ARTROPLASTIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '813100', 'ARTROPLASTIA DE PIE Y DEDOS CON O SIN  PROTESIS  SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '813240', 'ARTROPLASTIA POR INTERPOSICION DE HUESOS DEL TARSO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '813250', 'ARTROPLASTIA POR INTERPOSICION DE HUESOS DEL METATARSO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814101', 'ARTROPLASTIA POR INTERPOSICION O RESECCION DE CADERA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810101', 'ARTRODESIS OCCIPITOCERVICAL MEDIANTE TECNICA TRANSORAL  SIN INSTRUMENTACION  (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810103', 'ARTRODESIS OCCIPITOCERVICAL VIA POSTERIOR  SIN INSTRUMENTACION    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810106', 'ARTRODESIS C1-C2 MEDIANTE TECNICA TRANSORAL ANTERIOR SIN  INSTRUMENTACION    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810108', 'ARTRODESIS  C1-C2 MEDIANTE VIA POSTERIOR  SIN INSTRUMENTACION    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810301', 'ARTRODESIS DE NIVEL C2 O POR DEBAJO,  TECNICA POSTERIOR  O POSTEROLATERAL SIN INSTRUMENTACION    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810501', 'ARTRODESIS DE LA REGION TORACICA O TORACOLUMBAR,  TECNICA POSTERIOR  SIN INSTRUMENTACION    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810801', 'ARTRODESIS DE LA REGION LUMBAR O LUMBOSACRA,  TECNICA POSTERIOR O POSTEROLATERAL  SIN INSTRUMENTACION    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810811', 'ARTRODESIS POSTEROLATERAL INTERCORPORAL (PLIF) DE COLUMNA VERTEBRAL SIN INSTRUMENTACION    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810913', 'REFUSION DE COLUMNA TORACICA, VIA POSTERIOR O POSTEROLATERAL , CON INJERTO    (102)  (104)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812301', 'ARTRODESIS DE HOMBRO VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812401', 'ARTRODESIS RADIOCUBITAL DISTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812100', 'ARTRODESIS DE CADERA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812907', 'ARTRODESIS SACROILIACA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812200', 'ARTRODESIS DE RODILLA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '811101', 'FUSION TIBIO-TALAR O TIBIOASTRAGALINA VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '811201', 'ARTRODESIS DE TALO A CALCANEO Y CALACANEO A CUBOIDES Y ESCAFOIDES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '811300', 'ARTRODESIS SUBASTRAGALINA O SUBTALAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '811400', 'ARTRODESIS MEDIOTARSAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '811500', 'ARTRODESIS TARSOMETATARSAL (UNA O MAS) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '811600', 'ARTRODESIS METATARSOFALANGICA (UNA O MAS) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '811701', 'PANARTRODESIS DEL PIE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '775201', 'CORRECCION DE HALLUX VALGUS CON ARTRODESIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '811702', 'ARTRODESIS DE ARTEJO (UNO O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797100', 'REDUCCION CERRADA DE LUXACION EN HOMBRO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '799100', 'REDUCCION DE FRACTURAS INTRAARTICULARES Y LUXOFRACTURAS EN HOMBRO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797200', 'REDUCCION CERRADA DE LUXACION EN CODO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797501', 'REDUCCION CERRADA DE LUXACION CONGENITA DE CADERA (UNI O BILATERAL)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797502', 'REDUCCION CERRADA DE LUXACION TRAUMATICA DE CADERA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797603', 'REDUCCION CERRADA DE LUXACION TRAUMATICA DE ROTULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797601', 'REDUCCION CERRADA DE LUXACION TRAUMATICA DE RODILLA NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797602', 'REDUCCION CERRADA DE LUXACION TIBIOPERONERA PROXIMAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '798101', 'REDUCCION ABIERTA DE LUXACION ACROMIO CLAVICULAR CON O SIN DISPOSITIVOS DE FIJACION [OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '798102', 'REDUCCION ABIERTA DE LA LUXACION GLENOHUMERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '818200', 'REPARACION DE LUXACION RECURRENTE DE HOMBRO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '798201', 'REDUCCION ABIERTA DE LA LUXACION DE LA CABEZA RADIAL    (166)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797503', 'REDUCCION CERRADA DE LUXACION CONGENITA DE CADERA CON TENOTOMIA DE ADUCTORES Y/O PSOAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '798501', 'REDUCCION ABIERTA DE LUXACION CONGENITA DE  PELVIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '798502', 'REDUCCION ABIERTA DE LUXACION TRAUMATICA DE PELVIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '798602', 'REDUCCION ABIERTA DE LUXACION DE ROTULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '798601', 'REDUCCION ABIERTA DE LUXACION  DE RODILLA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '798701', 'REDUCCION ABIERTA DE LUXACION DE TOBILLO (TIBIOASTRAGALINA)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '799701', 'REDUCCION ABIERTA DE LUXO-FRACTURA TOBILLO SIN DISPOSITIVOS DE FIJACION U OSTEOSINTESIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831905', 'ESCALENOTOMIA O SECCION DE ESCALENO ANTERIOR SIN RESECCION DE COSTILLA CERVICAL    (105)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831203', 'LIBERACION DE FLEXORES DE CADERA    (105)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831204', 'LIBERACION DE MUSCULATURA PELVITROCANTERICA    (105)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831202', 'LIBERACION PERIARTICULAR DE LA PELVIS, CON TENOTOMIAS (CADERA COLGANTE)    (105)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831402', 'INCISION  DE BANDA ILIOTIBIAL    (105)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831403', 'ESCISION PARCIAL DE FASCIA    (105)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831451', 'FASCIOTOMIA DESCOMPRESIVA DE LA ARTERIA BRAQUIAL    (105)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '818603', 'LIBERACION ANTERIOR O POSTERIOR DE CODO PARA CONTRACTURA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831450', 'FASCIOTOMIA EN ANTEBRAZO, CON LIBERACION EN CODO Y MUÑECA    (105)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831461', 'FASCIOTOMIA EN MUSLO, POR UNA O MAS INCISIONES    (105)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831471', 'FASCIOTOMIA EN PIERNA, POR UNA O MAS INCISIONES NCOC    (105)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '831481', 'FASCIOTOMIA EN PIE,  UNA O MAS INCISIONES    (105)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '836301', 'REPARACION VIA ABIERTA DEL MANGUITO ROTADOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '833101', 'ESCISION DE GANGLION DE ENVOLTURA DE TENDON, EXCEPTO DE MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '833202', 'ESCISION O RESECCION DE : HUESO HETEROTOPICO O CALCIFICACIONES HETEROTOPICAS EN MUSCULO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '833001', 'RESECCION DE TUMOR BENIGNO DE FASCIA, MUSCULO, TENDON O SINOVIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '833002', 'RESECCION DE TUMOR MALIGNO DE FASCIA ,MUSCULO , TENDON O SINOVIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '835100', 'BURSECTOMIA ABIERTA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '833901', 'ESCISION DE QUISTE POPLITEO O DE BAKER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '833201', 'ESCISION DE  MIOSITIS OSIFICANTE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '838601', 'CUADRICEPLASTIA ABIERTA  (107)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '838501', 'ALARGAMIENTO TENDON POPLITEO    (107)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '838502', 'ALARGAMIENTO O ACORTAMIENTO DEL TENDON DE AQUILES     (107)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '819420', 'RECONSTRUCCION DE TENDON DE AQUILES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '838505', 'REPARACION DEL TENDON DEL CUADRICEPS CON  FIJACION    (107)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '819520', 'REPARACION O RECONSTRUCCION  DEL TENDON ROTULIANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '839101', 'LISIS DE ADHERENCIAS DE TENDON O TENOLISIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '823200', 'ESCISION DE TENDON DE MANO PARA INJERTO (DIFERENTE REGION OPERATORIA) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '834100', 'ESCISION DE TENDON PARA INJERTO (DIFERENTE REGION OPERATORIA) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862701', 'ONICECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862703', 'MATRICECTOMIA TOTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780202', 'APLICACION DE ALOINJERTO ESTRUCTURAL EN HUMERO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780502', 'APLICACION DE ALOINJERTO ESTRUCTURAL EN DIAFISIS DE FEMUR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780503', 'APLICACION DE ALOINJERTO ESTRUCTURAL OSTEOCONDRAL EN FEMUR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780702', 'APLICACION DE ALOINJERTO ESTRUCTURAL EN DIAFISIS DE TIBIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780703', 'APLICACION DE ALOINJERTO ESTRUCTURAL OSTEOCONDRAL  EN TIBIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780706', 'APLICACION DE ALOINJERTO ESTRUCTURAL EN DIAFISIS DE PERONE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780921', 'APLICACION DE ALOINJERTO ESTRUCTURAL EN PELVIS O CADERA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '849501', 'CIRUGIA RECONSTRUCTIVA MULTIPLE: OSTEOTOMIAS Y/O FIJACION INTERNA [DISPISITIVOS DE FIJACION U OSTEOSINTESIS] EN FEMUR, TIBIA Y PERONE; TRANSFERENCIAS MUSCULOTENDINOSAS; TENOTOMIAS Y/O ALARGAMIENTOS TENDINOSOS EN MUSLO, PIERNA Y PIE; TRIPLE ARTODESIS EN PI' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '770401', 'DRENAJE, CURETAJE O SECUESTRECTOMIA EN HUESOS DEL CARPO (UNO O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '770402', 'DRENAJE, CURETAJE O SECUESTRECTOMIA EN METACARPIANOS (UNO O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '796400', 'LAVADO Y DESBRIDAMIENTO DE FRACTURAS EXPUESTAS DE FALANGES EN MANO SOD  (98)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '796301', 'LAVADO Y DESBRIDAMIENTO DE FRACTURA ABIERTA O EXPUESTA EN MANO (EXCEPTO FALANGES)    (98)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808041', 'DESBRIDAMIENTO, LAVADO Y LIMPIEZA DE ARTICULACION EN  MANO Y/O DEDOS VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786401', 'EXTRACCION DE DISPOSITIVO IMPLANTADO  EN  CARPIANOS O METACARPIANOS (UNO O MAS)  (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786402', 'EXTRACCION DE CUERPO EXTRAÑO  EN  CARPIANOS O METACARPIANOS (UNO O MAS) POR VIA ABIERTA    (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '829911', 'EXTRACCION DE CUERPO EXTRAÑO EN REGION TENAR O TUNEL CARPIANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '829912', 'EXTRACCION DE CUERPO EXTRAÑO EN TEJIDOS BLANDOS DE MANO (EXCEPTO DEDOS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '830102', 'ELIMINACION DE CUERPOS RICIFORMES DE VAINA DE TENDON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '786911', 'EXTRACCION DE CUERPO EXTRAÑO  EN FALANGES (UNO O MAS) DE PIE , VIA ABIERTA    (96)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '829910', 'EXTRACCION DE CUERPO EXTRAÑO EN TEJIDOS BLANDOS DE DEDOS DE LA  MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776401', 'ESCISION TUMOR BENIGNO EN CARPIANOS O METACARPIANOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776402', 'ESCISION TUMOR MALIGNO EN CARPIANOS O METACARPIANOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '776902', 'ESCISION DE TUMOR MALIGNO  EN FALANGES DE PIE O DE MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772401', 'OSTEOTOMIA EN CARPIANO O METACARPIANO CON FIJACION INTERNA O EXTERNA [DISPOSITIVOS DE FIJACION  U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772402', 'OSTEOTOMIA EN CARPIANO Y  METACARPIANO CON FIJACION INTERNA O EXTERNA [DISPOSITIVOS DE FIJACION  U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772901', 'OSTEOTOMIA EN FALANGES DE MANO (UNO O MAS HUESOS)  CON FIJACION INTERNA O EXTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '782741', 'ACORTAMIENTO DE FALANGES DE MANO MEDIANTE RESECCION/ OSTEOTOMIA (UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778401', 'HEMI O DIAFISECTOMIA METACARPIANOS (UNO O MAS)  (94)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779405', 'METACARPECTOMIA (UNO O MAS HUESOS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778901', 'HEMIDIAFISECTOMIA FALANGES DE MANO (UNA O MAS)    (94)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '778911', 'HEMIDIAFISECTOMIA FALANGES DE PIE (UNA O MAS)    (94)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '779401', 'CARPECTOMIA (UNO O MAS HUESOS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780401', 'INJERTO OSEO EN HUESOS DEL CARPO (EXCEPTO ESCAFOIDES)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780402', 'INJERTO OSEO EN ESCAFOIDES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780403', 'INJERTO OSEO EN METACARPIANOS (UNO O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780901', 'INJERTO OSEO EN FALANGES DE LA MANO (UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797300', 'REDUCCION CERRADA DE LUXACION EN MUÑECA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797401', 'REDUCCION CERRADA DE LUXACION CARPIANA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797402', 'REDUCCION CERRADA DE LUXACION CARPOMETACARPIANA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797403', 'REDUCCION CERRADA DE LUXACION METACARPOFALANGICA (UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '797404', 'REDUCCION CERRADA DE LUXACION INTERFALANGICA (UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '791901', 'REDUCCION CERRADA DE FRACTURA CON FIJACION PERCUTANEA (PINES) DE FALANGES (UNA O MAS) DE PIE O DE MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '799302', 'REDUCCION CERRADA Y FIJACION  DE LUXOFRACTURA DE BENNET' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793401', 'REDUCCION ABIERTA DE FRACTURA DE HUESOS DE CARPO (UNA O MAS) CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792401', 'REDUCCION ABIERTA SIN FIJACION DE FRACTURA DE HUESOS DEL CARPO O METACARPO (UNO O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792901', 'REDUCCION ABIERTA DE FRACTURA SIN FIJACION INTERNA DE FALANGES DE MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793402', 'REDUCCION ABIERTA DE FRACTURA DE METACARPIANOS (UNA O MAS) CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '793901', 'REDUCCION ABIERTA DE FRACTURA EN FALANGES DE MANO (UNA O MAS) CON FIJACION INTERNA    (97)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '799401', 'REDUCCION ABIERTA CON FIJACION DE  FRACTURA  INTRAARTICULAR DE MANO (UNA O MAS ARTICULACIONES)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '799301', 'REDUCCION ABIERTA Y FIJACION   DE LUXOFRACTURA DE BENNET' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '798411', 'REDUCCION ABIERTA CON FIJACION DE LUXACION CARPIANA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '798421', 'REDUCCION ABIERTA CON FIJACION DE LUXACION CARPO-METACARPIANA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '798431', 'REDUCCION ABIERTA CON FIJACION DE  LUXACION METACARPOFALANGICA O INTERFALANGICA (UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '783401', 'ALARGAMIENTO DE  METACARPIANOS (UNO O MAS) POR INJERTO SIN DISPOSITIVOS INTERNOS DE FIJACION O OSTEOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '840100', 'AMPUTACION Y DESARTICULACION DE DEDOS DE LA MANO (UNO O MAS) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '840200', 'AMPUTACION Y DESARTICULACION DE PULGAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '840300', 'AMPUTACION A TRAVES DE MANO(CARPO) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '843500', 'REMODELACION [REVISION] [ RECONSTRUCCION] DEL MUÑON DE AMPUTACION DE DEDOS DE MANO  (UNO O MAS) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '843400', 'REMODELACION [REVISION] [ RECONSTRUCCION] DEL MUÑON DE AMPUTACION DE MANO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '824601', 'MIORRAFIA DE EXTENSORES DE MANO,( UNO O MAS )' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '824611', 'MIORRAFIA DE FLEXORES DE MANO (UNO O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '822201', 'ESCISION DE TUMOR BENIGNO EN MUSCULO DE MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '822202', 'ESCISION DE TUMOR MALIGNO EN MUSCULO DE MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '820102', 'ELIMINACION DE CUERPOS RICIFORMES DE VAINA DE TENDON DE MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '820200', 'MIOTOMIA DE MANO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '821901', 'DIVISION DE MUSCULO DE MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '823400', 'ESCISION DE MUSCULO O FASCIA DE MANO PARA INJERTO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '829101', 'LIBERACION DE ADHESIONES DE FASCIA, MUSCULO Y TENDON DE MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '825501', 'ALARGAMIENTO DE TENDON EN MANO (UNO O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '825306', 'REINSERCION DE TENDON EN MANO (UNO O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '827902', 'INJERTO DE TENDON FLEXOR DE MANO O DEDOS (UNO O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '827102', 'INJERTO DE TENDON DE FLEXOR DE UN DEDO CON RECONSTRUCCION DE POLEAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '827103', 'INJERTO DE TENDON DE FLEXOR DE DOS O MAS DEDOS CON RECONSTRUCCION DE POLEAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '827910', 'INJERTO TENDINOSO CON IMPLANTE EN DEDOS DE LA MANO (CADA UNO)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '828401', 'CORRECCION QUIRURGICA DE DEDO EN BOTONERA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '828402', 'CORRECCION QUIRURGICA DE DEDO EN CUELLO DE CISNE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '828403', 'CORRECCION QUIRURGICA DE DEDO EN MARTILLO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '828404', 'CORRECCION QUIRURGICA DE DEDO EN GATILLO (DEDO DE RESORTE)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '820101', 'EXPLORACION DE VAINA DE TENDON DE MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '800401', 'EXTRACCION DE DISPOSITIVO IMPLANTADO EN MANO Y DEDO POR ARTROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '801400', 'ARTROTOMIA EN MANO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '804301', 'CAPSULOTOMIA METACARPOFALANGICA (UNA O MAS)  (99)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '804302', 'CAPSULOTOMIA INTERFALANGICA (UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '822102', 'RESECCION DE GANGLION DORSAL DE MUÑECA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '822103', 'RESECCION DE GANGLION PALMAR DE MUÑECA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '822101', 'RESECCION DE GANGLION EN DEDOS DE MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '800402', 'EXTRACCION DE CUERPO EXTRAÑO EN ARTICULACION DE MANO POR ARTROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812501', 'ARTRODESIS RADIOCARPIANA SIN INJERTO OSEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812905', 'ARTRODESIS RADIOCARPIANA (MUÑECA O PUÑO) SIN INJERTO OSEO NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812502', 'ARTRODESIS RADIOCARPIANA CON INJERTO OSEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812904', 'ARTRODESIS  RADIOCARPIANA (MUÑECA O PUÑO) CON INJERTO OSEO NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812601', 'ARTRODESIS TRAPECIO-METACARPIANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812700', 'ARTRODESIS METACARPO-FALANGICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812801', 'ARTRODESIS INTERFALANGICA EN MANO SIN INJERTO (UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812802', 'ARTRODESIS INTERFALANGICA  EN MANO CON INJERTO  (UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812901', 'ARTRODESIS INTERCARPIANA SIN INJERTO OSEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812902', 'ARTRODESIS INTERCARPIANA CON INJERTO OSEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '817301', 'REEMPLAZO PROTESICO DE MUÑECA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '817202', 'ARTROPLASTIA RESECCION TRAPECIO-METACARPIANA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '817102', 'REEMPLAZO PROTESICO DE LA ARTICULACION TRAPECIO-METACARPIANA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '817203', 'ARTROPLASTIA METACARPO-FALANGICA (UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '817901', 'ARTROPLASTIAS INTERFALANGICAS (POR CADA DEDO)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '817101', 'REEMPLAZO PROTESICO EN HUESOS DEL CARPO (UNO O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '819341', 'CAPSULORRAFIA ARTICULAR EN MUÑECA (UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '817205', 'LIGAMENTORRAFIA O REINSERCION LIGAMENTOS''(UNA O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '828910', 'RECONSTRUCCION DE LIGAMENTOS EN MANO (UNO O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '804401', 'CAPSULODESIS EN MANO  (99)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '823100', 'BURSECTOMIA DE MANO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '820400', 'INCISION Y DRENAJE DE ESPACIO PALMAR O TENAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '821200', 'FASCIOTOMIA DE MANO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '830101', 'EXPLORACION DE VAINA DE TENDON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '823501', 'ESCISION DE APONEUROSIS EN MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '823502', 'ESCISION DE APONEUROSIS EN DEDOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868507', 'PLASTIA EN Z, EN CADA DEDO DE LA  MANO O DEL PIE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868504', 'PLASTIA EN Z O W EN MANO (SIN INCLUIR DEDOS), ENTRE UNA A DOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868505', 'PLASTIA EN Z  O W EN MANO ( SIN INCLUIR DEDOS), ENTRE TRES A CINCO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868506', 'PLASTIA EN Z  O W  EN MANO ( SIN INCLUIR DEDOS), MAS DE CINCO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '828302', 'CORRECCION DE SINDACTILIA SIMPLE (UNO O MAS ESPACIOS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '828304', 'CORRECCION DE SINDACTILIA COMPLEJA  (31)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '804310', 'LIBERACION DE BANDAS CONSTRICTIVAS [STREETER]  (99)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '828355', 'CORRECCION DE DEFORMIDAD DE MADELUNG' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '828320', 'CORRECCION SIMPLE DE CAMPTODACTILIA (UNO O MAS DEDOS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '828330', 'CORRECCION DE CAMPTODACTILIA CON OSTEOTOMIA (UNO O MAS DEDOS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '828340', 'CORRECCION QUIRURGICA DE CLINODACTILIA (UNO O MAS DEDOS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '828351', 'CORRECCION DE POLIDACTILIA CON EXCISION SIMPLE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '828350', 'CORRECCION DE POLIDACTILIA CON RECONSTRUCCION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '828310', 'CORRECCION QUIRURGICA DE LA MACRODACTILIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '849400', 'CORRECCION DE SINOSTOSIS RADIOCUBITAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '828200', 'REPARACION DE DEFORMIDADES CONGENITAS DE LA MANO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '842303', 'REIMPLANTE DE MANO A NIVEL DE LA MUÑECA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '842302', 'REIMPLANTE DE MANO A NIVEL DEL METACARPO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '842202', 'REIMPLANTE DE UN DEDO EN MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '842203', 'REIMPLANTE DE DOS DEDOS EN MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '842204', 'REIMPLANTE DE TRES  DEDOS EN MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '842205', 'REIMPLANTE DE CUATRO O MAS DEDOS  EN MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '826100', 'PULGARIZACION O POLICITACION CON SUMINISTRO NEUROVASCULAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '842100', 'REIMPLANTE DE PULGAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '759101', 'DRENAJE DE COLECCION OBSTETRICA  (DE EPISIOTOMIA O EPISIORRAFIA) EN PERINE POR INCISION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '861102', 'DRENAJE DE COLECCION PROFUNDA EN PIEL Y/O TEJIDO CELULAR SUBCUTANEO POR INCISION O ASPIRACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862203', 'DESBRIDAMIENTO ESCISIONAL  POR LESION SUPERFICIAL HASTA EL 10%  DE SUPERFICIE CORPORAL, EN AREA GENERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862204', 'DESBRIDAMIENTO ESCISIONAL  POR LESION SUPERFICIAL ENTRE EL 10% AL 20% DE SUPERFICIE CORPORAL, EN AREA GENERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862205', 'DESBRIDAMIENTO ESCISIONAL  POR LESION SUPERFICIAL ENTRE EL 20% AL 30% DE SUPERFICIE CORPORAL, EN AREA GENERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862206', 'DESBRIDAMIENTO ESCISIONAL  POR LESION SUPERFICIAL ENTRE EL 30% AL 50% DE SUPERFICIE CORPORAL, EN AREA GENERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862207', 'DESBRIDAMIENTO ESCISIONAL  POR LESION SUPERFICIAL DE MAS DEL 50% DE SUPERFICIE CORPORAL, EN AREA GENERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862303', 'DESBRIDAMIENTO  ESCISIONAL POR LESION DE TEJIDOS PROFUNDOS HASTA EL 10% DE SUPERFICIE CORPORAL, EN AREA GENERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862304', 'DESBRIDAMIENTO  ESCISIONAL POR LESION DE TEJIDOS PROFUNDOS ENTRE EL 10% AL 20% DE SUPERFICIE CORPORAL, EN AREA GENERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862305', 'DESBRIDAMIENTO  ESCISIONAL POR LESION DE TEJIDOS PROFUNDOS  ENTRE  EL 20% AL 30%  DE SUPERFICIE CORPORAL, EN AREA GENERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862306', 'DESBRIDAMIENTO  ESCISIONAL POR LESION DE TEJIDOS PROFUNDOS  ENTRE EL 30% AL 50%  DE SUPERFICIE CORPORAL, EN AREA GENERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862307', 'DESBRIDAMIENTO  ESCISIONAL POR LESION DE TEJIDOS PROFUNDOS DE MAS DEL 50%  DE SUPERFICIE CORPORAL, EN AREA GENERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862312', 'DESBRIDAMIENTO DE LESION PROFUNDA(ULCERA) CON COCCIGECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862311', 'ESCISION DE ULCERA (SACRA, ISQUIATICA,TROCANTERICA Y OTRAS LOCALIZACIONES) CON OSTECTOMIA Y CIERRE CON COLGAJO COMPUESTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862310', 'ESCISION DE ULCERA (SACRA, ISQUIATICA,TROCANTERICA Y OTRAS LOCALIZACIONES), CON CIERRE PRIMARIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '861201', 'EXTRACCION DE CUERPO EXTRAÑO EN PIEL O TEJIDO CELULAR SUBCUTANEO DE AREA GENERAL POR INCISION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '864101', 'RESECCION DE TUMOR BENIGNO O MALIGNO DE PIEL Y/O TEJIDO CELULAR SUBCUTANEO AREA GENERAL HASTA TRES CENTIMETROS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '864102', 'RESECCION DE TUMOR BENIGNO O MALIGNO  DE PIEL Y/O TEJIDO CELULAR SUBCUTANEO AREA GENERAL, ENTRE TRES A CINCO CENTIMETROS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '864103', 'RESECCION DE TUMOR BENIGNO O MALIGNO  DE PIEL Y/O TEJIDO CELULAR SUBCUTANEO AREA GENERAL, ENTRE CINCO A DIEZ CENTIMETROS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '864104', 'RESECCION DE TUMOR BENIGNO O MALIGNO  DE PIEL Y/O TEJIDO CELULAR SUBCUTANEO AREA GENERAL, DE MAS DE DIEZ CENTIMETROS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '864105', 'RESECCION DE TUMOR BENIGNO  DE PIEL Y/O TEJIDO CELULAR SUBCUTANEO AREA GENERAL, CON REPARACION (COLGAJO Y/O INJERTO)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '864106', 'RESECCION DE TUMOR MALIGNO  DE PIEL Y/O TEJIDO CELULAR SUBCUTANEO AREA GENERAL, CON REPARACION (COLGAJO Y/O INJERTO)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862900', 'FISTULECTOMIA DE PIEL  Y TEJIDO CELULAR SUBCUTANEO SOD *' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '869101', 'RESECCION DE  GLANDULAS SUDORIPARAS AXILARES SIMPLE CON RESECCION GANGLIONAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '869102', 'RESECCION DE  GLANDULAS SUDORIPARAS AXILARES  CON RESECCION TOTAL DEL AREA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '869103', 'RESECCION  PARCIAL DE  GLANDULAS SUDORIPARAS NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '869104', 'RESECCION TOTAL DE  GLANDULAS SUDORIPARAS NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '866101', 'INJERTO DE PIEL PARCIAL EN AREA GENERAL HASTA EL DIEZ 10% DE SUPERFICIE CORPORAL TOTAL  (109)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '866102', 'INJERTO DE PIEL PARCIAL EN AREA GENERAL ENTRE  EL DIEZ 10% HASTA EL 20%  DE SUPERFICIE CORPORAL TOTAL    (109)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '866103', 'INJERTO DE PIEL PARCIAL EN AREA GENERAL ENTRE  EL VEINTE 20% HASTA EL 30%  DE SUPERFICIE CORPORAL TOTAL    (109)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '866104', 'INJERTO DE PIEL PARCIAL EN AREA GENERAL MAYOR DEL TREINTA 30%  DE SUPERFICIE CORPORAL TOTAL    (109)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '866201', 'INJERTO DE PIEL TOTAL LIBRE EN AREA GENERAL  HASTA EL DIEZ 10% DE SUPERFICIE CORPORAL TOTAL    (109)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '866202', 'NJERTO DE PIEL TOTAL LIBRE EN AREA GENERAL  ENTRE EL DIEZ 10% HASTA EL 20% DE SUPERFICIE CORPORAL TOTAL    (109)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '866203', 'NJERTO DE PIEL TOTAL LIBRE EN AREA GENERAL ENTRE EL 20% HASTA EL 30% DE SUPERFICIE CORPORAL TOTAL    (109)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '866204', 'NJERTO DE PIEL TOTAL LIBRE EN AREA GENERAL  MAS DEL 30% DE SUPERFICIE CORPORAL TOTAL    (109)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '866702', 'INJERTO DERMOGRASO NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '867001', 'COLGAJO LOCAL SIMPLE DE PIEL  HASTA DE DOS CENTIMETROS CUADRADOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '867002', 'COLGAJO LOCAL SIMPLE DE PIEL ENTRE DOS A CINCO CENTIMETROS CUADRADOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '867003', 'COLGAJO LOCAL SIMPLE DE PIEL DE MAS DE CINCO CENTIMETROS CUADRADOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '543301', 'ESCISION DE LESION AMPLIA EN LA PARED ABDOMINAL CON ROTACION DE COLGAJO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '867105', 'COLGAJO LIBRE CUTANEO CON TECNICA MICROVASCULAR    (327)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '867106', 'COLGAJO LIBRE COMPUESTO CON TECNICA MICROVASCULAR    (327)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862801', 'DESBRIDAMIENTO  NO ESCISIONAL DE TEJIDO DESVITALIZADO HASTA DEL 5% DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862802', 'DESBRIDAMIENTO  NO ESCISIONAL DE TEJIDO DESVITALIZADO  ENTRE EL 5%AL 10% DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862803', 'DESBRIDAMIENTO  NO ESCISIONAL DE TEJIDO DESVITALIZADO  ENTRE EL 10% AL 20% DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862804', 'DESBRIDAMIENTO  NO ESCISIONAL DE TEJIDO DESVITALIZADO  ENTRE EL 20% AL 30% DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862805', 'DESBRIDAMIENTO  NO ESCISIONAL DE TEJIDO DESVITALIZADO  ENTRE EL 30% AL 40% DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862806', 'DESBRIDAMIENTO  NO ESCISIONAL DE TEJIDO DESVITALIZADO  ENTRE EL 40% AL 50% DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862807', 'DESBRIDAMIENTO  NO ESCISIONAL DE TEJIDO DESVITALIZADO  MAYOR DEL 50% DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862321', 'ESCAROTOMIA DESCOMPRESIVA EN TRONCO O POR  EXTREMIDAD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862322', 'ESCAROTOMIA CUADRICULADA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862320', 'ESCAROTOMIA DESCOMPRESIVA EN MANOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862323', 'ESCARECTOMIA TANGENCIAL TEMPRANA HASTA EL 5%  DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862324', 'ESCARECTOMIA TANGENCIAL TEMPRANA ENTRE EL 5% AL 10%  DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862325', 'ESCARECTOMIA TANGENCIAL TEMPRANA ENTRE EL 10%AL 15%  DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862326', 'ESCARECTOMIA TANGENCIAL TEMPRANA ENTRE EL 15% AL 20%  DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862327', 'ESCARECTOMIA TANGENCIAL TEMPRANA DE MAS DEL 20%  DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862330', 'ESCARECTOMIA TANGENCIAL TARDIA CON INJERTOS DE PIEL HASTA EL 5%  DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862331', 'ESCARECTOMIA TANGENCIAL TARDIA CON INJERTOS DE PIEL , ENTRE EL  5% AL 10%  DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862332', 'ESCARECTOMIA TANGENCIAL TARDIA CON INJERTOS DE PIEL , ENTRE EL  10% AL 20%  DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862333', 'ESCARECTOMIA TANGENCIAL TARDIA CON INJERTOS DE PIEL , DE MAS DEL 20% DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862340', 'ESCARECTOMIA AVULSIVA HASTA EL 5%   DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862341', 'ESCARECTOMIA AVULSIVA ENTRE EL 5%  AL 10%   DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862342', 'ESCARECTOMIA AVULSIVA ENTRE EL  10% AL 20%   DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862343', 'ESCARECTOMIA AVULSIVA MAYOR DEL 20%   DE SUPERFICIE CORPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868101', 'RESECCION SIMPLE DE CICATRIZ EN AREA GENERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868103', 'RESECCION DE CICATRIZ HIPERTROFICA O QUELOIDE, EN AREA GENERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868401', 'PLASTIA EN Z  O W  EN AREA GENERAL, ENTRE UNA A DOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868402', 'PLASTIA EN Z  O W  EN AREA GENERAL, ENTRE TRES A CINCO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868403', 'PLASTIA EN Z  O W  EN AREA GENERAL, MAS DE CINCO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868302', 'RESECCION DE BOLSAS ADIPOSAS DE BICHAT EN CARA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '869601', 'INSERCION (SUBCUTANEA) (TEJIDO BLANDO) DE EXPANSOR DE TEJIDOS [UNICO O MULTIPLE] NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '869201', 'DERIVACION LINFATICA  [MANEJO DE LINFEDEMA]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862501', 'DERMOABRASION (QUIMICA Y/O MECANICA) DE AREA GENERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '869700', 'RETIRO DE EXPANSOR TISULAR [UNICO O MULTIPLE] SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '861202', 'EXTRACCION DE CUERPO EXTRAÑO EN PIEL O TEJIDO CELULAR SUBCUTANEO DE AREA ESPECIAL ( CARA, CUERO CABELLUDO, CUELLO, MANOS, PIES, PLIEGUES DE FLEXION, GENITALES) POR INCISION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '864201', 'RESECCION DE TUMOR BENIGNO O MALIGNO DE PIEL Y/O TEJIDO CELULAR SUBCUTANEO DE AREA ESPECIAL, HASTA UN CENTIMETRO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '864202', 'RESECCION DE TUMOR BENIGNO O MALIGNO DE PIEL Y/O TEJIDO CELULAR SUBCUTANEO DE AREA ESPECIAL, ENTRE UNO A DOS CENTIMETROS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '864203', 'RESECCION DE TUMOR BENIGNO O MALIGNO DE PIEL Y/O TEJIDO CELULAR SUBCUTANEO DE AREA ESPECIAL, ENTRE DOS A TRES CENTIMETROS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '864204', 'RESECCION DE TUMOR BENIGNO O MALIGNO DE PIEL Y/O TEJIDO CELULAR SUBCUTANEO DE AREA ESPECIAL, ENTRE TRES A CINCO CENTIMETROS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '864205', 'RESECCION DE TUMOR BENIGNO O MALIGNO DE PIEL Y/O TEJIDO CELULAR SUBCUTANEO DE AREA ESPECIAL,  DE MAS DE CINCO CENTIMETROS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '865208', 'SUTURA DE AVULSION EN  PABELLON AURICULAR, NARIZ, LABIOS, PARPADOS O GENITALES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '866120', 'INJERTO DE PIEL PARCIAL EN AREA ESPECIAL HASTA EL CINCO 5% DE SUPERFICIE CORPORAL TOTAL  (109)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '866220', 'INJERTO DE PIEL TOTAL LIBRE EN AREA ESPECIAL HASTA EL CINCO 5% DE SUPERFICIE CORPORAL TOTAL    (109)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '866121', 'INJERTO DE PIEL PARCIAL EN  AREA ESPECIAL DE MAS DEL CINCO 5% DE SUPERFICIE CORPORAL TOTAL    (109)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '866221', 'INJERTO DE PIEL TOTAL LIBRE EN  AREA ESPECIAL MAS DEL CINCO 5% DE SUPERFICIE CORPORAL TOTAL    (109)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '866300', 'INJERTO CONDROCUTANEO SOD    (109)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '826920', 'INJERTOS AL PULGAR: OSEO Y PEDICULO DE PIEL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '866401', 'INJERTO DE CUERO CABELLUDO [EN ALOPECIA SECUELA POST-TRAUMA]    (109)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '867101', 'COLGAJO  UNICO DE CUERO CABELLUDO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '867102', 'COLGAJO MULTIPLE  DE CUERO CABELLUDO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '867103', 'COLGAJO CUTANEO A DISTANCIA, EN VARIOS TIEMPOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '867104', 'COLGAJO COMPUESTO  A DISTANCIA, EN VARIOS TIEMPOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '867201', 'COLGAJO LOCAL DE PIEL COMPUESTO DE VECINDAD  HASTA DE DOS CENTIMETROS CUADRADOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '867202', 'COLGAJO LOCAL DE PIEL COMPUESTO DE VECINDAD ENTRE DOS A CINCO CENTIMETROS CUADRADOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '867203', 'COLGAJO LOCAL DE PIEL COMPUESTO DE VECINDAD ENTRE CINCO A DIEZ CENTIMETROS CUADRADOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '867107', 'COLGAJO NEUROVASCULAR ( EN ISLA)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '858701', 'RECONSTRUCCION DEL COMPLEJO AREOLA, PEZON    (225)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862702', 'MATRICECTOMIA PARCIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '861103', 'DRENAJE DE HEMATOMA SUBUNGUEAL  POR INCISION O ASPIRACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868603', 'RECONSTRUCCION DEL LECHO UNGUEAL CON INJERTO DE MATRIZ UNGUEAL  (32)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868604', 'RECONSTRUCCION DE MATRIZ UNGUEAL CON INJERTO COMPUESTO    (32)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862201', 'DESBRIDAMIENTO ESCISIONAL  POR LESION SUPERFICIAL  EN  AREA ESPECIAL DE MENOS DEL CINCO 5% DE SUPERFICIE CORPORAL TOTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862202', 'DESBRIDAMIENTO ESCISIONAL  POR LESION SUPERFICIAL  EN  AREA ESPECIAL DE  MAS DEL CINCO 5% DE SUPERFICIE CORPORAL TOTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862301', 'DESBRIDAMIENTO ESCISIONAL POR LESION DE TEJIDOS PROFUNDOS  EN  AREA ESPECIAL DE MENOS DEL CINCO 5% DE SUPERFICIE CORPORAL TOTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862302', 'DESBRIDAMIENTO ESCISIONAL POR LESION DE TEJIDOS PROFUNDOS  EN  AREA ESPECIAL DE MAS DEL CINCO 5% DE SUPERFICIE CORPORAL TOTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868102', 'RESECCION SIMPLE DE CICATRIZ EN AREA ESPECIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868104', 'RESECCION DE CICATRIZ HIPERTROFICA O QUELOIDE, EN AREA ESPECIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868510', 'PLASTIA EN Z O W, EN ZONAS DE FLEXION    (168)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868501', 'PLASTIA EN Z  O W  EN AREA ESPECIAL  (CARA, CUELLO, MANOS, PIES, PLIEGUES DE FLEXION, GENITALES), ENTRE UNO A DOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868502', 'PLASTIA EN Z  O W  EN AREA ESPECIAL  (CARA, CUELLO, MANOS, PIES, PLIEGUES DE FLEXION, GENITALES), ENTRE TRES A CINCO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868503', 'PLASTIA EN Z  O W  EN AREA ESPECIAL  (CARA, CUELLO, MANOS, PIES, PLIEGUES DE FLEXION, GENITALES), MAS DE CINCO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862502', 'DERMOABRASION (QUIMICA Y/O MECANICA) DE AREA ESPECIAL  (CUERO CABELLUDO,  CUELLO, MANOS, PIES, PLIEGUES DE FLEXION, GENITALES)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862503', 'DERMOABRASION PARCIAL DE CARA  (QUIMICA Y/O MECANICA)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '862504', 'DERMOABRASION TOTAL  DE CARA (QUIMICA Y/O MECANICA)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868602', 'REPOSICION UÑA DE POLIETILENO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '760902', 'DECORTICACION O CURETAJE OSEO  EN HUESO FACIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '830233', 'MIOTOMIA DE MASETERO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '830231', 'MIOTOMIA  DELTEMPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '830232', 'MIOTOMIA PTERIGOIDEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '766605', 'CORTICOTOMIA  TIPO LEFORT I (DISYUNCION PALATINA)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '766202', 'OSTEOTOMIA RAMA MANDIBULAR VIA TRANSCUTANEA, CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]    (163)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '766302', 'OSTEOTOMIA DE CUERPO MANDIBULAR VIA TRANSCUTANEA, CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]    (163)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '766100', 'OSTEOPLASTIA CERRADA (OSTEOTOMIA) DE RAMA MANDIBULAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '766201', 'OSTEOTOMIA DE RAMA MANDIBULAR VIA TRANS MUCOSA, CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '766601', 'OSTEOTOMIA LEFORT I, CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '766603', 'OSTEOTOMIA LEFORT II, CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '766604', 'OSTEOMIA LEFORT III, CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '766403', 'OSTEOTOMIA DE MENTON, CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '766501', 'OSTEOTOMIA LEFORT I SEGMENTARIA, CON FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '763101', 'MANDIBULECTOMIA PARCIAL SIMPLE, MARGINAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '763102', 'MANDIBULECTOMIA PARCIAL SIMPLE, SEGMENTARIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '763103', 'HEMIMANDIBULECTOMIA SIN DESARTICULACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '763104', 'HEMIMANDIBULECTOMIA CON DESARTICULACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '763903', 'HEMIMAXILECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '764201', 'MANDIBULECTOMIA TOTAL SIN RECONSTRUCCION OSEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '764101', 'MANDIBULECTOMIA TOTAL CON RECONSTRUCCION OSEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '760901', 'OSTEOTOMIA MAXILAR PARA EXTRACCION DE CUERPO EXTRAÑO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '768801', 'ARTRECTOMIA  TEMPOROMANDIBULAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '761201', 'ARTROCENTESIS DIAGNOSTICA DE ARTICULACION TEMPOROMANDIBULAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '765201', 'MENISECTOMIA TEMPOROMANDIBULAR CON INJERTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '765105', 'MENISCOPEXIA TEMPOROMANDIBULAR, POR VIA EXTERNA    (227)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '765202', 'MENISECTOMIA TEMPOROMANDIBULAR CON COLGAJO    (348)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '766205', 'CORONOIDECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '763902', 'CONDILECTOMIA DE  LA MANDIBULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '762105', 'ELIMINACION DE EXOSTOSIS Y/O TUBEROSIDADES FIBROSAS EN  MAXILAR SUPERIOR O INFERIOR (90)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '766902', 'OSTEOTOMIA DESLIZANTE (VISERA)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '768500', 'INSERCION DE EXPANSOR DE PERIOSTO  EN HUESO O ARTICULACION FACIAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '766970', 'LAVADO Y DESBRIDAMIENTO DE FRACTURA ABIERTA DE HUESOS FACIALES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '762101', 'ENUCLEACION, RESECCION Y CURETAJE DE LESIONES BENIGNAS EN MAXILAR SUPERIOR O INFERIOR, DE MENOS DE TRES CMS VIA TRANSMUCOSA  (90)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '762102', 'ENUCLEACION, RESECCION Y CURETAJE DE LESIONES BENIGNAS EN MAXILAR SUPERIOR O INFERIOR, DE MENOS DE TRES CMS, VIA TRANSCUTANEA    (90)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '762103', 'ENUCLEACION, RESECCION Y CURETAJE DE LESIONES BENIGNAS EN MAXILAR SUPERIOR O INFERIOR, DE MAS DE TRES CMS VIA TRANSMUCOSA    (90)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '762104', 'ENUCLEACION, RESECCION Y CURETAJE DE LESIONES BENIGNAS EN MAXILAR SUPERIOR O INFERIOR, DE MAS DE TRES CMS, VIA TRANSCUTANEA    (90)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '766903', 'OSTEOPLASTIA SIMULTANEA DE VARIOS HUESOS FACIALES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '441500', 'BIOPSIA ABIERTA DEL ESTOMAGO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '482500', 'BIOPSIA ABIERTA DE RECTO O SIGMOIDE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '482600', 'BIOPSIA DE TEJIDO PERIRRECTAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '413202', 'BIOPSIA ABIERTA DE BAZO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '413201', 'BIOPSIA CERRADA [POR ASPIRACION] (PERCUTANEA) DE BAZO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '552401', 'BIOPSIA RIÑON POR VIA ABIERTA O LUMBOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '552310', 'BIOPSIA CON AGUJA [PERCUTANEA]  O TROCAR  DE RIÑON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '552500', 'BIOPSIA TEJIDOS PERIRENALES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '582301', 'BIOPSIA DE URETRA VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '573500', 'BIOPSIA DE TEJIDO PERIVESICAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '563400', 'BIOPSIA ABIERTA  DE URETER  SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '582401', 'BIOPSIA DE TEJIDO PERIURETRALVIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '630100', 'BIOPSIA DE  EPIDIDIMO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '611101', 'BIOPSIA DE  ESCROTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '641100', 'BIOPSIA DE PENE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '601101', 'BIOPSIA CERRADA [PERCUTANEA] [CON AGUJA] DE PROSTATA POR ABORDAJE TRANSRECTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '601102', 'BIOPSIA CERRADA [PERCUTANEA] [CON AGUJA] DE PROSTATA  POR ABORDAJE PERINEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '601500', 'BIOPSIA DE TEJIDO PERIPROSTATICO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '607100', 'ASPIRACION PERCUTANEA CON AGUJA DE VESICULAS SEMINALES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '611102', 'BIOPSIA DE  TUNICA VAGINALIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '621100', 'BIOPSIA CERRADA [PERCUTANEA] [POR AGUJA] DE TESTICULO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '621200', 'BIOPSIA ABIERTA  DE TESTICULO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '630200', 'BIOPSIA DE CORDON ESPERMATICO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '630300', 'BIOPSIA DE CONDUCTO DEFERENTE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '601200', 'BIOPSIA DE PROSTATA  VIA ABIERTA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '601301', 'BIOPSIA  CERRADA [PERCUTANEA] [CON AGUJA] DE VESICULAS SEMINALES POR ABORDAJE TRASRECTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '711120', 'BIOPSIA DE CLITORIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '711110', 'BIOPSIA DE LABIO MAYOR VULVA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '711300', 'BIOPSIA DE PERINE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '702300', 'BIOPSIA DE FONDO DE SACO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '702400', 'BIOPSIA DE VAGINA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '711130', 'BIOPSIA- ESCISION GLANDULA DE BARTHOLIN' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '671201', 'BIOPSIA EN SACABOCADO DE CUELLO UTERINO (EXOCERVIX)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '681601', 'BIOPSIA DE ENDOMETRIO POR PINZA SACABOCADO O DE LEGRADO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '681603', 'BIOPSIA DE ENDOMETRIO POR ASPIRACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '681310', 'BIOPSIA DE MIOMETRIO POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '651201', 'BIOPSIA  EN OVARIO POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '651203', 'BIOPSIA PERCUTANEA [CON AGUJA] EN OVARIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '661110', 'BIOPSIA DE TROMPAS DE FALOPIO POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '681400', 'BIOPSIA ABIERTA DE LIGAMENTOS DE UTERO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '671202', 'BIOPSIA DE CUELLO UTERINO CIRCUNFERENCIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '382101', 'BIOPSIA DE VASO SANGUINEO  SUPERFICIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '382102', 'BIOPSIA DE VASO SANGUINEO PROFUNDO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '761101', 'BIOPSIA DE HUESOS MAXILARES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '761102', 'BIOPSIA DE PALADAR OSEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '774002', 'BIOPSIA DE HUESO EN SITIO NO ESPECIFICADO, POR VIA PERCUTANEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '774910', 'BIOPSIA DE VERTEBRA, POR VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '413101', 'BIOPSIA POR ASPIRACION DE MEDULA OSEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '803101', 'BIOPSIA ARTICULAR  DE HOMBRO VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '803201', 'BIOPSIA ARTICULAR  DE CODO VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '803301', 'BIOPSIA ARTICULAR  DE MUÑECA VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '803401', 'BIOPSIA  ARTICULAR  EN  MANO Y DEDO VIA  ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '803501', 'BIOPSIA ARTICULAR DE PELVIS VIA  ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '803601', 'BIOPSIA ARTICULAR DE RODILLA VIA  ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '803701', 'BIOPSIA ARTICULAR  DE TOBILLO VIA  ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '803801', 'BIOPSIA ARTICULAR  EN PIE Y ARTEJOS VIA ABIERTA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '492200', 'BIOPSIA DE TEJIDO PERIANAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '860101', 'BIOPSIA DE PIEL CON SACABOCADO Y SUTURA SIMPLE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '860102', 'BIOPSIA INCISIONAL O ESCISIONAL DE PIEL, TEJIDO CELULAR SUBCUTANEO O MUCOSA (CON SUTURA)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '860103', 'BIOPSIA ESCISIONAL DE UÑA (LECHO Y/O MATRIZ)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '851102', 'BIOPSIA DE MAMA CON AGUJA TRU -CUT' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '851200', 'BIOPSIA ABIERTA DE MAMA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '492300', 'BIOPSIA DE ANO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '446604', 'CIRUGIA ANTIRREFLUJO GASTRESOFAGICO MAS RECONSTRUCCION DE ESFINTER POR LAPAROSCOPIA O TORACOSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '802501', 'ARTROSCOPIA DIAGNOSTICA DE PELVIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '819810', 'ENDOSCOPIA DIAGNOSTICA DE COLUMNA VERTEBRAL NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '761301', 'ARTROSCOPIA DIAGNOSTICA DE ARTICULACION TEMPOROMANDIBULAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '802201', 'ARTROSCOPIA DIAGNOSTICA DE CODO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '802301', 'ARTROSCOPIA DIAGNOSTICA DE MUÑECA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '802701', 'ARTROSCOPIA DIAGNOSTICA DE TOBILLO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '802101', 'ARTROSCOPIA DIAGNOSTICA DE HOMBRO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '802401', 'ARTROSCOPIA DIAGNOSTICA DE FALANGES (UNA O MAS) DE MANO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '802601', 'ARTROSCOPIA DIAGNOSTICA DE RODILLA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '802801', 'ARTROSCOPIA DIAGNOSTICA EN PIE O DEDOS DE PIE (UNO O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '818302', 'ACROMIOPLASTIA POR ARTROSCOPIA  (38)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812302', 'ARTRODESIS DE HOMBRO POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812903', 'ARTRODESIS ESCAFOSEMILUNAR POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '811102', 'ARTRODESIS TIBIO-TALAR POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '835500', 'BURSECTOMIA POR ARTROSCOPIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '798105', 'CAPSULORRAFIA  POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '819310', 'CAPSULORRAFIA  TIPO BANKART PARA LUXACION DE HOMBRO POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '818307', 'REPARACION DE HOMBRO POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '818606', 'CONDROPLASTIA DEL CODO, VIA ARTROSCOPICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814102', 'CONDROPLASTIA DE ABRASION DE CADERA POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '817207', 'CONDROPLASTIA DE ABRASION DE FALANGES POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814725', 'CONDROPLASTIA DE ABRASION PARA ZONA PATELAR POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814704', 'CONDROPLASTIA DE ABRASION MAS OSTEOTOMIA TIBIAL POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '838602', 'CUADRICEPLASTIA POR ARTROSCOPIA  (107)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '804303', 'DESBRIDAMIENTO DE FIBROCARTILAGO TRIANGULAR O EXTRACCION DE CUERPO EXTRAÑO EN MUÑECA POR ARTROSCOPIA (99)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808022', 'DESBRIDAMIENTO, LAVADO Y LIMPIEZA DE ARTICULACION DE CODO POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808032', 'DESBRIDAMIENTO, LAVADO Y LIMPIEZA DE ARTICULACION DE MUÑECA POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808042', 'DESBRIDAMIENTO, LAVADO Y LIMPIEZA DE  ARTICULACION EN MANO Y/O DEDOS POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808112', 'EXTRACCION DE CUERPOS LIBRES INTRA-ARTICULARES DE HOMBRO POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808202', 'EXTRACCION DE CUERPOS LIBRES INTRA-ARTICULARES DE CODO POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808302', 'EXTRACCION DE CUERPOS LIBRES INTRA-ARTICULARES DE  MUÑECA POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808602', 'EXTRACCION DE CUERPOS LIBRES INTRA-ARTICULARES DE RODILLA POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808702', 'EXTRACCION DE CUERPOS LIBRES INTRA-ARTICULARES DE TOBILLO POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808052', 'LAVADO Y/O DESBRIDAMIENTO DE PELVIS POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808502', 'EXTRACCION DE CUERPOS LIBRES INTRA-ARTICULARES DE PELVIS POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808402', 'EXTRACCION DE CUERPOS LIBRES INTRA-ARTICULARES DE FALANGES (UNA O MAS) POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808802', 'EXTRACCION DE CUERPOS LIBRES INTRA-ARTICULARES DE PIE O ARTEJOS (UNO O MAS) POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814708', 'FIJACION DE LA  RODILLA POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '791401', 'FIJACION INTERNA DE FRACTURA E INESTABILIDAD DE MUÑECA POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814709', 'FIJACION E INJERTO OSEO DE LA RODILLA POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814723', 'LIBERACION DE ADHERENCIAS DE RODILLA POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '806103', 'MENISCECTOMIA MEDIAL O LATERAL POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '806104', 'MENISCECTOMIA MEDIAL Y LATERAL POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814724', 'REMODELACION DE MENISCO MEDIAL Y LATERAL POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '805103', 'DISCECTOMIA O MICRODISCECTOMIA  ENDOSCOPICA O TRANSARTROSCOPICA  CERVICAL         (100)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '805125', 'DISCECTOMIA ENDOSCOPICA O TRANSARTROSCOPICA  TORACICA    (100)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '805135', 'DISCECTOMIA ENDOSCOPICA O TRANSARTROSCOPICA  LUMBAR    (100)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '799602', 'REDUCCION DE LAS FRACTURAS INTRAARTICULARES DE RODILLA CON FIJACION INTERNA POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '799710', 'REDUCCION  CON FIJACION  DE LAS FRACTURAS DE TOBILLO POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814504', 'RECONSTRUCCION DE LIGAMENTO CRUZADO ANTERIOR CON INJERTO AUTOLOGO O CON ALOINJERTO POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814505', 'RECONSTRUCCION DE LIGAMENTO CRUZADO POSTERIOR CON INJERTO AUTOLOGO O  CON ALOINJERTO POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814706', 'RELAJACION DE RETINACULO LATERAL POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814707', 'RELAJACION DE RETINACULO LATERAL MAS REALINEACION DISTAL O PROXIMAL POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814705', 'RELAJACION DE RETINACULO LATERAL, MAS OSTEOTOMIA DE REALINEACION, MAS PLICATURA DE RETINACULO MEDIAL  POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808114', 'REMOCION DE EXOSTOSIS DE  HOMBRO POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808204', 'REMOCION DE PLICAS DE CODO POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814726', 'REMODELACION DE MENISCO ROTO (PICO DE LORO) POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814904', 'REPARACION DE LIGAMENTO PERONEO ASTRAGALINO ANTERIOR POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808062', 'LAVADO Y/O DESBRIDAMIENTO DE RODILLA POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808012', 'DESBRIDAMIENTO, LAVADO Y LIMPIEZA DE ARTICULACION DE HOMBRO POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '808072', 'DESBRIDAMIENTO, LAVADO Y LIMPIEZA DE TOBILLO POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814905', 'RESECCION DE LESION OSTEOCONDRAL, PERFORACIONES Y/O CURETAJE DE TOBILLO POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '814906', 'RESECCION DE LESION OSTEOCONDRAL CON FIJACION EN TOBILLO POR ARTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '765101', 'MENISCOPEXIA TEMPOROMANDIBULAR POR VIA ENDOSCOPICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '441400', 'BIOPSIA CERRADA [ENDOSCOPICA] DE ESTOMAGO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '451301', 'ESOFAGOGASTRODUODENOSCOPIA (EGD) DIAGNOSTICA O EXPLORATORIA SIN BIOPSIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '451402', 'BIOPSIA ENDOSCOPICA DIRECTA DE INTESTINO DELGADO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '451600', 'ESOFAGOGASTRODUODENOSCOPIA [EGD] CON BIOPSIA CERRADA SOD    (229)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '451302', 'ENTEROSCOPIA O ENDOSCOPIA DE INTESTINO DELGADO DESPUES DE DUODENO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '429201', 'DILATACION ESOFAGICA CON BUJIAS DE MERCURIO    (198)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '429202', 'DILATACION NEUMATICA ENDOSCOPICA CON BALON    (198)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '423302', 'CONTROL ENDOSCOPICO DE HEMORRAGIA O FULGURACION DE MUCOSA ESOFAGICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '423306', 'ABLACION O RESECCION ENDOSCOPICA DE NEOPLASIA ESOFAGICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '429203', 'DILATACION ENDOSCOPICA CONDUCIDA, TIPO EDER-PUESTOW O SAVARY' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '434102', 'CONTROL ENDOSCOPICO DE HEMORRAGIA GASTRICA MEDIANTE ESCLEROTERAPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '434103', 'CONTROL ENDOSCOPICO DE HEMORRAGIA GASTRICA MEDIANTE CORRIENTE BIPOLAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '441301', 'ESOFAGOGASTROSCOPIA (CON EXTRACCION DE CUERPO EXTRAÑO)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '469701', 'EXTRACCION ENDOSCOPICA DE CUERPO EXTRAÑO EN INTESTINO DELGADO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '453001', 'ENTEROSCOPIA CON CONTROL DE HEMORRAGIA O FULGURACION DE LESION EN MUCOSA DUODENAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '451200', 'ENDOSCOPIA DE INTESTINO DELGADO A TRAVES DE ESTOMA ARTIFICIAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '511000', 'COLANGIO-PANCREATOGRAFIA RETROGRADA ENDOSCOPICA (CPRE) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '518500', 'ESFINTERECTOMIA Y PAPILOTOMIA ENDOSCOPICA SOD    (39)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '518801', 'EXTRACCION ENDOSCOPICA DE CALCULOS DE LAS VIAS BILIARES CON ESFINTEROTOMIA     (39)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '521400', 'BIOPSIA CERRADA [ENDOSCOPICA] DE DUCTO PANCREATICO SOD     (39)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '529400', 'EXTRACCION ENDOSCOPICA DE CALCULOS DEL CONDUCTO PANCREATICO SOD     (39)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '529800', 'DILATACION ENDOSCOPICA DE DUCTO PANCREATICO SOD     (39)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '512200', 'FULGURACION DE LESIONES POR COLANGIOPANCREATOGRAFIA ENDOSCOPICA RETROGRADA SOD     (39)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '512104', 'COLECISTECTOMIA POR LAPAROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '482400', 'BIOPSIA CERRADA [ENDOSCOPICA] DE RECTO O SIGMOIDE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '452302', 'COLONOSCOPIA IZQUIERDA CON EQUIPO FLEXIBLE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '451401', 'BIOPSIA CERRADA CON CAPSULA DE INTESTINO DELGADO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '469702', 'EXTRACCION ENDOSCOPICA DE CUERPO EXTRAÑO EN INTESTINO GRUESO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '454203', 'CONTROL ENDOSCOPICO DE HEMORRAGIA DE COLON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '468020', 'DESCOMPRESION ENDOSCOPICA DE DILATACION AGUDA DE COLON (OGILVIE)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '468021', 'DESCOMPRESION ENDOSCOPICA DE VOLVULO DE COLON (SIGMOIDE)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '489400', 'DILATACION INSTRUMENTAL  ENDOSCOPICA  DE RECTO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '502103', 'DRENAJE DE  LESION HEPATICA POR LAPAROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '651202', 'BIOPSIA  EN OVARIO  POR LAPAROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '661120', 'BIOPSIA DE TROMPAS DE FALOPIO POR LAPAROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '681500', 'BIOPSIA ENDOSCOPICA DE LIGAMENTOS DE UTERO, SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '681610', 'BIOPSIA DE MIOMETRIO POR LAPAROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '563510', 'ENDOSCOPIA (CISTOSCOPIA) DEL CONDUCTO ILEAL    (40)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '563520', 'ENDOSCOPIA (CISTOSCOPIA) DEL CONDUCTO COLONICO    (40)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '573201', 'CISTOSCOPIA TRANSURETRAL    (40)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '573301', 'BIOPSIA UNICA O SIMPLE DE VEJIGA POR CISTOSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '573302', 'BIOPSIA MULTIPLE O MAPEO VESICAL POR CISTOSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '552320', 'BIOPSIA ENDOSCOPICA DE RIÑON' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '598001', 'CATETERISMO URETERAL DE AUTORETENCION VIA ENDOSCOPICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '570500', 'HEMOSTASIA VESICAL TRANSURETRAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '574100', 'ABLACION TRANSURETRAL DE ADHERENCIAS INTRALUMINALES VESICALES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '569002', 'DILATACION URETERAL VIA ENDOSCOPICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '574202', 'FULGURACION ENDOSCOPICA DE LESION VESICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '599500', 'LITIASIS URINARIA FRAGMENTADA INTRACORPOREA ENDOSCOPICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '579302', 'CONTROL DE HEMORRAGIA (POSTQUIRURGICA) DE VEJIGA VIA ENDOSCOPICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '981905', 'EXTRACCION ENDOSCOPICA DE CUERPO EXTRAÑO DE URETRA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '579101', 'ESFINTEROTOMIA VESICAL CERRADA [ENDOSCOPICA]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '583102', 'ESCISION ENDOSCOPICA DE VALVA CONGENITA  DE URETRA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '583101', 'FULGURACION ENDOSCOPICA  DE LESIONES URETRALES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '600112', 'DRENAJE DE COLECCION EN PROSTATA VIA ENDOSCOPICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '609402', 'CONTROL DE HEMORRAGIA PROSTATICA VIA CISTOSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '753100', 'AMNIOSCOPIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '681200', 'HISTEROSCOPIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '681611', 'BIOPSIA DE ENDOMETRIO Y LESION ENDOMETRIAL POR HISTEROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '702201', 'COLPOSCOPIA CON BIOPSIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '662100', 'ABLACION U OCLUSION DE TROMPA DE FALOPIO UNICA VIA ENDOSCOPICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '662200', 'ABLACION U OCLUSION BILATERAL DE TROMPA DE FALOPIO VIA ENDOSCOPICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '549202', 'EXTRACCION DE CUERPO EXTRAÑO INTRAPERITONEAL ( O DIU PERDIDO), POR LAPAROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '652902', 'LIBERACION O LISIS DE ADHERENCIAS (LEVES, MODERADAS O SEVERAS) DE OVARIO POR LAPAROSCOPIA   (24)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '669902', 'LIBERACION O LISIS DE ADHERENCIAS (LEVES, MODERADAS O SEVERAS) DE OVARIO Y TROMPAS DE FALOPIO POR LAPAROSCOPIA    (24)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '691201', 'ESCISION Y ABLACION DE  ENDOMETROSIS ESTADOS I Y II   POR LAPAROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '691202', 'ESCISION Y ABLACION DE  ENDOMETROSIS ESTADOS III Y IV  POR LAPAROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '652102', 'CISTECTOMIA DE OVARIO POR LAPAROSCOPIA   (24)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '682403', 'MIOMECTOMIA UTERINA ( UNICA O MULTIPLE) POR LAPAROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '694102', 'HISTERORRAFIA POR LAPAROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '659120', 'ASPIRACION FOLICULAR DE  OVARIO POR LAPAROSCOPIA    (24)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '691902', 'DRENAJE DE  COLECCION DE LIGAMENTO ANCHO VIA ENDOSCOPICA    (24)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '707703', 'COLPOPEXIA POR LAPAROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '684020', 'HISTERECTOMIA TOTAL POR LAPAROSCOPIA    (160)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '698102', 'EXTRACCION DE CUERPO EXTRAÑO INTRAUTERINO POR HISTEROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '682102', 'LIBERACION DE ADHERENCIAS INTRALUMINALES DE UTERO POR HISTEROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '682202', 'INCISION O ESCISION DE TABIQUE CONGENITO UTERINO POR HISTEROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '682404', 'MIOMECTOMIA UTERINA POR HISTEROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '682510', 'ABLACION ENDOMETRIAL O ENDOMETRECTOMIA POR HISTEROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '71400', 'BIOPSIA DE HIPOFISIS POR VIA TRANSESFENOIDAL SOD    (134)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '37300', 'DERIVACION LUMBO PERITONEAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '934100', 'TRACCION ESPINAL CON EMPLEO DE DISPOSITIVO CRANEAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '28400', 'COLOCACION DE TRACTOR CEFALICO [HALO CHALECO] SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '28500', 'EXTRACCION DE TRACTOR CEFALICO [HALO CHALECO] SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '36100', 'LISIS O RESECCION DE ADHERENCIAS EXTRADURALES EN MEDULA ESPINAL Y RAICES DE NERVIOS ESPINALES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '32301', 'LESION DE TRACTOS DE ENTRADA DE RAICES POSTERIORES  ( DREZ ), POR RADIOFRECUENCIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '32400', 'MIELOTOMIA ABIERTA SOD *' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '39400', 'RETIRO DE ELECTRODOS Y/O RECEPTOR DE NEUROESTIMULACION ESPINAL SOD *' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '47101', 'REPARACION DE NERVIO FACIAL, POR INTERPOSICION DE NERVIO HIPOGLOSO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '55100', 'EXPLORACION DE PLEJO O TRONCO (CERVICAL, LUMBAR O SACRO) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'M01620', 'RESECCION DE TUMOR DE PLEJO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '82600', 'TARSECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '140000', 'EXTRACCION DE CUERPO EXTRAÑO DEL SEGMENTO POSTERIOR DEL OJO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'M02605', 'QUERATECTOMIA CON EXCIMER LASER (FOTORREFRACTIVA O FOTOTERAPEUTICA )' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '115100', 'SUTURA DE CORNEA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '126400', 'TRABECULECTOMIA PRIMARIA SOD   (67)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '126700', 'INSERCION DE IMPLANTE PARA GLAUCOMA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '143101', 'REPARACION DE DESGARRO RETINAL (RETINOPEXIA) POR DIATERMIA O CRIOTERAPIA   (68)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '142300', 'ABLACION DE LESION CORIORETINAL, POR FOTOCOAGULACION (LASER) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '143300', 'REPARACION DE DESGARRO RETINAL POR FOTOCOAGULACION (LASER) SOD    (68)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '137200', 'IMPLANTE DE LENTE  INTRAOCULAR SECUNDARIO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '180300', 'EXTRACCION DE CUERPO EXTRAÑO DE CONDUCTO AUDITIVO EXTERNO, CON INCISION SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '183104', 'AURICULECTOMIA  (PARCIAL O TOTAL ) CON RESECCION PARCIAL O TOTAL DEL HUESO TEMPORAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '186200', 'RECONSTRUCCION DE MEATO AUDITIVO EXTERNO SOD    (70)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '187100', 'RECONSTRUCCION DE PABELLON AURICULAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '200101', 'TIMPANOSTOMIA CON DRENAJE DE MEMBRANA TIMPANICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '194101', 'TIMPANOPLASTIA TIPO I (CIERRE DE PERFORACION)   (71)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '194102', 'TIMPANOPLASTIA TIPO II (CON RECONSTRUCCION DE CADENA OSEA: MARTILLO, YUNQUE Y/O ESTRIBO U OSICULOPLASTIA)    (71)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '191100', 'ESTAPEDECTOMIA O ESTAPEDOTOMIA CON COLOCACION DE PROTESIS SOD    (71)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '192100', 'REVISION DE ESTAPEDECTOMIA O ESTAPEDOTOMIA SOD    (71)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '199100', 'CIERRE DE FISTULA PERILINFATICA DE OIDO MEDIO SOD    (71)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '205900', 'PETROSECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '209600', 'IMPLANTACION O SUSTITUCION DE PROTESIS COCLEAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '209601', 'INSERCION DE PROTESIS COCLEAR DE CANAL UNICO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '209602', 'INSERCION DE PROTESIS COCLEAR DE CANAL MULTIPLE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '215101', 'RESECCION DE  TUMOR BENIGNO DE CAVUM  CON EXTENSION INTRACRANEANA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '215103', 'RESECCION DE TUMOR MALIGNO DE CAVUM, VIA TRANSPALATINA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '213101', 'RESECCION  DE TUMOR BENIGNO DE FOSA NASAL, VIA TRANSNASAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '218701', 'TURBINOPLASTIA VIA TRANSNASAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '218702', 'TURBINOPLASTIA ENDOSCOPICA VIA TRANSNASAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '218801', 'SEPTORRINOPLASTIA FUNCIONAL PRIMARIA NCOC **' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '218401', 'SEPTORRINOPLASTIA FUNCIONAL SECUNDARIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '218100', 'SUTURA DE LACERACION EN NARIZ SOD    (181)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '217100', 'REDUCCION CERRADA DE FRACTURA NASAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '217200', 'REDUCCION ABIERTA DE  FRACTURA NASAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '210400', 'CONTROL DE EPISTAXIS, POR LIGADURA DE ARTERIAS ETMOIDALES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '210500', 'CONTROL DE EPISTAXIS, POR LIGADURA DE ARTERIA MAXILAR INTERNA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '210800', 'CONTROL DE EPISTAXIS, POR LIGADURA DE ARTERIA ESFENOPALATINA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '218903', 'CORRECCION DE ATRESIA DE COANAS, VIA TRANSPALATINA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '210900', 'CONTROL DE EPISTAXIS POR DERMOPLASTIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '213100', 'ESCISION LOCAL O ABLACION DE LESION INTRANASAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '226308', 'MAXILOETMOIDECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '222101', 'ANTROSTOMIA MAXILAR INTRANASAL VIA MEATO INFERIOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '223100', 'ANTROTOMIA MAXILAR RADICAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '226100', 'ESCISION DE LESION DE SENO MAXILAR CON ABORDAJE CADWELL-LUC SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '213104', 'RESECCION DE TUMOR MALIGNO DE FOSA NASAL, VIA CRANEOFACIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '213105', 'RESECCION DE TUMOR MALIGNO DE FOSA NASAL, VIA TRANSORBITARIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '213106', 'RESECCION DE TUMOR MALIGNO DE FOSA NASAL, POR RINOTOMIA LATERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '213107', 'RESECCION DE TUMOR MALIGNO DE FOSA NASAL, POR DESPEGAMIENTO FACIAL VIA SUBLABIAL [DEGLOVIN]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '226202', 'RESECCION DE LESION MALIGNA EN SENO MAXILAR, POR MAXILECTOMIA SUPERIOR,  PARCIAL O MEDIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '225300', 'INCISION DE MULTIPLES SENOS PARANASALES SOD    (184)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '226400', 'ESFENOIDECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '313102', 'EXTRACCION DE CUERPO EXTRAÑO  DE LARINGE VIA LARINGOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '311100', 'CRICOTIROTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '311200', 'TRAQUEOTOMIA TEMPORALSOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '311300', 'TRAQUEOSTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '317400', 'REVISION  DE TRAQUEOSTOMIA SOD    (197)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '311400', 'PUNCION (ASPIRACION) TRANSTRAQUEAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '315101', 'RESECCION ENDOSCOPICA ( RIGIDA O DE FIBRA OPTICA ) DE LESION EN TRAQUEA CON PINZA DE BIOPSIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '301101', 'HEMILARINGECTOMIA HORIZONTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '301102', 'HEMILARINGECTOMIA VERTICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '301103', 'HEMILARINGECTOMIA VERTICAL AMPLIADA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '301200', 'EPIGLOTIDECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '301400', 'CORDECTOMIA VOCAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '303200', 'LARINGECTOMIA TOTAL (DISECCION EN BLOQUE DE LARINGE) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '319300', 'INSERCION DE MOLDE (PROTESIS O STENT) LARINGEO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '319401', 'EXTRACCION DE MOLDE (PROTESIS O STENT) LARINGEO VIA EXTERNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '303101', 'LARINGOFARINGECTOMIA CON RECONSTRUCCION CON COLGAJO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '316401', 'REDUCCION ABIERTA DE FRACTURA LARINGEA CON SUTURA Y/O ALAMBRE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '316402', 'REDUCCION ABIERTA DE FRACTURA LARINGEA CON MINIPLACAS DE FIJACION INTERNA [DISPOSITIVOS DE FIJACION U OSTEOSINTESIS]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '316403', 'REDUCCION DE LUXACION DE ARITENOIDES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '316501', 'ARITENOPEXIA VIA EXTERNA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '316503', 'ARITENOPLASTIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '317501', 'RECONSTRUCCION TRAQUEAL O  LARINGOTRAQUEAL TERMINOTERMINAL    (372)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '316201', 'FISTULECTOMIA LARINGOTRAQUEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '317300', 'CIERRE DE FISTULA TRAQUEOESOFAGICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '319100', 'DILATACION DE LA LARINGE SOD    (198)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '310101', 'INYECCION ENDOSCOPICA EN PLIEGUE VOCAL LATERAL CON TEJIDO AUTOLOGO (GRASA)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '310102', 'INYECCION ENDOSCOPICA EN PLIEGUE VOCAL LATERAL CON MATERIAL INERTE (COLAGENO,TEFLON O GELFOAM)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '316700', 'REINERVACION DE LARINGE CON PEDICULO NEUROMUSCULAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '317100', 'SUTURA DE LACERACION DE TRAQUEA (TRAQUEORRAFIA) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '282100', 'AMIGDALECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '286100', 'ADENOIDECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '284100', 'RESECCION DE RESTOS ADENOAMIGDALINOS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '280200', 'DRENAJE TRANSORAL EN  AMIGDALA Y ESTRUCTURAS PERIAMIGDALARES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '293200', 'DIVERCULECTOMIA FARINGEA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '295301', 'FISTULECTOMIA FARINGEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '285101', 'RESECCION DE AMIGDALA LINGUAL, BANDAS FARINGEAS LATERALES Y MEMBRANA CONGENITA CON ELECTROFULGURACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '295400', 'LISIS DE ADHERENCIAS FARINGEAS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '293301', 'RESECCION DE TUMOR BENIGNO DE OROFARINGE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '315000', 'RESECCION ABIERTA DE  LESION DE TRAQUEA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '293302', 'RESECCION DE TUMOR MALIGNO DE OROFARINGE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '293303', 'RESECCION DE LESIONES DE FARINGE CON LASER' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '294100', 'CORRECCION DE ATRESIA NASOFARINGEA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '294200', 'CORRECCION DE ESTENOSIS NASOFARINGEA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '290200', 'FARINGOSTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '60200', 'REAPERTURA DE HERIDA DE AREA TIROIDEA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '69100', 'REEXPLORACION DE CUELLO Y MEDIASTINO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '280100', 'DRENAJE TRANSCERVICAL EN AMIGDALA Y ESTRUCTURAS PERIAMIGDALARES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '63902', 'TIROIDECTOMIA RESIDUAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '63903', 'TIROIDECTOMIA SUBTOTAL (LOBECTOMIA TIROIDEA PARCIAL DE AMBOS LOBULOS O TOTAL DE UNO Y PARCIAL DE OTRO)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '65100', 'TIROIDECTOMIA RETROESTERNAL PARCIAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '64100', 'TIROIDECTOMIA TOTAL  SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '65200', 'TIROIDECTOMIA RETROESTERNAL TOTAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '67000', 'RESECCION  DE CONDUCTO TIROGLOSO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '67100', 'RESECCION  DE QUISTE TIROGLOSO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '67200', 'RESECCION  DE FISTULA TIROGLOSA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '68100', 'PARATIROIDECTOMIA TOTAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '68900', 'PARATIROIDECTOMIA  PARCIAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '296301', 'RESECCION RADICAL DE OROFARINGE (TEJIDOS BLANDOS Y DUROS) POR TUMOR [OPERACION DE MONOBLOQUE]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383900', 'RESECCION CON ANASTOMOSIS DE VENAS DE MIEMBROS INFERIORES SOD    (8)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '384900', 'RESECCION CON SUSTITUCION DE VENAS DE MIEMBROS INFERIORES SOD     (9)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '392800', 'DERIVACION O PUENTES EN VASOS PERIFERICOS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '404100', 'VACIAMIENTO   LINFATICO RADICAL DE CUELLO, UNILATERAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '404200', 'VACIAMIENTO LINFATICO RADICAL  DE CUELLO, BILATERAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '404301', 'VACIAMIENTO LINFATICO RADICAL MODIFICADO DE CUELLO, UNILATERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '404302', 'VACIAMIENTO LINFATICO RADICAL MODIFICADO DE CUELLO, BILATERAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '402300', 'ESCISION DE GANGLIO LINFATICO AXILAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '405500', 'ESCISION RADICAL DE GANGLIOS LINFATICOS RETROPERITONEALES SOD    (157)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '380700', 'TROMBOEMBOLECTOMIA DE VENAS ABDOMINALES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '381500', 'ENDARTERECTOMIA DE VASOS TORACICOS SOD   (76)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '385500', 'OCLUSION, PINZAMIENTO  O LIGADURA DE VASOS TORACICOS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '383403', 'RECONSTRUCION DE AORTA TORACICA DESCENDENTE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '358301', 'CIERRE DE DUCTUS ARTERIOSO PERSITENTE  POR TORACOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '359700', 'CERCLAJE DE ARTERIA PULMONAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '373301', 'ESCISION DE TUMOR DEL CORAZON   (75)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '373100', 'PERICARDIECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '371200', 'PERICARDIOTOMIA SOD   (144)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '352600', 'REEMPLAZO DE LA VALVULA AORTICA Y AORTA ASCENDENTE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '351200', 'COMISUROTOMIA, VALVULOTOMIA O VALVULOPLASTIA  MITRAL VIA ABIERTA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '352200', 'REEMPLAZO DE VALVULA MITRAL CON PROTESIS O BIOPROTESIS (AUTOLOGA O HETEROLOGA) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '351400', 'COMISUROTOMIA, VALVULOTOMIA O VALVULOPLASTIA TRICUSPIDEA VIA ABIERTA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '356100', 'ATRIOSEPTOPLASTIA CON INJERTO (PARCHE) DE TEJIDO SOD *' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '355201', 'REPARACION DE DEFECTO DE TABIQUE INTERVENTRICULAR CON PROTESIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '361501', 'ANASTOMOSIS  SIMPLE O SECUENCIAL DE ARTERIA MAMARIA-ARTERIA CORONARIA, POR ESTERNOTOMIA O TORACOTOMIA    (13)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '361701', 'ANASTOMOSIS CORONARIA PARA REVASCULARIZACION CARDIACA DE UNO O MAS VASOS CON VENA SAFENA POR ESTERNOTOMIA O TORACOTOMIA    (13)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '363200', 'REINTERVENCION DE REVASCULARIZACION  CARDIACA ( DERIVACION O PUENTES CORONARIOS) SOD *    (145)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '377401', 'INSERCION O SUSTITUCION DE ELECTRODO EPICARDICO POR ESTERNOTOMIA O TORACOTOMIA    (146)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '373302', 'RESECCION ABIERTA DE  HACES ANOMALOS DEL SISTEMA DE CONDUCCION    (75)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '379401', 'IMPLANTACION DE CARDIOVERSOR/DESFIBRILADOR  POR VIA INFRACLAVICULAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '342000', 'TORACENTESIS DIAGNOSTICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '340101', 'TORACENTESIS DE DRENAJE O DESCOMPRESIVA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '340400', 'TORACOSTOMIA PARA DRENAJE CERRADO [TUBO DE TORAX] SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '340200', 'TORACOTOMIA EXPLORATORIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '343401', 'EXTRACCION DE CUERPO EXTRAÑO DE MEDIASTINO POR TORACOTOMIA CON O SIN RESECCION DE COSTILLA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '344300', 'EXTRACCION DE CUERPO EXTRAÑO EN PARED TORACICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '340300', 'TORACOSTOMIA ABIERTA CON RESECCION COSTAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '347001', 'RECONSTRUCCION DE PARED TORACICA ANTERIOR CON COLGAJO (MUSCULAR O DE EPIPLON)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '347200', 'CIERRE DE TORACOSTOMIA ABIERTA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '347600', 'TORACOPLASTIA EXTRAPLEURAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '347300', 'TORACOPLASTIA CON CIERRE DE FISTULA BRONCOPLEURAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '347000', 'RECONSTRUCCION DE LA PARED TORACICA  SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '347500', 'REPARACION DE DEFORMIDAD DE PECTUS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '343201', 'RESECCION DE QUISTE O TUMOR BENIGNO DEL MEDIASTINO  POR TORACOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '343301', 'RESECCION DE  TUMOR MALIGNO  DEL MEDIASTINO POR TORACOTOMIA    (15)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '343302', 'RESECCION DE  TUMOR MALIGNO  DEL MEDIASTINO POR ESTERNOTOMIA    (15)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '78100', 'ESCISION PARCIAL DE TIMO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '347801', 'RECONSTRUCCION DEL ESTERNON  CON  INTERPOSICION DE MUSCULOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '347802', 'RECONSTRUCCION DEL ESTERNON CON INTERPOSICION DE PROTESIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '347005', 'RECONSTRUCCION DE LA PARED TORACICA CON PROTESIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '325300', 'NEUMONECTOMIA CON DECORTICACION CONCOMITANTE (PLEURONEUMONECTOMIA) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '334301', 'NEUMORRAFIA SIMPLE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '335100', 'TRASPLANTE UNILATERAL DE PULMON SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '335200', 'TRASPLANTE BILATERAL DE PULMON SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '325400', 'OBTENCION PULMONAR PARA TRANSPLANTE SOD    (185)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '336100', 'TRANSPLANTE DE PULMON CORAZON SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '325500', 'OBTENCION DE CORAZON-PULMON SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '427100', 'ESOFAGOTOMIA CERVICAL CON MIOTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '421100', 'ESOFAGOSTOMIA CON  MIOTOMIA CERVICAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '423201', 'RESECCION DE TUMOR DE ESOFAGO  VIA  CERVICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '425502', 'RECONSTRUCCION ESOFAGICA INTRATORACICA CON INTERPOSICION DE COLON VIA TORACOABDOMINAL Y CERVICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '425501', 'RECONSTRUCCION ESOFAGICA INTRATORACICA CON INTERPOSICION DE COLON VIA ABDOMINAL Y CERVICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '530100', 'HERNIORRAFIA INGUINAL DIRECTA SOD   (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '530200', 'HERNIORRAFIA INGUINAL INDIRECTA SOD    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '530500', 'HERNIORRAFIA INGUINAL CON INJERTO O  PROTESIS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '531100', 'REPARACION BILATERAL DE HERNIA INGUINAL DIRECTA SOD    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '531200', 'REPARACION BILATERAL DE HERNIA INGUINAL INDIRECTA SOD    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '531300', 'REPARACION BILATERAL DE HERNIA INGUINAL CON  UNA PROTESIS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '531400', 'REPARACION BILATERAL DE HERNIA INGUINAL CON DOS PROTESIS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '535202', 'HERNIORRAFIA EPIGASTRICA REPRODUCIDA    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '535203', 'HERNIORRAFIA EPIGASTRICA  CON PROTESIS    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '534200', 'HERNIORRAFIA UMBILICAL CON PROTESIS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '535100', 'REPARACION DE HERNIA INCISIONAL (EVENTRACION) SOD    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '536200', 'HERNIORRAFIA ISQUIATICA SOD    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '536300', 'HERNIORRAFIA ISQUIORRECTAL SOD    (84)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '541200', 'LAPAROTOMIA EXPLORATORIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '540000', 'DRENAJE DE COLECCION  EN PARED ABDOMINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '543100', 'ESCISION DE TUMOR BENIGNO EN LA PARED ABDOMINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '542500', 'LAVADO PERITONEAL DIAGNOSTICO SOD    (186)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '541301', 'DRENAJE DE COLECCION  INTRAPERITONEAL(EPIPLOICO, OMENTAL,PERIESPLENICO, PERIGASTRICO, SUBHEPATICO, DE LA FOSA ILIACA O PLASTRON APENDICULAR)  POR LAPAROTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '541502', 'RESECCION DE TUMOR RETROPERITONEAL CON VACIAMIENTO GANGLIONAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '544101', 'OMENTECTOMIA PARCIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '544102', 'OMENTECTOMIA TOTAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '545000', 'LISIS DE ADHERENCIAS PERITONEALES  POR LAPAROTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '502203', 'HEPATECTOMIA DERECHA O IZQUIERDA    (81)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '518100', 'DILATACION DEL ESFINTER DE ODDI SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '516200', 'ESCISION DE LA AMPOLLA DE VATER (AMPULECTOMIA), CON REIMPLANTACION DE COLEDOCO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '513000', 'ANASTOMOSIS DE VESICULA BILIAR O VIA BILIAR  CON PANCREATOGRAFIA RETROGRADA ENDOSCOPICA (ERP) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '513100', 'ANASTOMOSIS DE VESICULA BILIAR A CONDUCTOS HEPATICOS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '513300', 'ANASTOMOSIS DE VESICULA BILIAR A PANCREAS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '513400', 'ANASTOMOSIS DE VESICULA BILIAR A ESTOMAGO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '513700', 'ANASTOMOSIS DE CONDUCTO HEPATICO A TUBO DIGESTIVO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '514100', 'EXPLORACION DEL CONDUCTO BILIAR PRINCIPAL PARA EXTRACCION DE CUERPO EXTRAÑO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '514500', 'EXPLORACION DE VIA HEPATO BILIAR COMUN SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '510000', 'COLECISTOTOMIA Y COLECISTOSTOMIA CON EXTRACCION DE CALCULOS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '512103', 'COLECISTECTOMIA CON EXPLORACION DE VIAS BILIARES POR COLEDOCOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '512102', 'COLECISTECTOMIA POR MINILAPAROTOMIA SUBXIFOIDEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '526100', 'PANCREATECTOMIA TOTAL POR NECIDIOBLASTOSIS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '70000', 'EXPLORACION DE AREA SUPRARENAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '74100', 'DRENAJE DE GLANDULA SUPRARRENAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '415100', 'ESPLENECTOMIA TOTAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '416100', 'ESPLENORRAFIA SOD    (369)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '438200', 'GASTRECTOMIA PARCIAL, CON RECONSTRUCCION CON O SIN VAGOTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '439100', 'GASTRECTOMIA TOTAL CON INTERPOSICION INTESTINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '436100', 'GASTRODUODENOSTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '460102', 'YEYUNOSTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '460301', 'EXTERIORIZACION DE INTESTINO GRUESO- CECOSTOMIA, COLOSTOMIA EN ASA O SIGMOIDOSTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '461200', 'COLOSTOMIA PERMANENTE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '453301', 'RESECCION INTESTINAL DE DIVERTICULOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '453302', 'RESECCION INTESTINAL DE TUMOR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '458000', 'COLECTOMIA TOTAL  CON RESECCION DE ILEOTERMINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '458300', 'COLECTOMIA TOTAL CON ANASTOMOSIS PELVICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '471100', 'APENDICECTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '471200', 'APENDICECTOMIA POR PERFORACION, CON DRENAJE DE ABCESO, LIBERACION DE PLASTRON Y/O DRENAJE DE PERITONITIS LOCALIZADA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '471300', 'APENDICECTOMIA CON DRENAJE DE PERITONITIS GENERALIZADA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '465200', 'CIERRE DE ESTOMA DE INTESTINO GRUESO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '468000', 'CORRECCION DE MALROTACION INTESTINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '456100', 'RESECCION SEGMENTARIA MULTIPLE DE INTESTINO DELGADO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '483100', 'ABLACION DE LESION O TEJIDO RECTAL POR DIATERMIA, CRIO O ELECTROCOAGULACION SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '486400', 'RESECCION POSTERIOR DE RECTO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '485301', 'PROCTOSIGMOIDECTOMIA CON COLOSTOMIA CON ABORDAJE PERINEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '486101', 'RESECCION DE TUMOR RECTAL POR PROCTECTOMIA TRANS-SACRA O TRANS-COCCIGEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '486800', 'RESECCION RECTO CON RECONSTRUCCION TIPO PULL-THROUGH SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '490200', 'DRENAJE DE ABSCESO PERIANAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '495100', 'ESFINTEROTOMIA ANAL LATERAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '495200', 'ESFINTEROTOMIA ANAL POSTERIOR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '490400', 'ESCISION DE LESION O  TEJIDO PERIANAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '494602', 'ESCISION DE HEMORROIDES EXTERNAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '494601', 'ESCISION DE HEMORROIDES INTERNAS   (20)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '497100', 'SUTURA DE LACERACION O DESGARRO DE ANO SOD   (80)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '495300', 'ESFINTEROTOMIA ANAL CON COLOSTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '497200', 'IMPLANTACION DE UN ANILLO EN LA CIRCUNFERENCIA ANAL (CERCLAJE) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'M08260', 'DRENAJE DE QUISTE PILONIDAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'M08250', 'DILATACION ESFINTER DE ANO (SESION)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '565600', 'URETEROENTEROSTOMIA CUTANEA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'M09320', 'DIVERTICULECTOMIA DE VEJIGA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '578900', 'CISTOPEXIA (SUSPENSION VESICAL) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '595100', 'SUSPENSION URETRAL RETROPUBICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '578100', 'SUTURA DE LACERACION O DESGARRO  VESICAL (CISTORRAFIA) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '580100', 'URETROSTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '585000', 'URETROLISIS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '605100', 'PROSTATECTOMIA RADICAL (PROSTATOVESICULECTOMIA) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '584500', 'REPARACION DE EPISPADIAS O HIPOSPADIAS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '852500', 'ESCISION DE PEZON SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '854100', 'MASTECTOMIA  SIMPLE UNILATERAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '854200', 'MASTECTOMIA SIMPLE TOTAL BILATERAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '854400', 'MASTECTOMIA SIMPLE AMPLIADA BILATERAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '853301', 'MASTECTOMIA SUBCUTANEA CON RECONSTRUCCION SIMULTANEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '854600', 'MASTECTOMIA RADICAL BILATERAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '854800', 'MASTECTOMIA RADICAL AMPLIADA BILATERAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '853101', 'MAMOPLASTIA DE REDUCCION POR GINECOMASTIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '852200', 'RESECCION DE CUADRANTE DE MAMA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'M11101', 'OOFOROSTOMIA; INCLUYE DRENAJE DE ABSCESO O QUISTE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'M11212', 'SALPINGOPLASTIA  O  SALPINGONEOSTOMIA,  POR MICROCIRUGIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '682300', 'RESECCION DE POLIPO ENDOMETRIAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '684000', 'HISTERECTOMIA  TOTAL ABDOMINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '683100', 'HISTERECTOMIA  SUBTOTAL O SUPRACERVICAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '685100', 'HISTERECTOMIA VAGINAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '708100', 'OBLITERACION Y ESCISION LOCAL DE VAGINA (COLPOCLEISIS) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '772100', 'OSTEOTOMIA EN CLAVICULA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780200', 'INJERTO  OSEO EN HUMERO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '790300', 'REDUCCION CERRADA DE FRACTURA SIN FIJACION INTERNA DE CUBITO O RADIO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '790903', 'REDUCCION CERRADA DE FRACTURA SIN FIJACION INTERNA DE  FRACTURA DE HUESOS PELVIANOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '791902', 'REDUCCION CERRADA DE FRACTURA CON FIJACION INTERNA DE  HUESOS PELVIANOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '792903', 'REDUCCION ABIERTA DE FRACTURA SIN FIJACION INTERNA DE  HUESOS PELVIANOS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780500', 'INJERTO OSEO EN FEMUR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '780700', 'INJERTO OSEO EN TIBIA O PERONE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '790907', 'REDUCCION CERRADA DE FRACTURA DE COLUMNA CERVICAL E INMOVILIZACION CON THOMAS, PHILADELPHIA U OTROS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '790908', 'REDUCCION CERRADA FRACTURA COLUMNA VERTEBRAL [DORSAL O LUMBAR] E INMOVILIZACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '790909', 'REDUCCION CERRADA O MANIPULACION DE FRACTURA DE SACRO O COCCIX' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '790906', 'REDUCCION CERRADA DE FRACTURA CERVICAL E INMOVILIZACION CON HALOYESO O HALOCHAQUETA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810104', 'ARTRODESIS OCCIPITOCERVICAL VIA POSTERIOR  CON  INSTRUMENTACION SIMPLE  (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810105', 'ARTRODESIS OCCIPITOCERVICAL VIA POSTERIOR  CON  INSTRUMENTACION MODULAR    (102)  (103)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810502', 'ARTRODESIS DE LA REGION TORACICA O TORACOLUMBAR,  TECNICA POSTERIOR O POSTEROLATERAL  CON INSTRUMENTACION SIMPLE    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810503', 'ARTRODESIS DE LA REGION TORACICA O TORACOLUMBAR,  TECNICA POSTERIOR O POSTEROLATERAL  CON INSTRUMENTACION MODULAR    (102)  (103)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810802', 'ARTRODESIS DE LA REGION LUMBAR O LUMBOSACRA,  TECNICA POSTERIOR O POSTEROLATERAL  CON  INSTRUMENTACION SIMPLE    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810803', 'ARTRODESIS DE LA REGION LUMBAR O LUMBOSACRA,  TECNICA POSTERIOR O POSTEROLATERAL  CON  INSTRUMENTACION MODULAR     (102)  (103)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810201', 'ARTRODESIS DE NIVEL C2 O POR DEBAJO,  TECNICA ANTERIOR (INTERSOMATICA) O ANTEROLATERAL SIN INSTRUMENTACION    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810202', 'ARTRODESIS DE NIVEL C2 O POR DEBAJO, TECNICA ANTERIOR (INTERSOMATICA) O ANTEROLATERAL CON INSTRUMENTACION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810402', 'ARTRODESIS DE LA REGION TORACICA O TORACOLUMBAR,  TECNICA ANTERIOR O ANTEROLATERAL (INTERSOMATICA) CON INSTRUMENTACION    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810602', 'ARTRODESIS DE LA REGION LUMBAR O LUMBOSACRA,  TECNICA ANTERIOR O ANTEROLATERAL (INTERSOMATICA) CON INSTRUMENTACION    (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '810702', 'ARTRODESIS O FUSION ESPINAL LUMBAR Y/O  LUMBOSACRA, TECNICA LATERAL INTERTRANSVERSA CON INSTRUMENTACION     (102)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '801801', 'ARTROTOMIA EN PIE SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '835101', 'BURSECTOMIA ABIERTA DE HOMBRO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812400', 'ARTRODESIS DE CODO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '182500', 'TOMA DE INJERTO CONDRAL DE PABELLON AURICULAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '45100', 'INJERTO DE NERVIO PERIFERICO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '45101', 'INJERTO DE NERVIO PERIFERICO A NERVIO FACIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '829900', 'DESBRIDAMIENTO DE  MUSCULO, TENDON Y FASCIA EN MANO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'M14153', 'REDUCCION CERRADA DE LUXOFRACTURA DE BENNET' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'M14154', 'REDUCCION CON PINES DE LUXACION CARPIANA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'M14155', 'REDUCCION CON PINES DE LUXACION CARPOMETACARPIANA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'M14156', 'REDUCCION CON PINES DE LUXACION METACARPOFALANGICA (UNA A DOS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'M14157', 'REDUCCION CON PINES DE LUXACION METACARPOFALANGICA (TRES O MAS)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '812600', 'ARTRODESIS CARPOMETACARPIANA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '858100', 'SUTURA DE HERIDA DE LA MAMA SOD *' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868304', 'REDUCCION DE TEJIDO ADIPOSO DE PARED ABDOMINAL, POR LIPOSUCCION O LIPECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868301', 'REDUCCION DE TEJIDO ADIPOSO EN CARA, POR LIPOSUCCION O LIPECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868303', 'REDUCCION DE TEJIDO ADIPOSO EN AREA SUBMANDIBULAR, POR LIPOSUCCION O LIPECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '868305', 'REDUCCION DE TEJIDO ADIPOSO EN MUSLOS, PELVIS, GLUTEOS O BRAZOS, POR LIPOSUCCION O LIPECTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'M15215', 'SUTURA EN AVULSION TOTAL DE CUERO CABELLUDO (ESCALPE)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '278400', 'CORRECCION DE MACRO O MICROSTOMA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '278301', 'CIERRE VELOFARINGEO CON COLGAJO FARINGEO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '218904', 'RINOQUEILOPLASTIA  (CORRECCION DE SECUELA DE NARIZ FISURADA)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '274100', 'FRENILLECTOMIA LABIAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '867300', 'DIFERIMIENTO DE CUALQUIER COLGAJO (DELAY) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '853100', 'MAMOPLASTIA DE REDUCCION  SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '857200', 'RECONSTRUCCION DE MAMA CON COLGAJO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '857100', 'RECONSTRUCCION DE MAMA CON PROTESIS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '859400', 'EXTRACCION DE IMPLANTE DE MAMA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '269301', 'CATETERIZACION Y SIALOMETRIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '260300', 'DRENAJE DE GLANDULA SALIVAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '262901', 'RESECCION DE MUCOCELE DE GLANDULA SALIVAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '261201', 'BIOPSIA ESCISIONAL DE GLANDULA SALIVAL MENOR (CON CONDUCTO SALIVAL)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '263205', 'SIALOADENECTOMIA  DE GLANDULAS PALATINAS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '262101', 'MARSUPIALIZACION DE LA RANULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '263100', 'SIALOADENECTOMIA PARCIAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '263202', 'PAROTIDECTOMIA TOTAL CONSERVADORA DEL  VII PAR CRANEAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '264200', 'CIERRE O REPARACION DE FISTULA SALIVAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '264201', 'CIERRE O REPARACION DE FISTULA SALIVAL CON INJERTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '260100', 'SIALOLITOTOMIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '264901', 'SIALOPLASTIA (REPARACION DEL CONDUCTO)  CON INJERTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '264902', 'FISTULIZACION DE GLANDULA SALIVAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '271100', 'DRENAJE DE COLECCION DE PALADAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '256301', 'DRENAJE DE COLECCION EN LENGUA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '251000', 'RESECCION DE LESION SUPERFICIAL EN LA LENGUA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '255100', 'SUTURA DE LACERACION DE LENGUA (GLOSORRAFIA) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '255902', 'GLOSOPEXIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '255903', 'PLASTIA DE FRENILLO LINGUAL    (161)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '252000', 'RESECCION DE LENGUA EN CUÑA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '252502', 'HEMIGLOSECTOMIA CON COLGAJO PEDICULADO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '254000', 'GLOSECTOMIA RADICAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'M16231', 'UVULORRAFIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '255901', 'GLOSOPLASTIA CON INJERTO CUTANEO O MUCOSO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '251100', 'RESECCION DE LESION PROFUNDA EN LA LENGUA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '273101', 'ESCISION DE LESION SUPERFICIAL DE PALADAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '276200', 'CORRECCION DE PALADAR FISURADO (ESTAFILORRAFIA) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '241102', 'BIOPSIA ESCISIONAL DE ENCIA  CON CIERRE PRIMARIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '241103', 'BIOPSIA  ESCISIONAL DE ENCIA Y RECUBRIMIENTO CON COLGAJO O INJERTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275303', 'CIERRE DE FISTULA OROSINUSAL U ORONASAL, CON COLGAJO PALATINO, LINGUAL O BUCAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275304', 'CIERRE DE FISTULA OROSINUSAL CON SINUSOTOMIA, CON O SIN REMOCION DE ''CUERPO  EXTRAÑO O COLGAJO PALATINO, LINGUAL O BUCAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'M16315', 'OSTEOTOMIA MANDIBULAR POR SEUDOARTROSIS; INCLUYE CORRECCION DE ANQUILOSIS CON O SIN APLICACION DE PROTESIS' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'M16312', 'MANDIBULECTOMIA PARCIAL CON RECONSTRUCCION' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'b', 'REDUCCION CERRADA LUXACION ARTICULACION TEMPORO MANDIBULAR' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '275900', 'PROFUNDIZACION  DE SURCO VESTIBULAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '767200', 'REDUCCION ABIERTA DE FRACTURA DE ARCO CIGOMATICO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( 'M16550', 'DESCENSO DE AGUJERO MENTONERO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '33200', 'BIOPSIA DE MEDULA ESPINAL O MENINGES ESPINALES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '112200', 'BIOPSIA DE CORNEA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '122400', 'BIOPSIA DE CUERPO CILIAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '122300', 'BIOPSIA DE ESCLEROTICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '122200', 'BIOPSIA DE IRIS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '162200', 'ASPIRACION DIAGNOSTICA DE ORBITA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '162300', 'BIOPSIA DE PARED DE ORBITA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '181101', 'BIOPSIA DE AURICULA (PABELLON AURICULAR)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '181102', 'BIOPSIA DE CONDUCTO AUDITIVO EXTERNO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '221100', 'BIOPSIA CERRADA  [ENDOSCOPICA] [PUNCION CON AGUJA] DE PARED SENO PARANASAL SOD    (228)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '289100', 'BIOPSIA DE AMIGADALAS Y/O VEGETACIONES ADENOIDES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '289101', 'BIOPSIA POR ASPIRACION CON AGUJA FINA DE AMIGDALAS Y ADENOIDES' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '272400', 'BIOPSIA DE PARED DE CAVIDAD BUCAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '272401', 'BIOPSIA POR ASPIRACION CON AGUJA FINA EN CAVIDAD ORAL [BACAF]' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '241101', 'BIOPSIA  INCISIONAL DE ENCIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '261100', 'BIOPSIA CERRADA [PUNCION]  [ASPIRACION CON AGUJA FINA] DE GLANDULA O CONDUCTO  SALIVAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '261200', 'BIOPSIA ABIERTA DE GLANDULA O CONDUCTO SALIVAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '272301', 'BIOPSIA INCISIONAL DE LABIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '272302', 'BIOPSIA ESCISIONAL DE LABIO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '314502', 'BIOPSIA ABIERTA DE LARINGE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '250100', 'BIOPSIA CERRADA [PUNCION] [ASPIRACION CON AGUJA FINA] DE LENGUA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '250201', 'BIOPSIA EN CUÑA O POR TRUCUT DE LENGUA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '250202', 'BIOPSIA INCISIONAL DE LENGUA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '250203', 'BIOPSIA ESCISIONAL DE LENGUA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '272101', 'BIOPSIA DE UVULA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '272102', 'BIOPSIA INCISIONAL DE PALADAR  (36)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '272103', 'BIOPSIA ESCISIONAL DE PALADAR  (36)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '61300', 'BIOPSIA  DE  PARATIROIDES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '61200', 'BIOPSIA ABIERTA DE GLANDULA TIROIDES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '61100', 'BIOPSIA POR ASPIRACION [PERCUTANEA] DE TIROIDES (TRU CUT) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '422500', 'BIOPSIA ABIERTA DE ESOFAGO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '342501', 'BIOPSIA PERCUTANEA  DE ORGANO O TEJIDO DE MEDIASTINO CON AGUJA FINA O CORTANTE  (37)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '342600', 'BIOPSIA ABIERTA DE ORGANO O TEJIDO DE  MEDIASTINO SOD    (37)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '372600', 'BIOPSIA DE PERICARDIO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '342401', 'BIOPSIA DE PLEURA CON AGUJA    (143)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '342300', 'BIOPSIA DE PARED TORACICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '342402', 'BIOPSIA DE PLEURA POR TORACOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '332601', 'BIOPSIA PERCUTANEA DE PULMON CON AGUJA FINA O CORTANTE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '332500', 'BIOPSIA ABIERTA DE BRONQUIO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '332801', 'BIOPSIA DE PÙLMON POR TORACOTOMIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '372700', 'BIOPSIA DE CORAZON SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '314501', 'BIOPSIA ABIERTA DE  TRAQUEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '342700', 'BIOPSIA DE DIAFRAGMA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '542301', 'BIOPSIA ABIERTA DE PERITONEO (MESENTERIO Y OMENTO)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '452600', 'BIOPSIA ABIERTA DE INTESTINO GRUESO SOD    (153)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '451500', 'BIOPSIA ABIERTA DE INTESTINO DELGADO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '542400', 'BIOPSIA CERRADA [PERCUTANEA] [CON AGUJA] DE MASA INTRAABDOMINAL  SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '501200', 'BIOPSIA ABIERTA DE HIGADO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '511300', 'BIOPSIA ABIERTA DE VESICULA BILIAR O VIAS BILIARES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '511200', 'BIOPSIA PERCUTANEA [AGUJA] DE VESICULA BILIAR O VIAS BILIARES SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '501100', 'BIOPSIA CERRADA PERCUTANEA [AGUJA] DE HIGADO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '521200', 'BIOPSIA ABIERTA DE PANCREAS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '521100', 'BIOPSIA POR ASPIRACION [AGUJA] CERRADA DE PANCREAS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '71100', 'BIOPSIA [PERCUTANEA] [AGUJA] DE GLANDULA SUPRARRENAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '71200', 'BIOPSIA ABIERTA DE GLANDULA SUPRARRENAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '573400', 'BIOPSIA VESICAL A CIELO ABIERTO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '402100', 'ESCISION DE GANGLIO LINFATICO CERVICAL PROFUNDO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '402200', 'ESCISION DE GANGLIO LINFATICO MAMARIO INTERNO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '832100', 'BIOPSIA DE TEJIDO BLANDO: MUSCULOS, TENDON, FASCIA Y BURSA (INCLUYENDO MANO) SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '150100', 'BIOPSIA DE MUSCULO O TENDON EXTRAOCULAR SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '542200', 'BIOPSIA ABIERTA DE PARED ABDOMINAL U OMBLIGO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '221402', 'ANTROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '314204', 'ESTROBOSCOPIA LARINGEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '314300', 'BIOPSIA CERRADA DE LARINGE [ENDOSCOPICA] SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '314203', 'MICROENDOSCOPIA LARINGEA DIAGNOSTICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '291100', 'FARINGOSCOPIA DIAGNOSTICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '332301', 'BRONCOSCOPIA RIGIDA CON LAVADO BRONQUIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '314400', 'BIOPSIA CERRADA DE TRAQUEA [ENDOSCOPICA] SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '332100', 'BRONCOSCOPIA A TRAVES DE ESTOMA ARTIFICIAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '332201', 'BRONCOSCOPIA FIBROOPTICA CON LAVADO BRONQUIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '332400', 'BIOPSIA CERRADA [ENDOSCOPICA] BRONQUIAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '332701', 'BRONCOSCOPIA FIBRO-OPTICA CON CEPILLADO BRONQUIAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '332001', 'BRONCOSCOPIA FIBRO-OPTICA CON PUNCION [ASPIRACION] TRANSTRAQUEAL O TRANSBRONQUIAL CON AGUJA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '213102', 'RESECCION   ENDOSCOPICA DE TUMOR BENIGNO DE FOSA NASAL POR VIA TRANSNASAL    (326)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '213103', 'RESECCION ENDOSCOPICA DE TUMOR MALIGNO DE FOSA NASAL, VIA  TRANSNASAL    (326)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '215102', 'RESECCION ENDOSCOPICA DE TUMOR MALIGNO DE CAVUM, VIA TRANSNASAL    (326)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '218902', 'CORRECCION DE ATRESIA DE COANAS, VIA TRANSNASAL  ENDOSCOPICA    (326)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '222102', 'ANTROSTOMIA MAXILAR INTRANASAL VIA MEATO MEDIO ENDOSCOPICA    (326)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '223903', 'ANTROTOMIA MAXILAR EXPLORATORIA VIA ENDOSCOPICA    (326)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '226201', 'RESECCION DE LESION BENIGNA EN SENO MAXILAR (CON EXTENSION NARIZ-COANA), VIA ENDOSCOPICA    (326)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '226401', 'ESFENOIDECTOMIA ENDOSCOPICA TRANSNASAL    (326)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '319402', 'EXTRACCION DE MOLDE (PROTESIS O STENT) LARINGEO VIA ENDOSCOPICA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '313201', 'EXTRACCION ENDOSCOPICA  (RIGIDA O DE FIBRA OPTICA) DE CUERPO EXTRAÑO DE TRAQUEA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '313202', 'EXTRACCION ENDOSCOPICA  (RIGIDA O DE FIBRA OPTICA) DE CUERPO EXTRAÑO DE LARINGE' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '339400', 'EXTRACCION ENDOSCOPICA (RIGIDA O DE FIBRA OPTICA). DE CUERPO EXTRAÑO DE BRONQUIO O PULMON SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '320202', 'RESECCION ENDOSCOPICA DE LESION BRONQUIO CON LASER O CRIOTERAPIA U OTRAS TECNICAS.' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '332205', 'BRONCOSCOPIA FIBRO-OPTICA CON APLICACION O RETIRO  DE FUENTE RADIACTIVA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '315102', 'RESECCION ENDOSCOPICA ( RIGIDA O DE FIBRA OPTICA ) DE LESION EN TRAQUEA CON LASER, BRAQUITERAPIA CRIOTERAPIA, ELECTROTERAPIA O DIATERMIA.' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '332700', 'BIOPSIA CERRADA [ENDOSCOPICA] [TORACOSCOPIA] DE PULMON SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '341102', 'EXPLORACION Y DRENAJE DE MEDIASTINO POR MEDIASTINOSCOPIA CERVICAL' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '342200', 'MEDIASTINOSCOPIA DIAGNOSTICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '342403', 'BIOPSIA PLEURAL POR TORACOSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '343202', 'RESECCION DE QUISTE O TUMOR BENIGNO  DEL MEDIASTINO POR TORACOSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '343303', 'RESECCION DE  TUMOR MALIGNO  DEL MEDIASTINO POR TORACOSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '343402', 'EXTRACCION DE CUERPO EXTRAÑO DE MEDIASTINO Y/O LIBERACION DE ADHERENCIAS POR TORACOSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '422100', 'ESOFAGOSCOPIA OPERATORIA POR INCISION SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '422200', 'ESOFAGOSCOPIA A TRAVES DE ESTOMA ARTIFICIAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '422300', 'ESOFAGOSCOPIA VIA ORAL EXPLORATORIA O DIAGNOSTICA SIN BIOPSIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '422400', 'BIOPSIA DE ESOFAGO CERRADA [ENDOSCOPICA] SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '434000', 'ESCISION ENDOSCOPICA DE POLIPOS GASTRICOS SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '431100', 'GASTROSTOMIA  PERCUTANEA [ENDOSCOPICA] SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '442200', 'DILATACION ENDOSCOPICA DE PILORO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '516400', 'ESCISION ENDOSCOPICA DE LESION EN LAS VIAS BILIARES SOD     (39)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '512300', 'LITROTIPSIA MECANICA BILIAR POR COLANGIOPANCREATOGRAFIA RETROGRADA ENDOSCOPICA SOD     (39)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '519500', 'EXTRACCION DE DISPOSITIVO PROTESICO DE VIA BILIAR SOD     (39)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '518400', 'DILATACION ENDOSCOPICA DE AMPOLLA Y CONDUCTO BILIAR SOD     (39)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '522100', 'RESECCION ENDOSCOPICA DE LESION O TEJIDO DE PANCREAS SOD     (39)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '510300', 'DRENAJE BILIAR PERCUTANEO [ENDOSCOPICO] Y COLOCACION DE PROTESIS SOD    (82)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '492100', 'ANOSCOPIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '452500', 'BIOPSIA CERRADA [ENDOSCOPICA] DEL INTESTINO GRUESO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '452100', 'ENDOSCOPIA TRANSABDOMINAL DE INTESTINO GRUESO SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '482700', 'MANOMETRIA RECTAL ENDOSCOPICA  SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '452200', 'ENDOSCOPIA DE INTESTINO GRUESO A TRAVES DE ESTOMA ARTIFICIAL SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '483803', 'EXTRACCION ENDOSCOPICA DE CUERPO EXTRAÑO EN RECTO' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '454202', 'RESECCION  ENDOSCOPICA DE LESIONES  DE  COLON NCOC' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '542100', 'LAPAROSCOPIA DIAGNOSTICA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '501300', 'BIOPSIA DE HIGADO POR LAPAROSCOPIA SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '541302', 'DRENAJE DE COLECCION  INTRAPERITONEAL ( EPIPLOICO, OMENTAL,PERIESPLENICO, PERIGASTRICO, SUBHEPATICO, SUBFRENICO, DE LA FOSA ILIACA O PLASTRON APENDICULAR ) POR LAPAROSCOPIA' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '573100', 'CISTOSCOPIA A TRAVES DE ESTOMA ARTIFICIAL O CISTOSTOMIA SOD    (40)' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '563300', 'BIOPSIA CERRADA [ENDOSCOPICA] DE URETER SOD' ,4,  now(),  now(), 'f', 1);

 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
                VALUES ( '545100', 'LISIS DE ADHERENCIAS PERITONEALES POR LAPAROSCOPIA SOD' ,4,  now(),  now(), 'f', 1);
