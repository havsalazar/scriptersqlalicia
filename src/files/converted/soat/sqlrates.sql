
 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1250', 'Punción cisternal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1251', 'Punción ventricular' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1252', 'Punción subdural' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17802', 'Hueso' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17100', 'Tejido intracraneal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1152', 'Biopsia esterotáxica de lesiones cerebrales' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3461', 'Operación de Lynch; incluye mucocele frontal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1240', 'Eliminación de derivación' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1103', 'Craneotomía para extracción secuestro' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1102', 'Craneotomía para drenaje hematoma epidural o subdural' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1104', 'Craneotomía para drenaje de hematoma de fosa posterior' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1101', 'Craneotomía para extracción cuerpo extraño; incluye esquirlectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1161', 'Craniectomía lineal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1221', 'Derivación ventrículo peritoneal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1225', 'Derivación subduro peritoneal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1144', 'Extirpación de lesión y/o tejido de las meninges cerebrales' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1106', 'Craneotomía para ruptura de senos de duramadre' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1142', 'Lobotomía (psicocirugía estereotáxica)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1140', 'Leucotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1262', 'Nucleotomía percutánea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14100', 'Drenaje, curetaje, secuestrectomía, huesos carpo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1162', 'Craneoplastia para corrección de defecto por resección del tumor óseo o infección' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1165', 'Craneoplastia con remplazo óseo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1125', 'Craneotomía para resección de tumores de fosa anterior' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1126', 'Craneotomía para resección de tumores de fosa media' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1127', 'Craneotomía para resección de tumores de fosa posterior' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1131', 'Tratamiento por vía anterior para tumores de clivus' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13321', 'Resección parcial del ilíaco' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1132', 'Craneotomía para tumores de hoz de cerebro' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1150', 'Punción estereotáxica de quistes, abcesos y hematomas intracraneanos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1108', 'Craneotomía para drenaje hematoma intracerebral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1123', 'Craneotomía para drenaje y extracción de tumores intraventriculares (Incluye: quiste coloide del tercer ventrículo)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1112', 'TRATAMIENTO DE MALFORMACIONES ARTERIO VENOSAS DE LINEA MEDIA E INTRAVENTRICULAR' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1143', 'Hemisferectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1141', 'Lobectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1148', 'Corrección de enfermedad de Crouzón' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1163', 'Esquirlectomía craneal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1160', 'Corrección hundimiento craneano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1164', 'Craneoplastia con acrílico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16508', 'OSTEOPLASTIA VARIOS HUESOS, LESION FIBRO OSEA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1352', 'Injerto dural' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1351', 'Reparación fístula líquido cefalorraquídeo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3454', 'CORRECCION FISTULA OROANTRAL; INCLUYE FISTULA GINGIVONASAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1146', 'Reparación meningocele craneal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1145', 'Reparación encéfalocele' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1220', 'Derivación ventrículo atrial' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1227', 'Ventriculostomía (drenaje externo)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1223', 'Derivación ventrículo subaracnoidea cervical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1241', 'Revisión de derivación' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7190', 'SECCION ADHERENCIAS PERITONEALES' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1107', 'Trepanación para monitoreo de presión intracraneana' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1151', 'Implantación estereotáxica de electrodos y material radio activo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1343', 'Implantación percutánea de electrodos de neuroestimulación, epidural o intradural' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13583', 'APLICACION DE TUTORES EXTERNOS' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1149', 'Injertos intracraneanos (médula suprarrenal)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1301', 'Laminectomía para exploración del canal raquídeo, uno o más segmentos Extradural, Subdural o Intramedular (cervical, dorsal, lumbar o sacra)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1311', 'Uno o más interespacios cervical, toráxica o lumbar, unilateral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1322', 'Laminectomía para rizotomía, uno o dos segmentos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1324', 'Laminectomía para cordotomía, unilateral, en un tiempo, cervical o dorsal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1341', 'Lesión estereotáxia de la médula percutánea, cualquier modalidad, inclusive estimulación y/o registro' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1321', 'Laminectomía para mielotomía, tipo Bischof, dorsal o lumbar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17103', 'Meninges vertebrales' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1332', 'Resección de meningocele raquídeo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1334', 'Resección de meningomieloradiculocele' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1147', 'Tratamiento de platibasia (Síndrome de Arnold Chiari)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1387', 'Microcirugía de raíces, médula y nervios, por aracnoiditis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1224', 'Derivación subduro atrial' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1122', 'Craneotomía para resección de Cráneofaringioma' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1405', 'Neurólisis percutánea con radiofrecuencia o sustancias químicas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1389', 'Instalación de bomba de infusión para dolor' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1345', 'Laminectomía para implantación de electrodos de neuroestimulación, intradurales' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1346', 'Revisión o remoción de electrodos de neuro estimulación, espinales' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1348', 'Revisión o remoción de receptor de neuroestimulador, espinal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3310', 'Cirugía del conducto auditivo interno; incluye neurectomía del nervio vestibular, resección neurinoma del acústico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1403', 'Descompresión neurovascular en hemiespasmo facial, neuralgia del V par, tortícolis espasmódica, vértigo o neuralgia del glosofaríngeo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1402', 'Rizotomía intracraneana para dolor' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1502', 'Gangliectomía esfenopalatina' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16553', 'Neurectomía periférica; incluye infraorbitario, largo bucal, lingual, mentonero' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13850', 'Resección tumor nervio brazo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13851', 'Resección tumor nervio antebrazo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14410', 'Resección tumor de nervio mano o dedos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13852', 'Resección tumor nervio muslo o pierna' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13853', 'Resección tumor nervio pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1620', 'Resección tumor plejo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17105', 'Nervio periférico profundo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1407', 'Rizotomía para dolor, abordaje por fosa posterior' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3420', 'Electrocoagulación nervio vidiano y/o extirpación por microcirugía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13880', 'Neurólisis nervio brazo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13881', 'Neurólisis nervio antebrazo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14430', 'Neurólisis nervio mano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14431', 'Neurólisis nervio dedos (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13883', 'Neurólisis nervio pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1401', 'Anastomosis microquirúrgica de pares craneanos, intra o extracraneana' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14422', 'Neurorrafia de colaterales en un dedo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13860', 'Neurorrafia un nervio brazo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13862', 'Neurorrafia un nervio antebrazo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14420', 'Neurorrafia un nervio mano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13870', 'Neurorrafia nervio muslo o pierna' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13873', 'Neurorrafia nervio pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1404', 'Descompresión de nervio facial en peñasco y fosa media' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3231', 'Descompresión nervio facial (2a y  3a porción)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18223', 'Liberación del tunel carpiano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13841', 'Descompresión nervio antebrazo; incluye en túnel carpiano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13843', 'Descompresión nervio pie; incluye túnel tarsiano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13840', 'Descompresión nervio brazo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14400', 'Descompresión nervio mano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14401', 'Descompresión nervio dedos (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13842', 'Descompresión nervio muslo o pierna; incluye tratamiento quirúrgico meralgia parestésica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3230', 'Injerto o anastomosis de nervio facial' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13844', 'Transposición de nervio en miembro superior' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15142', 'Colgajo muscular, miocutáneo y fasciocutáneo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15242', 'Colgajo libre (con microcirugía)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1501', 'SIMPATECTOMIA O GANGLIECTOMIA SIMPATICA, INCLUYE CERVICAL TORACICA, LUMBAR' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1503', 'Bloqueos simpáticos por regiones' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1408', 'Gangliolisis con radiofrecuencia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1611', 'Reconstrucción de plejo con neurorrafias' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1612', 'Reconstrucción de plejo con injerto de nervio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1613', 'Reconstrucción de plejo con neurotizaciones' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1601', 'Exploración plejo cervical, lumbar o sacro' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1610', 'Descompresión de tronco' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '4102', 'Exploración cuello (cuando no se practica otra intervención específica)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '4101', 'Drenaje absceso tiroideo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15101', 'Drenaje profundo partes blandas; incluye absceso profundo, flegmón' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17311', 'Abierta de tiroides' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17312', 'Percutánea de tiroides' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17310', 'Glándula paratiroides' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '4110', 'Tiroidectomía sub total; incluye lobectomía tiroidea total o parcial' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '4112', 'Tiroidectomía total' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '4120', 'Resección conducto tirogloso' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '4121', 'Resección fístula tiroglosa' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '4130', 'Paratiroidectomía parcial o total' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9121', 'Lumbotomía exploradora' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18600', 'Laparoscopia exploradora' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17602', 'Glándula suprarrenal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17402', 'Organo mediastinal (incluye timo)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7401', 'Adrenalectomía (suprarrenalectomía); incluye parcial o total' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7403', 'Toma de injerto para tratamiento parquinsonismo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9120', 'Drenaje absceso renal o perirrenal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1124', 'Craneotomía para pinealectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1120', 'Craneotomía para extirpación adenomas hipofisiarios' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1121', 'Craneotomía para extirpación adenomas hipofisiarios (vía transesfenoidal)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6111', 'Timectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17208', 'Párpado' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2220', 'Fulguración párpado' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2211', 'Drenaje resección chalazión' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2221', 'Resección tumor benigno párpado' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2222', 'Resección tumor maligno párpado' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2224', 'Resección tumor maligno párpado con reconstrucción total' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2223', 'Tarsectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2231', 'Tarsorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2272', 'Corrección ptosis palpebral deslizamiento músculo frontal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2273', 'Corrección ptosis palpebral con injerto fascia lata' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2270', 'Corrección ptosis palpebral (resección externa o interna del músculo elevador)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2271', 'Corrección ptosis palpebral (procedimiento de Fassanella y Servat)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2241', 'Corrección entropión' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2246', 'Injerto párpado (corrección ectropión o entropión)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2240', 'Corrección ectropión' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2262', 'Cantotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2261', 'Cantorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2260', 'Cantoplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2247', 'Blefaroplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2248', 'Tarsoplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2245', 'Injerto cartílago tarsal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2265', 'Corrección telecanto y blefarofimosis por disrrupción orbital' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2264', 'Corrección epicanto con cuatro colgajos (Mustarde)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2230', 'Blefarorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2250', 'Electrólisis o electrofulguración de pestañas por distriquiasis o triquiasis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15234', 'INJERTO REGION PILOSA; INCLUYE BARBA, CEJA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2101', 'Drenaje glándula lagrimal; incluye saco lagrimal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17205', 'Glándula lagrimal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17200', 'Conducto lagrimal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2111', 'Resección de glándula lagrimal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2102', 'Extracción cuerpo extraño glándula lagrimal; Incluye saco lagrimal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2110', 'Dacriocistectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2130', 'Plastia de canalículos lagrimales' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2141', 'Entropión punto lagrimal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2143', 'Oclusión puntos lagrimales' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2121', 'Dacriocistorrinostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2120', 'Conjuntivodacriocistorrinostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17201', 'Conjuntiva' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2305', 'Resección quiste o tumor conjuntival' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2306', 'Resección quiste o tumor conjuntival con injerto de mucosa' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2302', 'Resección pterigión' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2303', 'Resección pterigión con injerto de conjuntiva' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2301', 'Peritomía total' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2312', 'Corrección simbléfaron' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2311', 'Injerto de la conjuntiva; incluye transplante y plastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2310', 'Sutura de la conjuntiva' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2602', 'Extracción cuerpo extraño de córnea profundo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17202', 'Córnea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2612', 'Resección tumor córnea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2610', 'Cauterización de córnea ( termo o crío aplicación)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2644', 'Sutura corneoesclera' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2620', 'Sutura córnea superficial' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2621', 'Sutura córnea perforante' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2640', 'Escleroqueratoplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2623', 'Queratoplastia penetrante (retiro puntos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2624', 'Queratoplastia superficial o lamelar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2622', 'Queratoplastia penetrante' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2628', 'Queratoplastia penetrante más cirugía combinada de catarata, antiglaucomatosa o lente intraocular' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2629', 'Implante de prótesis corneana (queratoprótesis)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2604', 'Queratotomía radial miópica o astigmática' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2627', 'Queratomileusis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2611', 'Queratectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2613', 'Tatuaje de la córnea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2501', 'Extracción cuerpo extraño endocular' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2701', 'Iridectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2723', 'Iridotomía por fotocoagulación' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2721', 'Fijación iris' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2603', 'Paracentesis de cámara anterior' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17206', 'Iris' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17204', 'Esclerótica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17203', 'Cuerpo ciliar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2722', 'Iridoplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2731', 'Sinequiotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2702', 'Iridodiálisis anterior' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2720', 'Coreoplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15224', 'Reparación de coloboma; incluye naso oculares, oro oculares' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2707', 'Resección tumor iris' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2732', 'Ciclocrioterapia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2706', 'Resección tumor cuerpo ciliar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2802', 'Goniotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2805', 'Trabeculotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2801', 'Ciclodiálisis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2804', 'Trabeculectomía (esclerectomía subescleral)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2806', 'Fotocoagulación del ángulo camerular (Trabeculoplastia)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2643', 'Sutura de esclerótica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2642', 'Resección tumor de la esclerótica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2641', 'Escleroplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2601', 'Evacuación de hifema' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2903', 'Extracción intracapsular o extracapsular de cristalino (excepto por facoemulsificación)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2904', 'Extracción de cristalino por facoemulsificación' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2907', 'Capsulotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2905', 'Extracción catarata más lente intraocular' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2902', 'Inclusión secundaria de lente intraocular suturado' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2811', 'Retinopexia por crio, o diatermia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2812', 'Fotocoagulación intraquirúrgica de retina, con laser' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2813', 'Retinopexia; incluye bucle escleral total o parcial y gases' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2910', 'Vitrectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2911', 'Vitrectomía con o sin inserción de silicón o gases y endolaser' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2912', 'Vitrectomía más retinopexia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17816', 'Tendón' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2540', 'Corrección estrabismo horizontal o vertical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2430', 'Plastia de órbita (Inserción de prótesis orbitaria); incluye reinserción de prótesis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2403', 'Extracción cuerpo extraño de órbita' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17207', 'Órbita' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2511', 'Enucleación con implante' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2510', 'Enucleación con injerto dermograso' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2420', 'Exenteración de órbita' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2530', 'Inserción secundaria de prótesis (con formación de fondos de saco conjuntivales)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15211', 'Sutura heridas múltiples cara (más de tres o una extensa de más de 10  cms)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2431', 'Plastia de órbita con reconstrucción de fondos de saco con injertos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1166', 'Tratamiento para descompresión y corrección órbitaria.' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2401', 'Descompresión de órbita (excepto vía techo órbita)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2410', 'Resección tumor órbita' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2402', 'Drenaje absceso de órbita' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3114', 'Resección tumor maligno conducto auditivo externo; incluye reconstrucción de la cavidad operatoria' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3240', 'Resección glomus yugularis (quemodectoma)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3102', 'Extracción cuerpo extraño conducto auditivo externo con incisión' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17220', 'Oído externo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3111', 'Resección fístula pre auricular' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3110', 'Resección apéndice pre auricular' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3112', 'Resección quiste pabellón auricular' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15233', 'Injerto condrocutáneo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3113', 'Resección tumor benigno conducto auditivo externo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3121', 'Suturas heridas de pabellón auricular; incluye Cartílago' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15250', 'Reparación oreja; incluye en pantalla, prominente' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3120', 'Corrección agenesia conducto auditivo externo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15251', 'Reconstrucción de la oreja; incluye ausencia de: lóbulo, oreja' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15252', 'Reinserción oreja' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3220', 'Estapedectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3202', 'Miringotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3210', 'Miringoplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3211', 'Miringoplastia con reemplazo de cadena ósea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3201', 'Miringocentesis con colocación de válvula o diábolo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3101', 'Drenaje absceso de Bezold' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3233', 'Mastoidectomía simple (ático antromastoidectomía)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3232', 'Mastoidectomía radical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3302', 'Laberintotomía (derivación saco endolinfático)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3301', 'Laberintectomía; incluye diatermia, crioterapia, electrocoagulación, ultrasonido y vestibulotomía para tratamiento del vértigo (vía abierta)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3311', 'Prótesis: cóclea artificial o implantes coclares' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3450', 'Cirugía para tratamiento de epistaxis; incluye ligadura carótida externa, ligadura etmoidales, ligadura maxilar interna' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3453', 'Dermoplastia para epistaxis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3404', 'Resección tumor benigno de nariz; incluye polipectomía nasal, extracción rinolito' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3411', 'Drenaje absceso o hematoma tabique nasal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17210', 'Nariz' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3431', 'Sutura herida de nariz; incluye cartílago y/o mucosa nasal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15253', 'Reparación nariz; incluye corrección aplanamiento de fosas nasales, en silla de montar, implante de  nariz' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15254', 'Reinserción y reconstrucción nariz' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3430', 'Septorrinoplastia (para función respiratoria, no estética)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15223', 'Rinoqueiloplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15255', 'RINOPLASTIA (NO ESTETICA)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3440', 'Reducción fractura cerrada huesos propios' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3441', 'Reducción fractura abierta huesos propios' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3464', 'Cirugía endoscópica transnasal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3401', 'Cirugía del escleroma nasal; incluye resección de masas tumorales, permeabilización de luz nasal, tratamiento quirúrgico de las secuelas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3451', 'Corrección atresia coanas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3403', 'Resección tumor benigno de cavum (vía retrofaríngea, transpalatina o transnasal); incluye fibroma nasofaríngeo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3405', 'Resección tumor maligno de cavum (vía retrofaríngea o transpalatina)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3421', 'Turbinoplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3412', 'Septoplastia; incluye extirpación, reposición cartílago y hueso del séptum' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3410', 'Cierre perforación septal; incluye injerto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17211', 'Pared de senos paranasales' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18108', 'Nasosinusoscopia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18100', 'Laringoscopia o antroscopia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3452', 'Antrotomía intranasal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3463', 'Operación de Cadwell Luc (sinusotomía maxilar)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3460', 'Frontotomía radical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3468', 'Etmoidectomía intranasal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3469', 'Maxilectomía superior' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3467', 'Etmoidectomía externa' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3462', 'Maxilo etmoidectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3466', 'Esfenoidotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16503', 'Exodoncia de incluidos; incluye fijación interdentaria o intermaxilar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16341', 'Injertos aloplásticos cerámicos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16342', 'Injertos aloplásticos metálicos (técnica de tornillo espiral o autopenetrante)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16347', 'Implante de oseointegración' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16551', 'Exploración conducto dentario inferior; incluye descompresión, neurectomías' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17302', 'Encía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17301', 'Pared de cavidad bucal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16502', 'Resección quiste no odontogénico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16500', 'Extirpación lesión maligna de encía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16501', 'Extirpación lesión maligna de encía con vaciamiento ganglionar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16261', 'Estomatorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16513', 'Enucleación de quiste epidermoide, vía intraoral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16514', 'Enucleación de quiste epidermoide, vía extraoral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16510', 'Enucleación quiste odontogénico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16515', 'Extirpación de tumor odontogénico encapsulado (preservación de seno o nervio dentario inferior)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16518', 'Resección tumor odontogénico no encapsulado con injerto óseo, para reconstrucción inmediata (no incluye toma de injerto)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16511', 'Marsupialización quiste odontogénico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17307', 'Lengua' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16211', 'Glosectomía parcial y/o biopsia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16210', 'Glosectomía total o radical; incluye hemiglosectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16214', 'Glosoplastia; incluye injerto cutáneo o mucoso' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16213', 'Glosopexia; incluye plastia frenillo lingual' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16201', 'Incisión y drenaje de absceso cavidad bucal, intraoral; incluye hematoma' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16215', 'Glosorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16111', 'Sialolitotomia de Stensen o de Warthon' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16108', 'Exploración glándula salival' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16100', 'Cateterización y/o drenaje de glándula salival' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17304', 'Glándula salival' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16207', 'Marzupialización de ránula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16104', 'Resección de mucocele; incluye quiste de glándula salival' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16103', 'Parotidectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16101', 'Adenectomía sublingual, submaxilar o palatina; incluye mucocele, quiste glándula salival' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16110', 'Cierre o reparación de fístula glándula salival con injerto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16106', 'Cierre o reparación salival sin injerto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16109', 'Sialoplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16202', 'Incisión y drenaje de abseso, cavidad bocal, extraoral; incluye hematoma' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17308', 'Paladar y úvula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17305', 'Labio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16243', 'Extirpación lesión superficial paladar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16244', 'Extirpación lesión profunda paladar; incluye adenoma, lesiones superficiales extensas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15203', 'Resección tumor maligno de piel y/o tejido celular subcutáneo, en cara, reparación primaria' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15204', 'Resección tumor maligno de piel y/o tejido celular subcutáneo, en cara, reparación con colgajo o  injerto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16262', 'Resección lesión superficial mucosa oral con biopsia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16263', 'Resección lesión profunda mucosa oral; con biopsia; incluye superficial extensa' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15227', 'Resección fosetas labiales' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16265', 'Remoción cuerpo extraño tejidos blandos boca' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15210', 'Sutura herida cara; incluye sutura labios' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16271', 'Resección fístula boca, intraoral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16273', 'Resección fístula boca, extra-oral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16270', 'Cierre fístula oroantral con colgajo bucal; incluye oronasal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16274', 'Cierre fístula orosinusal y antrostomía, incluye remoción de cuerpo extraño o diente' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15221', 'Corrección secuelas de labio hendido' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15230', 'Injerto de piel en área especial; incluye cara, cuello, genitales, planta de pie, zonas de  flexión, (no incluye dedos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15238', 'Lipoinjerto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16330', 'Profundización piso bucal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16332', 'Ventibuloplastia con injerto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16331', 'Vestibuloplastia sin injerto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16242', 'Palatorrafia; incluye estafilorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16241', 'Injerto óseo paladar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16240', 'Faringoplastia; incluye colgajo faríngeo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16230', 'Uvulotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16231', 'Uvulorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15220', 'Corrección macro o microstoma' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3641', 'Drenaje absceso laterofaríngeo (vía externa)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3601', 'Amigdalectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3602', 'Adenoamigdalectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3644', 'Resección amígdala lingual; incluye electrofulguración' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3603', 'Adenoidectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3630', 'Control hemorragia post amigdalectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17300', 'Amígdalas y/o vegetaciones adenoides' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3671', 'Extirpación de bandas faríngeas; incluye electro fulguración, membrana congénita' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3672', 'Extracción cuerpo extraño enclavado en faringe (por vía externa)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3640', 'Drenaje absceso faríngeo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18102', 'Rinofaringoscopia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17303', 'Faringe' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3502', 'Traqueostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3645', 'Resección tumor benigno de faringe' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3646', 'Resección tumor maligno de faringe' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3642', 'Resección divertículo faringoesofágico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3531', 'Laringofaringuectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3632', 'Operación de monobloque' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3670', 'Dilatación faringe (sesión)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3661', 'Corrección de estenosis nasofaríngea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3643', 'Resección fístula faríngea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3581', 'Traqueorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3660', 'Cierre fístula branquial' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7251', 'ESFINTEROPLASTIA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3510', 'Resección lesión laringe; incluye popilomatosis laringea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3585', 'Sección de adherencia de laringe (sinequiotomía anterior)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3550', 'Laringuectomía parcial; incluye hemilaringuectomía frontal, frontolateral, horizontal o cordectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3545', 'Laringorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3580', 'Cierre de fístula tráqueal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3570', 'Reconstrucción plástica de la tráquea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3542', 'Aritenoídopexia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3544', 'Laringoplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3530', 'Laringuectomía total' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3501', 'Laringotomía (Laringofisura); incluye para extracción de cuerpo extraño' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18120', 'Fibrobroncoscopia para extracción de cuerpo extraño' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18109', 'Fibronasolaringoscopia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18101', 'Microlaringoscopia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17306', 'Laringe o cuerda vocal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3582', 'Dilatación de la laringe (sesión)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3541', 'Aplicación molde laríngeo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3543', 'Extracción molde laríngeo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3584', 'Inyección intracordal de teflón o similar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18103', 'Broncoscopia con toma de biopsia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18105', 'Fibrobroncoscopia diagnóstica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17408', 'Tráquea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3511', 'Resección lesión tráquea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6450', 'Cierre de fístula traqueoesofágica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3540', 'Anastomosis laringo tráqueal término terminal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3583', 'Dilatación de la tráquea (sesión)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6210', 'Resección tumor de bronquio por toracostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6223', 'Cierre de fístula bronquial; incluye fístula broncocutánea, fístula broncopleural' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6222', 'Cierre de broncostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6220', 'Broncoplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6221', 'Broncorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6320', 'Neumorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6120', 'Pleurectomía; incluye decorticación pulmonar y/o resección de bulas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6311', 'Lobectomía total' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6310', 'Lobectomía segmentaria' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3610', 'LOBECTOMIA SEGMENTARIA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6313', 'Neumectomía simple' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6314', 'Neumectomía radical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6331', 'Neumectomía uni o bilateral (donante)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6330', 'Trasplante pulmón uni o bilateral o con corazón' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18104', 'Broncoscopia con lavado bronquial' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17400', 'Bronquio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17406', 'Pulmón por punción' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18106', 'Torascopia por toracostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17407', 'Pulmón por toracotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6201', 'Exploración de bronquio por toracostomía; incluye extracción de cuerpo extraño' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6101', 'Toracostomía simple (con o sin resección de costilla); incluye liberación adherencias' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6102', 'Toracostomía con drenaje cerrado' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17405', 'Pleura por toracotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15110', 'Sutura herida, excepto cara' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15111', 'Sutura heridas múltiples, excepto cara (más de tres o una extensa de más de 10  cms)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6107', 'Toracoplastia con resección costal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18107', 'Mediastinoscopia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6110', 'Mediastinotomía para drenaje de mediastino, extracción cuerpo extraño mediastinal o resección tumor del mediastino' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17404', 'Pleura por punción' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17500', 'Diafragma' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7114', 'Herniorrafia diafragmática por vía abdominal o torácica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5620', 'Valvulotomías y/o valvuloplastias' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5654', 'Corrección total cardiopatías congénitas complejas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5622', 'Cambios valvulares con aplicación de prótesis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5650', 'Atrioseptoplastias sin aplicación de prótesis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5651', 'Atrioseptoplastias con aplicación de prótesis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5653', 'Ventrículo septoplastias con aplicación de prótesis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5652', 'Ventrículo septoplastias sin aplicación de prótesis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5530', 'Reparación y/o anastomosis de la aorta torácica o de arteria pulmonar; incluye ampliación de la luz de la aorta, anastomosis de la arteria pulmonar derecha con aorta ascendente y pulmonar izquierda (Potts   Smith), subclavia pulmonar (Blalock Taussing), cayado aórtico doble, coartación(congénita o adquirida), escisión o implantación de injerto  (hematoma disecante)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5531', 'Sección y sutura de conducto arterioso persistente' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5681', 'Endarterectomía coronaria (tromboendarterectomía)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5680', 'Bypass coronario (aorto coronario con vena safena)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5682', 'Bypass coronario con mamaria interna' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5520', 'Aneurisma vaso intratorácico; incluye aorta ascendente con circulación extracorpórea, escisión del aneurisma, extirpación de fístula, reemplazo con injerto (teflón), resección con injerto (parche)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5614', 'RESECCION DE ANEURISMA VENTRICULAR' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5704', 'Ligadura de fístula arterio venosa coronaria' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5603', 'Ventana Pericárdica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17403', 'Pericardio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17409', 'Endomiocárdica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5613', 'Pericardiectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5610', 'Extirpación de quiste pericárdico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5611', 'Extirpación de tumor del miocardio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5706', 'Cirugía para arritmias cardíacas: Crio-ablación intracavitaria Operación de mase para fibrilación auricular Resección subendocárdica Resección haces anómalos del sistema de conducción' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5601', 'Extracción cuerpo extraño intracardíaco' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5602', 'Extracción cuerpo extraño intrapericárdico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5670', 'Cardiorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5710', 'Trasplante de corazón' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5711', 'Cardiectomía (donante)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5702', 'Colocación y manejo de balón intraórtico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5705', 'Implantación de desfibrilador' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5701', 'Implantación de marcapaso con electrodo epicárdico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5302', 'Trombectomía de vasos sanguíneos de cabeza y cuello' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1117', 'Embolización para cateterismo de arterias intracraneanas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5102', 'Trombectomía vaso periférico (de grueso calibre)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5104', 'Trombolisis periférica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5401', 'Exploración y/o trombectomía de vaso sanguíneo intraabdominal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5551', 'Trombólisis mediante cateterismo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1116', 'Endarterectomía de vaso de cuello' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5310', 'Endarterectomía en la cabeza, cuello o base del encéfalo; incluye extracción del trombo o arterioesclerótico, resección de la íntima' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5110', 'Endarterectomía de vasos periféricos (de grueso calibre); incluye resección de la íntima tromboendarterectomía con: parche de injerto sintético o venoso' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5510', 'Endarterectomía intratorácica; incluye tromboendarterectomía (aorta)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5442', 'Derivación aorto renal por injerto en Y, o de aorta a las dos arterias renales' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5410', 'Endarterectomía intraabdominal; incluye cierre simple, resección de la íntima con: extracción de trombo o de material arteriosclerótico, parche de injerto venoso' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17700', 'Arteria o vena superficial' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17701', 'Arteria o vena profunda' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1110', 'Tratamiento de malformaciones arterio venosas supratentoriales' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1111', 'Tratamiento de malformaciones arterio venosas infratentoriales' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5321', 'Fistulectomía arteriovenosa de la cabeza, cuello o base del encéfalo; incluye endoaneurismorrafia, extirpación (simple), ligadura completa, parcial o cuádruple, sutura término terminal (arterial)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5160', 'Reconstrucción de vaso periférico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5521', 'Aneurisma de vaso intratorácico; incluye aorta descendente sin circulación extracorpórea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5440', 'Reconstrucción de arteria intraabdominal por medio de injerto; incluye derivaciones aorto femoral y aorto ilíaca con homoinjerto o injerto sintético simple o en Y' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5540', 'Reconstrucción de arteria intratorácica por medio de injerto; incluye cayado de la aorta, injerto: de derivación, de reemplazo, sintético (dracrón, nylon); tronco braquiocefálico por: homoinjerto arterial, injerto autógeno de vena (safena), injerto sintétetico.' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1386', 'Laminectomía para resección u oclusión de malformación arteriovenosa de la médula, cervical, dorsal o dorso lumbar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5443', 'Anastomosis de aorta intraabdominal; incluye anastomosis arterial directa, arterioplastia por injerto en parche sin endarterectomía asociada (estenosis renal)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5444', 'Anastomosis venosa intraabdominal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5170', 'Anastomosis venosa (vaso de grueso calibre); incluye anastomosis directa, anastomosis término  terminal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5420', 'Aneurismectomía de aorta intraabdominal; incluye resección con injerto en parche' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5421', 'Aneurismectomía intraabdominal (excepto aorta); incluye aneurisma hipogástrico, extirpación  fístula arteriovenosa (pélvica), resección o colocación de injerto en parche, sutura' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5320', 'Aneurismectomía vasos de la cabeza, cuello o base del encéfalo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1113', 'Apertura de seno cavernoso por fístula o aneurisma' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5350', 'Ligadura de vasos del cuello (de grueso calibre)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5101', 'Exploración vaso periférico (de grueso calibre)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5501', 'Exploración y/o trombectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5120', 'Arteriectomía periférica (de grueso calibre)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5121', 'Venectomía periférica (de grueso calibre)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5130', 'Fleboextracción y/o ligadura múltiples' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9185', 'Implantación de cáteter subclavio, femoral, yugular o peritoneal por punción' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9183', 'Construcción de fístula arteriovenosa con o sin injerto sintético o autólogo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9184', 'Implantación de cánula arteriovenosa (Scribner)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5441', 'Derivación aorto poplítea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5172', 'Angiorrafia de vasos periféricos (de grueso calibre)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5141', 'Escisión de fístula arteriovenosa periférica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1118', 'Angioplastia intraluminar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5301', 'Exploración quirúrgica vasos sanguíneos cabeza y cuello' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9180', 'Colocación o retiro de cateter peritoneal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17702', 'Ganglio o vaso linfático superficial' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17703', 'Ganglio o vaso linfático profundo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5201', 'Extirpación de higroma quístico de cuello' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5202', 'Extirpación de linfangioma de cuello' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '4114', 'Vaciamiento unilateral de cuello' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '4115', 'Vaciamiento bilateral de cuello' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '4116', 'Vaciamiento suprahiodeo de cuello' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5213', 'Vaciamiento linfático axilar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5211', 'Vaciamiento linfático inguino ilíaco' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11631', 'VULVECTOMIA RADICAL; INCLUYE LINFADENECTOMIA EXTRAPERITONEAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9332', 'CISTECTOMIA RADICAL (TOTAL MAS LINFADENECTOMIA MAS DERIVACION.)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5210', 'Vaciamiento linfático abdomino inguinal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9531', 'Linfadenectomía pélvica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9633', 'LINFADENECTOMIA RETROPERITONEAL; INCLUYE CLASIFICATORIA, CISTORREDUCTORA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9333', 'Linfadenectomía retroperitoneal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5230', 'Cierre de fístula del conducto torácico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5232', 'Ligadura del conducto torácico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5224', 'Derivación linfovenosa' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5231', 'Ligadura (obliteración) en el área ilíaca' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5220', 'Anastomosis de vasos linfáticos (de grueso calibre)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5222', 'Linfangiorrafia (vaso de grueso calibre)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5221', 'Linfangioplastia (vaso de grueso calibre)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5223', 'Transplante de linfáticos autógenos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7800', 'Trasplante de médula ósea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17800', 'Médula ósea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17511', 'Hígado por punción' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17510', 'Hígado por laparotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7501', 'Esplenectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7510', 'Esplenorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6401', 'Esófagotomía; incluye drenaje absceso de esófago' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6402', 'Esófagostomía; incluye cervical, fistulización (externa)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18301', 'Esofagogastroduodenoscopia en acto quirúrgico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18302', 'Esofagoscopia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17401', 'Esófago' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6410', 'Diverticulectomía de esófago' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6411', 'Resección tumor de esófago (vía abierta)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18316', 'Esofagoscopia para control de hemorragia o para fulguración de lesión de mucosa' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18314', 'Esofagoscopia para esclerosis de várices (sesión)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6420', 'Esofagectomía; incluye parcial, total' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6430', 'Anastomosis intratorácicas en esófago; incluye intrapleural, retroesternal, esófago colostomía, esófago enterostomía, esófago Esofagotomía, esófago gastrostomía, esófago ileostomía, esófago yeyunostomía, esófago duodenostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6440', 'Anastomosis supra torácica en esófago; incluye esófago gastrostomía supra esternal, interposición de: asa yeyunal, colon' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6461', 'Operación de Heller para la acalasia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6453', 'Esofagoplastia con inserción de tubo de silicón a través de esófago (paliativa)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6452', 'Esófagorrafia por toracotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6431', 'Corrección atresia esófago' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6460', 'Ligadura transtorácica de várices esofágicas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18312', 'Esofagoscopia para dilatación (sesión)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18313', 'Esofagoscopia para dilatación neumática con balón (sesión)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18315', 'Esofagoscopia con colocación de prótesis endoesofágica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6403', 'Extracción cuerpo extraño de esófago (vía abierta)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18310', 'Esofagoscopia rigida para extracción de cuerpo extraño' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7601', 'Gastrostomía; incluye extracción cuerpo extraño' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18318', 'Gastrotomía endoscópica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7610', 'Piloroplatia; incluye pilororectomía anterior, piloromiotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18317', 'Papilotomía endoscópica en estómago o duodeno' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18319', 'E G D C para control de hemorragia o fulguración de lesión en mucosa' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7630', 'Anastomosis del estómago; incluye gastroduodenostomía, gastroyeyunostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7621', 'Gastrectomía subtotal radical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7620', 'Gastrectomía parcial más vaguectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7622', 'Gastrectomía total' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6421', 'Esófagogastrectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7650', 'Vaguectomía selectiva y supraselectiva' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18300', 'Esofagogastroduodenoscopia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18320', 'E G D C con extracción de cuerpo extraño' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17504', 'Estómago por laparatomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7640', 'Gastrorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7743', 'Enterorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7660', 'Cierre de fístula de gastroduodenostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6451', 'Esofagoplastia con ascenso de estómago (esófago gastroplastia)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7641', 'Operación anti reflujo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7662', 'Desvascularización gástrica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7663', 'Reducción vólvulo estómago' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7701', 'Enterotomía; incluye extracción de cuerpo extraño' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7702', 'Drenaje absceso de divertículo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18322', 'Endoscopia de intestino delgado con extracción de cuerpo extraño' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18324', 'Endoscopia de ileostomía continente' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17505', 'Intestino delgado' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18505', 'Colonoscopia en acto quirúrgico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18504', 'Colonoscopia total' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18503', 'Colonoscopia izquierda' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18502', 'Rectosigmoidoscopia-equipo flexible' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17503', 'Colon' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18323', 'Endoscopia de intestino delgado con papilotomía control de hemorragia o fulguración de lesión de mucosa' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7721', 'Resección divertículo duodenal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7720', 'Extirpación lesión local intestino' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18514', 'Colonoscopia para resección de pólipos, control de hemorragia o fulguración de lesión de mucosa' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8121', 'Escisión mucosa rectal; incluye extirpación pólipos papilomas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7722', 'Resección intestinal; incluye duodenectomía, enterocolectomía, enterectomía, yeyunectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7724', 'Colectomía subtotal; incluye hemicolectomía o ileocolectomía, sigmoidectomía, cecostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7725', 'Colectomía total' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7740', 'Anastomosis intestino delgado' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7741', 'Anastomosis intestino delgado con grueso' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7742', 'Anastomosis intestino grueso' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7712', 'Duodenostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7710', 'Colostomía e ileostomía; incluye cecostomía, colostomía transversostomía, sigmoídostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18321', 'Yeyunostomía endoscópica percutánea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7750', 'Cierre comunicación intestinal a piel; incluye cierre de: cecostomía, colostomía, duodenostomía, enterostomía, fístula: fecal o yeyunal, ileostomía, sigmoidostomía, yeyunostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7180', 'Operación de Noble modificada' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8140', 'Proctopexia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7770', 'Corrección atresia intestinal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7771', 'Corrección malrotación intestinal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7760', 'Reducción vólvulo intestino' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18515', 'Colonoscopia para descompresión de vólvulus' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18511', 'Colonoscopia para extracción de cuerpo extraño' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7730', 'Apéndicectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8104', 'Proctotomía con colostomía; incluye por vía abdominal o perineal.' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18501', 'Rectosigmoidoscopia-equipo rígido' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17506', 'Recto o sigmoide' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18506', 'Manometría rectal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8120', 'Cauterización rectal; incluye diatermia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8103', 'Extracción cuerpo extraño en recto por vía rectal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8102', 'Extracción cuerpo extraño en recto por vía abdominal con colostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8130', 'Protectomía con colostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8132', 'PROTECTOMIA PARCIAL VIA TRANSACRA (KRASKE)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8131', 'Proctosigmoidectomía con colostomía; incluye abordaje perineal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8133', 'Protectomía con descenso abdomino perineal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8134', 'Resección de proicidencia rectal cononastomosis vía perineal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8141', 'Proctoplastia con colostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8142', 'Proctorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8122', 'Fistulectomía rectal con colostomía; incluye fístula, recto vaginal, recto vesical, traumática del recto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8144', 'Descenso rectal por vía sagital posterior' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8145', 'Descenso rectal por vía anterior y posterior' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8135', 'Protectomía completa para el megacolon' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8143', 'Proctoplastia sin colostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8246', 'Reparo de incontinencia (Thiersch)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8243', 'Corrección atresia anal y rectal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8151', 'Miomectomía anorrectal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8101', 'Drenaje absceso rectal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8110', 'Drenaje absceso perirrectal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17603', 'TEJIDOS PERIRRENALES' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8210', 'Fistulectomía anal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8250', 'Dilatación esfinter ano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8201', 'Drenaje absceso isquiorrectal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8202', 'Drenaje absceso perianal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8212', 'Resección tumor ano; incluye fulguración' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18500', 'Anoscopia (proctoscopia)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17900', 'Piel y tejido celular subcutáneo, en otros sitios no clasificados' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17920', 'Ano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8220', 'Hemorroidectomía externa' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8221', 'Trombectomía por hemorroides' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8203', 'Esfinterotomía anal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8240', 'Anorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11642', 'Corrección desgarro perineall I o II, sin atención del parto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8241', 'Esfinteroplastia anal con colostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8245', 'Esfinteroplastia anal sin colostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11530', 'Cierre fístula vaginal (por cualquier vía)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7201', 'Drenaje abierto de absceso hepático' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18601', 'Laparoscopia con biopsia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7212', 'Hepatectomía segmentaria' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7210', 'Resección quiste hidatídico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7241', 'Hepatectomía total (donante)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7240', 'Trasplante de hígado' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7230', 'Hepatorrafia simple' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7231', 'Hepatorrafia múltiple; incluye debridamiento y hemostasis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7260', 'Colecistostomía; incluye extracción de los cálculos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18415', 'E R C P para colocación o reinserción, de Stent, en conducto biliar o pancreática' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18401', 'E R C P Endoscopia para colangiopancreatografía retrógrada' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18400', 'Colangiografía retrógrada transduodenal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18413', 'E R C P para manometría de esfinter, de Oddi' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7121', 'Laparotomía exploradora' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18410', 'E R C P para esfinterotomía y/o papilotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7270', 'Colecistectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18412', 'E R C P para litotripsia de cálculos biliares cualquier método' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7254', 'Derivación bilio digestiva' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7272', 'Resección tumor vías biliares' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7250', 'Anastomosis de vías biliares' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7252', 'Reexploración de vías biliares; incluye colangiografía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7271', 'Exploración de vías biliares (Tubo en T)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7253', 'Reconstrucción de vías biliares' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18411', 'E R C P para extracción de cálculos biliares' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18416', 'E R C P para dilatación con balón de ampolla, de conducto biliar o pancreático' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18414', 'E R C P para drenaje nasobiliar (sin Kit)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7301', 'Drenaje absceso páncreas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7330', 'Marsupialización quiste del páncreas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17512', 'Páncreas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7313', 'Resección lesión de páncreas; incluye   fistulectomía, pancreatolitotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7312', 'Pancreatectomía subtotal (operación de Child)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7310', 'Pancreatectomía distal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7311', 'Pancreatoduodenectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7341', 'PANCREATECTOMIA (DONANTE)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7340', 'Trasplante de páncreas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7320', 'ANASTOMOSIS DEL PANCREAS; INCLUYE CISTODUODENOSTOMIA, CISTOGASTROSTOMIA, CISTOYEYUNOSTOMIA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7321', 'Pancreatoyeyunostomía lateral (operación de Puestow)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7102', 'Herniorrafia inguinal (excepto recidiva)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7103', 'HERNIORRAFIA INGUINAL POR RECIDIVA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7105', 'Herniorrafia femoral o crural por recidiva' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7109', 'Herniorrafia umbilical; incluye Recidiva' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7106', 'Eventrorrafia; incluye malla de Marles' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2625', 'REPARACION HERIDA CORNEOESCLERA CON HERNIA UVEAL O FAQUECTOMIA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7107', 'Herniorrafia epigástrica (excepto recidiva);incluye herniorrafia de Spiegel' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7108', 'HERNIORRAFIA EPIGASTRICA POR RECIDIVA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7112', 'Herniorrafia lumbar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7113', 'Herniorrafia obturadora' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7110', 'Herniorrafia isquiática' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7111', 'Herniorrafia isquiorrectal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7141', 'Drenaje absceso retroperitoneal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7120', 'Drenaje absceso de pared abdominal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7140', 'Drenaje absceso intraperitoneal; incluye epiplóico (omental), de fosa ilíaca, periesplénico, perigástrico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7162', 'Resección tumor retroperitoneal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7161', 'Resección lesión del epiplón o mesenterio; incluye benigna, maligna' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7142', 'Drenaje peritonitis generalizada' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11113', 'Resección quiste o tumor de ovario y biopsia contralateral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17501', 'Mesenterio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7122', 'Extirpación tumor benigno pared abdominal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15106', 'Resección tumor benigno piel que requiera reparación con colgajo y/o injerto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7101', 'Cierre evisceración' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7163', 'Corrección gastros chisis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7160', 'Corrección onfalocele' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7723', 'Resección de divertículo de Meckel' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11477', 'Extracción de dispositivo perdido extrauterino intraabdominal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9170', 'Aspiración, resección o marsupialización, de quiste e inyección esclerosante (pecutánea)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9101', 'Nefrolitotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9102', 'Nefrostomía a cielo abierto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9103', 'Nefrolitotomía percutánea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9110', 'Pielolitotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9112', 'Pielonefrostomía para cálculo coraliforme' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9111', 'Pielostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18702', 'Ureterorrenoscopia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18700', 'Pieloscopia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17601', 'Percutánea de riñón' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17600', 'Riñón por lumbotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9141', 'Nefrectomía parcial' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18716', 'Resección de lesión piélica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9140', 'Diverticulectomía calicial' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9146', 'Nefroureterectomía con segmento vesical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9145', 'Nefrourecterectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9144', 'Nefrectomía simple' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9143', 'Nefrectomía radical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9191', 'Nefrectomía (donante)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9190', 'Trasplante renal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9171', 'Nefropexia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9162', 'Nefrorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9164', 'Resección fístula reno-cutánea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9165', 'Resección fístula reno-viseral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9220', 'Ureterostomía cutánea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9160', 'Anastomosis uretero calicial' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9222', 'Ureteroenterostomía cutánea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9163', 'Pieloplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18717', 'Pieloplastia endoscópica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9251', 'Pieloureterolisis con transposición intraperitoneal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11475', 'LIBERACION DE ADHERENCIAS DEL UTERO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18718', 'Colocación de prótesis endoureteral (cateter J J )' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18710', 'Ureterolitotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9202', 'Meatotomía ureteral abierta' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18715', 'Meatotomía ureteral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9201', 'Exploración ureter' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9203', 'Ureterolitotomía (vía abierta)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18701', 'Ureteroscopia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17612', 'Pelvis o uréter' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18703', 'Cistoscopia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9210', 'Diverticulectomía ureteral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9216', 'URETERECTOMIA RESIDUAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9252', 'Ureteroplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9223', 'Ureteroneoileostomía cutánea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9224', 'Ureteroneoproctostomía (anastomosis ureteres a recto aislado in situ)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9225', 'Reemplazo ureteral por intestino' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9240', 'Ureteroneocistostomía (anastomosis ureterovesical o reimplantación ureterovesical)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9241', 'Ureteroneocistostomía con técnica de alargamiento vesical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9242', 'Uretero   ureterostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9253', 'Ureterorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9214', 'Resección de fístula urétero-cutánea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9215', 'Resección de fístula urétero-viseral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9213', 'Resección de ureterocele (vía abierta)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9212', 'Resección de ureterocele (transuretral)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9412', 'Meatotomía uretral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18719', 'Evacuación endoscópica de coágulos o detritus endovesicales' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18712', 'Extracción cuerpo extraño en vejiga' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18720', 'Fulguración transuretral por sangrado (no incluye sangrado post-operatorio)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9301', 'Extracción de cuerpo extraño en vejiga (vía abierta)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9303', 'CISTOTOMIA SUPRAPUBICA (TALLA VESICAL)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9352', 'Vesicostomía cutánea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18704', 'Cistoscopia y biopsia vesical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17611', 'Vejiga por laparotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9310', 'Resección fulguración tumor vesical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9375', 'Resección por persistencia del uraco (Incluye quiste del uraco)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9322', 'Resección fulguración tumor vesical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9330', 'Cistectomía parcial' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9323', 'Resección transvesical cuello vesical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9311', 'Resección cuello vesical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9331', 'Cistectomía total' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9334', 'Exanteración pélvica completa' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9513', 'Prostatocistectomía (seguida de derivación)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9350', 'Cistorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9353', 'Corección de fístula vésico-cutánea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9351', 'Corrección fístula vesical, vésico entérica, vésico vaginal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9452', 'Esfinterotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9321', 'Plastia de cuello vesical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9370', 'Correccion extrofia vesical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9341', 'Ileocistoplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9340', 'Colocistoplastia (Sigmoídoplastía)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9345', 'Gastrocistoplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9342', 'Ileo ceco cistoplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9344', 'Cistopexia retropúbica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18811', 'Esfinterotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9450', 'Operación para incontinencia urinaria masculina' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9410', 'Extirpación carúnculas uretrales' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9401', 'Uretrolitotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18817', 'Uretrotomía interna endoscópica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9402', 'Uretrostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18800', 'Uretroscopia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17610', 'Uretra' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17613', 'Tejido periuretral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18814', 'Extirpación y/o electrofulguración lesiones uretrales' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18812', 'Resección de valvas congénitas uretrales' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9453', 'Extirpación y/o electrofulguración lesiones uretrales (vía abierta)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9454', 'Resección de valvas congénitas uretrales (vía: abierta)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9423', 'Uretrectomía simple' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9422', 'Uretrectomía radical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9424', 'Uretrorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9431', 'Cierre de uretrostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9430', 'Resección de fístula uretro rectal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9435', 'Fistulectomía uretro-cutánea y uretroplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9433', 'Uretroplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9820', 'Corrección epispadias o hipospadias' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9438', 'Uretroplastia transpúbica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9434', 'Uretroplastia con otros tejidos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9411', 'Meatoplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9420', 'Diverticulectomía uretral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9440', 'Dilatación de la uretra' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9441', 'Uretrotomía interna' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18813', 'Uretrolitotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9455', 'Drenaje absceso periuretral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9250', 'Ureterolisis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9371', 'Drenaje perivesical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9346', 'Cistouretropexia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9432', 'Uretrocistopexia retropúbica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18819', 'Inyección periuretral para tratamiento de incontinencia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11545', 'Uretrocolpopexia vía abdominal o vaginal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9436', 'Uretrocistopexia con control endoscópico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18711', 'Ureterolitotomía ultrasónica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9501', 'Drenaje perineal absceso próstata' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18815', 'Drenaje absceso próstata' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9502', 'Prostatolitotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17623', 'Próstata por punción; incluye perineal, transrectal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17624', 'Próstata (vía abierta)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9511', 'Prostatectomía transuretral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9510', 'Prostatectomía abierta' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9512', 'Prostatectomía radical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9520', 'Vesiculotomía seminal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9521', 'Vesiculectomía (espermatocistectomía)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18816', 'Control hemorragia prostática' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9602', 'Incisión y/o drenaje del cordón espermático, escroto o testículo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17621', 'Escroto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17625', 'Testículo, túnica vaginal o cordón espermático' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9601', 'Hidrocelectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9664', 'Resección quiste sebáceo escroto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9621', 'Fulguración de lesión escrotal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9622', 'Resección parcial del escroto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9623', 'Resección total del escroto y reconstrucción con plastias cutáneas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9665', 'Sutura herida escroto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9620', 'Fistulectomía del escroto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9605', 'Aspiración de hidrocele' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9603', 'Resección del hematocele; incluye cordón espermático, túnica vaginal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9660', 'Extracción cuerpo extraño del escroto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9663', 'Resección de apéndice testicular' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9632', 'Orquidectomía radical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9631', 'Orquidectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9651', 'Orquidopexia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9650', 'Fijación testicular profiláctica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9642', 'Implante de testículo en tejidos vecinos por destrucción del escroto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9641', 'Orquidorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9640', 'Implante prótesis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9661', 'Extracción cuerpo extraño del testículo cordón espermático, túnica vaginal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17620', 'Epidídimo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17626', 'Conducto deferente' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9604', 'Varicocelectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9705', 'Espermatocelectomía (Resección quiste del epidídimo)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9703', 'Epididimectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9662', 'Reducción quirúrgica torsión del cordón espermático' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9704', 'Epididimovasostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9701', 'Vasectomía (deferentectomía)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9707', 'Reconstrucción de conducto deferente seccionado (vasovasostomía)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9706', 'Incisión y drenaje del epidídimo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9803', 'Circuncisión' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17622', 'Pene' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9802', 'Fulguración de condilomas venéreos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9810', 'Amputación parcial del pene' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9811', 'Amputación total del pene' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9843', 'Sutura herida pene' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9821', 'Extirpación de Cordée (cuerda)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9606', 'Cirugía genitales ambiguos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9827', 'Reconstrucción peneana' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9822', 'Extirpación de nódulos de la enfermedad de Peyronie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9826', 'Plastia del frenillo peneal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9829', 'Inyección de placas de fibrosis de pene' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9801', 'Prepuciotomía; incluye reducción quirúrgica de parafimosis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9825', 'Implante intracavernoso para tratamiento quirúrgico de la impotencia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9824', 'Retiro de prótesis peneana' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9842', 'Derivación safeno cavernosa o cavernosa esponjosa para priapismo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9841', 'Intervenciones para priapismo; incluye punción o drenaje cuerpos cavernosos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9828', 'Corrección de angulación peneana' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9840', 'Incisión y drenaje flegmón peneano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17643', 'Ovario' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11111', 'Resección quiste o tumor de ovario' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11110', 'Resección cuneiforme de ovario' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11101', 'Ooforostomía; incluye drenaje de absceso o quiste' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11112', 'Resección quiste paraovárico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11140', 'Liberación adherencias de ovario (ovariolisis con microcirugía)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11120', 'Ooforectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11131', 'Ooforoplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11132', 'Oofororrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11130', 'Ooforopexia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '12113', 'Resección embarazo ectópico' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11213', 'Salpingostomía y drenaje trompa de Falopio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11212', 'Salpingostomía y anastomosis trompa de Falopio (Microcirugía)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17644', 'Trompa de Falopio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18910', 'Sección y/o ligadura de trompa de Falopio por laparoscopia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11242', 'Sección y/o ligadura de trompa de falopio (Pomeroy)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11201', 'Salpingectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11301', 'Extirpación tumor de ligamento ancho' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11221', 'Salpingorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11121', 'Salpingooforectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11210', 'Salpingohisterostomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11241', 'Resección adherencia trompa de Falopio (salpingolisis con microcirugía)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11220', 'Salpingoplastia; incluye uso de dispositivos protésicos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11231', 'Insuflación trompa de Falopio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11222', 'Salpingooforoplastia (operación de Estes)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11430', 'Legrado uterino ginecológico (terapéutico o diagnóstico)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17640', 'Cuello uterino (cérvix)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11441', 'Conización' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11411', 'Extirpación pólipo cuello uterino' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11402', 'Traquelectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11440', 'Amputación del cérvix' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11543', 'Operación de Manchester(colporrafia anterior con amputación de cuello)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11442', 'Cerclaje del istmo (orificio interno cuello)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11463', 'Traquelorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11462', 'Traqueloplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11401', 'Histerotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18902', 'Histeroscopia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17642', 'Miometrio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17641', 'Endometrio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11460', 'Histeroplastia (operación de Strasman)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11413', 'Resección de pólipo endometrial' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11410', 'Miomectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11470', 'Histerectomía abdominal (total o subtotal)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11476', 'Exenteración o evisceración pélvica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11472', 'Histerectomía abdominal ampliada' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11473', 'Histerectomía vaginal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11471', 'Histerectomía abdominal radical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11474', 'Histerectomía vaginal radical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11300', 'Drenaje de absceso o hematoma' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11302', 'Histeropexia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11461', 'Histerorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11412', 'Extracción cuerpo extraño intrauterino; incluye dispositivos anticonceptivos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11478', 'Implantación intrauterina de platinas radioactivas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1376', 'Cerclaje cervical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11501', 'Colpotomía (incisión del fondo de saco de Douglas)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11503', 'Incisión de septum vaginal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11500', 'Drenaje absceso o hematoma cúpula vaginal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11504', 'Vaginoperineotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18903', 'Colposcopia (vaginoscopia)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17633', 'Vagina' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11523', 'Himenectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11522', 'Extirpación del tabique vaginal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11525', 'Resección tumor benigno de vagina' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11520', 'Colpectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11524', 'Vaginectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11540', 'Colporrafia anterior; incluye corrección quirúrgica del cistocele y uretrocele  I, II  y  III' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11542', 'Colporrafia posterior; incluye corrección quirúrgica de rectocele I, II  y III' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11541', 'Colporrafia anterior y posterior' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11551', 'Reconstrucción vagina' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11641', 'Corrección desgarroperineal en atención del parto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11546', 'Colpopexia; incluye prolapso de cúpula con muñón restante' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11550', 'Construcción vagina artificial' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11521', 'Colpocleisis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11601', 'Drenaje absceso glándula de Bartholín' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17631', 'Labio mayor y labio menor' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17630', 'Clítoris' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17634', 'Glándula de Bartholín' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17632', 'Periné' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11621', 'Drenaje absceso glándula de Bartholín y marsupialización' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11620', 'Resección glándula de Bartholín (Bartholinectomía)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11611', 'Resección glándula de Skene' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11633', 'Resección de endometrioma perineal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11634', 'Resección granuloma vulvo-perineal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11630', 'Clitoridectomía; incluye amputación parcial' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11632', 'Vulvectomía simple' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11640', 'Cierre fístula perineal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11602', 'Extracción cuerpo extraño periné' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '12102', 'Parto intervenido (forceps o espátulas)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '12101', 'Parto normal incluye episiorrafia y/o perineorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '12110', 'Cesárea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '12111', 'Legrado uterino (obstétrico); incluye por aborto aborto incompleto o endometritis puerperal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '12112', 'Amniocentesis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18900', 'Amnioscopia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '12103', 'Extracción de placenta, sin atención del parto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11613', 'Limpieza, debridamiento y cierre de dehiscencia de episiorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11600', 'Drenaje absceso de episiorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16203', 'Secuestrectomía para osteomiolitis intraoral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16204', 'Secuestrectomía para osteomilitis extraoral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16302', 'Osteotomía maxilar para extracción de cuerpo extraño' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16206', 'Curetaje óseo, maxilar o mandibular' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17309', 'Biopsia de huesos maxilares' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16323', 'Artrocentesis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18201', 'Artroscopia diagnóstica de codo, muñeca, tobillo o temporomandibular' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16506', 'Extirpación tumor benigno en maxilar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16335', 'Exostosis maxilar superior' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16504', 'Resección parcial en bloque, maxilar o mandibular' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16312', 'Mandibulectomía parcial simple' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16314', 'Resección parcial maxilar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16320', 'Condilectomía maxilar inferior, incluye artrotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16313', 'Mandibulectomía parcial con reconstrucción' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16340', 'Injerto óseo autógeno en maxilares; incluye implates protésicos. No incluye procedimiento quirúrgico para toma de injerto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15226', 'Retroposición quirúrgica de la premaxila' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18211', 'Extracción de cuerpos libres intraarticulares en hombro, codo, rodilla, tobillo o articulación temporomandibular' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16321', 'Menisectomía articulación temporomandibular; incluye resección tubérculo articular del temporal, plastia de cápsula articular, meniscorrafia,  meniscopexia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16322', 'Reemplazo total de articulación temporomandibular; incluye injerto de cartílago de crecimiento, reemplazo articular con prótesis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16301', 'Osteotomía mandibular por seudoartrosis; incluye corrección de anquilosis con o sin aplicación de prótesis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16310', 'Cirugía ortognática de maxilar inferior; incluye fijación maxilo-mandibular, fijación rígida' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16327', 'Coronoidectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16305', 'Osteotomía segmentaria mandibular o maxilar; incluye: fijación maxilo-mandibular, fijación rígida' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16300', 'Osteotomía mentón' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16306', 'Corticotomía Lefort I, para expansión de maxilar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16307', 'Osteotomía Lefort II' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16308', 'Osteotomía Lefort III' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16350', 'Reducción cerrada fractura de maxilar superior; incluye inmovilización intermaxilar, fijación maxilomandibular, suspensión esquelética, fijación rígida' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16303', 'Osteotomía deslizante' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16507', 'Osteoplastia maxilar de lesión fibro ósea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16401', 'Reducción abierta fractura de arco cigomático' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16402', 'Reducción abierta fractura de malar; incluye fractura del piso de la órbita (Blow out)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16353', 'Reducción cerrada fractura de malar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16360', 'Reducción abierta fractura de maxilar superior (Lefort I); incluye inmovilización intermaxilar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16361', 'Reducción abierta fractura de maxilar superior (Lefort II y III); incluye fijación intermaxilar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16351', 'Reducción cerrada fractura de maxilar inferior; incluye inmovilización intermaxilar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16362', 'Reducción abierta fractura de maxilar inferior; incluye inmovilización intermaxilar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16363', 'Reducción abierta de fractura alveolar superior o inferior; incluye fractura de tuberosidad   maxilar, reimplante dental y fijación' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16352', 'Reducción cerrada fracturas alveolares superior o inferior; incluye reimplante dental y fijación' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2433', 'Reducción fractura' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2432', 'Reconstrucción piso' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16403', 'Reducción abierta fracturas múltiples de huesos faciales; incluye implante o injerto piso orbitario' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15236', 'Injerto óseo en cara' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16324', 'Reducción manual de luxación aguda' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16325', 'Reducción manual de luxación con fijación inter  maxilar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16346', 'Implante hidrosilapotita con expansor de periostio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16326', 'Artrectomía (anquilosis)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13100', 'Drenaje, curetaje, secuestrectomía, de escápula y clavícula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13101', 'Drenaje, curetaje, secuestrectomía, de húmero' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13200', 'Drenaje, curetaje, secuestrectomía, de cúbito o radio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14101', 'Drenaje, curetaje, secuestrectomía, metacarpianos ((uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13400', 'Drenaje, curetaje, secuestrectomía, de fémur' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13401', 'Drenaje, curetaje, secuestrectomía, de rótula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13500', 'Drenaje, curetaje, secuestrectomía, de tibia o peroné' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13501', 'Drenaje, curetaje, secuestrectomía, de huesos pie (excepto falanges)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13502', 'Drenaje, curetaje, secuestrectomía, falanges pie (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14103', 'Drenaje, curetaje, secuestrectomía, falanges (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13300', 'Drenaje, curetaje, secuestrectomía, de pelvis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13601', 'Drenaje, curetaje, secuestrectomía, de columna vertebral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13120', 'Osteotomía en escápula o clavícula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13620', 'Osteotomía esternón o costillas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13121', 'Osteotomía en húmero' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13220', 'Osteotomía de cúbito o radio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14120', 'Osteotomía en metacarpiano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13470', 'Osteosíntesis en fémur (diáfisis)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13471', 'Osteosíntesis en fémur (cuello, intertrocantérica, supracondilea)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13422', 'Osteotomía del cuello femoral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13420', 'Osteotomía simple de fémur' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13430', 'Patelectomía o hemipatelectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13580', 'Osteosíntesis en tibia o peroné' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13520', 'Osteotomía de tibia o peroné' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13521', 'Osteotomía de huesos pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14121', 'Osteotomía en falange' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13522', 'Osteotomía falanges pie (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13320', 'Osteotomía de pelvis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13551', 'Corrección hallux valgus' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14252', 'Corrección quirúrgica dedo en martillo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13143', 'Resección tumor benigno huesos hombro' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13145', 'Resección tumor maligno huesos hombro' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6106', 'Resección de costilla (una o más)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13144', 'Resección tumor benigno húmero' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13146', 'Resección tumor maligno húmero' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13243', 'Resección tumor benigno cúbito o radio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13244', 'Resección tumor maligno cúbito o radio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14111', 'Resección tumor óseo benigno en mano, sin injerto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14113', 'Resección tumor maligno en mano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13442', 'Resección tumor benigno fémur' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13444', 'Resección tumor maligno fémur' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13443', 'Resección tumor benigno rótula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13445', 'Resección tumor maligno rótula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13546', 'Resección tumor benigno tibia o peroné' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13548', 'Resección tumor maligno tibia o peroné' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13549', 'Resección tumor maligno huesos pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13547', 'Resección tumor benigno huesos pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13102', 'Extracción de depósitoscalcáneos o bursa subdeltoideos o intratendinosos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13342', 'Resección tumor benigno huesos pelvis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13343', 'Resección tumor maligno huesos pelvis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13641', 'Resección tumor benigno en columna vertebral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13240', 'INJERTO OSEO EN CUBITO O RADIO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13140', 'INJERTO OSEO EN CLAVICULA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13141', 'INJERTO OSEO EN HUMERO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14140', 'INJERTO OSEO EN HUESOS CARPO (EXCEPTO ESCAFOIDES)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13440', 'INJERTO OSEO EN FEMUR' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13540', 'INJERTO OSEO EN TIBIA O PERONE' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13122', 'Hemidiafisectomía en clavícula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18226', 'Resección de tercio distal de clavícula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13132', 'Resección epicóndilo o epitróclea' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13123', 'Hemidiafisectomía en húmero' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13221', 'Hemidiafisectomía en cúbito o radio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13232', 'Resección extremo distal cúbito' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13231', 'Resección cabezas de radio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13230', 'Resección olecranón' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14122', 'Hemidiafisectomía metacarpianos (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14132', 'Resección cabeza de metacarpianos  (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13421', 'Hemidiafisectomía en fémur' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13524', 'Hemidiafisectomía en tibia y peroné' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14124', 'Hemidiafisectomía falanges (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14134', 'Resección cabeza de falange  (una o dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13525', 'Hemidiafisectomía en huesos pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13380', 'Hemipelvectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1378', 'Abordaje transoral por lesión cervical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13662', 'REDUCCION ABIERTA FRACTURA COLUMNA DORSAL O LUMBAR; INCLUYE APOFISIS TRANSVERSA, CUERPO VERTEBRAL, ELEMENTOS POSTERIORES DE LA COLUMNA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13124', 'Claviculectomía parcial o total' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6105', 'Costocondrectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14130', 'Carpectomía (uno a dos) huesos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13530', 'Astragalectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13592', 'Amputación de dedos pie (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13630', 'Coccigectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14141', 'Injerto óseo en escafoides' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14142', 'Injerto óseo en metacarpianos (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13541', 'Injerto óseo en pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14144', 'Injerto óseo en falanges (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13340', 'Injerto óseo en pelvis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13640', 'Injerto óseo en columna vertebral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13241', 'Epifisiodesis cúbito y radio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13542', 'Epifisiodesis tibia o peroné' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13550', 'Alargamiento miembros inferiores' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13170', 'Osteosíntesis en clavícula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13171', 'Osteosíntesis en húmero' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13271', 'Osteosíntesis en cúbito o radio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14161', 'Reducción abierta fractura metacarpianos (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13472', 'Osteosíntesis en rótula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13582', 'Osteosíntesis hueso de pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13113', 'Extracción quirúrgica de material de osteosíntesis en hombro o brazo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13110', 'Extracción cuerpo extraño de escápula o clavícula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13111', 'Extracción cuerpo extraño de húmero' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13212', 'Extracción quirúrgica de material de osteosíntesis en antebrazo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14110', 'Extracción cuerpo extraño en mano (excepto dedos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13413', 'Extracción quirúrgica de material de osteosíntesis, en muslo o rodilla' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13410', 'Extracción cuerpo extraño de fémur' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13411', 'Extracción cuerpo extraño de rótula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13513', 'Extracción quirúrgica de material de osteosíntesis en pierna, tobillo o pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13510', 'Extracción cuerpo extraño de tibia o peroné' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13511', 'Extracción cuerpo extraño pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14114', 'Extracción cuerpo extraño en dedos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13311', 'Extracción no quirúrgica de material de osteosíntesis pelvis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13310', 'Extracción cuerpo extraño de pelvis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13611', 'Extracción quirúrgica de material de osteosíntesis columna vertebral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13612', 'Extracción cuerpo extraño de columna vertebral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13160', 'Reducción abierta fractura escápula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13150', 'Reducción cerrada fractura escápula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13152', 'Reducción cerrada fractura húmero' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13252', 'Reducción cerrada fractura de colles' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13251', 'Reducción cerrada fractura cúbito o radio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14150', 'Reducción cerrada fractura huesos carpo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14151', 'Reducción cerrada fractura metacarpianos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13450', 'Reducción cerrada fractura fémur' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13451', 'Reducción cerrada fractura rótula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13560', 'Reducción cerrada fractura tibia y peroné' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13562', 'Reducción cerrada fractura tarso y/o metatarso' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14152', 'Reducción cerrada fractura falanges mano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13563', 'Reducción cerrada falanges pie (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13350', 'Reducción cerrada fractura pelvis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13650', 'Reducción cerrada fractura columna cervical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13651', 'Reducción cerrada fractura columna dorsal o lumbar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13652', 'Reducción cerrada fractura coxis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13273', 'Tratamiento fractura de colles' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18229', 'Osteosíntesis por fracturas osteocondrales o de la espinal tibial' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13660', 'Reducción abierta fractura costal; incluye una o más costillas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13162', 'Reducción abierta fractura húmero' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13261', 'Reducción abierta fractura cúbito y radio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14160', 'Reducción abierta fractura huesos carpo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13461', 'Reducción abierta fractura rótula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13570', 'Reducción abierta fractura tibia y/o peroné' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13571', 'Reducción abierta fractura tarso o metatarso' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14163', 'Reducción abierta fractura falanges mano (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13572', 'Reducción abierta fractura falanges  pie (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13360', 'Reducción abierta fractura pelvis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13661', 'Reducción abierta fractura columna cervical' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13272', 'Osteosíntesis en cúbito y radio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13581', 'Osteosíntesis de luxo fractura o fractura cuello pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13370', 'Osteosíntesis de acetábulo, reborde posterior' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13371', 'Osteosíntesis de acetábulo, compuesta (anterior, posterior y superior)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13671', 'Artrodesis anterior de columna con instrumentación' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13670', 'Artrodesis posterior de columna con instrumentación' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13750', 'Reducción cerrada luxación de hombro' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13751', 'Reducción cerrada luxación del codo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14154', 'Reducción cerrada luxación carpiana' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14155', 'Reducción cerrada luxación carpometacarpiana' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14156', 'Reducción cerrada luxación metacarpofalángica  (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14158', 'Reducción cerrada luxación interfalángica  (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13752', 'Reducción cerrada de luxación congénita de cadera' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13754', 'Reducción cerrada de luxación traumática de cadera' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13755', 'Reducción cerrada de luxación traumática de rótula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13756', 'Reducción cerrada de luxación traumática cuello de pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13760', 'Reducción abierta de luxación acromio clavicular' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13761', 'Reducción abierta de luxación escápulo humeral; incluye antigua o recidivante' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18219', 'Capsulorrafia para luxación de hombro' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13147', 'Escapulopexia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13762', 'Reducción abierta de luxación de codo; incluye antigua o recidivante' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13262', 'Reducción abierta de luxación radiocubital distal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14171', 'Reducción abierta luxación carpiana' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14172', 'Reducción abierta luxación carpometarpiana' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14173', 'Reducción abierta luxación metacarpofalángica  (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13763', 'Reducción abierta de luxación congénita de cadera; incluye salter' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13764', 'Reducción abierta de luxación traumática de cadera' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13765', 'Reducción abierta de luxación traumática de rótula' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13766', 'Reducción abierta de luxación cuello pie; incluye antigua o recidivante' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13250', 'Reducción cerrada fractura codo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13260', 'Reducción abierta fractura codo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13270', 'Osteosíntesis en codo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14170', 'Reducción abierta o percutánea fractura o luxo  fractura de Bennet' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14153', 'Reducción cerrada luxofractura de Bennet' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14165', 'Reducción abierta fractura intra articular mano (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13704', 'Artrotomía en rodilla' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18230', 'Osteosíntesis por fracturas intraarticulares u osteítis disecante en rodilla' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13574', 'Reducción abierta de luxo fractura cuello pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13565', 'Reducción cerrada luxo fractura cuello pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18242', 'Osteosíntesis franturas de tobillo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13710', 'Extracción cuerpo extraño intra articular hombro' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13711', 'Extracción cuerpo extraño intra articular codo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13712', 'Extracción cuerpo extraño intra articular muñeca' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14300', 'Artrotomía en mano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13713', 'Extracción cuerpo extraño intra articular cadera' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13714', 'Extracción cuerpo extraño intra articular rodilla' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13715', 'Extracción cuerpo extraño intra articular en cuello de pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13105', 'Artrotomía con exploración, drenaje, biopsia y  extracción de cuerpo extraño, de articulación acromioclavicular o externo clavicular' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13701', 'Artrotomía en codo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13702', 'Artrotomía en muñeca' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13703', 'Artrotomía en cadera' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13705', 'Artrotomía en cuello de pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13706', 'Artrotomía en pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18202', 'Artroscopia diagnóstica de hombro, rodilla o falanges' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18200', 'Artroscopia diagnóstica de cadera' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17812', 'Cápsula articular' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18220', 'Tratamiento de capsulitis adhesiva de hombro' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14301', 'Capsulotomía metacarpofalángicas (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14303', 'Capsulotomía interfalángicas (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18225', 'Debridamiento en fibrocartílago triangular en muñeca' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18212', 'Extracción de cuerpos libres intraarticulares en muñeca o falanges' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14520', 'Bandas constrictivas (Streeter)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18213', 'Sinovectomía: Cualquier articulación, excepto falanges' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13556', 'Corrección pie varus equino' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13555', 'Corrección pie tallus valgus' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1371', 'Disquectomía cervical, abordaje anterior sin artrodesis, un solo interespacio' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13720', 'Resección de disco intervertebral (hernia discal)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1315', 'Microdiscoidectomía, uno o más interespacios' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1375', 'Cirugía de Cloward' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1383', 'Discólisis enzimática' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13721', 'Meniscectomía rodilla' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18233', 'Menisectomía media o lateral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13722', 'Sinovectomía rodilla' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13104', 'Artrotomía con exploración, drenaje, biopsia cuerpo extraño o sinovectomía, de articulación glenohumeral (hombro)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14344', 'Sinovectomía carpo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14347', 'Sinovectomía una a dos interfalángicas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14345', 'Sinovectomía una a dos metacarpo falángicas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18214', 'Sinovectomía de falanges' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18210', 'Extracción de cuerpos libres intraarticulares en cadera' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18237', 'Tratamiento de artritis séptica de rodilla' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13824', 'Resección higroma rodilla' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18221', 'Remoción de plicas en codo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18238', 'Resección de plica en rodilla' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18216', 'Condroplastia de codo, muñeca, cadera o tobillo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13740', 'Artrodesis simple de columna' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13745', 'Artrodesis de pie (triple o cuello de pie)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13746', 'Artrodesis dedos pie (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13743', 'Artrodesis de cadera' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13744', 'Artrodesis de rodilla' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13741', 'Artrodesis de hombro' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13233', 'Resección tercio distal cúbito con artrodesis radio cubital distal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13742', 'Artrodesis de codo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14311', 'Artrodesis puño sin injerto óseo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14310', 'Artrodesis puño con injerto óseo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14312', 'Artrodesis trapecio metacarpiana' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14316', 'Artrodesis carpometacarpianas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14313', 'Artrodesis metacarpo falángica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14314', 'Artrodesis una interfalángica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14315', 'Artrodesis interfalángicas, con injerto óseo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14317', 'Artrodesis intercarpiana' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14318', 'Artrodesis intercarpiana más injerto óseo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18222', 'Artrodesis escafosemilunar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13736', 'Artroplastia falanges pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15277', 'Plastia artejos (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13732', 'Artroplastia parcial de la cadera' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13723', 'Corrección quirúrgica primaria de lesión en ligamentos de rodilla' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13724', 'Corrección quirúrgica rótula luxable' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13725', 'Corrección quirúrgica ligamentaria sustitutiva por auto injerto o aloinjerto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18234', 'Reconstrucción de ligamento cruzado anterior con injerto autólogo o con aloinjerto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18235', 'Reconstrucción de ligamento cruzado posterior con injerto autólogo o aloinjerto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18215', 'Condroplastia de hombro o rodilla' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18241', 'Relajación de retináculo lateral más osteotomía de realineación, más plicatura de retináculo medial en rodilla' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18239', 'Relajación de retináculo lateral en rodilla' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18240', 'Relajación de retináculo lateral más osteotomía de realineación en rodilla' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18236', 'Sutura de menisco, medial o lateral' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18231', 'Liberación de adherencias en rodilla' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14332', 'Ligamentorrafia o reinserción ligamentos (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18243', 'Reparación del ligamento peroneoastragalino anterior' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13733', 'Implante total de cadera por prótesis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13734', 'Implante total de rodilla por prótesis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13735', 'Reemplazo protésico cuello de pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14321', 'Artroplastia trapecio metacarpiana' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14320', 'Artroplastia puño' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14322', 'Artroplastia metacarpo falángicas (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18217', 'Condroplastia de falanges' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14324', 'Artroplastia interfalángicas (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13730', 'Reemplazo protésico de hombro' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13148', 'Acromioplastia; incluye resección calcificaciones' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18218', 'Acromioplastia más extracción de calcificaciones' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13731', 'Reemplazo protésico de codo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13801', 'Fasciotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18224', 'Sutura de fibrocartílago triangular en muñeca' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14330', 'Capsulorrafia articulaciones (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13830', 'Sutura de fascia y/o músculo y/o tendón' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13836', 'Alargamiento del tendón de Aquiles' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14253', 'Corrección quirúrgica dedo en gatillo (dedo en resorte)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14349', 'Resección de quiste vaina tendinosa' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14200', 'Miotomía mano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13823', 'Bursectomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14350', 'Fasciotomía mano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14254', 'Tenotomía mano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13820', 'Resección de ganglión' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14305', 'Resección ganglión puño' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14204', 'Extirpación tumor músculo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14340', 'Tenosinovectomía extensores mano (una a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14342', 'Tenosinovectomía flexores mano (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13827', 'Tenosinovectomía (enfermedad de Quervain)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14351', 'Extirpación aponeurosis mano (Enf de Dupuytren)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14215', 'Tenorrafia flexores dedos (cada uno)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14213', 'Tenorrafia flexores mano (uno a cuatro), con neurorrafias' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14212', 'Tenorrafia extensores dedos (cada uno)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14210', 'Tenorrafia extensores mano (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14201', 'Miorrafia extensores mano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14202', 'Miorrafia flexores mano (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14222', 'Alargamiento tendón mano (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14226', 'Transferencia tendón mano y puño (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14224', 'Reinserción tendón mano (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14604', 'Pulgarización dedo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14234', 'Injerto de tendón flexor un dedo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14235', 'Injerto de tendón flexor dos o más dedos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14501', 'CORRECCION QUIRURGICA CICATRIZ EN MANO CON COLGAJO A DISTANCIA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14230', 'Injerto de tendón extensor mano (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14232', 'Injerto de tendón flexor mano (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14236', 'Primer tiempo injerto tendinoso (implante de silastic) un dedo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14605', 'Transposición dedo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14603', 'Trasplante dedo del pie a mano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14513', 'Tratamiento quirúrgico mano zamba radial' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14510', 'Corrección sindactilia (un espacio)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14511', 'Corrección sindactilia (dos espacios)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14512', 'Macrodactilia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14515', 'Corrección quirúrgica camptodactilia (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14517', 'Corrección quirúrgica clinodactilia (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14519', 'Corrección polidactilia (dedos supernumerario)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14522', 'Deformidad de madelung' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14250', 'Corrección quirúrgica dedo en botonera' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14251', 'Corrección quirúrgica dedo en cuello de cisne' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14220', 'Tenodesis mano (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15280', 'CORRECCION PARALISIS FACIAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14240', 'Tenolisis extensores mano (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14242', 'Tenolisis flexores mano (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15103', 'Desbridamiento por lesión de tejidos profundos, más del 5% área corporal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13826', 'Resección de bolsa tendinosa, fascia, músculo o tendón' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16268', 'Miotomía temporal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16269', 'Miotomía pterigoideo externo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16267', 'Miotomía macetero' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13800', 'Tenotomía' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17815', 'Músculo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13822', 'Resección tumor de fascia y/o músculo y/o tendón' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13821', 'Resección de miositis osificante' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13825', 'Resección quiste poplíteo (quiste de Baker)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13831', 'Tenorrafia flexores antebrazo (uno a cuatro), con neurorrafia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13727', 'Reparación del manguito rotador del hombro' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18228', 'Sutura del manguito rotador' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13834', 'Transposición de tendón' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13833', 'Transposición de músculo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14227', 'Transferencia tendón mano y puño (tres o más)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13835', 'Cuadricepsplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18232', 'Liberación de adherencias más cuadriceplastia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13837', 'Tenodesis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13838', 'Liberación de adherencias de tendón (tenolisis)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13810', 'Extracción de cuerpo extraño en bolsa sinovial y/o músculo y/o tendón' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13181', 'Amputación del brazo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14180', 'Amputación y/o desarticulación dedos mano (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14182', 'Amputación de la mano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13282', 'Desarticulación de la muñeca' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13280', 'Amputación del antebrazo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13281', 'Desarticulación del codo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13182', 'Desarticulación del hombro' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13180', 'Amputación intertoracoescapular' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13590', 'Amputación de la pierna' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13591', 'Amputación del pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13594', 'Desarticulación pie; incluye mediotarsiana (Chopart), tarsometatarsiana (Lisfranc),   supramaleolar (Syme)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13481', 'Desarticulación de la rodilla' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13480', 'Amputación del muslo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13381', 'Desarticulación de la cadera' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14601', 'Reimplante de un dedo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14602', 'Reimplante de dos o más dedos' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13290', 'Reimplante de miembro superior a nivel del antebrazo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14600', 'Reimplante de la mano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13190', 'Reimplante de miembro superior a nivel del brazo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13558', 'Reimplante de pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13557', 'Reimplante de la pierna' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13142', 'Revisión y/o reconstrucción de muñón de amputación hombro o brazo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13242', 'Revisión y/o reconstrucción de muñón de amputación antebrazo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14185', 'Revisión y/o reconstrucción muñón de amputación mano' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14183', 'Revisión y/o reconstrucción muñón de amputación dedos mano (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13441', 'Revisión y/o reconstrucción de muñón de amputación muslo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13543', 'Revisión y/o reconstrucción de muñón de amputación pierna' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13545', 'Revisión y/o reconstrucción de muñón de amputación artejos pie (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13341', 'Revisión y/o reconstrucción de muñón de amputación pelvis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13552', 'Corrección pie cavo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13553', 'Corrección pie cavo equino' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14521', 'Sinostosis radiocubital' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '10101', 'Mastotomía; incluye drenaje de la mama' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '10102', 'Extracción cuerpo extraño mama; incluye granuloma' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17910', 'Glándula mamaria' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '10114', 'Estirpación fibroadenoma' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '10116', 'Cuadrantectomía con o sin vaciamiento' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '10120', 'Escisión tejido aberrante mama (glándula supernumeraria)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15257', 'Mamoplastia de reducción' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15258', 'Reconstrucción seno con colgajo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '10113', 'MASTECTOMIA RADICAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '10111', 'Mastectomía radical modificada o simple ampliada con implante' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '10112', 'Mastectomía radical modificada o simple ampliada sin implante' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15237', 'Injerto de piel retracción del seno' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15140', 'Colgajo de piel regional' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15256', 'Cirugía reparadora de seno; incluye reconstrucción de: areola, pezón, pezón invertido' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15109', 'Extracción cuerpo extraño en piel o tejido celular subcutáneo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8260', 'Drenaje de quiste pilonidal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8261', 'Resección quiste pilonidal; incluye la efectuada por cierre parcial, extirpación abierta o marsupialización' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15260', 'Tratamiento quirúrgico quemaduras cara' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15102', 'Desbridamiento por lesión superficial, más del 5% área corporal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15261', 'Tratamiento quirúrgico quemaduras cuello' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15263', 'Tratamiento quirúrgico quemaduras en manos (no incluye dedos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15264', 'Tratamiento quirúrgico quemaduras pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15160', 'Tratamiento quirúrgico quemaduras en área general, hasta 5% (tratamiento total)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15161', 'Tratamiento de quemaduras en área general de 6 a 15% (tratamiento total)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15162', 'Tratamiento de quemaduras en área general de 16 a 25% (tratamiento total)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15163', 'Tratamiento de quemaduras en área general de 26% en adelante (tratamiento total)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15183', 'Dermoabración área general' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15283', 'Dermoabración cara (total)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15282', 'Dermoabración cara (parcial)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15200', 'Onicectomía una a dos uñas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15104', 'Fistulectomía de piel y/o tejido celular  subcutáneo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15105', 'Resección tumor benigno de piel y/o tejido celular subcutáneo, excepto cara' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15107', 'Resección tumor maligno de piel y/o tejido celular subcutáneo, excepto cara' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15108', 'Resección tumor maligno de piel que requiera  reparación con colgajo y/o injerto' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15202', 'Resección tumor benigno de piel y/o tejido celular subcutáneo, en cara' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15212', 'Avulsión cuero cabelludo (escalpe)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15130', 'Injerto de piel en área general hasta 5%' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15131', 'Injerto de piel en área general entre 6 a 15%' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15132', 'Injerto de piel en área general más del 16%' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15235', 'Tratamiento qirúrgico para alopecia  post secuelas de trauma' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15240', 'Colgajo de cuero cabelludo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15241', 'Colgajo de piel a distancia (incluidos varios tiempos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15170', 'Corrección quirúrgica cicatriz en área general hasta 5%' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15272', 'Corrección quirúrgica cicatriz en genitales' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15270', 'Corrección quirúrgica cicatriz en cara' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15180', 'Dermolipectomía abdominal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15172', 'Plastia en Z, (una a dos) en área general' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15173', 'Plastia en Z, (tres o más) en área general' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15273', 'Plastia en Z (una a dos), en área especial; incluye: cara, cuello, genitales, planta de pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15274', 'Plastia en Z (tres o más), en área especial; incluye: cara, cuello, genitales, planta de pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14502', 'Plastia en Z, mano o dedos (uno a dos)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14503', 'Plastia en Z, mano o dedos (tres o más)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15275', 'Plastia en Z zonas de flexión (no incluye dedos); incluye: axila, codo, cuello, dorso de pie, hueco poplíteo, región inguinal' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15284', 'Reposición uña de polietileno' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15112', 'Tratamiento hiperhidrosis axilar' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15113', 'Tratamiento hidradenitis' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15182', 'Tratamiento quirúrgico linfedema' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15181', 'Expansores tisulares (1  tiempo)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18303', 'Estudio de motilidad esofágica' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13726', 'Movilización articular bajo anestesia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13610', 'Extracción cuerpo extraño de esternón o costillas' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18810', 'Extracción cuerpo extraño en uretra' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13211', 'Extracción no quirúrgica de material de osteosíntesis en antebrazo' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13512', 'Extracción no quirúrgica de material de osteosíntesis en pierna, tobillo o pie' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13412', 'Extracción no quirúrgica de material de osteosíntesis, en muslo o rodilla' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1222', 'DERIVACION VENTRICULO PLEURAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1226', 'DRENAJE DE QUISTE HACIA AURICULA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1261', 'IMPLANTACION DE MARCAPASOS TIPO CEREBELOSO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1323', 'LAMINECTOMIA PARA RIZOTOMIA, MAS DE DOS SEGMENTOS' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1325', 'LAMINECTOMIA PARA CORDOTOMIA, BILATERAL, EN UN TIEMPO, CERVICAL O DORSAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1326', 'LAMINECTOMIA PARA CORDOTOMIA, BILATERAL, EN DOS TIEMPOS, CERVICAL O DORSAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1342', 'ESTIMULACION ESTEREOTAXICA DE LA MEDULA, PERCUTANEA O PROCEDIMIENTO SEPARADO NO SEGUIDO DE CIRUGIA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1344', 'LAMINECTOMIA PARA IMPLANTACION DE ELECTRODOS DE NEUROESTIMULACION, EXTRADURALES' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '1347', 'INCISION PARA LA COLOCACION SUBCUTANEA DE RECEPTOR DE NEUROESTIMULACION, ACOPLAMIENTO DIRECTO O INDUCTIVO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2112', 'RESECCION TUMOR GLANDULA LAGRIMAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2140', 'REMOCION CALCULOS CANALICULOS LAGRIMALES' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2142', 'ECTROPION PUNTO LAGRIMAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2210', 'CAUTERIZACION CHALAZION' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2242', 'CORRECCION ENTROPION CON EXCESO DE LAXITUD HORIZONTAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2243', 'CORRECCION ENTROPION RECURRENCIA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2244', 'ENTROPION POR INFECCION CON ECTROPION PUNTO LAGRIMAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2263', 'CORRECCION EPICANTO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2266', 'CORRECCION TELECANTO, BLEFAROFIMOSIS Y EPICANTO (CONGENITA)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2304', 'RESECCION PTERIGION REPRODUCIDO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2703', 'IRIDODIALISIS POSTERIOR' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2810', 'RETINOPEXIAS; INCLUYE BUCLE ESCLERAL TOTAL O PARCIAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2814', 'RETINOPEXIA INTRAQUIRURGICA CON LASER; INCLUYE BUCLE ESCLERAL TOTAL O PARCIAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2901', 'EXTRACCION CATARATA POR FACOEMULSIFICACION, MAS LENTE INTRAOCULAR' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2906', 'INCLUSION SECUNDARIA DE LENTE INTRAOCULAR' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2908', 'EXTRACCION CATARATA MAS LENTE INTRAOCULAR SUTURADO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '2913', 'VITRECTOMIA CON INSERCION DE SILICON Y/O GASES' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '3423', 'TURBINECTOMIA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '4122', 'RESECCION QUISTE TIROGLOSO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '5532', 'LIGADURA DE CONDUCTO ARTERIOSO PERSISTENTE' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '6103', 'TORACOSTOMIA CON DRENAJE ABIERTO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '7661', 'CIERRE DE FISTULA DE GASTROYEYUNOSTOMIA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '8136', 'COLECTOMIA TOTAL MAS DESCENSO ILEAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9104', 'NEFROSTOMIA PERCUTANEA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9343', 'CISTOPEXIA VAGINAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9347', 'CISTOURETROPEXIA VAGINAL CON CONTROL ENDOSCOPICO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9374', 'TRATAMIENTO HIDROSTATICO PARA TUMOR VESICAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9437', 'RESECCION DE FISTULA URETROCUTANEA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9456', 'DRENAJE DE ABSCESO URINOSO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9514', 'PROSTATECTOMIA TOTAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9812', 'AMPUTACION TOTAL DEL PENE; INCLUYE LINFADENECTOMIA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '9823', 'EXTIRPACION DE NODULOS DE LA ENFERMEDAD DE PEYRONIE CON INJERTO DE PIEL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '10117', 'RESECCION QUISTE' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11211', 'SALPINGOOFOROSTOMIA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11240', 'RESECCION DE TUMOR TROMPA DE FALOPIO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11400', 'HISTEROTOMIA TOTAL ABDOMINAL, POR ENDOMETRITIS' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11502', 'DRENAJE VAGINA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11603', 'EXTRACCION CUERPO EXTRAÑO VULVA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '11612', 'RESECCION TUMOR BENIGNO VULVA; INCLUYE TUMORES DE PERINE, TUMORES PARAVAGINALES' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13131', 'RESECCION EXTREMOS CLAVICULA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13523', 'OSTEOTOMIA FALANGES PIE (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13544', 'REVISION Y/O RECONSTRUCCION DE MUÑON DE AMPUTACION PIE O DE TRES O MAS ARTEJOS' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13561', 'REDUCCION CERRADA FRACTURA PERONE' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13564', 'REDUCCION CERRADA FALANGES PIE (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13573', 'REDUCCION ABIERTA FRACTURA FALANGES  PIE (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13600', 'DRENAJE, CURETAJE, SECUESTRECTOMIA, DE ESTERNON O COSTILLAS' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13653', 'REDUCCION CERRADA FRACTURA COSTAL; INCLUYE UNA  O MAS COSTILLAS' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13700', 'ARTROTOMIA EN HOMBRO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13747', 'ARTRODESIS DEDOS PIE (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13753', 'REDUCCION DISPLASIA UNI O BILATERAL DE CADERA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13832', 'TENORRAFIA FLEXORES ANTEBRAZO (CINCO O MAS), CON NEURORRAFIA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13861', 'NEURORRAFIA DOS NERVIOS BRAZO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13863', 'NEURORRAFIA DOS NERVIOS ANTEBRAZO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13864', 'NEURORRAFIA DE UN NERVIO EN BRAZO CON INJERTO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13865', 'NEURORRAFIA DE DOS NERVIOS EN BRAZO CON INJERTO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13866', 'NEURORRAFIA DE UN NERVIO EN ANTEBRAZO CON INJERTO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13867', 'NEURORRAFIA DE DOS NERVIOS EN ANTEBRAZO CON INJERTO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13871', 'NEURORRAFIA NERVIO MUSLO CON INJERTO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '13872', 'NEURORRAFIA NERVIO PIERNA CON INJERTO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14102', 'DRENAJE, CURETAJE, SECUESTRECTOMIA, METACARPIANOS (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14104', 'DRENAJE, CURETAJE, SECUESTRECTOMIA, FALANGES (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14112', 'RESECCION TUMOR OSEO BENIGNO EN MANO, CON INJERTO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14123', 'HEMIDIAFISECTOMIA METACARPIANOS (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14125', 'HEMIDIAFISECTOMIA FALANGES (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14131', 'CARPECTOMIA (TRES O MAS) HUESOS' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14133', 'RESECCION CABEZA DE METACARPIANOS (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14135', 'RESECCION CABEZA DE FALANGE (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14143', 'INJERTO OSEO EN METACARPIANOS (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14145', 'INJERTO OSEO EN FALANGES (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14157', 'REDUCCION CERRADA LUXACION METACARPOFALANGICA (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14159', 'REDUCCION CERRADA LUXACION INTERFALANGICA (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14162', 'REDUCCION ABIERTA FRACTURA METACARPIANOS  (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14164', 'REDUCCION ABIERTA FRACTURA FALANGES MANO (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14166', 'REDUCCION ABIERTA FRACTURA INTRA ARTICULAR MANO (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14174', 'REDUCCION ABIERTA LUXACION METACARPOFALANGICA (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14175', 'REDUCCION ABIERTA LUXACION INTERFALANGICA (UNA A DOS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14176', 'REDUCCION ABIERTA LUXACION INTERFALANGICA (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14181', 'AMPUTACION Y/O DESARTICULACION DEDOS MANO (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14184', 'REVISION Y/O RECONSTRUCCION MUÑON DE AMPUTACION DEDOS MANO (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14203', 'MIORRAFIA FLEXORES MANO (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14211', 'TENORRAFIA EXTENSORES MANO (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14214', 'TENORRAFIA FLEXORES MANO (CINCO O MAS)  CON NEURORRAFIAS' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14221', 'TENODESIS MANO (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14223', 'ALARGAMIENTO TENDON MANO (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14225', 'REINSERCION TENDON MANO (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14231', 'INJERTO DE TENDON EXTENSOR MANO (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14233', 'INJERTO DE TENDON FLEXOR MANO (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14241', 'TENOLISIS EXTENSORES MANO (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14243', 'TENOLISIS FLEXORES MANO (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14302', 'CAPSULOTOMIA METACARPOFALANGICAS (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14304', 'CAPSULOTOMIA INTERFALANGICAS (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14323', 'ARTROPLASTIA METACARPO FALANGICAS(TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14325', 'ARTROPLASTIA INTERFALANGICAS (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14331', 'CAPSULORRAFIA ARTICULACIONES (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14333', 'LIGAMENTORRAFIA O REINSERCION LIGAMENTOS (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14341', 'TENOSINOVECTOMIA EXTENSORES MANO (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14343', 'TENOSINOVECTOMIA FLEXORES MANO (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14346', 'SINOVECTOMIA TRES O MAS METACARPO FALANGICAS' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14348', 'SINOVECTOMIA TRES O MAS INTERFALANGICAS' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14402', 'DESCOMPRESION NERVIO DEDOS (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14421', 'NEURORRAFIA DOS NERVIOS MANO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14423', 'NEURORRAFIA DE COLATERALES EN DOS DEDOS' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14424', 'NEURORRAFIA DE COLATERALES EN TRES O MAS DEDOS' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14425', 'NEURORRAFIA DE UN NERVIO EN MANO CON INJERTO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14426', 'NEURORRAFIA DE DOS NERVIOS EN MANO CON INJERTO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14427', 'NEURORRAFIA DE COLATERALES EN UN DEDO CON INJERTO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14428', 'NEURORRAFIA DE COLATERALES EN DOS DEDOS CON INJERTO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14429', 'NEURORRAFIA DE COLATERALES EN TRES O MAS DEDOS CON INJERTO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14432', 'NEUROLISIS NERVIO DEDOS (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14516', 'CORRECCION QUIRURGICA CAMPTODACTILIA (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '14518', 'CORRECCION QUIRURGICA CLINODACTILIA (TRES O MAS)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15133', 'LIPOINJERTO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15201', 'ONICECTOMIA TRES O MAS UÑAS' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '15222', 'QUEILOPLASTIA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16212', 'RESECCION LESION SUPERFICIAL DE LENGUA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16266', 'MIOTOMIA MUSCULOS MASTICATORIOS; INCLUYE PARCIAL DE MASETERO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16272', 'CIERRE FISTULA OROANTRAL CON COLGAJO PALATINO O LINGUAL; INCLUYE ORONASAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16333', 'FRENILLECTOMIA EN V (RESECCION CUÑA)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16334', 'FRENILLECTOMIA EN Z' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16336', 'EXOSTOSIS MANDIBULAR' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16337', 'OSTEOTOMIA DESLIZANTE (VISERA)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16343', 'INJERTOS ALOPLASTICOS METALICOS (TECNICA DE LAMINA FENESTRADA)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16344', 'INJERTOS ALOPLASTICOS METALICOS (TECNICA SUBPERIOSTICA)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16348', 'IMPLANTE DE OSEOINTEGRACION E INJERTO PARA ELEVACION DEL PISO DE SENO MAXILAR' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16349', 'IMPLANTE DE OSEOINTEGRACION CON DESPLAZAMIENTO DEL NERVIO DENTARIO INFERIOR E INJERTO OSEO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16516', 'EXTIRPACION DE TUMOR ODONTOGENICO ENCAPSULADO (COMPROMISO DE NERVIO DENTARIO INFERIOR O SENO MAXILAR)' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16517', 'EXTIRPACION DE TUMOR ODONTOGENICO NO ENCAPSULADO' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '16519', 'EXTIRPACION DE TUMOR ODONTOGENICO CEMENTIFICANTE' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17102', 'MEDULA ESPINAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17104', 'NERVIO PERIFERICO SUPERFICIAL' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '17814', 'LIGAMENTO biopsia' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18512', 'SIGMOIDOSCOPIA PARA RESECCION DE POLIPOS, CONTROL DE HEMORRAGIA O FULGURACION DE LESION DE MUCOSA' , 1, now(), now(), 'f', 1);

 INSERT INTO "rate_procedures"("custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ( '18818', 'RESECCION TRANSURETRAL DE DIVERTICULOS URETRALES' , 1, now(), now(), 'f', 1);
