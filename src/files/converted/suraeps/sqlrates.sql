
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30001', 'ABORDAJES COMBINADOS FOSA MEDIA + FOSA POSTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30002', 'ABORDAJES COMBINADOS SUPRA E INFRATENTORIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30003', 'ABORDAJES COMBINADOS FOSA ANTERIOR + FOSA MEDIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30015', 'BIOPSIA DE MENINGE; POR CRANEOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30023', 'COLOCACION DE SENSOR INTRACRANEAL PARA MONITOREO DE PRESION O CATETER INTRAVENTRICULAR, SUBDURAL O INTRAPARENQUIMATOSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30025', 'CRANEOPLASTIA PARA DEFECTO CONGENITO CON CIRUGIA REPARATIVA DEL CEREBRO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30027', 'CRANEOTOMIA PARA CIERRE DE FISTULA DE LCR PISO ANTERIOR (RINOLAQUIA) O MEDIO (OTOLIQUIA) UN SOLO LADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30028', 'CRANEOTOMIA PARA DRENAJE DE ABCESO INTRACEREBRAL O EMPIEMA UN SOLO LADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30029', 'CRANEOTOMIA PARA DRENAJE DE HEMATOMA EPIDURAL AGUDO DE UN LADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30030', 'CRANEOTOMIA PARA DRENAJE DE HEMATOMA SUBDURAL Y COLOCACION DE CATETER UN SOLO LADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30031', 'CRANEOTOMIA PARA RESECCION DE TUMOR OSEO CRANEAL U OSTEOMIELITIS SIN COMPROMISO DE DURAMADRE SIN CRANEOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30032', 'CRANEOTOMIA POR DISPLASIA FIBROSA SIN PLASTIA EN BOVEDA CRANEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30033', 'CRANEOTOMIA POR FRACTURA MINUTA DEPRIMIDA MAS REPARO DE DURA O TEJIDO NERVIOSO, DEBIDA A CUALQUIER TIPO DE TRAUMA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30034', 'CRANEOTOMIA POR LESION OSEA MAS PLASTIA DE DURA Y/O CRANEOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30035', 'CRANEOTOMIA SUBOCCIPITAL PARA ABSCESOS O EMPIEMAS EN FOSA POSTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30037', 'CRANEOTOMIA Y/O CRANEOTOMIA DESCOMPRESIVA PARA DRENAJE DE HEMATOMA SUBDURAL O INTRAPARENQUIMATOSO AGUDO UN SOLO LADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30044', 'DRENAJE DE ESPACIO SUBDURAL, POR DERIVACION SUBDURO PERITONEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30050', 'IMPLANTACION DE NEUROESTIMULADOR POR CRANEOTOMIA GUIADA POR ESTEREOTAXIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30052', 'CRANEOTOMIA PARA RESECCION TUMOR INFRATENTORIALES DEL DERMIS CEREBELOSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30053', 'CRANEOTOMIA PARA RESECCION TUMORINFRATENTORIALES TALLO Y IV VENTRICULO Y ANGULO PONTO CEREBELOSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30056', 'PALIDOTOMIA POR ESTEREOTAXIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30059', 'PLASTIA DE DURA MADRE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30061', 'PUNCION CISTERNAL PARA ESTUDIO DE LCR C MAGNA O INYECCION DE SUSTANCIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30062', 'PUNCION DE RESERVORIO DE VALVULA DE DERIVACION PARA ASPIRACION PARA ASPIRACION O INYECCION FARMACOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30063', 'PUNCION SUBDORAL POR FONTANELA EN LACTANTES, UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30064', 'PUNCION VENTRICULAR A TRAVES DE ORIFICIO TREPANACION PREVIO (YA EXISTENTE), POR FONTANELA O POR RESERVORIO PARA ASPIRACION O PARA INYECCION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30065', 'PUNCION VENTRICULAR CON AGUJERO DE TREPANACION O INYECCION PARA VENTRICULOGRAFIA O TRATAMIENTO MEDICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30069', 'SECCION DE TEJIDO CEREBRAL (TRACTOS CEREBRALES) POR ABLACION [TERMOLESION] ESTEREOTAXICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30070', 'SECCION V PAR, VIA SUBTEMPORAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30075', 'SUPRATENTORIALES FOSA MEDIA (INCLUYE PARIETAL Y OCCIPITAL), TUMORES DE LINEA MEDIA CUERPO CALLOSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30076', 'SUPRATENTORIALES FOSA MEDIA (INCLUYE PARIETAL Y OCCIPITAL), TUMORES DE LINEA MEDIA EN GENERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30077', 'SUPRATENTORIALES FOSA MEDIA (INCLUYE PARIETAL Y OCCIPITAL), TUMORES DE LINEA MEDIA III VENTRICULO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30078', 'SUPRATENTORIALES FOSA MEDIA (INCLUYE PARIETAL Y OCCIPITAL), TUMORES DE LINEA MEDIA REGION PINEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30079', 'SUPRATENTORIALES FOSA MEDIA (INCLUYE PARIETAL Y OCCIPITAL), TUMORES DE LINEA MEDIA REGION SELLAR E HIPOTALAMICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30080', 'SUPRATENTORIALES FOSA MEDIA (INCLUYE PARIETAL Y OCCIPITAL), TUMORES DE LINEA MEDIA REGION TALAMICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30081', 'SUPRATENTORIALES FOSA MEDIA (INCLUYE PARIETAL Y OCCIPITAL), TUMORES DE LINEA MEDIA SISTEMA VENTRICULAR SUPRATENTORIAL (NO INCLUYE III VENTRICULO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30085', 'TUMORES DE LA BASE DEL CRANEO (GLOMUS Y YUGULARE)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30086', 'TUMORES DE LA BASE DEL CRANEO (SENO ESFENOIDAL Y SILLA TURCA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31002', 'ABORDAJE DE TUMORES TUMORES EPIDURALES EN LA MEDULA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31003', 'ARTRODESIS (CADA ESPACIO ADICIONAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31008', 'ARTRODESIS POR VIA ANTERIOR ATLAS-AXIS (C1-C2)MAS INJERTO (INCLUYE OBTENCION DEL INJERTO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31009', 'ARTRODESIS VIA ANTERIOR PARA DEFORMIDAD ESPINAL (ESCOLIOSIS, CIFOSIS) CON O SIN YESO MAS INJERTO, 4 - 7 VERTEBRAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31010', 'ARTRODESIS VIA ANTERIOR PARA DEFORMIDAD ESPINAL CON O SIN YESO MAS INJERTO (AUTO O ALOINJERTO), 2 O 3 VERTEBRAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31011', 'ARTRODESIS VIA ANTERIOR PARA DEFORMIDAD ESPINAL ESCOLIOSIS, CIFOSIS) CON O SIN YESO MAS INJERTO, 8 O MAS VERTEBRAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31012', 'ARTRODESIS VIA POSTERIOR PARA DEFORMIDAD ESPINAL (ESCOLIOSIS, CIFOSIS) CON O SIN YESO MAS INJERTO, 6 VERTEBRAS O MENOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31013', 'ARTRODESIS VIA POSTERIOR PARA DEFORMIDAD ESPINAL (ESCOLIOSIS, CIFOSIS) CON O SIN YESO MAS INJERTO, 7 O MAS VERTEBRAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31014', 'BIOPSIA ABIERTA DE CUERPO VERTEBRAL LUMBAR O CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31015', 'BIOPSIA ABIERTA DE CUERPO VERTEBRAL TORACICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31016', 'BIOPSIA ABIERTA POSTERIOR DE VERTEBRA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31018', 'BIOPSIA DE CUERPO VERTEBRAL CON TROCAR O AGUJA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31022', 'ABORDAJE DE ATLAS O AXIS VIA TRANSORAL CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31044', 'DERIVACION LUMBAR SUBARACNOIDEA PERITONEAL O PLEURAL PERCUTANEA MAS LAMINECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31045', 'DERIVACION LUMBAR SUBARACNOIDEA PERITONEAL O PLEURAL PERCUTANES SIN LAMINECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31046', 'DERIVACION LUMBO - SUBARACNOIDEA REMOCION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31047', 'DERIVACION LUMBO - SUBARACNOIDEA REVISION O REEMPLAZO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31055', 'DISCECTOMIA ANTERIOR CERVICAL INCLUYENDO RESECCION OSTEOFITOS (ESPACIO ADICIONAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31057', 'DISCECTOMIA ANTERIOR TORACICA 1 ESPACIO, INCLUYENDO RESECCION OSTEOFITOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31058', 'DISCETOMIA PERCUTANEA (NUCLEOTOMO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31059', 'DISCOIDECTOMIA CERVICAL VIA ANTERIOR MAS FUSION (INCLUYE TOMA INJERTO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31060', 'DISCOIDECTOMIA CERVICAL VIA POSTERIOR MAS FORAMINECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31065', 'DRENAJE ABSCESO O HEMATOMA PROFUNDO CUELLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31090', 'INMOVILIZACION BLANDA DE ESGUINCE DE COLUMNA CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31092', 'INMOVILIZACION CON YESO MINERVA DE FRACTURA DE COLUMNA CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31093', 'INMOVILIZACION YESO DE ESGUINCE DE COLUMNA CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31094', 'INMOVILIZACION YESO MINERVA (HALO CHAQUETA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31159', 'REPRACION DE MERINGOCELE LUMBOSACRO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31165', 'OSTEOTOMIA VERTEBRAL ABORDAJE POSTERIOR, PARA CORRECION DEFORMIDAD, 1 SEGMENTO, TORACICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31166', 'OSTEOTOMIA VERTEBRAL ABORDAJE ANTERIOR, PARA CORRECION DEFORMIDAD, 1 SEGMENTO, TORACXICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31167', 'OSTEOTOMIA VERTEBRAL ABORDAJE ANTERIOR, PARA CORRECION DEFORMIDAD, 1 SEGMENTO, CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31168', 'OSTEOTOMIA VERTEBRAL ABORDAJE ANTERIOR, PARA CORRECION DEFORMIDAD, 1 SEGMENTO, LUMBAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31169', 'OSTEOTOMIA VERTEBRAL ABORDAJE POSTERIOR, PARA CORRECION DEFORMIDAD, 1 SEGMENTO, CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31170', 'OSTEOTOMIA VERTEBRAL PARA CORRECION DE DEFORMIDAD CADA SEGMENTO ADICIONAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31173', 'PUNCION LUMBAR CON INYECCION DE MATERIAL RADIACTIVO (HIDROCEFALIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31213', 'SECCION ESTERNOCLEIDOMASTOIDEO POR TORTICULIS CONGENITA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31221', 'TRATAMIENTO CERRADO FRACTURA SACRO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31222', 'TRATAMIENTO CON REDUCCION POR MANIPULACION O TRACCION E INMOVILIZACION CON YESO LUXOFRACTURA O FRACTURA COLUMNA DORSOLUMBAR Y SACRO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32012', 'BLOQUEO NERVIO INTERCOSTAL (MULTIPLE)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32037', 'DESCOMPRESION NERVIO PERIFERICO EN PUQO, MANO, DEDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32040', 'DESCOMPRESION NERVIO PLANTAR DIGITAL EXCEPTO MEDIANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32064', 'INJERTO DE NERVIO (INCLUYE OBTENCION DE INJERTO) MULTIPLES CABLES MANO O PIE (HASTA 4 CMS, UN SOLO NERVIO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32066', 'INJERTO DE NERVIO (INCLUYE OBTENCION DE INJERTO), 1 CABLE BRAZO, ANTEBRAZO O MUSLO, PIERNA (MAS DE 4 CMS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32067', 'INJERTO DE NERVIO (INCLUYE OBTENCION DE INJERTO), 1 CABLE MANO O PIE (HASTA 4 CMS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32068', 'INJERTO DE NERVIO (INCLUYE OBTENCION DE INJERTO), 1 CABLE MANO O PIE (MAS DE 4 CMS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32069', 'INJERTO DE NERVIO (INCLUYE OBTENCION DE INJERTO), MULTIPLES CABLES MANO O PIE (MAS DE 4 CMS, UN SOLO NERVIO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32070', 'INJERTO DE NERVIO (INCLUYE OBTENCION DEL INJERTO) UNO SOLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32071', 'INJERTO DE NERVIO PLEJO BRAQUIAL (1 O 2 NERVIOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32072', 'INJERTO DE NERVIO PLEJO BRAQUIAL ADICIONAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32073', 'INJERTO DE NERVIO, CADA NERVIO ADICIONAL, 1 CABLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32074', 'INJERTO DE NERVIO, CADA NERVIO ADICIONAL, MULTIPLES CABLES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32078', 'NEUROLISIS NERVIO ANTEBRAZO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32079', 'NEUROLISIS NERVIO BRAZO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32080', 'NEUROLISIS NERVIO DEDOS (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32081', 'NEUROLISIS NERVIO DEDOS (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32082', 'NEUROLISIS NERVIO MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32090', 'NEURORRAFIA DE COLATERALES EN UN DEDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32097', 'NEURORRAFIA DE DOS NERVIOS EN ANTEBRAZO CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32098', 'NEURORRAFIA DE DOS NERVIOS EN BRAZO CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32099', 'NEURORRAFIA DE DOS NERVIOS EN MANO CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32100', 'NEURORRAFIA DE TRES O MAS NERVIOS EN MANO CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32101', 'NEURORRAFIA DE UN NERVIO EN ANTEBRAZO CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32102', 'NEURORRAFIA DE UN NERVIO EN MANO CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32103', 'NEURORRAFIA DOS NERVIOS ANTEBRAZO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32104', 'NEURORRAFIA DOS NERVIOS BRAZO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32105', 'NEURORRAFIA DOS NERVIOS MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32107', 'NEURORRAFIA TRES O MAS NERVIOS MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32108', 'NEURORRAFIA UN NERVIO ANTEBRAZO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32109', 'NEURORRAFIA UN NERVIO BRAZO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32110', 'NEURORRAFIA UN NERVIO MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32112', 'NEUROTIZACION EN PLEJO BRAQUIAL (UNO O DOS NERVIOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32116', 'REMOCION DE REVISION DE CATETER O NEUROESTIMULADOR ESPINAL O DE BOMBA DE INFUSION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32121', 'RESECCION TUMOR DE NERVIO MANO DEDOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32123', 'RESECCION TUMOR NERVIO BRAZO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32124', 'RESECCION TUMOR NERVIO EN PLEJO BRANQUIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32129', 'SECCION O NEUROLISIS DE UN NERVIO PERIFERICO (MAXILAR, SUBOCCIPITAL, FRENICO, INTERCOSTAL, ETC) CADA UNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32135', 'SIMPATECTOMIA CERVICAL O CERVIDORSAL BILATERAL EN UN TIEMPO GLANGLIECTOMIA ESTELAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32138', 'SIMPATECTOMIA TORACO LUMBAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32158', 'TRANSPOSICION DE NERVIO EN MIEMBRO SUPERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32159', 'TRANSPOSICION DE NERVIOS (VG CUBITAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33002', 'ABLACION DE LESION CORIORETINAL, POR DIATERMIA O CRIOTERAPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33012', 'BLEFAROPLASTIA 4 PARPADOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33013', 'BLEFAROPLASTIA UNILATERAL O BILATERAL SUPERIOR O INFERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33035', 'CORRECCION DE RETRACCION PALPEBRAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33036', 'CORRECCION ECTROPION UNILATERAL O BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33037', 'CORRECCION ENTROPION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33038', 'CORRECCION ENTROPION CON EXCESO HORIZONTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33039', 'CORRECCION EPICANTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33048', 'DECOMPRESION NERVIO OPTICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33066', 'EXENTERACION DE ORBITA SIN INCLUIR INJERTO DE PIEL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33070', 'EXTIRPACION CHALAZION BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33071', 'EXTIRPACION CHALAZION MULTIPLE EN MISMO PARPADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33078', 'EXTRACCION CUERPO EXTR INTRAOCULAR DE LA CAMARA ANTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33096', 'INJERTO CARTÍLAGO TARSAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33099', 'INSERCION DE UN IMPLANTE OCULAR CON O SIN INJERTO CONJUNTIVAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33105', 'IRIDECTOMIA CON LASER UNILATERAL O BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33106', 'IRIDECTOMIA PERIFERICA PARA GLAUCOMA UNILATERAL O BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33110', 'LASER: BARRERA RETINAL EN 360' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33123', 'ORBITOTOMIA CON EXTIRPACION DE LESION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33128', 'PROFILAXIS P4 DEGENERACION RETINIANAS O AGUJEROS RETINIANOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33135', 'QUERATOPLASTMA PEN EXTRACCION DE CRISTALINO E IMPL LENTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33140', 'RECONSTRUCCION DE SUPERFICIE CORNEANA CON MEMBRANA AMNIOTICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33142', 'RECONSTRUCCION TOTAL DE PARPADO SUPERIOR EN UNA ETAPA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33143', 'RECONSTRUCCION TOTAL SEGUNDA ETAPA SUPERIOR O INFERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33144', 'REDUCCION DE SOBRECORRECCION DE PTOSIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33146', 'REINSERCION O RETROINSERCION DE CUATRO MUSCULOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33147', 'REINSERCION O RETROINSERCION DE TRES MUSCULOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33149', 'REPARO DE ENTROPISN MEDIANTE SUTURA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33150', 'REPARO DE PTOSIS PALPEBRAL VIA MZSCULO FRONTAL CON SUTURA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33151', 'REPARO HERIDA CSRNEA O ESCLERAL PERFORANTE SIN TEJIDO UVEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33156', 'RESECCION CONJUNTIVO-TARSO ELEVADOR TIPO FASENELLA SERVAT' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33160', 'RESECCION DE TUMOR BENIGNO EN PARPADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33167', 'RESECCION TUMOR DE PARPADOS BENIGNO CIERRE CON COLGAJO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33168', 'RESECCION TUMOR DE PARPADOS BENIGNO CIERRE CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33169', 'RESECCION TUMOR DE PARPADOS BENIGNO CIERRE DIRECTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33170', 'RESECCION TUMOR DE PARPADOS MALIGNO CIERRE CON COLGAJO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33171', 'RESECCION TUMOR DE PARPADOS MALIGNO CIERRE CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33172', 'RESECCION TUMOR DE PARPADOS MALIGNO CIERRE DIRECTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33173', 'RESECCION TUMOR MALIGNO DE PARPADO CON RECONSTRUCCION TOTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33186', 'SUTURA HERIDAS DE PARPADO COMPLEJA (2 A 4 PLANOS CADA UNA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33187', 'SUTURA HERIDAS DE PARPADO SIMPLE (HASTA 2 PLANOS CADA UNA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33189', 'TARSORRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33190', 'TATUAJE DE CORNEA MECANICO O QUMMICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33193', 'TRABECULECTOMIA BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33196', 'TRABECULOTOMIA CON LASER' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33204', 'VITRECTOMIA ANTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33205', 'VITRECTOMIA CON O SIN INSERCION DE SILICON O GASES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34007', 'DESCOMPRENSION DEL NERVIO FACIAL INTRATEMPORAL MEDIANTE AL GANGLIO GENICULADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34008', 'DESCOMPRESION DEL NERVIO FACIAL INTRATEMPORAL LATERAL AL GANGLIO GENICULADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34010', 'DESCOMPRESION TOTAL NERVIO FACIAL C/S REPARACION PUEDE INCLUIR INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34018', 'INFILTRACION INTRATIMPANICA CON ESTEROIDE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34042', 'RECONSTRUCCION DE OREJA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34045', 'REINSERCION OREJA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34048', 'REPARACION COMPLETA DE OREJA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('35022', 'LISIS DE SINEQUIA INTRANASAL (SINEQUITOMIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('35024', 'POLIPO NASAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('35026', 'REPARACION ATRESIA COANAL (INTRANASAL) BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('35027', 'REPARACION ATRESIA COANAL (INTRANASAL) UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('35028', 'REPARACION ATRESIA COANAL TRANSPALATINA BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('35029', 'REPARACION ATRESIA COANAL TRANSPALATINA UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('35037', 'TURBINOPLASTIA UNILATERAL CUALQUIER TECNICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('36008', 'MAXILOETMOIDECTOMIA UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('36015', 'SINUSOTOMIA FRONTAL OBLITERATIVA CON COLGAJO OSTEOPLASTICO, INCISION POR CEJA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('36016', 'SINUSOTOMIA FRONTAL TRANSORBITARIA UNILATERAL (PARA MUCOCELE U OSTEOMA) TIPO LYNCH' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('36017', 'SINUSOTOMIA FRONTAL VIA EXTERNA SIMPLE (CON TREPANACION)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('36020', 'ANTROSTOMIA MAXILAR INTRANASAL BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('36023', 'CIERRE FISTULA DE LCR ABORDAJE TRANSETMOIDOESFENOIDAL MICROQUIRURGICA-ENDOSCOPICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('36024', 'CIERRE FISTULA DE LCR TRANSESFENOIDAL MICROQUIRURGICA O ENDOSCOPICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('37016', 'TTO ABIERTO DE LA FRACTURA COMPLICADA ABIERTA O CERRADA INCLUYENDO EL ARCO CIGOMATICO, EL TRIPODE MALAR Y EL HUNDIMIENTO DEL PISO ORBITARIO CON RECONSTRUCCION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('37017', 'TTO ABIERTO DE LA FRACTURA CON DEPRESION MALAR, INCLUYENDO EL ARCO CIGOMATICO Y EL TRIPODE MALAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('37018', 'TTO ABIERTO DEL HUNDIMIENTO DEL PISO ORBITARIO POR FRACTURA MEDIANTE UNA VIA DE ACCESO COMBINADA (CADWELL LUC+PERIORBITARIO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('37019', 'TTO ABIERTO DEL HUNDIMIENTO DEL PISO ORBITARIO POR FRACTURA MEDIANTE UNA VIA DE ACCESO PERIORBITAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('37025', 'TTO CERRADO FRAC AREA MALAR INCLUYENDO EL ARCO CIGOMATICO Y EL TIPO DE MALAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('37028', 'TTO FRACTURA ABIERTA CONMINUTIVA DE SEPTUM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38007', 'BIOPSIA MUCOSA ORAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38010', 'CIERRE FISTULA BRANQUIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38039', 'ESTOMATORRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38045', 'EXPLORACION DE GLANDULA O CONDUCTO SALIVAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38088', 'REPARO LACERACION DE LA LENGUA, PISO BOCA DE MAS DE 2 CM DE LONGITUD O COMPLEJA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38091', 'REPARO LACERACION DEL TERCIO POSTERIOR DE LA LENGUA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38092', 'RESECCION CONDUCTO TIROGLOSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38102', 'RESECCION FISTULA TIROGLOSA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38105', 'RESECCION LESION EN PALADAR Y UVULA CON CIERRE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38107', 'RESECCION LESION SUPERFICIAL DE LA LENGUA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38108', 'RESECCION LESION SUPERFICIAL MUCOSA ORAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38110', 'RESECCION RADICAL DE GLANDULAS SALIVAL (EXCEPTO PAROTIDA) INCLUYE: VACIAMIENTO GANGLIONAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38111', 'RESECCION SIMPLE DE GLANDULAS SALIVAL (EXCEPTO PAROTIDA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38120', 'SIALOLITOTOMIA, SUBMANDIBULAR (SUBMAXILAR) SUBLINGUAL O PAROTIDA, NO COMPLICADO, INTRAORAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38122', 'SUTURA LABIO SUPERIOR(SISTEMA DIGESTIVO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39005', 'ARTRECTOMIA ARTICULACION TEMPOROMANDIBULAR UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39006', 'ARTROPLASTIA ARTICULACION TEMPOROMANDIBULAR BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39007', 'ARTROPLASTIA ARTICULACION TEMPOROMANDIBULAR UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39008', 'ARTROTOMIA ARTICULACION TEMPEROMANDIBULAR BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39009', 'ARTROTOMIA ARTICULACION TEMPEROMANDIBULAR UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39018', 'EXTIRPACION BIOPSIA LESION MALIGNA EN ENCIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39027', 'MANDIBULECTOMIA PARCIAL SIMPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39030', 'MENISECTOMIA ARTICULACION TEMPOROMANDIBULAR UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39032', 'MENISECTOMIA ARTICULACION TEMPOROMANDIBULAR, INCLUYE:RESECCION TUBERCULO ARTICULAR DEL TEMPORAL, PLASTIA DE CAPSULA ARTICULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39034', 'OSTEOPLASTIA MAXILAR HUESO FACIAL CARA HIPOPLASTIA O RETRUSION (OPERACION TIPO LEFORT) CON INJERTO DE HUESO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39039', 'OSTEOTOMIA MANDIBULAR POR SEUDOARTROPIAS, INCLUYE: CORRECCION DE ANQUILOSIS CON O SIN APLICACION DE PROTESIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39041', 'OSTEOTOMIA MAXILAR PARA EXTRACCION DE CUERPO EXTRAÑO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39045', 'REDUCCION ABIERTA FRACTURA DE MAXILAR INFERIOR, INCLUYE: INMOVILIZACION INTERMAXILAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39046', 'REDUCCION ABIERTA FRACTURA DE MAXILAR SUPERIOR (LEFORT I), INCLUYE: INMOVILIZACION INTERMAXILAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39047', 'REDUCCION ABIERTA FRACTURA DE MAXILAR SUPERIOR (LEFORT II Y III), INCLUYE: FIJACION INTERMAXILAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39050', 'REDUCCION CERRADA FRACTURA DE MAXILAR INFERIOR, INCLUYE: INMOVILIZACION INTERMAXILAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39051', 'REDUCCION CERRADA FRACTURA DEL MAXILAR SUPERIOR, INCLUYE: INMOVILIZACION INTERMAXILAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39052', 'REDUCCION CERRADA FRACTURAS ALVEOLARES SUPERIOR O INFERIOR, INCLUYE: REIMPLANTE DENTAL Y FIJACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39056', 'RESECCION LESION MALIGNA DE MAXILARES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39057', 'RESECCION PARCIAL MAXILAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39061', 'TTO ABIERTO FRACTURA REBORDE ALVEOLAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39063', 'TTO CERRADO LUXACION TEMPOROMANDIBULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39064', 'TTO FRACTURA ABIERTA O CERRADA MANIPULACION, PUEDE INCLUIR FIJACION EXTERNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39068', 'EXCISION TUMOR MALIGNO MANDIBULA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41007', 'CORDECTOMIA VOCAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41017', 'LARINGECTOMIA SUPRAGLOTICA CUELLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41018', 'LARINGECTOMIA TOTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41019', 'LARINGECTOMIA TOTAL RADICAL (INCLUYE LOBULO TIROIDES)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41021', 'LARINGOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41025', 'LARINGOSCOPIA DIRECTA CON INYECCION INTRACORDAL CON MICROSCOPIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41026', 'LARINGOSCOPIA DIRECTA CON RESECCION DE LA LESION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41027', 'LARINGOSCOPIA DIRECTA DIAGNOSTICA (PROCED APARTE)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41028', 'MICROCIRUGIA DE LARINGE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41029', 'LARINGOSCOPIA DIRECTA OPERATORIA, CON ARITENOIDECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41030', 'LARINGOSCOPIA DIRECTA P4EXTRAER EXT CPO EXTRAÑO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41031', 'LARINGOSCOPIA DX CON FIBROSCOPIO FLEXIBLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41032', 'LARINGOSCOPIA DX CON FIBROSCOPIO FLEXIBLE CON BIOPSIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41033', 'LARINGOSCOPIA DX CON FIBROSCOPIO FLEXIBLE CON EXTRACCION CPO EXTRAÑO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41034', 'LARINGOSCOPIA P4 ESTENIODES LARINGEA CON INJERTO O MOLDE, INCLUYE TRAQUEOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41035', 'LARINGOSC P´MEMBRANA INTERCONDAL 2 ETAPAS CON COLOCACION DE SEPARADOR Y POSTERIORMENTE SU RETIRO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41036', 'LARINGUECTOMIA PARCIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41037', 'LARINGUECTOMIA PARCIAL ANTEROVERTICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41054', 'SUTURA DE LA TRAQUEA INTRATORACICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42003', 'AMIGDALECTOMIA Y ADENOIDECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42016', 'EXTRACCION CUERPO EXTRAÑO FARINGEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42017', 'FARINGECTOMIA LIMITADA CON VACIAMIENTO RADICAL DE CUELLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42018', 'FARINGECTOMIA LIMITADA SIN VACIAMIENTO DE CUELLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42020', 'FARINGOSTOMIA FISTULIZACION EXTERNA FARINGE P4ALIMENTAR POR ALLI' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42021', 'INCISION Y DRENAJE ABSCESO PERIAMIGALINO A TRAVES DE VIA EXTERNA O RETROFARINGEA O PARAFARINGEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42022', 'INCISION Y DRENAJE ABSCESO PERIAMIGDALINO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42023', 'INCISION Y DRENAJE ABSCESO PERIAMIGDALINO POR VIA RETROFARINGEA O VIA INTRAORAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42024', 'RESECCION DE AMIGDALA LINGUAL (COMO PROCEDIMIENTO APARTE)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42026', 'RESECCION DIVERTICULO FARINGEOSOFAGICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42027', 'RESECCION FISTULA FARINGEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42028', 'RESECCION LESION FARINGEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42029', 'RESECCION QUISTE BRANQUIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42030', 'RESECCION TUMOR DE CAVUM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('43001', 'BIOPSIA DE TIROIDES CON AGUJA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('43003', 'ESCISION DE QUISTE TIROGLOSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('43006', 'EXCISION TUMOR CPO CAROTIDEO CON EXCISION DE LA ARTERIA CAROTIDA INTERNA Y BY-PASS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('43007', 'EXCISION TUMOR CPO CAROTIDEO SIN EXCISION DE LA ARTERIA CAROTIDA INTERNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('43008', 'INCISION Y DRENAJE DE QUISTE TIROGLOSO (INFECTADO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('43009', 'LOBECTOMIA TOTAL DEL TIROIDES, UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('43010', 'PARATIROIDECTOMIA O EXPLORACION DE PARATIROIDES CON EXPLORACION MEDIASTINAL, ESTERNOTOMIA O TORACOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('43011', 'PARATIROIDECTOMIA O EXPLORACION DE PARATIROIDES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('43012', 'RESECCION DE FISTULA TIROGLOSA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('43014', 'TIROIDECTOMIA CON RESECCION DE TIROIDES SUBESTERNAL, INCISION CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('43015', 'TIROIDECTOMIA SUBTOTAL O PARCIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('43016', 'TIROIDECTOMIA TOTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('43017', 'TIROIDECTOMIA TOTAL CON DISECCION CUELLO MODIFICADA UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('43018', 'TIROIDECTOMIA TOTAL CON DISECCION CUELLO MODIFICADA BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44001', 'LIGADURA Y DIVISION CON REMOCION COMPLETA DE LA VENA SAFENA MAYOR Y MENOR, UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44003', 'ANASTOMOSIS ARTERIAL ESTRA-INTRACRANEAL (MICROCIRUGIA VASCULAR)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44004', 'ANASTOMOSIS CAVA MESENTERICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44009', 'ANEURISMA DE AORTA INFRERENAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44010', 'ANEURISMA DE LA AORTA ABDOMINAL QUE COMPROMETE LAS ARTERIAS VISERAS, MESENTERICAS, CELIACA Y RENALES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44011', 'ANEURISMA DE LA ARTERIA ESPLENICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44021', 'PUENTE AORTO ILIOFEMORAL BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44022', 'PUENTE AORTO FEMORAL BILATERAL CON FEMORO POPLITEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44023', 'PUENTE AORTO ILIOFEMORAL UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44030', 'ARTERIOGRAFIA DE MIEMBROS SUPERIORES (INCLUYE: AORTOGRAMA TORACICO, ARTERIOGRAFIA SELECTIVA DE MIEMBROS SUPERIORES) BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44032', 'ARTERIORRAFIA DE VASO MAYOR EN ABDOMEN' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44034', 'ARTERIORRAFIA DE VASO MAYOR EN EXTREMIDAD' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44043', 'CATETER CENTRAL IMPLANTABLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44044', 'CATETER CENTRAL POR PUNCION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44068', 'CRANEOTOMIA SUBOCCIPITAL PARA EXERESIS DE MAV' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44069', 'CRANEOTOMIA SUPRATENTORIAL PARA EXERESIS DE MAV' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44082', 'DERIVACION O PUENTE AORTO FEMORAL CON FEMORO POPLITEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44084', 'DERIVACION O PUENTE AORTO ILIOFEMORAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44085', 'DERIVACION O PUENTE AORTO ILIOFEMORAL BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44095', 'DERIVACION O PUENTE ILIO ILIACO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44096', 'DERIVACION O PUENTE POPLITEO TIBIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44098', 'DERIVACION O PUENTE SUBCLAVIO SUBCLAVIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44129', 'PUENTE O INJERTO FEMORAL, TIBIAL ANTERIOR, TIBIAL POSTERIOR O PERONEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44130', 'PUENTE O INJERTO FEMORO FEMORAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44131', 'PUENTE O INJERTO FÉMORO POPLÍTEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44135', 'FLEBOEXTRACCION Y/O LIGADURA DE PERFORANTE SUBFASCIAL RADICAL, TIPO LINTON, CON O SIN INJERTO CUTANEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44138', 'FLEBOEXTRACCION Y/O LIGADURA MULTIPLE VARICES TIPO I UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44139', 'FLEBOEXTRACCION Y/O LIGADURA MULTIPLE VARICES TIPO II BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44140', 'FLEBOEXTRACCION Y/O LIGADURA MULTIPLE VARICES TIPO II UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44141', 'FLEBOEXTRACCION Y/O LIGADURA MULTIPLE VARICES TIPO III BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44142', 'FLEBOEXTRACCION Y/O LIGADURA MULTIPLE VARICES TIPO III UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44161', 'REPARACION DE VASO SANGUINEO O FISTULA ARTERIOVENOSA INTRAABDOMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44162', 'REPARACION DE VASO SANGUINEO O FISTULA ARTERIOVENOSA INTRATORACICO SIN BY-PASS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44163', 'PUENTE O INJERTO ILIO FEMORAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44165', 'INJERTO AORTO ILIACO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44183', 'LIGADURA DE ARTERIA MAYOR EN ABDOMEN' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44188', 'LIGADURA DE CAROTIDEA INTERNA O EXTERNA EN CUELLO, UN LADO, PROGRESIVA O INMEDIATA, O SIMPLE REPARO PARA CIRUGIA INTRACRANEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44192', 'LIGADURA DE LA ARTERIA CAROTIDA INTERNA O COMUN' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44193', 'LIGADURA DE LA ARTERIA CAROTIDA INTERNA O COMUN CON OCLUSION GRADUAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44194', 'LIGADURA DE LA ARTERIA ILIACA INTERNA, PROCEDIMIENTO SEPARADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44196', 'LIGADURA DE VENA MAYOR EN ABDOMEN' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44202', 'LIGADURA Y DIVISION CON REMOCION COMPLETA DE LA VENA SAFENA MAYOR Y MENOR BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44203', 'LIGADURA Y DIVISION CON REMOCION COMPLETA DE LAVENA SAFENA MAYOR Y MENOR CON EXCISION RADICAL DE LA ULCERA E INJERTO CUTANEO UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44204', 'LIGADURA Y DIVISION DE LA VENA SAFENA MAYOR Y MENOR, UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44205', 'LIGADURA Y DIVISION DE LA VENA SAFENA MAYOR EN LA UNION SAFENAFEMORAL, CON INTERRUPCIONES DISTALES BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44207', 'LIGADURA Y DIVISION DE LA VENA SAFENA MAYOR Y MENOR BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44215', 'EMBOLECTOMIA O TROMBECTOMIA ARTERIALEN ARTERIA AXILAR O BRAQUIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44220', 'PUENTE CON INJERTO DE VENA DE LA ARTERIA CAROTIDA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44221', 'PUENTE DE VENA CAROTIDA SUBCLAVIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44223', 'REPARACION DE VASO SANGUINEO O FISTULA ARTERIOVENOSA EN CUELLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44224', 'REPARACION DE VASO SANGUINEO O FISTULA ARTERIOVENOSA EN LA EXTREMIDAD SUPERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44226', 'REPARACION DIRECTA DE LA ANEURISMA DE LA ARTERIA SUBCLAVIA O CAROTIDA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44234', 'SECCION Y SUTURA DE DUCTUS ARTERIOSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44237', 'SUTURA DE VENA MAYOR EN ABDOMEN' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44238', 'SUTURA DE VENA MAYOR EN CUELLO O EXTREMIDADES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44252', 'TROMBOENDARTERECTOMIA PULMONAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45002', 'BIOPSIA GANGLIONAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45004', 'CANULACION DE CONDUCTO TORACICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45005', 'CIERRE DE FISTULA DEL CONDUCTO TORACICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45006', 'DERIVACION LINFOVENOSA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45016', 'LIGADURA (OBLITERACION) EN EL AREA ILIACA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45017', 'LIGADURA DEL CONDUCTO TORACICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45019', 'LINFADENECTOMIA O VACIAMIENTO EXTRAPERITONEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45021', 'LINFADENECTOMIA O VACIAMIENTOINGUINOFEMORALO ILIACA; UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45025', 'LINFADENECTOMIA O VACIAMIENTO RADICAL DE CUELLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45029', 'PUNCION LINFOCELE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45030', 'RESECCION LINFOCELE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45032', 'TRATAMIENTO QUIRURGICO LINFEDEMA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45035', 'VACIAMIENTO LINFATICO RADICAL DE CUELLO; UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45036', 'VACIAMIENTO LINFATICO RADICAL MODIFICADO DE CUELLO; BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45037', 'VACIAMIENTO LINFATICO RADICAL MODIFICADO DE CUELLO; UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46003', 'ANASTOMOSIS AORTO PULMONAR IZQUIERDA (OPERACION DE PRTTSMITH)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46004', 'ANASTOMOSIS AORTO-PULMONAR DERECHA (OPERACION DE WATERSON)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46005', 'ANASTOMOSIS SUBCLAVIO PULMONAR DIRECTA CON INTERPOSICION DE GORTEX (OPERACION DE BLALOCK TAUSSING150' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46006', 'ANGIOPLASTIA CORONARIA (INCLUYE: COLOCACION DE MARCAPASO TEMPORAL Y CORONARIOGRAFIA POST ANGIOPLASTIA INMEDIATA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46007', 'ANGIOPLASTIA CORONARIA Y/O ATERECTOMIA DOS O MAS VASOS, MAS COLOCACION INTRAVASCULAR DE UNO O MAS STENT' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46008', 'ANGIOPLASTIA CORONARIA Y/O ATERECTOMIA UN VASO, MAS COLOCACION INTRAVASCULAR DE UNO O MAS STENT' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46009', 'ANGIOPLASTIA CORONARIO EN MAS DE DOS VASOS, INCLUYE: CATETERISMO IZQUIERDO, COLOCACION DE MARCAPASO TEMPORAL Y CORONOGRAFIA INMEDIATA DE CONTROL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46016', 'BIOPSIA CARDIACA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46017', 'BIOPSIA PERICARDICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46038', 'DERIVACION VENTRICULAR AL APARATO URINARIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46039', 'DERIVACION VENTRICULOATRIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46040', 'DILATACION DE COARTACION AORTICA CON CATETER BALON' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46052', 'IMPLANTACION DE MARCAPASO DEFINITIVO CON ELECTRODO VENOSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46053', 'IMPLANTACION DE MARCAPASO DEFINITIVO TRICAMERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46054', 'IMPLANTACION DE MARCAPASO TRANSITORIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46061', 'PERICARDIECTOMIA COMO OPERACION SEPARADA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46065', 'PROCEDIMIENTOS PARA REPARAR ESTENOSIS DEL TRACTO DE SALIDA DEL VENTRICULO DERECHO CON EL USO DE HOMO-INJERTOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46066', 'PROCEDIMIENTOS PARA REPARAR ESTENOSIS DEL TRACTO DE SALIDA DEL VENTRICULO DERECHO CON PLASTIA O RESECCION DE LA VALVULA PULMONAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46068', 'PUENTE AORTO CORONARIO +TODO ARTERIAL;' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46069', 'PUENTE AORTO CORONARIO CON VENA SAFENA O MAMARIA INTERNA UNO O MAS VASOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46085', 'REPARO DE HERIDA CARDIACA CON CIRCULACION EXTRACORPOREA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46086', 'REPARO DE HERIDA CARDIACA SIN CIRCULACION EXTRACORPOREA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46093', 'RESECCION COMPLETA DEL PERICARDIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46095', 'RESECCION DE ANEURISMA VENTRICULAR ASOCIADO A PUENTES CORONARIOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46096', 'RESECCION DE ESTENOSIS SUB-VALVULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46097', 'RESECCION DE LA AORTA ASCENDENTE Y RESTITUCION DE LA CONTINUIDAD ARTERIAL, TUBO VALVULADO Y REIMPLANTE DE LAS ARTERIAS CORONARIAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46100', 'RESECCION DE TUMOR INTRA O EXTRA CARDIACO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46103', 'REVASCULARIZACION CORONARIA CON RAYOS LASER' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46104', 'SECCION Y SUTURA DE DUCTUS ARTERIOSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46105', 'SEPTECTOMIA ATRIAL ABIERTA O CERREDA (OPERACION DE BLAOLOCK HANLON)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46122', 'VENTRICULOPERITONEOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46126', 'ARTERIOGRAFIA SELECTIVA DE MIEMBROS INFERIORES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46131', 'EXTRACCION PERCUTANEA DE CUERPO EXTRANO INTRAVASCULAR-VENOSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46140', 'STENT CORONARIO 2 O MAS VASOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46141', 'ATERECTOMIA CORONARIA (DIRECCIONAL, ROTACIONAL O DE EXTRACCION)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46143', 'ESTUDIO ELECTROFISIOLOGICO POST-OPERATORIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47005', 'BIOPSIA DE TUMOR POR TORACOSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47007', 'BIOPSIA POR TORACOTOMA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47022', 'CORRECCION HERNIA DIAFRAGMATICA TRANSABDOMINAL INCLUYENDO FUNDOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47026', 'ENUCLEACION EXTRAPLEURAL DE EMPIEMA CON LOBECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47034', 'MEDIASTINOSCOPIA CON O SIN BIOPSIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47036', 'MEDIASTINOTOMIA CON EXPLORACION O DRENAJE , VIA EXTERNOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47037', 'MEDIASTINOTOMIA CON EXPLORACION O DRENAJE, ABORDAJE, TRANSTORACICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47038', 'MEDIASTINOTOMIA CON EXPLORACION O DRENAJE, VIA CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47044', 'PLEURECTOMIA PARIETAL (PROCEDIMIENTO ZNICO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47055', 'RESECCION DE COSTILLAS, EXTRAPLEURAL POR CADA COSTILLA ADICIONAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47056', 'RESECCION DE COSTILLAS, EXTRAPLEURAL POR LA PRIMERA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47067', 'RESECCION DE TUMOR SOLIDO CON DISECCION DE ESTRUCTURAS MEDIASTINALES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47069', 'RESECCION PARCIAL DE COSTILLA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47072', 'RESECCION RADICAL DEL ESTERNON POR TUMOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47088', 'TORACOTOMIA PARA BIOPSIA DE PULMSN O PLEURA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48003', 'BIOPSIA PULMONAR A CIELO ABIERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48004', 'BIOPSIA PULMONAR POR TORACOSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48005', 'BRONCOPLASTIA, REPARACION CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48006', 'BRONCOSCOPIA CON LAVADO BRONQUIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48007', 'BRONCOSCOPIA PARA EXTRACCION DE CUERPO EXTRAÑO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48008', 'BRONCOSCOPIA SIMPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48009', 'BRONCOSCOPIA Y BIOPSIA ENDOBRONQUIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48012', 'CIERRE ABIERTO DE FISTULA BRONQUIAL MAYOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48013', 'CIERRE DE FISTULA BRONCOCUTANEA O BRONCOPLEURAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48014', 'CIERRE DE FISTULA BRONQUIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48015', 'CIERRE DE FISTULA BRONQUIAL POR TORACOSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48016', 'DECORTICACION PULMONAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48024', 'LOBECTOMIA TOTAL O SEGMENTARIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48025', 'NEUMORRAFIA SIMPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48033', 'RESECCION DE METASTASIS BILATERALES (ESTERNOTOMIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48034', 'RESECCION DE METASTASIS PULMONARES (TORACOTOMIA - REINTERVENCION)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48035', 'RESECCION DE METASTASIS UNILATERALES (TORACOTOMIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48039', 'RESECCION PULMONAR CON RECONSTRUCCION DE PARED TORACICA CON PROTESIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48040', 'RESECCION PULMONAR CON RECONSTRUCCION DE PARED TORACICA SIN PROTESIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48041', 'RESECCION PULMONAR CON RECOSTRUCCION MAYOR DE PARED TORACICA, CON PROTESIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48042', 'RESECCION PULMONAR CON RECOSTRUCCION MAYOR DE PARED TORACICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49006', 'CIERRE DE ESOFAGOSTOMIA O FISTULA VIA CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49010', 'CONTROL ENDOSCOPICO DE HEMORRAGIA O FULGURACION DE MUCOSA ESOFAGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49011', 'CORRECION DE FISTULA TRAQUEO-ESOFAGICA VIA TORACCICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49013', 'DILATACION ESOFAGICA CON DILATADORES O BUJIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49016', 'DIVERTICULECTOMIA DE LA HIPOFARINGE POR ESOFAGO CON O SIN MIOTOMIAS, VIA CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49024', 'ESOFAGOGASTROSTOMIA (CARDIOPLASTIA) CON O SIN VAGOTOMIA O PILOROPLASTIAABORDAJE ABDOMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49025', 'ESOFAGOGASTROSTOMIA (CARDIOPLASTIA) CON O SIN VAGOTOMIA O PILOROPLASTIA ABORDAJE TORACICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49035', 'ESOFAGOSCOPIA CON FULGURACION DE LESION DE MUCOSA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49037', 'ESOFAGOSCOPIA CON INSERCION DE TUBO PLASTICO O CANULA (SIN KIT)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49038', 'ESOFAGOSCOPIA CON LIGADURA DE VARICES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49039', 'ESOFAGOSCOPIA CON RESECCIÓN POLIPO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49041', 'ESOFAGOSCOPIA PARA DILATACIÓN' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49042', 'ESOFAGOSOTOMIA, FISTULA ESOFAGICA EXTERNA, VIA TORACICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49045', 'ESOFAGOSTOMIA, FISTULA ESOFAGICA EXTERNA VIA CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49057', 'LIGADURA DE FISTULA TRAQUEO ESOFAGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49058', 'LIGADURA DIRECTA DE VARICES ESOFAGICAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49064', 'REPARO DE FISTULA TRAQUEOESOFAGICA REPRODUCIDA POR VIA TORACICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49065', 'RESECCION DE FISTULA TRAQUEOESOFAGICA REPRODUCIDA VIA CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49066', 'RESECCION DE LESION LOCALIZADA EN ESOFAGO POR VIA TORACICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49067', 'RESECCION DE LESION LOCALIZADA EN ESOFAGO VIA CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49069', 'RESECCION Y SUTURA DE FISTULA TRAQUEOESOFAGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50003', 'BIOPSIA TRUCUT DE TUMOR ABDOMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50021', 'DRENAJE DE ABSCESO PELVICO POR LAPARATOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50022', 'DRENAJE DE ABSCESO PELVICO VIA TRANSRECTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50028', 'DRENAJE DE COLECCION INTRAPERITONEAL ( EPIPLOICO; OMENTAL;PERIESPLENICO; PERIGASTRICO; SUBHEPATICO; SUBFRENICO; DE LA FOSA ILIACA O PLASTRON APENDICU' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50033', 'EXANTERACION ANTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50034', 'EXANTERACION POSTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50035', 'EXANTERACION TOTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50039', 'EXTRACCION DE CUERPO EXTRAÑO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50045', 'HERNIORRAFIA EPIGASTRICA UNICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50055', 'LAPAROTOMIA CLASIFICADORA PARA HODGKIN' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50062', 'PARACENTESIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50067', 'REPARO DE HERNIA INGUINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50068', 'REPARO DE HERNIA INGUINAL BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50069', 'REPARO DE HERNIA LUMBAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50070', 'REPARO DE HERNIA UMBILICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50074', 'REPARO DE ONFALOCELE GRANDE O GASTROSQUISIS CON O SIN PROTESIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50075', 'REPARO DE ONFALOCELE PEQUEÑO CON CIERRE PRIMARIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50080', 'RESECCION DE SENO UMBILICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50085', 'RESECCION DE TUMOR MALIGNO EN PARED ABDOMINAL MAS PROTESIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50094', 'TRATAMIENTO LAPAROSCOPICO DE ENDOMETROSIS ESTADO I' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50095', 'TRATAMIENTO LAPAROSCOPICO DE ENDOMETROSIS ESTADO II' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50096', 'TRATAMIENTO LAPAROSCOPICO ENDOMETRIOSIS ESTADO III' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50097', 'TRATAMIENTO LAPAROSCOPICO ENDOMETRIOSIS ESTADO IV' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50098', 'VALVULA (DERIVACION) PERITONEOVENOSA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51002', 'ANASTOMOSIS DE VESICULA BILIAR A ESTOMAGO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51003', 'ANASTOMOSIS DE VESICULA BILIAR A PANCREAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51009', 'BIOPSIA DE HIGADO EN CUÑA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51010', 'BIOPSIA PERCUTANEA [AGUJA] DE VESICULA BILIAR O VIAS BILIARES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51012', 'BIOPSIA TRUCUT HIGADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51016', 'COLECISTECTOMIA MAS EXPLORACION DE VIAS BILIARES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51017', 'COLECISTECTOMIA CUALQUIER VIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51020', 'COLECISTOSTOMIA PERCUTANEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51023', 'COLECISTECTOMIA CON ENDOSCOPIA BILIAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51024', 'COLECISTECTOMIA CON ESFINTEROTOMIA O ESFINTEROPLASTIA TRANSDUODENAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51026', 'CORRECCION DE ATRESIA INTRAHEPATICA (INCLUYE BIOPSIA HEPATICA Y COLANGIOGRAFIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51029', 'DRENAJE ABDOMINAL POR PANCREATITIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51030', 'DRENAJE DE ABCESO O QUISTE HEPATICO VIA ABIERTA O LAPAROSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51031', 'DRENAJE DE ABCESO O QUISTE HEPATICO VIA PERCUTANEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51038', 'ERCP CON ESFINTEROTOMIA Y/O PALIPOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51039', 'ERCP PARA DILATACIÓN CON BALON DE AMPOLLA Y/O CONDUCTOS BILIAR O PANCREATICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51040', 'ERCP PARA INSERCIÓN DE STENT EN CONDUCTO BILIAR O PANCRÉATICO (SIN KIT)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51041', 'ERCP PARA LITOTRIPSIA DE CALCULOS POR CUALQUIER METODO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51046', 'EXPLORACION DE VIA HEPATO BILIAR COMUN' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51057', 'HEPATICOTOMIA O HEPARICOSTOMIA CON EXPLORACION , DRENAJE O EXTRACCION DE CALCULOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51067', 'LOBECTOMIA IZQUIERDA TOTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51075', 'RESECCION EN CUÑA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51080', 'TRISEGMENTECTOMIA DE HIGADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52009', 'INSERCION ENDOSCOPICA DE TUBO TUTOR (STENT) EN EL CONDUCTO PANCRAETICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52015', 'DRENAJE DE ABSCESO PANCREATICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52017', 'DRENAJE DE PSEUDO QUISTE PANCREATICO POR ULTRASONIDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52023', 'PANCREATECTOMIA SUBTOTAL CON PRESERVACION DEL DUODENO (OPERACION DE CHILD)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55003', 'CIERRE DE FISTULA GASTRO - INTESTINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55006', 'CIERRE DE ULCERA PERFORADA Y EPIPLOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55012', 'CONTROL ENDOSCOPICO DE HEMORRAGIA GASTRICA O DUODENAL MEDIANTE HEMOCLIPS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55018', 'DUODENO-DUODENO ANASTOMOSIS POR ATRESIA O PANCREAS ANULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55019', 'ESFINTEROPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55021', 'ESOFAGOGASTRO DUODENOSCOPIA Y ESCLEROSIS DE VARICES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55022', 'ESOFAGOGASTRO DUODENOSCOPIA Y EXTRACCION DE CUERPO EXTRAÑO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55023', 'ESOFAGOGASTRO DUODENOSCOPIA Y RESECCION DE POLIPO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55024', 'ESOFAGOGASTRODUODENOSCOPIA Y ANULACION AMPOLLA DE VATER MAS COLANGIO O PANCREATOGRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55025', 'ESOFAGOGASTRODUODENOSCOPIA Y CANULACION DE AMPOLLA VATER' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55026', 'ESOFAGOGASTRODUODENOSCOPIA Y CONTROL DE HEMORRAGIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55028', 'EXCISION LOCAL DE ULCERA O TUMOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55032', 'GASTRECTOMIA TOTAL, INCLUYENDO ANASTOMOSIS INTESTINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55036', 'GASTRORRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55040', 'GASTROSTOMIA ENDOSCOPICA (SIN KIT)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56007', 'APENDICECTOMIA CON APENDICE PERFORADO Y ABSCESO O PERITONITIS GENERALIZADA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56008', 'APENDICECTOMIA INCIDENTAL (ADICIONAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56012', 'BIOPSIA DE LA PARED ANORRECTAL POR VIA ANAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56024', 'CIERRE DE FISTULA RECTOVESICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56025', 'CIERRE DE FISTULA RECTOVESICAL CON COLOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56029', 'COLECTOMIA CON RESECCION DE ILEON TERMINAL E ILEOCOLOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56032', 'COLECTOMIA, TOTAL, ABDOMINAL, CON ILEOSTOMIA O ILEOPROTOSTOMIA, CON PROTECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56040', 'COLECTOMIA PARCIAL, ANASTOMOSIS CON COLOSPROCTOSTOMIA(ANASTOMOSIS PELVICA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56041', 'COLECTOMIA PARCIAL, ANASTOMOSIS CON COLOSTOMIA Y CIERRE DEL SEGMENTO DISTAL(HARTMAN)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56046', 'COLECTOMIA PARCIAL, ANASTOMOSIS CON RESECCION CON COLOSTOMIA O ILEOSTOMIA Y CREACION DE UNA MUCOFISTULA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56050', 'PROCTOSIGMOIDOSCOPIA PARA DILATACIÓN INSTRUMENTAL CON RESECCION DE POLIPO O PAPILOMAS MULTIPLES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56054', 'CONSTRUCCION DE ANO POR AUSENCIA CONGENITA, VIA PERINEAL O SACROCOXIGEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56056', 'CORRECCION DE AGENESIA ANAL SIN FISTULA (ASPA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56059', 'CORRECCION DE ANO IMPERFORADO Y FISTULA RECTO VAGINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56062', 'CORRECCION DE FISTULA ANO VAGINAL (ASPA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56063', 'CORRECCION DE FISTULA ANOPERINEAL (ASPL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56064', 'CORRECCION DE MAR VIA ABDOMINO-PERINEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56065', 'CORRECCION DE MARY CLOACA (ASPA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56066', 'CORRECCION DE MARY SENO UROGENITAL (ASPA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56068', 'DESCENSO ABDOMINO-PERINEAL (SOAVE-DUHAMEL-SWENSON)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56071', 'DILATACION ANAL BAJO ANESTESIA GENERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56073', 'DILATACION DEL ANO Y RECTO BAJO ANESTESIA PARA HEMORROIDES (OPERACION DE LORD)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56094', 'ESFINTERECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56096', 'ESFINTEROPLASTIA, ANAL POR INCONTINENCIA O PROLAPSO EN EL ADULTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56109', 'HERNIORRAFIA ISQUIORRECTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56110', 'ILEOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56113', 'INCISION Y DRENAJE DE ABSCESO ISQUIORRECTAL O PERIRRECTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56119', 'LAPAROTOMIA DIAGNOSTICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56135', 'PROCTOPEXIA PARA PROLAPSO POR VIA ABDOMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56141', 'PROCTOSIGMOIDOSCOPIA MAS EXTRACCION DE CUERPO EXTRAÑO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56142', 'PROCTOSIGMOIDOSCOPIA PARA DILATACIÓN INSTRUMENTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56166', 'RESECCION DE TUMOR RECTAL SIMPLE POR VIA TRANS-ANAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56167', 'RESECCION DE TUMOR RECTAL TRANSANAL VIA ASP' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56170', 'RESECCION ILEO-COLICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56172', 'RESECCION PARCIAL DEL RECTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56182', 'SUTURA DE LACERACION O DESGARRO DE ANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57003', 'ASPIRACION PERCUTANEA QUISTE RENAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57004', 'ASPIRACION PERCUTANEA QUISTE RENAL + INYECCION SUSTANCIA ESCLEROSANTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57028', 'NEFRECTOMIA SIMPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57030', 'NEFROLITOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57035', 'NEFROSTOMIA, PERCUTANEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57047', 'PIELOPLASTIA, REINTERVENCION EN RIQON UNICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58003', 'CAMBIO DE TUBO DE URETEROSTOMIA POR CISTOCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58011', 'MEATOTOMIA ENDOSCOPICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58018', 'RESECCION ENDOSCOPICA DE URETEROCELE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58020', 'URETERECTOMIA RESIDUAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58024', 'URETEROLISIS CON TRANSPOSICION INTRAPERITEONAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58025', 'URETEROLISIS Y PIELOURETEROLISIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58028', 'URETEROLITOTOMIA ENDOSCOPICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59002', 'APENDICO-VESICOSTOMIA CON CIERRE DE CUELLO VESICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59005', 'BIOPSIA UNICA O SIMPLE DE VEJIGA POR CISTOSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59015', 'CISTECTOMIA RADICAL+NEOVEJIGA Y/O DERIVACION URINARIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59033', 'CORRECCION EXTROFIA VESICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59035', 'CORRECCION FISTULA VESICO-CUTANEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59055', 'TRATAMIENTO HIDROSTATICO PARA TUMOR VESICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59059', 'VESICOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59060', 'VESICOSTOMIA CUTANEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60006', 'DEVERTICULECTOMIA URETRAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60007', 'DILATACION URETRAL FEMENINA Y/O INSTILACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60021', 'MEATOPLASTIA: INCLUYE MEATOMIA URETRAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60022', 'OPERACION DE CABESTRILLO O HAMACA PARA LA INCONTINENCIA URINARIA (CON TIRA DE FASCIA O SINTETICA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60029', 'RESECCION-FULGURACION ENDOSCSPICA DE LESION URETRAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60045', 'URETRORRAFIA PERINEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60050', 'URETROTOMIA INTERNA ENDOSCOPICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('61003', 'CONTROL HEMORRAGIA PROSTATICA, EN PACIENTE NO INTERVENIDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('61005', 'ECOGRAFIA TRANSRECTAL DE PROSTATA + BIOPSIA PROSTATICA CON GIA ULTRASONICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('61009', 'PROSTATECTOMIA ABIERTA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('61011', 'PROSTATECTOMIA RADICAL (INCLUYE LINFADENECTOMÍA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62004', 'BIOPSIA LESION ESCROTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62005', 'BIOPSIA TESTICULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62011', 'EXPLORACION POR TESTICULO NO PALPABLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62022', 'LINFADENECTOMIA RETROPERITONEAL (CLASIFICATORIA, CITOREDUCTORA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62034', 'ORQUIDOPEXIA SIMPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62038', 'REDUCCION QUIRURGICA TORSIONCORDON ESPERMATICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62044', 'RETIRO DE PROTESIS TESTICULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('63005', 'EXPLORACION DE EPIDIDIMOCON O SIN BIOPSIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64004', 'CORRECCION DE ANGULACION DE PENE (Z- PLASTIA Y PENE PALMEADO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64005', 'CORRECCION EPISPADIAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64006', 'CORRECCION HIPOSPADIAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64021', 'INYECCION DE PLACAS DE FIBROSIS DE PENE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64025', 'PENECTOMIA PARCIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64026', 'PENECTOMIA TOTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64037', 'REPARACION SIMPLE DE HERIDA SUPERFICIAL EN PENE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64043', 'SUTURA HERIDA EN PENE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65003', 'BIOPSIA DE SENO CON AGUJA (PROCEDIMIENTO SEPARADO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65007', 'BIOPSIA POR TRUCUT' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65010', 'CIRUGIA REPARADORA DE LA MAMA INCLUYE RECONSTRUCCION DE AREOLA PEZSN' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65025', 'MAMA SUPERNUMERARIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('66003', 'CISTECTOMIA DE OVARIO, (RESECCION DE QUISTES DE OVARIO, UNI O BILATERAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('67009', 'SALPINGO-OOFORECTOMIA, COMPLETA O PARCIAL UNI O BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('67011', 'SALPINGOPLASTIA, CONVENIONAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68002', 'TRATAMIENTO DEL EMBARAZO ECTOPICO ABDOMINAL PRIMER TRIMESTRE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68003', 'ABLACION ENDOMETRIAL POR HISTEROCOPIA (ENDOMETRECTOMIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68016', 'CESAREA HISTERECTOMIA, TOTAL O SUBTOTAL, INCLUYENDO CONTROL POST-OPERATORIO INTRA-HOSPITALARIO (PROCEDIMIENTO SEPARADO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68036', 'EPISIOTOMIA O REPARACION VAGINAL POR PERSONAL DIFERENTE A QUIEN ATENDIO EL PARTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68042', 'HISTERECTOMIA CON REPARACION DE ENTEROCELE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68043', 'HISTERECTOMIA CON REPARACION PLASTICA DE VAGINA, COLPORRAFIA ANTERIOR Y O POSTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68046', 'HISTERECTOMIA CON COLPOURETROCISTOPEXIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68047', 'HISTERECTOMIA POR LAPAROSCOPIA INCLUYE SALPINGO OOFORECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68048', 'HISTERECTOMIA SUBTOTAL, C/S REMOCION DE TROMPAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68049', 'HISTERECTOMIA TOTAL CON SALPILGO-OMENTECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68052', 'HISTERECTOMIA TOTAL, AMPLIADA CANCER DEL CUERPO UTERINO,CON VAGINECTOMIA PARCIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68060', 'HISTEROSCOPIA OPERATORIA (LIBERACION DE ADHERENCIAS DE ESCISION TABIQUE)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68077', 'HISTEROTOMIA POR FETO MUERTO, SEGUNDO TRIMESTRE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68099', 'VAPORIZACION LASER DEL CERVIX, TRATAMIENTO LA DISPLASIA SEVERAO CARCINOMIA INSITU LASER DE LA DISPLASIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69001', 'BIOPSIA DE MUCOSA GENERAL EXTENSA, REQUIERE SUTURA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69002', 'BIOPSIA DE MUCOSA VAGINAL SIMPLE,PROCEDIMIENTO SEPARADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69005', 'COLPECTOMIA OBLITERACION PARCIAL DE VAGINA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69011', 'COLPORRAFIA ANTERIOR Y POSTERIOR COMBINADAS CON REAPARACION DE ENTEROCELE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69014', 'COLPOSCOPIA (VAGINOSCOPIA)(PROCEDIMIENTO SEPARADO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69023', 'ESCISION DE QUISTE DE CONDUCTO DE GARDNER U OTROS QUISTES O TUMORES DEL 1/3 SUPERIOR DE VAGINA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69024', 'ESCISION DE QUISTE O TUMOR 2/3 INFERIORES DE VAGINA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69028', 'EXTRACCION DE CUERPO EXTRAÑO VAGINAL BAJO ANESTESIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69030', 'REPARCION DE DESGARRO VAGINALGRADO 2 (PLANO MUSCULAR)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69034', 'HIMENOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69042', 'RESECCION NODULO TABIQUE RECTOVAGINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69044', 'URETROCISTOPEXIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69047', 'CIERRE DE FISTULA VESICO VAGINAL VIA ABDOMINAL CON CISTOSTOMIA CONCOMITANTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69048', 'CIERRE DE FISTULA RECTO VAGINAL VIA ABDOMINAL CON COLOSTOMIA CONCOMITANTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69049', 'CIERRE DE FISTULA RECTO VAGINAL VIA VAGINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69050', 'CIERRE DE FISTULA VESICO VAGINAL VIA VAGINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70021', 'HIMENOTOMIA, INCISION SIMPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70023', 'INCISION Y DRENAJE DE ABSCESO DE LA GLANDULA DE BARTHOLINO UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70025', 'INCISION Y DRENAJE DE LA GLANDULA DE SKENE, POR ABSCESO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70026', 'INCISION Y DRENAJE DE QUISTE SEBACEO O FORUNCULO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70036', 'VULVECTOMIA RADICAL CON LINFADENECTOMIA INGUINOFEMORAL UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70037', 'VULVECTOMIA RADICAL CON LINFADENECTOMIA INGUINOFEMORAL, ILIACA Y PELVICA BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70038', 'VULVECTOMIA RADICAL CON LINFADENECTOMIA INGUINOFEMORAL, ILIACA Y PELVICA UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70039', 'VULVECTOMIA RADICAL SIN INJERTO DE PIEL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71002', 'ACROMIOPLASTIA MAS EXTRACCION DE CALCIFICACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71003', 'ACROMIOPLASTIA O ACROMIECTOMIA PARCIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71014', 'ARTROPLASTIA GLENO-HUMERAL (PROTESIS TOTAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71028', 'DESARTICULACION GLENO-HUMERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71029', 'ARTROSCOPIA DIAGNOSTICA DE HOMBRO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71034', 'ESCAPULOPEXIA (PARA PARALISIS O ENFERMEDAD DE SPRENGEL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71038', 'ESCISION O CURETAJE DE QUISTE OSEO O TUMOR BENIGNO HUMERO (DIAFISIARIO), CON ALOINJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71039', 'ESCISION O CURETAJE DE QUISTE OSEO O TUMOR BENIGNO HUMERO (DIAFISIARIO), CON AUTO INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71040', 'ESCISION O CURETAJE DE QUISTE OSEO O TUMOR BENIGNO HUMERO (DIAFISIARIO), SIN INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71041', 'ESCISION PARCIAL OSEA (SAUCERIZACION), POR OSTEOMIELITIS, ESCAPULA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71042', 'ESCISION PARCIAL OSEA (SAUCERIZACION, DIAFISECTOMIA) POR OSTEOMIELITIS , CLAVICULA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71053', 'INMOVILIZACION FRACTURA CLAVICULA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71054', 'INMOVILIZACION LUXACION ARTICULACION ACROMIO-CLAVICULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71056', 'INMOVILIZACION FRACTURA ESCAPULA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71057', 'INMOVILIZACION FRACTURA TERCIO PROXIMAL DE HUMERO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71058', 'INMOVILIZACION FRACTURA TERCIO MEDIO DE HUMERO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71059', 'INMOVILIZACION FRACTURA TERCIO DISTAL DE HUMERO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71060', 'INMOVILIZACION FRACTURA EPICONDILO-EPITROCLEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71061', 'INMOVILIZACION FRACTURA CONDILO-TROCLEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71062', 'INMOVILIZACION SIN MANIPULACION LUXACION ARTICULACION ESTERNOCLAVICULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71064', 'LUXACION ANTERIOR RECIDIVANTE HOMBRO, CAPSULORRAFIA NEER, PUTTI-PLATT, MAGNUSON' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71069', 'LUXACION MULTIDIRECCIONAL RECIDIVANTE HOMBRO, CAPSULARRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71070', 'LUXACION MULTIDIRECCIONAL RECIDIVANTE HOMBRO, CAPSULARRAFIA CON OSTEOTOMIA DE CUELLO GLENOIDEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71071', 'LUXACION RECIDIVANTE POSTERIOR HOMBRO, CAPSULORRAFIA CON O SIN BLOQUE OSEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71083', 'REDUCCION BAG (CUELLO QUIRURGICO O ANATOMICO) DE FRACTURA TERCIO PROXIMAL DE HUMERO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71095', 'REDUCCION MAS PINES PERCUTANEOS FRACTURA CONDILO-TROCLEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71096', 'REDUCCION QUIRURGICA FRACTURA CLAVICULA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71097', 'REDUCCION QUIRURGICA LUXACIONARTICULACION ACROMIO-CLAVICULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71098', 'REDUCCION QUIRURGICA FRACTURA ESCAPULA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71100', 'REDUCCION QUIRURGICA FRACTURA TERCIO MEDIO DE HUMERO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71101', 'REDUCCION QUIRURGICA FRACTURA TERCIO DISTAL SUPRACONDILEO-TRANSCONDILEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71102', 'REDUCCION QUIRURGICA FRACTURA EPICONDILO-EPITROCLEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71103', 'REDUCCION QUIRURGICA FRACTURA CONDILO-TROCLEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71104', 'REDUCCION QUIRURGICA FRACTURA CONMINUTA (DISTAL HUMERO Y/O CUBITO PROXIMAL Y/O RADIO PROXIMAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71105', 'REDUCCION QUIRURGICA (3 O 4 FRAGMENTOS) FRACTURA TERCIO PROXIMAL DE HUMERO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71109', 'REDUCCION QUIRURGICA MAS PROTESIS FRACTURA CONMINUTA (DISTAL HUMERO Y/O CUBITO PROXIMAL Y/O RADIO PROXIMAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71110', 'REDUCCION QUIRURGICA TROQUITER FRACTURA TERCIO PROXIMAL DE HUMERO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71111', 'REDUCCION QURURGICA FRACTURA TERCIO PROXIMAL DE HUMERO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71113', 'REEMPLAZO CABEZA HUMERAL CON PROTESIS (HEMIARTROPLASTIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71118', 'REMOCION CUERPO EXTRAÑO HOMBRO PROFUNDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71119', 'REMOCION CUERPO EXTRAÑO HOMBRO SUBCUTANEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71122', 'REPARACION MANGUITO ROTADOR (CRONICO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71123', 'REPARACION QUIRURGICA (AGUDO) ESGUINCE-RUPTURAMANGO ROTADOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71127', 'RESECCION O CURETAJE DE QUISTE O TUMOR BENIGNO DE CLAVICULA O ESCAPULA (SIN INJERTO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71128', 'RESECCION O CURETAJE DE QUISTE O TUMOR BENIGNO DE CLAVICULA O ESCAPULA (CON ALOINJERTO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71136', 'RESECCION TERCIO DISTAL DE CLAVICULA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72011', 'AMPUTACION DE ANTEBRAZO ABIERTA, CIRCULAR (GUILLOTINA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72012', 'AMPUTACION DEL BRAZO ABIERTO TIPO GUILLOTINA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72013', 'AMPUTACION DEL BRAZO SIMPLE CON CIERRRE PRIMARIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72014', 'APLICACION DE FERULA LARGA BRAQUIO-METACARPIANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72017', 'APLICACION YESO BRAQUIO-METACARPIANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72019', 'ARTRODESIS DEL CODO SIN INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72021', 'ARTROPLASTIA CABEZA RADIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72024', 'ARTROPLASTIA DE INTERPOSICION DEL CODO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72025', 'ARTROPLASTIA TOTAL DE CODO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72027', 'ARTROTOMIA DE CODO CON EXPLORACION ARTICULAR PARA REMOCION DE CUERPO EXTRAÑO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72028', 'ARTROTOMIA DE CODO PARA BIOPSIA SINOVIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72029', 'ARTROTOMIA DE CODO PARA SINOVECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72030', 'ARTROTOMIA DE CODO POR INFECCION, CON EXPLORACION, DRENAJE EXTRACCION DE CUERPO EXTRAÑO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72034', 'CODO PRONADO (CODO NIÑERA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72035', 'CONDROPLASTIA TRATAMIENTO DE OSTEOCONDRITIS DEL CONDILO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72058', 'EXTRACCION PROTESIS TOTAL DE CODO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72060', 'FASCIOTOMIA DESCOMPRESIVA ANTEBRAZO CON EXPLORACION DE ARTERIA BRAQUIAL Y DESBRIDAMIENTO DE TEJIDO NO VIABLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72061', 'FASCIOTOMIA DESCOMPRESIVA ANTEBRAZO CON EXPLORACION DE ARTERIA BRAQUIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72064', 'INMOVILIZACION FRACTURA TERCIO MEDIO DE CUBITO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72065', 'INMOVILIZACION FRACTURA TERCIO DISTAL (INCLUYE ESTILOIDES CUBITAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72066', 'INMOVILIZACION FRACTURA TERCIO PROXIMAL Y CABEZA DE RADIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72067', 'INMOVILIZACIONN FRACTURA TERCIO MEDIO (DIAFISIS) DE RADIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72068', 'INMOVILIZACION FRACTURA TERCIO DISTAL (COLLES,SMITH, BARTON, DESLIZAMIENTO EPIFISIARIO) DE RADIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72069', 'INMOVILIZACION FRACTURA DE CUBITO Y RADIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72081', 'OSTEOTOMIA DE RADIO Y CUBITO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72083', 'OSTEOTOMIA DE RADIO, TERCIO MEDIO O PROXIMAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72091', 'REDUCCION BAG TERCIO PROXIMAL Y CABEZA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72097', 'REDUCCION E INMOVILIZACION LUXACION ARTICULACION DEL CODO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72098', 'REDUCCION E INMOVILIZACION FRACTURA TERCIO PROXIMAL - OLECRARON' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72100', 'REDUCCION E INMOVILIZACION ARTICULACION RADIO CUBITAL PROXIMAL (LUXOFRACTURA DE MONTEGGIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72102', 'REDUCCION E INMOVILIZACION LUXACION RADIO CUBITAL DISTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72106', 'REDUCCION MAS INMOVILIZACION FRACTURA CUBITO Y RADIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72108', 'REDUCCION QUIRURGICA TERCIO MEDIO CUBITO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72157', 'TENODESIS DEL BICEPS EN EL CODO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72159', 'TENOTOMIA ABIERTA DE CODO A HOMBRO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72167', 'TUTOR EXTERNO FRACTURA TERCIO DISTAL (COLLES, SMIYH,BARTON, DESLIZAMIENTOEPIFISARIO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73004', 'ALARGAMIENTO DE TENDON (Z PLASTIA, OTRO) FLEXOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73005', 'ALARGAMIENTO TENDON MANO (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73006', 'ALARGAMIENTO TENDON MANO (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73014', 'AMPUTACION Y/O DESARTICULACION DEDOS MANO (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73015', 'AMPUTACION Y/O DESARTICULACION DEDOS MANO (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73018', 'ARTOPLASTIA METACARPO-FALANGICAS (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73019', 'ARTOPLASTIA METACARPO-FALANGICAS (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73035', 'ARTRODESIS PUÑO CON INJERTO OSEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73036', 'ARTRODESIS PUÑO SIN INJERTO OSEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73037', 'ARTRODESIS TRAPECIO-METACARPIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73038', 'ARTROPLASTIA CARPO-METACARPIANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73043', 'ARTROPLASTIA CON REEMPLAZO PROTESICO, TRAPECIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73044', 'ARTROPLASTIA CON REEMPLZAO PROTESICO, RADIO DISTAL PARCIAL O TOTAL DEL CARPO (PROTESIS TOTAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73046', 'ARTROPLASTIA DE MUQECA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73047', 'ARTROPLASTIA DE MUQECA, CON IMPLANTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73048', 'ARTROPLASTIA DE MUQECA, TIPO SEUDOARTROSIS CON FIJACION INTERNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73049', 'ARTROPLASTIA METACARPO-FALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73050', 'ARTROPLASTIA METACARPO-FALANGICA CON IMPLANTE PROTESICO (CADA UNO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73051', 'ARTROPLASTIA METACARPO-FALANGICA CON INTERPOSICION DE PARTES BLANDAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73052', 'ARTROPLASTIA METACARPO-FALANGICA SIN INTERPOSICION DE PARTES BLANDAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73053', 'ARTROPLASTIA PARCIAL DE CARPO O REEMPLAZO TOTAL DE MUQECA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73055', 'ARTROTOMIA DE MUQECA PARA BIOPSIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73056', 'ARTROTOMIA DE MUQECA PARA SINOVECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73057', 'ARTROTOMIA EN MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73058', 'ARTROTOMIA RADIOCARPIANA O MEDIOCARPIANA PARA INFECCION, CON EXPLORACION, DRENAJE O REMOCION CUERPO EXTRAÑO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73059', 'BANDAS CONSTRICTIVAS (STREETER)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73064', 'CAPSULODESIS METACARPO-FALANGICA ADICIONAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73066', 'CAPSULORRAFIA ARTICULACIONES (UNA A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73068', 'CAPSULOTOMIA DE MUQECA ( POR CONTRACTURA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73070', 'CAPSULOTOMIA INTERFALANGICAS (UNA A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73072', 'CAPSULOTOMIA METACARPOFALANGICAS (UNA A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73074', 'CARPECTOMIA (UNO A DOS) HUESOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73075', 'CARPECTOMIA UN HUESO CON INTERPOSICION PARTES BLANDAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73076', 'CARPECTOMIA, 1 HUESO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73083', 'COLGAJO CUTANEO O MIOCUTANEO CON MICROANASTOMOSIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73086', 'ARTRODESIS EN EL CARPO CON INJERTO POR DESLIZAMIENTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73087', 'ARTRODESIS EN EL CARPO CON OSTEOSINTESIS CLAVO/ALAMBRE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73088', 'ARTRODESIS (FALANGES Y METACARPIANOS) CON OSTEOSINTESIS CLAVO/ALAMBRE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73089', 'ARTRODESIS EN EL CARPO CON OSTEOSINTESIS PLACA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73090', 'ARTRODESIS (FALANGES Y METACARPIANOS) CON OSTEOSINTESIS PLACA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73091', 'ARTRODESIS EN EL CARPO CON OSTEOSINTESIS TORNILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73092', 'ARTRODESIS (FALANGES Y METACARPIANOS) CON OSTEOSINTESIS TORNILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73096', 'CORRECCION QUIRURGICA CAMPTODACTILIA (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73097', 'CORRECCION QUIRURGICA CICATRIZ EN MANO CON COLGAJO A DISTANCIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73098', 'CORRECCION QUIRURGICA CICATRIZ EN MANO CON SUTURA PRIMARIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73100', 'CORRECCION QUIRURGICA CLINODACTILIA (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73106', 'CORRECCION SINDACTILIA (UN ESPACIO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73107', 'TRANSFERENCIA MUSCULOTENDINOSA DE FLEXOR O EXTENSOR, PUÑO O DEDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73111', 'BIOPSIA DE SINOVIAL ARTICULAR C-METACARPIANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73112', 'BIOPSIA DE SINOVIAL ARTICULAR IF' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73113', 'BIOPSIA DE SINOVIAL ARTICULAR M-F' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73114', 'BIOPSIA DE SINOVIAL EXTRA ARTICULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73115', 'BIOPSIA DE TENDON O NERVIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73118', 'DEDO EN BOTONERA O MARTILLO, TRATAMIENTO QUIRURGICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73126', 'DRENAJE DE ABSCESO O HEMATOMA PROFUNDO, ANTEBRAZO Y/O MUQECA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73136', 'ESCISION DE TUMOR DE ANTEBRAZO Y/O MUQECA, PROFUNDO, SUBFASCIAL O INTRAMUSCULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73137', 'ESCISION DE TUMOR DE ANTEBRAZO Y/O MUQECA, SUBCUTANEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73139', 'ESCISION LESION QUISTICA VAINA TENDINOSA (MUCOIDE)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73144', 'ESCISION, LESION DE VAINA TENDINOSA ANTEBRAZO Y/O MUQECA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73161', 'FIJACION CON TUTOR EXTERNO - ARTRODESIS FALANGES Y METACARPIANOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73170', 'HEMIDIAFISECTOMIA FALANGES (UNA A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73175', 'INJERTO DE TENDON EXTENSOR MANO (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73176', 'INJERTO DE TENDON EXTENSOR MANO (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73177', 'INJERTO DE TENDON FLEXOR DOS DEDOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73178', 'INJERTO DE TENDON FLEXOR MANO (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73179', 'INJERTO DE TENDON FLEXOR MANO (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73180', 'INJERTO DE TENDON FLEXOR UN DEDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73181', 'INJERTO DE TENDON FLEXOR UN DEDO CON RECONSTRUCCION DE POLEAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73185', 'INJERTO OSEO EN FALANGES (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73187', 'INJERTO OSEO EN METACARPIOS (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73189', 'INJERTO VASCULAR (VENA EN ARTERIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73193', 'INMOVILIZACION FRACTURADE HUESOS DEL CARPO (EXCEPTO ESCAFOIDES)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73195', 'INMOVILIZACION LUXO FRACTURA Y ESGUINCE DE ESCAFOIDES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73196', 'INMOVILIZACION FRACTURA METACARPIANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73197', 'INMOVILIZACION LUXOFRACTURADE ARTICULACION INTERFALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73204', 'CAPSULOTOMIA INTRAFALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73207', 'LIBERACION MUSCULOS TENARES EN CONTRACTURA PULGAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73213', 'MACRODACTILIA-REPARACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73217', 'CARPECTOMIA O METACARPECTOMIA (UNO A DOS) HUESOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73219', 'CAPSULTOMIA METACARPO-FALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73223', 'MIORRAFIA FLEXORES MANO (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73226', 'OSTEOPLASTIA PARA ALARGAMIENTO MATACARPIANO O FALANGE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73231', 'OSTEOTOMIA EN METACARPIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73232', 'OSTEOTOMIA PARA DEFORMIDAD FALANGE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73237', 'POLIDACTILIA RECONSTRUCCION DEDO SUPERNUMERARIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73240', 'PULGARIZACION (DE CUALQUIER DEDO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73242', 'ARTROPLASTIA RADIO DISTAL Y CARPO PARCIAL O COMPLETO REEMPLAZO TOTAL DE MUQECA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73243', 'CAPSULOTOMIA RADIOCARPIANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73251', 'RECONSTRUCCION DE PULGAR CON ARTEJO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73252', 'RECONSTRUCCION LIGAMENTO COLATERAL IF CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73257', 'REDUCCION ABIERTA FRACTURAS FALANGES MANO (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73258', 'REDUCCION ABIERTA FRACTURAS FALANGES MANO (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73259', 'REDUCCION ABIERTA FRACTURAS HUESOS CARPO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73260', 'REDUCCION ABIERTA FRACTURAS INTRA-ARTICULAR MANO (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73262', 'REDUCCION ABIERTA FRACTURAS METACARPIOS (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73264', 'REDUCCION ABIERTA LUXACION CARPOMETACARPIANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73266', 'REDUCCION ABIERTA LUXACION INTERFALANGICA (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73267', 'REDUCCION ABIERTA LUXACION METACARPOFALANGICA (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73268', 'REDUCCION ABIERTA LUXACION METACARPOFALANGICA (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73274', 'REDUCCION CERRADA FRACTURAS HUESOS CARPO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73277', 'REDUCCION CERRADA LUXACION CARPOMETACARPIANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73279', 'REDUCCION CERRADA LUXACION INTERFALANGICA (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73281', 'REDUCCION CERRADA LUXACION METACARPOFALANGICA (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73289', 'REDUCCION CERRADA MAS PINES - LUXAXION RADIO CARPIANA O INTERCARPIANA (UNO O MAS HUESOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73290', 'REDUCCION CERRADA MAS TUTOR - LUXAXION RADIO CARPIANA O INTERCARPIANA (UNO O MAS HUESOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73293', 'REDUCCION E INMOVILIZACIONLUXACION ARTICULACIONES CARPO METACARPIANA Y METACARPOFALANGICAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73294', 'REDUCCION E INMOVILIZACIONLUXACION ARTICULACION INTERFALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73295', 'REDUCCION E INMOVILIZACION LUXOFRACTURA ARTICULACION INTERFALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73299', 'REDUCCION MAS FIJACION CON TUTOR FRACTURA METOCARPIANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73300', 'REDUCCION MAS FIJACION CON TUTOR LUXOFRACTURA ARTICULACION INTERFALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73301', 'REDUCCION MAS FIJACION PERCUTANEA LUXACION ARTICULACION INTERFALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73302', 'REDUCCION MAS FIJACION PERCUTANEA LUXOFRACTURA ARTICULACION INTERFALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73304', 'REDUCCION MAS FIJADOR EXTERNO FRACTURA FALANGES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73306', 'REDUCCION MAS INMOVILIZACION FRACTURAS METACARPIANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73307', 'REDUCCION MAS INMOVILIZACION FRACTURA FALANGES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73308', 'REDUCCION MAS TORNILLO LUXO FRACTURA Y ESGUINCE ARTICULACIONES CARPO METACARPIANA Y METACARPOFALANGICAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73309', 'REDUCCION MAS TUTOR FRACTURA HUESOS DEL CARPO (EXCEPTO ESCAFOIDES)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73345', 'REINSERCION DE TENDON EXTENSOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73346', 'REINSERCION DE TENDON FLEXOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73361', 'REPARACION O CAPSULODESIS LIGAMENTOS (REPARACION EN EL CARPO Y MF)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73374', 'RESECCION DE FALANGE CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73376', 'RESECCION DE METACARPIANO CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73384', 'RESECCION TOTAL DE FALANGES DE MANO (UNA O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73391', 'REVISION Y/O RECONSTRUCCION MUQSN DE AMPUTACION DEDOS MANO (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73393', 'REVISION Y/O RECONSTRUCCION MUQSN DE AMPUTACION DEDOS MANO (UNO O DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73394', 'SECUESTRECT P4OSTEOMIELITIS O ABSCESO OSEO ANTEBR Y/O MUQECA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73396', 'ARTRODESIS EN EL CARPO SIN OSTEOSINTESIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73397', 'ARTRODESIS (FALANGESY METACARPIANOS) SIN OSTEOSINTESIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73398', 'SINDACTILA SIMPLE-REPARACION CON COLGAJOS E INJERTOS LIBRES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73400', 'SINDACTILIA SIMPLE-REPARACION CON COLGAJOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73403', 'SINOVECTOMIA CARPO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73408', 'SINOVECTOMIA TRES O MAS INTERFALANGICAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73409', 'SINOVECTOMIA TRES O MAS METACARPOFALANGICAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73410', 'SINOVECTOMIA UNA A DOS INTERFALANGICAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73411', 'SINOVECTOMIA UNA A DOS METACARPOFALANGICAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73412', 'SUTURA DE FIBROCARTILAGO TRIANGULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73414', 'TENODESIS DE FLEXORES DE LOS DEDOS EN LA MUQECA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73415', 'TENODESIS EN ANTEBRAZO, PUÑO O MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73416', 'TENODESIS MANO (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73417', 'TENODESIS MANO (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73422', 'TENOLISIS EXTENSORES MANO (TRES O MAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73423', 'TENOLISIS EXTENSORES MANO (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73425', 'TENOLISIS FLEXORES MANO (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73426', 'TENOLISIS, TENDON FLEXOR O EXTENSOR, ANTEBRAZO Y/O MUQECA, CADA UNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73430', 'TENORRAFIA DE FLEXOR EN DEDO EN ZONA II' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73433', 'TENORRAFIA EXTENSORES MANO (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73435', 'TENORRAFIA FLEXORES ANTEBRAZO (UNO A CUATRO) <' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73436', 'TENORRAFIA FLEXORES DEDOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73438', 'TENORRAFIA FLEXORES MANO (UNO A CUATRO) CON NEURORRAFIAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73441', 'TENOSINOVECTOMIA EXTENSORES MANO (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73442', 'TENOSINOVECTOMIA EXTENSORES MANO (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73444', 'TENOSINOVECTOMIA FLEXORES MANO (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73454', 'TRANSFERENCIA TENDINOSA, FLEXOR O EXTENSOR, ANTEBRAZO Y/O MUQECA, CON INJERTO (INCLUYE OBTENCION DEL INJERTO), UNO A DOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73455', 'TRANSFERENCIA TENDINOSA, FLEXOR O EXTENSOR, ANTEBRAZO Y/O MUQECA, CON INJERTO (INCLUYE OBTENCION DEL INJERTO, TRES O MAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73459', 'TRANSPLANTE DE ARTEJO A PULGAR (MICROCIRUGIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73466', 'APLICACION DE FERULA EN DEDO (DINAMICA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73467', 'APLICACION DE FERULA EN DEDO (ESTATICA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74001', 'APLICACION DE ESPICA DE CADERA (UNILATERAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74002', 'APLICACION DE ESPICA DE CADERA BILATERAL O UNA Y MEDIA ESPICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74003', 'APLICACION DE FERULA INGUINOPEDICA O INGUINOMALEOLAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74007', '"DESLIZAMIENTO EPIFISIARIO, PINES ""IN SITU"""' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74015', 'ARTRODESIS DE CADERA CON OSTEOTOMIA PELVICA O FEMORAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74021', 'ARTROPLASTIA - REVISION REEMPLAZO TOTAL DE CADERA AMBOS COMPONENTES CON O SIN AUTO O ALOINJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74022', 'ARTROPLASTIA - REEMPLAZO TOTAL DE CADERA CON O SIN INJERTO O ALOINJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74027', 'ARTROTOMIA DE CADERA PARA BIOPSIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74028', 'ARTROTOMIA DE CADERA PARA DRENAJE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74029', 'ARTROTOMIA DE CADERA PARA EXTRACCION DE CUERPOS LIBRES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74030', 'ARTROTOMIA DE CADERA PARA SINOVECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74040', 'DESCOMPRESION DE CADERA TIPO HUNGERFORD' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74041', 'DESCOMPRESION DE CADERA TIPO HUNGERFORD MAS INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74048', 'DRENAJE ABSCESO PROFUNDO O HEMATOMA, PELVIS O CADERA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74049', 'DRENAJE BURSA INFECTADA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74050', 'DRENAJE POR VENTANA OSEA (OSTEOMIELITIS O ABSESO OSEO), PELVIS O CADERA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74051', 'EPIFISIODESIS CON INJERTO O GRAPA, TROCANTER MAYOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74052', 'ESCISION BURSA ISQUIATICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74053', 'ESCISION BURSA O CALCIFICACION TROCANTERICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74054', 'ESCISION TUMOR SUBCUTANEO EN PELVIS O CADERA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74061', 'EXTRACCION PROTESIS TOTAL DE CADERA COMPLICADA DEJANDO ESPACIADOR DE METILMETACRILATO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74062', 'EXTRACCION PROTESIS TOTAL DE CADERA (PROCEDIMIENTO SEPARADO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74066', 'HEMIPELVECTOMIA (HUESO INNOMINADO TOTAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74067', 'INMOVILIZACION FRACTURA O LUXOFRACTURAARTICULACION DE CADERA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74068', 'INMOVILIZACION POR ABDUCCION (FERULA) O TRACCION SIN ANESTESIA NI MANIPULACION LUXACION CONGENITA DE CADERA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74069', 'INMOVILIZACION Y TENOTOMIA DE ADUCTORES BAG LUXACION CONGENITA DE CADERA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74079', 'OSTEOTOMIA INTER O PERTROCANTERICA CON O SIN FIJACION INTERNA Y/O YESO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74086', 'REDUCCION CERRADA LUXACION PROTESIS DE CADERA, SIN ANESTESIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74089', 'REDUCCION QUIRURGICA LUXOFRACTURAARTICULACION SACROILIACA Y SINFIS DEL PUBIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74090', 'REDUCCION QUIRURGICA FRACTURA O LUXACIONDECOCCIX' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74093', 'REDUCCION QUIRURGICA CON OSTEOSINTESIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74095', 'REDUCCION QUIRURGICA DE CADERA CON TENOTOMIA, OSTEOTOMIA ACETABULAR Y ACORTAMIENTO FEMORAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74096', 'REDUCCION QUIRURGICA DE CADERA, CON TENOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74097', 'REDUCCION QUIRURGICA DE CADERA, CON TENOTOMIA Y ACORTAMIENTO FEMORA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74098', 'REDUCCION QUIRURGICA DE CADERA, CON TENOTOMIA Y OSTEOTOMIA ACETABULAR (EXCEPTO SALTER)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74099', 'REDUCCION QUIRURGICA DE CADERA, CON TENOTOMIA Y SALTER' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74101', 'REDUCCION QUIRURGICA SACROILIACA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74103', 'REDUCCION QUIRURGICA SINFISIS LUXACION ARTICULACION SACROILIACA Y SINFISIS DEL PUBIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74104', 'REDUCCION QUIRURGICA SINFISIS Y SACROILIACA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74119', 'SINOVECTOMIA TOTAL ARTROSCOPIA DE CADERA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74121', 'TENOTOMIA DE ADUCTORES, ABIERTA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74122', 'TENOTOMIA DE ADUCTORES, ABIERTA CON NEURECTOMIA DEL OBTURADOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74123', 'TENOTOMIA DE ADUCTORES, SUBCUTANEA CERRADA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74124', 'TENOTOMIA DE PSOAS, ABIERTA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74125', 'TRACCION ESQUELETICA (HOSPITALIZADO)TRATAMIENTO UNICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74126', 'TRANSFERENCIA DE ADUCTORES A ISQUION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74132', 'TRATAMIENTO AMBULATORIO SIN MANIPULACION FRACTURA PELVIS Y CADERA (ILIACO, ISQUION Y PUBIS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74133', 'TRATAMIENTO AMBULATORIO SIN MANIPULACION LUXOFRACTURAARTICULACION SACROILIACA Y SINFISIS DEL PUBIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74138', 'TRATAMIENTO QUIRURGICO FRACTURA PELVIS Y CADERA (ILIACO,ISQUION Y PUBIS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75001', 'APLICACION DE FERULA CORTA DE PIERNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75015', 'AMPUTACION POR MUSLO, ABIERTA, CIRCULAR (GUILLOTINA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75016', 'AMPUTACION POR MUSLO, ADAPTACION PROTESICA INMEDIATA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75017', 'AMPUTACION POR MUSLO, CIERRE SECUNDARIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75018', 'AMPUTACION POR MUSLO, CUALQUIER NIVEL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75019', 'AMPUTACIONES DEL MUSLO A TRAVES DEL FEMUR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75036', 'ARTROTOMIA DE RODILLA PARA BIOPSIA SINOVIAL UNICAMENTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75037', 'ARTROTOMIA DE RODILLA PARA SINOVECTOMIA ANTERIOR O POSTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75038', 'ARTROTOMIA DE RODILLA PARA SINOVECTOMIA ANTERIOR Y POSTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75039', 'ARTROTOMIA DE RODILLA POR INFECCION, CON EXPLORACION, DRENAJE O EXTRACCION CUERPO EXTRAÑO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75043', 'CAPSULOTOMIA POSTERIOR (LIBERACION POSTERIOR)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75045', 'CONDROPLASTIA DE ABRASION PARA CUALQUIER ZONA CONDILAR, INTERCONDILAR O PATELAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75052', 'EPIFISIODESIS CON INJERTO O GRAPA, FEMUR DISTAL Y TIBIA Y PERONE PROXIMAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75053', 'EPIFISIODESIS CON INJERTO O GRAPA, TIBIA Y PERONE PROXIMAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75059', 'EXTRACCION DE CUERPOS LIBRES INTRA-ARTICULARES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75072', 'INMOVILIZACION FRACTURA TERCIO MEDIO (DIAFISIS) FEMUR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75074', 'INMOVILIZACION EPIFISSIS CERRADAS (SUPRACONDILEO O TRANSCONDILEO) TERCIO DISTAL FEMUR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75075', 'INMOVILIZACION CONDILOS (MEDIAL O LATERAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75078', 'INMOVILIZACION CON YESO FRACTURA ROTULA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75079', 'INMOVILIZACION SIN MANIPULACION FRACTURA TERCIO PROXIMAL FEMUR INTERTROCANTERICA, PERTROCANTERICA, SUBTROCANTERICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75080', 'INMOVILIZACION SIN MANIPULACION (YESO EN NIÑOS) FRACTURACUELLO INTERTROCANTERICO DE FEMUR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75081', 'LIBERACION ADHERENCIAS-ARTROFIBROSIS, CUADRICEPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75085', 'MENISCECTOMIA MEDIAL Y LATERAL INCLUYE CONDROPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75096', 'OSTEOSINTESIS SIN REDUCCION FRACTURA CUELLO FEMUR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75099', 'OSTEOTOMIA DE FEMUR (DIAFISIS O SUPRACONDILEA), SIN FIJACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75102', 'OSTEOTOMIA PROXIMAL TIBIA CON O SIN OSTEOTOMIA DE PERONE (CORRECCION GENU VARO O VALGO), DESPUES DE CIERRE DE EPIFISIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75110', 'RECONSTRUCCION LIGAMENTARIA (AUMENTACION), RODILLA, EXTRAARTICULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75111', 'RECONSTRUCCION LIGAMENTARIA, RODILLA, INTRAARTICULAR (ABIERTA) Y EXTRAARTICULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75112', 'RECONSTRUCCION PARA LUXACION RECIDIVANTE DE ROTULA, REALINEACION APARATO EXTENSOR AVANCE MUSCULAR O LIBERACION (CAMPBELL, GOLDWAIT)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75113', 'RECONSTRUCCION PARA LUXACION RECIDIVANTE DE ROTULA, REALINEACION APARATO EXTENSOR, AVANCE MUSCULAR O LIBERACION Y PATELECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75114', 'RECONSTRUCCION PARA LUXACION RECIDIVANTE DE ROTULA, REALINEACION DISTAL (HAUSER)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75120', 'RECONSTRUCCION SECUNDARIA DEL TENDON ROTULIANO CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75125', 'REDUCCION CERRADA Y FIJACION PERCUTANEA CONDILOS (MEDIAL O LATERAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75130', 'REDUCCION E INMOVILIZACION (SIN ANESTESIA) LUXACION RODILLA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75138', 'REDUCCION QUIRURGICA CON VARILLA O PLACA TERCIO MEDIO (DIAFISIS) FEMUR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75146', 'RELAJACION DE RETINACULO LATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75147', 'RELAJACION DE RETINACULO LATERAL MAS OSTEOTOMIA DE REALINEACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75148', 'RELAJACION DE RETINACULO LATERAL MAS OSTEOTOMIA DE REALINEACION MAS PLICATURA DE RETINACULO MEDIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75149', 'RELAJACION DE RETINACULO LATERAL MAS REALINEACION PROXIMAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75154', 'RESECCION O CURETAJE DE QUISTE OSEO O TUMOR BENIGNO FEMUR, CON ALOINJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75155', 'RESECCION O CURETAJE DE QUISTE OSEO O TUMOR BENIGNO FEMUR, CON AUTO INJERTO (INCLUYE OBTENCION) CON AUTO INJERTO (INCLUYE OBTENCION)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75156', 'RESECCION O CURETAJE DE QUISTE OSEO O TUMOR BENIGNO FEMUR, CON OSTEOSINTESIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75157', 'RESECCION O CURETAJE DE QUISTE OSEO O TUMOR BENIGNO FEMUR, SIN INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75167', 'SUTURA PRIMARIA DE CUADRICEPS O HAMSTRINGS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75169', 'TENOTOMIA ABIERTA DE ISQUIOTIBIALES, MULTIPLE, UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75170', 'TENOTOMIA ABIERTA DE ISQUIOTIBIALES, UNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75171', 'TENOTOMIA SUBCUTANEA DE ADUCTORES O HAMSTRING, UNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75172', 'TENOTOMIA SUBCUTANEA DE ADUCTORES O HAMSTRING,MULTIPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75173', 'TRACCION ESQUELETICA HOSP (TRATAMIENTO UNICO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75174', 'TRACCION ESQUELETICA PREVIA A CIRUGIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75175', 'TRANSFERENCIA DE ISQUIOTIBIALES A FEMUR (EGGERS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75176', 'TRANSFERENCIA DE ISQUIOTIBIALES A ROTULA, MULTIPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75177', 'TRANSFERENCIA DE ISQUIOTIBIALES A ROTULA, UNICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76006', 'REIMPLANTE DE PIE POR AMPUTACION COMPLETA (NO VIABLE)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76008', 'REIMPLANTE DE PIERNA POR AMPUTACION COMPLETA (NO VIABLE)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76011', 'ALARGAMIENTO DE TENDON DE AQUILES SUBCUTANEO (TENOTOMIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76015', 'ALARGAMIENTO O ACORTAMIENTO DE TENDON, PIERNA O TOBILLO, MULTIPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76016', 'ALARGAMIENTO O ACORTAMIENTO DE TENDON, PIERNA O TOBILLO, UNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76020', 'AMPUTACION DE PIERNA A TRAVES DE TIBIA Y PERONE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76021', 'AMPUTACION DE PIERNA, ABIERTA , CIRCULAR (GUILLOTINA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76022', 'AMPUTACION DE PIERNA, CIERRE SECUNDARIO O REVISION DE CICATRIZ' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76023', 'AMPUTACION DE PIERNA, CON APLICACION DE PROTESIS INMEDIATA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76025', 'AMPUTACION DEL DEDO, ARTICULACION INTERFALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76026', 'AMPUTACION DEL DEDO, ARTICULACION METATARSOFALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76028', 'AMPUTACION METATARSIANO Y DEDO (1 RAYO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76040', 'ARTRODESIS MEDIOTARSIANA O TARSOMETATARSIANA,1 ARTICULACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76041', 'ARTRODESIS MEDIOTARSIANA, NAVICULO-CUNEIFORME CON ALARGAMIENTO Y AVANCE TENDON (MILLER)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76044', 'ARTRODESIS SUBTALAR MAS INJERTO U OSTEOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76048', 'ARTROPLASTIA DE TOBILLO, CON PROTESIS TOTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76049', 'ARTROPLASTIA DE TOBILLO, RECONSTRUCCION SECUNDARIA PROTESIS TOTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76062', 'BIOPSIA, PIERNA O TOBILLO, DE TEJIDOS BLANDOS, SUPERFICIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76064', 'CAPSULOTOMIA DE MEDIOPIE, LIBERACION MEDIAL MAS ALARGAMIENTO TENDINOSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76065', 'CAPSULOTOMIA DE MEDIOPIE, LIBERACION MEDIAL UNICA (PROCEDIMIENTO SEPARADO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76066', 'CAPSULOTOMIA MEDIOTARSIANA (HEYMAN)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76067', 'CAPSULOTOMIA PARA CONTRACTURA INTERFALANGICA, CON O SIN TENORRAFIA, UNICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76068', 'CAPSULOTOMIA PARA CONTRACTURA METATARSOFALANGICA, CON O SIN TENORRAFIA, UNICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76072', 'ARTROSCOPIA DIAGNOSTICA DE TOBILLO Y PIE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76080', 'EPIFISIODESIS CON INJERTO O GRAPA, TIBIA Y PERONE DISTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76081', 'EPIFISIODESIS CON INJERTO O GRAPA, TIBIA Y PERONE PROXIMAL Y DISTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76082', 'EPIFISIODESIS CON INJERTO O GRAPA, TIBIA Y PERONE PROXIMAL Y DISTAL Y FEMUR DISTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76087', 'ESCISION TUMOR EN PIE, PROFUNDO, SUBFASCIAL, INTRAMUSCULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76099', 'FASCIECTOMIA, ESCISION FASCIA PLANTAR, PARCIAL (PROCEDIMIENTO SEPARADO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76101', 'FASCIOTOMIA DESCOMPRESIVA, PIERNA, ANTERIOR Y/O LATERAL UNICAMENTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76102', 'FASCIOTOMIA DESCOMPRESIVA, PIERNA, ANTERIOR Y/O LATERAL UNICAMENTE, CON DEBRIDAMIENTO DE MUSCULO O NERVIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76103', 'FASCIOTOMIA DESCOMPRESIVA, PIERNA, ANTERIOR Y/O LATERAL Y POSTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76104', 'FASCIOTOMIA DESCOMPRESIVA, PIERNA, POSTERIOR UNICAMENTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76105', 'FASCIOTOMIA DESCOMPRESIVA, PIERNA, POSTERIOR UNICAMENTE, CON DEBRIDAMIENTO DE MUSCULO O NERVIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76106', 'FASCIOTOMIA PLANTAR O DIGITAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76109', 'HALLUX RIGIDO, QUEILECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76112', 'HALLUX VALGUS, CORRECCION, ARTRODESIS METATARSO FALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76113', 'HALLUX VALGUS, CORRECCION, ARTRODESIS T-MT Y TEJIDOS BLANDOS DISTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76114', 'HALLUX VALGUS, CORRECCION, DOBLE OSTEOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76115', 'HALLUX VALGUS, CORRECCION, OSTEOTOMIA DISTAL METATARSIANO (CHEVRON, MITCHELL, OTRAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76116', 'HALLUX VALGUS, CORRECCION, OSTEOTOMIA PRIMERA CUÑA Y TEJIDOS BLANDOS DISTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76117', 'HALLUX VALGUS, CORRECCION, OSTEOTOMIA PROXIMAL FALANGE (AKIN)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76118', 'HALLUX VALGUS, CORRECCION, RESECCION OSEA MAS IMPLANTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76124', 'INMOVILIZACION TERCIO DISTAL TIBIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76125', 'INMOVILIZACION METTARSIANOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76126', 'INMOVILIZACION FRACTURA HALLUX' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76127', 'INMOVILIZACION FRACTURA FALANGE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76129', 'INMOVILIZACION ASTRAGALO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76130', 'INMOVILIZACION BLANDA METATARSIANOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76131', 'INMOVILIZACION BLANDA, BRACE TOBILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76132', 'INMOVILIZACION CALCANEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76135', 'INMOVILIZACION CON YESO FRACTURA BIMALEOLAR TOBILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76136', 'INMOVILIZACION CON YESO FRACTURA TRIMALEOLAR TOBILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76137', 'INMOVILIZACION CON YESO ESGUINCE MEDIOPIE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76138', 'INMOVILIZACION CON YESO, FERULA ESGUINCE TOBILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76140', 'INMOVILIZACION OTROS HUESOS DEL TARSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76145', 'LIBERACION SINDACTILIA, CADA ESPACIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76146', 'MACRODACTILIA, RECONSTRUCCION, RESECCION TEJIDOS BLANDOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76147', 'MACRODACTILIA, RECONSTRUCCION, RESECCION TEJIDOS BLANDOS Y HUESO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76157', 'OSTEOTOMIA DE ACORTAMIENTO, ANGULACION O DERROTACION PARA CORREGIR MALA UNION DE FRACTURA FALANGE PROXIMAL, HALLUX' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76158', 'OSTEOTOMIA DE ACORTAMIENTO, ANGULACION O DERROTACION PARA CORREGIR MALA UNION DE FRACTURA FALANGES, OTROS DEDOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76162', 'OSTEOTOMIA DE PERONE PARA CORREGIR MALA UNION DE TOBILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76164', 'OSTEOTOMIA DEL TARSO (EXCEPTO CALCANEO, ASTRAGALO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76165', 'OSTEOTOMIA DEL TARSO (EXCEPTO CALCANEO, ASTRAGALO), CON AUTOINJERTO (INCLUYE OBTENCION)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76169', 'OSTEOTOMIA PRIMER METATARSIANO, BASE O DIAFISIS, CON O SIN ALARGAMIENTO Y CORRECCION ANGULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76170', 'OSTEOTOMIA PRIMER METATARSIANO, BASE O DIAFISIS, CON O SIN ALARGAMIENTO Y CORRECCION ANGULAR, CON AUTOINJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76171', 'OSTEOTOMIA SIMPLE DE PERONE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76172', 'OSTEOTOMIA SIMPLE DE TIBIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76178', 'POLIDACTILIA, RESECCION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76183', 'REDUCCION ABIERTA CON O SIN FIJACION MEDIOTARSIANA (2 O + )' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76184', 'REDUCCION ABIERTA CON O SIN FIJACION MEDIOTARSIANA (UNA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76186', 'REDUCCION ABIERTA CON O SIN FIJACION TARSOMETARSIANA (3 O + )' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76187', 'REDUCCION ABIERTA CON O SIN FIJACION TARSOMETATARSIANA (DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76188', 'REDUCCION ABIERTA CON O SIN FIJACION TARSOMETATARSIANA (UNA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76199', 'REDUCCION CERRADA HUESOS DEL TARSO CON ANESTESIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76201', 'REDUCCION CERRADA LUXACION TARSO METATARSIANA CON ANESTESIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76202', 'REDUCCION CERRADA LUXACION TARSO SIN ANESTESIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76203', 'REDUCCION CERRADA LUXACION TARSOMETATARSIANA SIN ANESTESIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76204', 'REDUCCION CERRADA MAS FIJACION PERCUTANEA FRACTURA HALLUX' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76205', 'REDUCCION CERRADA MAS INMOVILIZACION FRACTURA HALLUX' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76206', 'REDUCCION CERRADA MAS INMOVILIZACION FRACTURA FALANGES EXCEPTO HALLUX' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76212', 'REDUCCION E INMOVILIZACION FRACTURA MALEOLO PERONEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76213', 'REDUCCION E INMOVILIZACION LUXACION ARTICULCION DEL TOBILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76216', 'REDUCCION E INMOVILIZACION IF CON ANESTESIA LUXOFRACTURA ARTICULACION METATARSO FALANGICA E INTERFALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76217', 'REDUCCION E INMOVILIZACION IF SIN ANESTESIA LUXOFRACTURA ARTICULACION METATARSO FALANGICA E INTERFALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76218', 'REDUCCION E INMOVILIZACION MTF CON ANESTESIA LUXOFRACTURA ARTICULACION METATARSO FALANGICA E INTERFALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76219', 'REDUCCION E INMOVILIZACION MTF SIN ANESTESIA LUXOFRACTURA ARTICULACION METATARSO FALANGICA E INTERFALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76221', 'REDUCCION MAS FIJACION PERCUTANEA FRACTURA TERCIO MEDIO DE TIBIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76222', 'REDUCCION MAS FIJACION PERCUTANEA IF LUXOFRACTURA ARTICULACION METATARSO FALANGICA E INTERFALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76223', 'REDUCCION MAS FIJACION PERCUTANEA MTF LUXOFRACTURA ARTICULACION METATARSO FALANGICA E INTERFALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76225', 'REDUCCION OTROS HUESOS DEL TARSO BAG' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76226', 'REDUCCION OTROS HUESOS DEL TARSO BAG MAS FIJACIONPERCUTANEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76228', 'REDUCCION QUIRURGICA FRACTURA PROXIMAL PERONE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76229', 'REDUCCION QUIRURGICA FRACTURA TRIMALEOLAR TOBILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76230', 'REDUCCION QUIRURGICA LUXOFRACTURA ARTICULACION METATARSO FALANGICA E INTERFALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76231', 'REDUCCION QUIRURGICA FRACTURA HALLUX' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76232', 'REDUCCION QUIRURGICA FRACTURA FALANGES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76234', 'REDUCCION QUIRURGICA METATRSIANOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76235', 'REDUCCION QUIRURGICA ASTRAGALO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76236', 'REDUCCION QUIRURGICA CALCANEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76237', 'REDUCCION QUIRURGICA CALCANEO MAS INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76238', 'REDUCCION QUIRURGICA COMPLICADA O CON VARILLA BLOQUEADA FRACTURA TERCIO DISTAL TIBIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76239', 'REDUCCION QUIRURGICA CON FIJACION LUXACION ARTICULACION TOBILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76241', 'REDUCCION QUIRURGICA CON VARILLA,PLACA Y/O CERCLAJE FRACTURA TERCIO MEDIO TIBIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76242', 'REDUCCION QUIRURGICA DE RUPTURA DE SINDESMOSIS FRACTURA TERCIO DISTAL TIBIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76243', 'REDUCCION QUIRURGICA DEL PILON Y MALEOLO LATERAL FRACTURA TERCIO DISTAL TIBIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76244', 'REDUCCION QUIRURGICA DIAFISIS TIBIAL DISTAL UNICAMENTE FRACTURA TERCIO DISTAL TIBIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76245', 'REDUCCION QUIRURGICA DIAFISIS TIBIAL DISTAL, PILON TIBIAL Y PERONE DISTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76246', 'REDUCCION QUIRURGICA DOS PLATILLOS FRACTURA TERCIO PROXIMAL TIBIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76247', 'REDUCCION QUIRURGICA DOS PLATILLOS Y EXTENSION DIAFISIARIA FRACTURA TERCIO PROXIMAL TIBIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76249', 'REDUCCION QUIRURGICA INFRASINDESMAL FRACTURA MALEOLO PERONEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76252', 'REDUCCION QUIRURGICA OTROS HUESOS DEL TARSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76253', 'REDUCCION QUIRURGICA SIN FIJACION LUXACION ARTICULCION TOBILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76254', 'REDUCCION QUIRURGICA SUPRASINDESMAL FRACTURA MALEOLO PERONEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76257', 'REDUCCION QUIRURGICA UN PLATILLO FRACTURA TERCIO PROXIMAL TIBIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76258', 'REDUCCION Y FIJACION PERCUTANEA LUXACION MEDIOTARSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76259', 'REDUCCION Y FIJACION PERCUTANEA LUXACION TARSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76260', 'REDUCCION Y FIJACION PERCUTANEA LUXACION TARSO METATARSIANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76262', 'REMOCION CUERPO EXTRAÑO, PIE, PROFUNDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76263', 'REMOCION CUERPO EXTRAÑO, PIE, SUBCUTANEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76267', 'REPARACION DEFECTO DE FASCIA, PIERNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76270', 'REPARACION QUIRURGICA (1 COLATERAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76271', 'REPARACION QUIRURGICA (2 COLATERALES)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76272', 'REPARACION SECUNDARIA LIGAMENTOS COLATERALES DE TOBILLO CON TRANSFERENCIAS (WATSON-JONES, CHRISTMAN-SNOOK ETC)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76273', 'REPARACION SECUNDARIA LIGAMENTOS COLATERALES DE TOBILLO SIN TRANSFERENCIAS TENDINOSAS (LARSON-BROSTROM)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76274', 'RESECCION CONDILAR DISTALES FALANGE DEDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76281', 'RESECCION O CURETAJE DE QUISTE OSEO O TUMOR BENIGNO, ASTRAGALO O CALCANEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76283', 'RESECCION O CURETAJE DE QUISTE OSEO O TUMOR BENIGNO, ASTRAGALO O CALCANEO, CON O SIN INJERTO (INCLUYE OBTENCION)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76294', 'RESECCION RADICAL FALANGE, POR TUMOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76297', 'RESECCION RADICAL DE TUMOR DE TEJIDOS BLANDOS (MALIGNO), PIERNA O TOBILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76299', 'RESECCION RADICAL METATARSIANO, POR TUMOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76300', 'SAUCERIZACION, SECUESTRECTOMIA O ESCISION DE FALANGE POR OSTEOMIELITIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76302', 'SAUCERIZACION, SECUESTRECTOMIA O ESCISION PARCIAL DE TUMOR DEL TARSO (EXCEPTO CALCANEO, ASTRAGALO) O METATARSIANO POR OSTEOMIELITIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76312', 'SINDACTILIA, RECONSTRUCCION CON O SIN RESECCION DE HUESO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76316', 'SINOVECTOMIA PARCIAL TOBILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76318', 'SINOVECTOMIA VAINA TENDINOSA EXTENSORES DEL PIE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76319', 'SINOVECTOMIA, INTERTARSIANA O TARSOMETATARSIANA, CADA UNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76320', 'SINOVECTOMIA, METATARSOFALANGICA, CADA UNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76321', 'SINOVECTOMIA, VAINA TENDINOSA FLEXORES PIE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76322', 'SINOVECTOMIA, VAINA TENDINOSA TIBIAL POSTERIOR O PERONEOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76325', 'TENOLISIS EXTENSOR PIE, UNICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76326', 'TENOLISIS FLEXOR PIE, MULTIPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76327', 'TENOLISIS FLEXOR PIE, UNICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76347', 'TRANSFERENCIA TENDINOSA ADICIONAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76350', 'TRATAMIENTO DE LESION OSTEOCONDRAL, FIJACION CON MATERIAL ABSORBIBLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76351', 'TRATAMIENTO DE LESION OSTEOCONDRAL, PERFORACIONES Y/O CURETAJE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('77001', 'TRANSPLANTE DE MEDULA OSEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('77002', 'ASPIRADO MEDULA OSEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('77003', 'BIOPSIA MEDULA OSEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('77004', 'TRANSPLANTE DE CELULAS MADRES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78020', 'VENDAJE CODO A MUQECA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78021', 'VENDAJE DE CINTURA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78022', 'VENDAJE DE TORAX' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78023', 'VENDAJE DEDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78024', 'VENDAJE HOMBRO (VELPEAU)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78025', 'VENDAJE MANO A DEDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78026', 'VENDAJE RODILLA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78027', 'VENDAJE TOBILLO-PIE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78030', 'ARTROCENTESIS, ASPIRACION Y/O INYECCION ARTICULACION MEDIANA, BURSA O GANGLION (TEMPORO MANDIBULAR, ACROMIOCLAVICULAR, MUQECA, CODO, TOBILLO, BURSA OLECRANEANA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78031', 'ARTROCENTESIS, ASPIRACION Y/O INYECCION PEQUEQA ARTICULACION, BURSA O GANGLION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78034', 'BIOPSIA DE GANGLIO PROFUNDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78035', 'BIOPSIA DE GANGLIO SUPERFICIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78037', 'BIOPSIA DE HUESO CON AGUJA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78045', 'BIOPSIA MUSCULAR PERCUTANEA CON AGUJA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78047', 'CAMBIO CUALQUIER TIPO DE YESO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78052', 'COLGAJO MUSCULAR MIOCUTANEO O FASCIOCUTANEO MIEMBRO INFERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78053', 'COLGAJO MUSCULAR MIOCUTANEO O FASCIOCUTANEO MIEMBRO SUPERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78054', 'COLGAJO NEUROVASCULAR PEDICULADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78083', 'DISECCION MODIFICADA DEL CUELLO UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78084', 'DISECCION  DEL CUELLO BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78085', 'DISECCION RADICAL DEL CUELLO UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78086', 'DISECCION SUPRAHIODEA DE CUELLO BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78087', 'DISECCION SUPRAHIODEA DE CUELLO UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78094', 'FASCIOTOMIA MANO Y DEDOS (DUPUYTREN)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78101', 'RESECCION DE TUMOR HUESO, CARTILAGO, SINOVIAL ARTICULAR (BENIGNO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78102', 'RESECCION DE TUMOR HUESO, CARTILAGO, SINOVIAL ARTICULAR (BENIGNO) CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78103', 'RESECCION DE TUMOR HUESO, CARTILAGO, SINOVIAL ARTICULAR (MALIGNO)CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78118', 'RESECCION DE FISTULA DE LA PRIMERA HENDIDURA BRANQUIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78119', 'RESECCION DE FISTULA DE LA SEGUNDA HENDIDURA BRANQUIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79021', 'COLGAJO DE PIEL A DISTANCIA (INCLUIDOS VARIOS TIEMPOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79033', 'CORRECCION CICATRIZ EN BOCA, CUELLO, AXILAS, GENITALES MAYOR DE 7 CM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79034', 'CORRECCION CICATRIZ EN BOCA, CUELLO, AXILAS, GENITALES MENOR DE 2,5 CM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79035', 'CORRECCION CICATRIZ EN EL CUERO CABELLUDO, TRONCO, E SUP Y E INF DE 2,5 A 7 CM (INCLUYE Z-PLASTIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79036', 'CORRECCION CICATRIZ EN EL CUERO CABELLUDO, TRONCO, E SUP Y E INF DE 7 CM EN ADELANTE (INCLUYE Z-PLASTIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79037', 'CORRECCION CICATRIZ EN EL CUERO CABELLUDO, TRONCO, E SUP Y E INF MENOR DE 2,5 (INCLUYE Z-PLASTIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79038', 'CORRECCION CICATRIZ EN FRENTE, MEJILLAS, MENTON, PARPADOS, NARIZ, OIDOS Y/O LABIOS DE 1 A 2,5 CM (INCLUYE Z-PLASTIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79039', 'CORRECCION CICATRIZ EN FRENTE, MEJILLAS, MENTON, PARPADOS, NARIZ, OIDOS Y/O LABIOS DE 2,5 A 7,5 CM (INCLUYE Z-PLASTIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79040', 'CORRECCION CICATRIZ EN FRENTE, MEJILLAS, MENTON, PARPADOS, NARIZ, OIDOS Y/O LABIOS DE MAS DE 7,5 CM (INCLUYE Z-PLASTIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79041', 'CORRECCION CICATRIZ EN FRENTE, MEJILLAS, MENTON, PARPADOS, NARIZ, OIDOS, Y/O LABIOS HASTA 1 CM (INCLUYE Z-PLASTA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79042', 'CORRECCION DE LABIO FISURADO BILATERAL EN DOS TIEMPO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79043', 'CORRECCION DE LABIO FISURADO BILATERAL EN UN TIEMPO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79045', 'CORRECCION PARCIAL DE LABIO FISURADO POR ADHESION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79046', 'CORRECCION PRIMARIA DE LABIO FISURADO UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79047', 'CORRECCION SECUNDARIA DE LABIO FISURADO UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79099', 'DRENAJE PROFUNDO PARTES BLANDAS, INCLUYE:ABSCESO PROFUNDO, FLEGMON' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79104', 'ESCAROTOMÍA EN TRONCO O UNA EXTREMIDAD' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79108', 'EXCICION DE LESION BENIGNA EN CUERO CABELLUDO, CUELLO MANOS, PIES Y GENITALES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79142', 'OPERACION RECONSTRUCTIVA GLABELAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79146', 'INCISION Y DRENAJE DE ABSCESO (CUTANEO, SUBCUTANEO O PARONIQUIA), COMPLICADO O MULTIPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79148', 'INCISION Y DRENAJE DE ABSCESO O HEMATOMA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79151', 'INCISION Y DRENAJE DE HEMATOMA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79152', 'INCISION Y DRENAJE DE HEMATOMA, SEROMA, COLECCION LIQUIDA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79154', 'INCISION Y REMOCION CUERPO EXTRAÑO TEJIDO CELULAR COMPLICADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79155', 'INCISION Y REMOCION CUERPO EXTRAÑO TEJIDO CELULAR SIMPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79157', 'INCISION Y REMOCION DE CUERPO EXTRAÑO, TEJIDO SUBCUTANEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79167', 'INJERTO DE PIEL EN AREA ESPECIAL INCLUYE:CARA, CUELLO, GENITALES, PLANTA DE PIE, ZONA DE FLEXION (NO INCLUYE DEDOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79168', 'INJERTO DE PIEL PARCIAL EN AREA GENERAL GRANDE (MAS DE 100 CMS CUADRADOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79169', 'INJERTO DE PIEL PARCIAL EN AREA GENERAL MEDIANO (MENOS DE 100 CMS CUADRADOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79170', 'INJERTO DE PIEL TOTAL EN AREA GENERAL(MAS DE 20 CMS CUADRADOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79171', 'INJERTO DE PIEL TOTAL EN AREA GENERAL(MENOS DE 20 CMS CUADRADOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79172', 'INJERTO LIBRE DE PIEL (EN ESTAMPILLA), UNICO O MULTIPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79174', 'OPERACIÓN RECONSTRUCTIVA DE LABIOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79177', 'LIPOINJERTO EN CARA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79181', 'LIPOSUCCIÓN LOCALIZADA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79194', 'REPARACION SIMPLE DE HERIDAS DE LA CARA, OIDOS, PARPADOS, NARIZ,LABIOS Y/O MUCOSAS HASTA 2,5CM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79195', 'REPARACION SIMPLE DE HERIDAS DE LA CARA, OIDOS, PARPADOS, NARIZ, LABIOS Y/O MUCOSAS DE 2,5 A 5 CM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79196', 'REPARACION SIMPLE DE HERIDAS DE LA CARA, OIDOS, PARPADOS, NARIZ, LABIOS Y/O MUCOSAS DE 5 A 7,5 CM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79197', 'REPARACION SIMPLE DE HERIDAS DE LA CARA, OIDOS, PARPADOS, NARIZ, LABIOS Y/O MUCOSAS 7,5 A 12,5 CM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79198', 'REPARACION SIMPLE DE HERIDAS DE LA CARA, OIDOS, PARPADOS, NARIZ, LABIOS Y/O MUCOSAS 12,5 A 20 CM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79199', 'REPARACION SIMPLE DE HERIDAS DE LA CARA, OIDOS, PARPADOS, NARIZ, LABIOS Y/O MUCOSAS, 20 A 30 CM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79200', 'REPARACION SIMPLE DE HERIDAS DE LA CARA, OIDOS, PARPADOS, NARIZ, LABIOS Y/O MUCOSAS DE MAS DE 30 CM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79201', 'REPARACION SIMPLE DE HERIDAS DEL CUERO CABELLUDO, NUCA, AXILAS, GENITALES EXTERNOS, TRONCO Y/O EXTREMIDADES (MENOR DE 2,5 CMS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79202', 'REPARACION SIMPLE DE HERIDAS DEL CUERO CABELLUDO, NUCA, AXILAS, GENITALES EXTERNOS, TRONCO Y/O EXTREMIDADES DE 2,5 - 7,5 CM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79203', 'REPARACION SIMPLE DE HERIDAS SUPERFICIALES, DEL CUERO CABELLUDO, NUCA, AXILAS, GENITALES EXTERNOS, TRONCO Y/O EXTREMIDADES DE 7,5 12,5 CM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79204', 'REPARACION SIMPLE DE HERIDAS DEL CUERO CABELLUDO, NUCA, AXILAS, GENITALES EXTERNOS, TRONCO Y/O EXTREMIDADES DE 12,5 - 20 CM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79205', 'REPARACION SIMPLE DE HERIDAS , DEL CUERO CABELLUDO, NUCA, AXILAS, GENITALES EXTERNOS, TRONCO Y/O EXTREMIDADES DE 20 A 30 CM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79206', 'REPARACION SIMPLE DE HERIDAS DEL CUERO CABELLUDO, NUCA, AXILAS, GENITALES EXTERNOS, TRONCO Y/O EXTREMIDADES MAS DE 30 CM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79209', 'REPARO SIMPLE DE HERIDAS SUPERFICIALES DEL CUERO CABELLUDO, CUELLO, AXILAS, GENITALES EXTERNOS, TRONCO Y/O EXTREMIDADES INCLUYENDO MANOS Y PIES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79233', 'RESECCION POR LA BASE DE LESION TIPO FIBROMA O SIMILAR 1 A 2' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79235', 'RESECCION POR LA BASE DE LESION TIPO FIBROMA O SIMILAR 3 A 5' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79237', 'RESECCION POR LA BASE DE LESION TIPO FIBROMA O SIMILAR 6 A 9' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79239', 'RESECCION POR LA BASE DE LESION TIPO FIBROMAO SIMILAR + DE 10' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79242', 'REVISION DE CICATRIZ DE TRAQUEOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79243', 'REVISION DE ESTOMA DE TRAQUEOSTOMIA, COMPLEJA, CON ROTACION DEL COLGAJO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79244', 'REVISION DEL ESTOMA DE TRAQUEOSTOMIA, SIMPLE, SIN ROTACION DE COLGAJO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79247', 'SUTURA AVULSION CUERO CABELLUDO (ESCALPE)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79248', 'SUTURA DE AVULSION EN PABELLON AURICULAR, NARIZ, LABIOS, PARPADOS O GENITALES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79249', 'SUTURA DE HERIDA EN PIEL ENTRE 6 Y 10 CM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79250', 'SUTURA DE HERIDA EN PIEL HASTA 5 CM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79251', 'SUTURA DE HERIDA EN PIEL MAYOR DE 11 CM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79252', 'SUTURA DE HERIDA EN PIEL Y FASCIA ENTRE 6 Y 10 CMS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79253', 'SUTURA DE HERIDA EN PIEL Y FASCIA HASTA 5 CMS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79254', 'SUTURA DE HERIDA EN PIEL Y FASCIA MAYOR DE 11 CMS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79255', 'SUTURA DE HERIDA UNICA O MULTIPLE DE PLIEGUES DE FLEXION, GENITALES, MANOS Y PIES (SUMATORIA DE LOS CENTIMETROS EN HERIDAS MULTIPLES)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79256', 'SUTURA DE HERIDAS MULTIPLES DE CUERO CABELLUDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79260', 'TRATAMIENTO MEDICO QUELOIDE INCLUYE: INFILTRACIONES CADA UNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79265', 'RESECCION DE TUMOR MALIGNO EN AREA GENERAL, UNA LESION HASTA TRES CENTIMETROS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79266', 'RESECCION DE TUMOR MALIGNO EN AREA GENERAL, UNA LESION MAYOR DE TRES CENTIMETROS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79269', 'RESECCION DE TUMOR MALIGNO EN AREA ESPECIAL, UNA LESION HASTA TRES CENTIMETROS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79270', 'RESECCION DE TUMOR MALIGNO EN AREA ESPECIAL, UNA LESION MAYOR DE TRES CENTIMETROS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79274', 'RESECCION TUMOR BENIGNO EN CARA, PABELLONES AURICULARES, PARADOS, NARIZ, LABIOS Y MEMB MUCOSAS, 2CM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79276', 'RESECCION TUMOR BENIGNO EN AREA ESPECIAL MAYOR DE  3 CENTIMETROS (1 A 5 LESIONES)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79277', 'RESECCION DE TUMOR BENIGNO EN AREA GENERAL, MAYOR DE TRES CENTIMETROS (2 A 5 LESIONES)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('80005', 'DRENAJE HEMATOMA SUBUNGUEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('80009', 'INJERTO LIBRE LECHO UNGUEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('80010', 'INJERTO VASCULARIZADO LECHO UNGUEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('80012', 'RECONSTRUCCION DE LECHO UNGUEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('80014', 'RESECCION MATRIZ MAS AMPUTACION DISTAL FALANGE (THOMPSON-TERWILLIGER)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('80025', 'REPOSICION UÑA DE POLIETILENO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30011', 'BIOPSIA DE CEREBRO POR TREPANACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30013', 'BIOPSIA CERRADA DE CEREBRO (PERCUTANEA AGUJA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30016', 'BIOPSIA DE REGION PINEAL Y TALLO CEREBRAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30018', 'CIRUGIA DE CHIARI I' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30019', 'CIRUGIA DE CHIARI II' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30020', 'CIRUGIA DE CHIARI III' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30024', 'CRANEOPLASTIA DE DEFECTO OSEO PREEXISTENTE, CON MATERIAL SINTETICO Y/O INJERTO OSEO AUTOLOGO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30026', 'CRANEOTOMIA O CRANIECTOMIA PARA IMPLENTACION DE ELECTRODOS CORTICALES CEREBRALES O CEREBELOSOS PARA REGISTRO Y/O TTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30038', 'DERIVACION DE VENTRICULO A CISTERNA MAGNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30039', 'DERIVACION VENTRICULAR A ESPACIO SUBARACNOIDEO CERVICAL [TORKILSEN]' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30040', 'DESCOMPRESION DE ORBITA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30041', 'DESCOMPRESION DE ORBITA CUALQUIER VIA MAS RESECCION DE MASA INTRAORBITRARIO CADA LADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30042', 'DESCOMPRESION DE ORBITA VIA LATERO INFERIOR CADA LADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30043', 'DESCOMPRESION NEUROVASCULAR DE NERVIO TRIGEMINAL, POR CRANEOTOMIA SUBOCCIPITAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30045', 'ELEVACION DE FRACTURA DEPRIMIDA EN BOLA PING-PONG NIÑOS EXTRADURAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30051', 'CRANEOTOMIA PARA RESECCION TUMOR INFRATENTORIALES DE LOS FEMISFERIOS CEREBELOSOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30066', 'REMOCION O REVISION DE SENSOR INTRACRANEAL DE PRESION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30068', 'RESECCION TUMOR DE MENINGE CEREBRAL, POR CRANIECTOMIA CON DUROPLASTIA Y CRANEOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30071', 'SUPRATENTORIALES FOSA ANTERIOR, TUMORES CORTICALES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30072', 'SUPRATENTORIALES FOSA ANTERIOR, TUMORES DE LINEA MEDIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30073', 'SUPRATENTORIALES FOSA ANTERIOR, TUMORES SUBCORTICALES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30074', 'SUPRATENTORIALES FOSA MEDIA (INCLUYE PARIETAL Y OCCIPITAL), TUMORES CORTICALES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30082', 'SUPRATENTORIALES FOSA MEDIA (INCLUYE PARIETAL Y OCCIPITAL), TUMORES SUBCORTICALES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30083', 'TALAMOTOMIA POR ESTEREOTAXIA [ESTIMULACION Y/O ABLACION DE UNO DE SUS NUCLEOS]' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('30084', 'TUMORES DE LA BASE DEL CRANEO (CAVUM, CLIVUS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31001', 'ABORDAJE DE ATLAS O AXIS VIA TRANSORAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31004', 'ARTRODESIS COLUMNA LUMBAR MAS INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31020', 'BIOPSIA PERCUTANEA MEDULA ESPINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31026', 'CORPECTOMIA PARCIAL O TOTAL MAS INJERTO ANTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31048', 'DESANCLAJE DE FILUM TERMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31054', 'DISCECTOMIA ANTERIOR CERVICAL (1 ESPACIO) INCLUYENDO RESECCION OSTEOFITOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31056', 'DISCECTOMIA ANTERIOR TORACICA (ESPACIO ADICIONAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31061', 'DISCOIDECTOMIA DORSAL VIA ANTEROLATERAL (TRANSTORAXICA) SIN INJERTO NI FUSION NO INCLUYE ENTRADA A TORAX' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31074', 'EXPLORACION DE FUSION ESPINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31081', 'HEMILAMINECTOMIA CON DESCOMPRESION DE NERVIO INCLUYENDO FACETECTOMIA PARCIAL, FORAMINOTOMIA O ESCISION HNP, REEXPLORACION, CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31082', 'HEMILAMINECTOMIA CON DESCOMPRESION DE NERVIO INCLUYENDO FACETECTOMIA PARCIAL, FORAMINOTOMIA O ESCISION HNP, REEXPLORACION, LUMBAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31083', 'HEMILAMINECTOMIA CON DESCOMPRESION DE NERVIO INCLUYENDO FACETECTOMIA PARCIAL, FORAMINOTOMIA O ESCISION HNP, UN ESPACIO, CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31084', 'HEMILAMINECTOMIA CON DESCOMPRESION DE NERVIO INCLUYENDO FACETECTOMIA PARCIAL, FORAMINOTOMIA O ESCISION HNP, UN ESPACIO, LUMBAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31085', 'HEMILAMINECTOMIA O LAMINECTOMIA BILATERAL SACRO-COXIGEA PARA DESCOMPRESION RESECCION DE MASAS, DISCOIDECTEMIA O DRENAJE DE QUISTES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31091', 'INMOVILIZACION CON COLLAR DE FRACTURA DE COLUMNA CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31096', 'INSERCION O REEMPLAZO CATETER SUBARACNOIDEO O EPIDURAL CON RESERVORIO Y BOMBA PARA INFUSION SIN LAMINECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31109', 'LAMINECTOMIA EXTRADURAL CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31110', 'LAMINECTOMIA EXTRADURAL LUMBAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31111', 'LAMINECTOMIA INTRADURAL EXTRAMEDULAR, CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31112', 'LAMINECTOMIA INTRADURAL EXTRAMEDULAR, LUMBAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31113', 'LAMINECTOMIA INTRADURAL EXTRAMEDULAR, SACRA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31114', 'LAMINECTOMIA INTRADURAL EXTRAMEDULAR, TORACICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31115', 'LAMINECTOMIA EXTRADURAL DORSAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31116', 'LAMINECTOMIA CERVICAL BILATERAL DESCOMPRESIVA MAS FORAMINECTOMIA DE UNO O MAS ESPACIOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31160', 'MICRODISCECTOMIA CERVICAL, DORSAL O LUMBAR CON O SIN INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31161', 'MIELOGRAFIA ESPINAL (CUALQUIER SEGMENTO) CADA UNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31163', 'NUCLEOTOMIA PERCUTANEA, NUCLEO PULPOSO, DISCO INTERVERTEBRAL LUMBAR (UNICO O MULTIPLE)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31164', 'OSTEOTOMIA VERTEBRAL ABORDAJE POSTERIOR, PARA CORRECION DEFORMIDAD, 1 SEGMENTO, LUMBAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31171', 'PUNCION ESPINAL LUMBAR DIAGNOSTICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31172', 'PUNCION ESPINAL TERAPEUTICA PARA DRENAJE DE LCR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31180', 'REDUCCION QUIRURGICA FRACTURA COLUMNA CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31181', 'REDUCCION QUIRURGICA FRACTURA SACRO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31189', 'REPARACION DE DURA CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31190', 'REPARACION DE LA DURA POR RUPTURA SIN LAMINECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31191', 'REPARACION DE MENINGOCELE (< 5 CM DIAMETRO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31192', 'REPARACION DE MENINGOCELE (> 5 CM DIAMETRO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31197', 'RESECCION DE LA PRIMERA COSTILLA O DE COSTILLA CERVICAL POR SMNDROME DEL OPIRCULO TORACICO U OTRA CAUSA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('31206', 'RESECCION RADICAL TUMOR MALIGNO TEJIDOS BLANDOS, CUELLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32001', 'ANASTOMOSIS FACIAL ESPINAL O FACIAL HIPOGLOSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32002', 'BIOPSIA DE NERVIO PERIFERICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32004', 'BLOQUEO ANESTESICO DE NERVIO (VAGO, CIATICO, PLEXO BRANQUIAL) GANGLIO ESTELAR CADA UNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32006', 'BLOQUEO CADENA SIMPATICA (TORACICA O LUMBAR)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32014', 'BLOQUEO NERVIO PARAVERTEBRAL (1 NIVEL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32015', 'BLOQUEO NERVIO PARAVERTEBRAL (MULTIPLE)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32016', 'BLOQUEO NERVIO PARAVERTEBRAL FACETARIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32017', 'BLOQUEO NERVIO PUDENDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32020', 'BLOQUEO PLEJO BRAQUIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32021', 'BLOQUEO PLEJO CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32030', 'DESCOMPRESION INTRACANALICULAR DE NERVIO FACIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32036', 'DESCOMPRESION NERVIO PERIFERICO BRAZO O ANTEBRAZO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32039', 'DESCOMPRESION NERVIO PIE O MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32042', 'DESCOMPRESION NEUROVASCULAR DE NERVIO ACUSTICO VESTIBULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32043', 'DESCOMPRESION NEUROVASCULAR DE NERVIOS IX Y X' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32058', 'ESCISION NEUROMA NERVIO PERIFERICO (EXCEPTO CIATICO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32061', 'EXPLORACION PLEJO CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32062', 'IMPLANTACION PERCUTANEA DE ELECTRODOS DE NEUROESTIMULACION ESPI-SUBDURAL ESPINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32065', 'INJERTO DE NERVIO (INCLUYE OBTENCION DE INJERTO), 1 CABLE BRAZO, ANTEBRAZO O MUSLO, PIERNA (HASTA 4 CMS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32077', 'NEUROLISIS DEL PLEJO BRANQUIAL O LUMBAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32111', 'NEUROTIZACION ADICIONAL EN PLEJO BRAQUIAL (C/U)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32113', 'RECONSTRUCCION DEL PLEJO CON INJERTO NERVIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32114', 'RECONSTRUCCION DEL PLEJO CON NEURORRAFIAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32120', 'RESECCION ESCALENO O PECTORALES POR COMPRESION NEUROVASCULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32125', 'REVISION O REMOCION DE ELECTRODOS DE NEUROESTIMULACION INTRACRANEANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('32140', 'SIMPATICETOMIA TORACO LUMBAR BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33005', 'BIOPSIA DE CORNEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33006', 'BIOPSIA DE PARPADO SOD' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33007', 'BIOPSIA PARA MZSCULO EXTRAOCULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33019', 'C EXTRAÑO CORNEA PROFUNDO EXTRACCION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33020', 'C EXTRAÑO DE ORBITA (EXTRACCION)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33021', 'CANTOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33022', 'CANTORRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33023', 'CANTOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33024', 'CAPSULOTOMIA POSTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33025', 'CAPSULOTOMIA POSTERIOR CON YAG LASER' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33033', 'COLOCACIÓN TAPONES LAGRIMALES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33041', 'CORRECCION PTOSIS PALPEBRAL (DESLIZAMIENTO MUSCULO FRONTAL) UNILATERAL O BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33042', 'CORRECCION PTOSIS PALPEBRAL (PROCEDIMIENTO DE FASSANELA Y CERVAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33043', 'CORRECCION PTOSIS PALPEBRAL +RESECCION INTERNA O EXTERNA DEL MZSCULO ELEVADOR; UNILATERAL O BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33044', 'CORRECCION PTOSIS PALPEBRAL CON INJERTO FACIA LATA UNILATERAL O BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33045', 'CORRECCION TELECANTO Y BLEFAROFIMOSIS POR DISRRUPCION ORBITAL UNILATERAL O BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33047', 'DACRIOCISTORINOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33056', 'DRENAJE DE HEMATOMA O ABSCESO POR ''BLEFAROTOMIA SOD' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33059', 'ENUCLEACION CON INJERTO DERMOGRASO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33063', 'EVISCERACION DEL CONTENIDO OCULAR CON IMPLANTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33069', 'EXTIRPACION CHALAZION SIMPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33072', 'EXTIRPACION DE LESION DE PARPADO EXCEPTO CHALAZION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33073', 'EXTIRPACION LESION CONJUNTIVA CON ECLERA ADYACENTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33074', 'EXTIRPACION MATERIAL IMPLANTADO EN SEGMENTO POSTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33076', 'EXTR CUERPO EXT INTRAOCULAR SEGMENTO POSTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33080', 'EXTRACCION EXTRACAPSULAR DE CRISTALINO POR FACOEMULSIFICACION + IMPLANTE DE LENTE INTRAOCULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33081', 'EXTRACION COAGULO CAMARA ANTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33082', 'EXTRACION DE CUERPO EXTRAÑO, SUPERFICIAL CONJUNTIVAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33083', 'FACOASPIRACION + VITRECTMONIA ANTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33087', 'FOTOCOAGULACION CON LASER EN CUERPO CILIAR POR SESION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33088', 'FOTOCOAGULACION RETINA CON RAYOS LASER X SESION NO > DE 2' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33090', 'HERIDA CONJUNTIVA (SUTURA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33091', 'HERIDA ESCLERAL (SUTURA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33092', 'IMPLANTACION DE LENTE INTRAOCULAR DE FIJACION IRIDOCAPSULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33098', 'INSERCION DE IMPLANTE PARA GLAUCOMA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33100', 'INYECCION DE AIRE O LMQUIDO EN CAMARA ANTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33102', 'INYECCIONES DE TOXINA BOTULICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33104', 'IRIDECTOMIA CON INCISION CSRNEA ESCLERAL P4EXTIRPACION LESION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33107', 'IRIDOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33114', 'LASER: PUPILOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33121', 'RETINOPEXIA MEDIANTE DIATERMMA CON O SIN DRENAJE DE LIQUIDO C/S AIRE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33122', 'ORBITOTOMIA COLGAJO OSEO Y ACCESO LATERAL TIPO KROENLEIN' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33125', 'PLASTIA DE ORBITA CON RECONSTRUCCION DE FONDO DE SACO CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33126', 'PLASTIA DE PUNTOS LAGRIMALES UNI O BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33127', 'PLASTIAS EN ESCLERA (ESCLEROPLASTIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33130', 'QUERATECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33133', 'QUERATOPLASTIA PENETRANTE AUTOINJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33134', 'QUERATOPLASTMA LAMELAR AUTO-INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33137', 'RECAMBIO LENTE INTRAOCULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33139', 'RECONSTRUCCION DE PARPADO DE TODAS SUS CAPAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33152', 'REPARO SIMBLEFARON Y CONJUNTIVOPL CON INJERTO LIBRE DE MUC' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33154', 'RESECCION CONJUNTIVA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33155', 'RESECCION CONJUNTIVA CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33157', 'RESECCION DE PTERIGIO SIN INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33159', 'RESECCION DE TUMOR BENIGNO DE ORBITA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33161', 'RESECCION DE TUMOR MALIGNO DE CONJUNTIVA, CON PLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33162', 'RESECCION DE TUMOR MALIGNO DE CONJUNTIVA, SIN PLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33166', 'RESECCION PTERIGIO REPRODUCIDO CON INJERTO DE CONJUNTIVA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33174', 'RETINOPEXIA NEUMATICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33182', 'SUTURA DE HERIDA EN PARPADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33192', 'TRABECULECTOMIA AB EXTERNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33194', 'TRABECULOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33195', 'TRABECULOTOMIA A EXTERNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33197', 'TTO ABIERTO DE LA FRACTURA ORBITA EXCEP HUNDIM CON IMPLAN' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33198', 'VITRECTOMIA + INSERCION LENTE+ SILICON' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33199', 'VITRECTOMIA + INSERCION LENTE+ SILICON+ENDOLASER' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33203', 'VITRECTOMIA + RETINOPEXIA POR IMPLANTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33206', 'VITRECTOMIA MECANICA CON VITREOFAGO VIA PARS-PLANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('33208', 'INTUBACION DE VIAS LAGRIMALES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34003', 'BIOPSIA DEL OIDO EXTERNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34004', 'COLOCACION DE IMPLANTE COCLEAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34005', 'CORRECCION AGENESIA CONDUCTO AUDITIVO EXTERNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34006', 'CURACION OIDO BAJO MICROSCOPIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34012', 'DRENAJE DE ABSCESO DE CANAL AUDITIVO EXTERNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34020', 'LABERINTECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34021', 'LAVADO DE OIDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34025', 'MASTOIDECTOMIA RADICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34026', 'MASTOIDECTOMIA RADICAL MODIFICADA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34027', 'MASTOIDECTOMIA SIMPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34028', 'MEATOPLASTIA DE CAE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34038', 'PLASTIA LOBULO OREJA: C LADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34041', 'RECONSTRUC CONDUCTO AUDITIVO ATRESIA CONGENITA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34053', 'RESECCION APENDICE PREAURICULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34054', 'RESECCION DE FISTULA PREAURICULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34056', 'RESECCION EXOSTOSIS CONDUCTO AUDITIVO EXTERNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34062', 'RESECCION HUESO TEMPORAL POR VIA EXTERNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34063', 'RESECCION LESIONES BLANDOS CONDUCTO AUDITIVO EXTERNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34065', 'RESECCION QUISTE PABELLON AURICULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34071', 'REVISION MASTOIDECTOMIA QUE RESULTA EN MASTOIDECTOMIA SIMPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34072', 'REVISION MASTOIDECTOMIA QUE RESULTA EN MASTOIDECTOMIA SIMPLE CON APICECTOMIA PETROSA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34073', 'REVISION MASTOIDECTOMIA QUE RESULTA EN MASTOIDECTOMIA RADICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34082', 'SUTURAS HERIDAS DE PABELLON AURICULAR INCLUYE CARTILAGO (SEGUN TAMAÑO)SIMPLE (SOLO PIEL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34083', 'SUTURAS HERIDAS DE PABELLON AURICULAR INCLUYE CARTILAGO (SEGUN TAMAÑO)COMPUESTA (CARTILAGO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34084', 'TIMPANOPLASTIA CON MASTOIDECTOMIA INCLUYE CANALOPLASTIA CIRUGIA OIDO MEDIO, REPARACOIDO MEDIO, REPARAC DE MEMBRANA TIMPANICA CON PARED CANAL INTACTA O RECONSTRUCION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34085', 'TIMPANOPLASTIA CON MASTOIDECTOMIA, RADICAL O COMPLETA CON RECONS CADENA OSCICULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34086', 'TIMPANOPLASTIA MAS ANTROSTOMIA O MASTOIDECTOMIA INCLUYENDO CANALOPLASTIA ATICOTOMIA, CIRUGIA DE OIDO MEDIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34087', 'TIMPANOPLASTIA SIN MASTOIDECTOMIA (INCLUYE CANALOPLASTIA, ATICOTOMIA Y/O CIRUGIA DEL OIDO MEDIO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34088', 'TIMPANOPLASTIA TIPO III,-IV-V ( INJERTO COLOCADO EN CONTACTO CON ESTRIBO MOVIL E INTACTO,PLATINA DEL ESTRIBO MOVIL DEJADA EXPUESTA CON BOLSA DE AIRE ENTRE VENTANA REDONDA E INJERTO,VENTANA EN CONDUCTO SEMICIRCULAR HORIZONTAL CUBIERTA POR INJERTO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34089', 'TIMPANOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34090', 'TIMPANOSTOMIA (REQUERIENDO LA INSERCION DE TUBOS DE VENTILACION DIAVOLOS) BAJO ANEST LOCAL O GNAL BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('34091', 'TIMPANOSTOMIA (REQUERIENDO LA INSERCION DE TUBOS DE VENTILACION DIAVOLOS) BAJO ANEST LOCAL O GNAL UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('35006', 'CIERRE DE PERFORACION SEPTAL INCLUYE INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('35008', 'COLOCACION DE PROTESIS EN TABIQUE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('35014', 'CORRECCION ATRESIA COANAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('35025', 'RECONSTRUCCION INTERNA DE LA NARIZ FUNCIONAL CON DERMOPLASTIA SEPTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('35036', 'TURBINOPLASTIA BILATERAL CUALQUIER TECNICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('35038', 'SEPTOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('36002', 'ETMOIDECTOMIA INTRANASAL ANTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('36003', 'ETMOIDECTOMIA TOTAL VIA EXTERNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('36007', 'MAXILOETMOIDECTOMIA BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('36013', 'SINUSOTOMIA COMBINADA TRES O MAS SENOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('36014', 'SINUSOTOMIA ESFENOIDAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('36018', 'ANTROSTNOMIA MAXILAR INTRANASAL UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('36025', 'ESFENOIDECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('36026', 'ETMOIDECTOMIA BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('36027', 'ETMOIDECTOMIA INTTRANASAL TOTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38002', 'ALARGAMIENTO DE PALADAR CON COLGAJO EN ISLA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38004', 'BIOPSIA PALADAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38005', 'BIOPSIA DE GLANDULA SALIVAL CON AGUJA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38006', 'BIOPSIA DE GLANDULA SALIVAL, INCISIONAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38008', 'CIERRE DE FISTULA DE GLANDULA SALIVAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38014', 'CORRECCION DE FISURA PALATINA, CON COLGAJO VOMERIANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38015', 'CORRECCION DE HENDIDURA ALVEOLOPALATINA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38016', 'CORRECCION MACRO O MICROSTOMA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38034', 'DRENAJE GLANDULA SALIVAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38047', 'EXTIRPACION LESION PROFUNDA PALADAR, INCLUYE:ADENOMA LESIONES SUPERFICIALES EXTENSAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38048', 'EXTIRPACION LESION SUPERFICIAL PALADAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38051', 'FARINGOPLASTIA INCLUYE:COLGAJO FARINGEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38052', 'FARINGOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38053', 'FISTULIZACION QUISTE SALIVAL SUBLINGUAL CON PROTESIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38055', 'GLOSECTOMIA PARCIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38056', 'GLOSECTOMIA TOTAL O RADICAL INCLUYE:HEMIGLOSECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38057', 'GLOSOPEXIA INCLUYE:PLASTIA FRENILLO LINGUAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38058', 'GLOSOPLASTIA INCLUYE:INJERTO CUTANO O MUCOSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38059', 'GLOSORRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38065', 'INCISION Y DRENAJE DE ABSCESO PERIAMIGDALINO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38067', 'INJERTO OSEO PALADAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38068', 'INSICION Y DRENAJE ABSCESO INTRAORAL QUISTE HEMATOMA LENGUA O PISO DE LA BOCA LINGUAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38071', 'MIOTOMIA CRICOFARINGEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38072', 'MIOTOMIA MUSCULOS MASTICATORIOS INCLUYE:PARCIAL DE MASETERO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38074', 'PALATORRAFIA EN Z (FURLOW)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38075', 'PAROTIDECTOMIA RADICAL TOTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38076', 'PAROTIDECTOMIA TOTAL, CON DISECCION RADICAL DE CUELLO UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38078', 'QUEILOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38079', 'RECONSTRUCCION DE BOVEDA PALATINA MEDIANTE COLGAJOS PEDICULADOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38080', 'REMOCION CUERPO EXTRAÑO TEJIDOS BLANDOS BOCA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38081', 'REPARACION DE COLOBOMA: INCLUYE:NASO-OCULARES, ORO-AURICULARES, ORO-OCULARES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38082', 'REPARACION DE FISTULA OROANTRAL Y/U ORONASAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38083', 'REPARO DE LA LENGUA Y PISO DE LA BOCA DE MAS DE 2 CMSDE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38084', 'REPARO DE LACERACION DEL TERCIO POSTERIOR DE LA LENGUA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38095', 'RESECCION De MUCOCELE INCLUYE:QUISTE DE GLANDULAS SALIVAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38103', 'RESECCION FOSETAS LABIALES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38106', 'RESECCION LESION PROFUNDA MUCOSA ORAL INCLUYE:SUPERFICIAL EXTENSA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38112', 'RETROPOSICION QUIRURGICA DE LA PREMAXILA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38113', 'RINOQUEILOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38114', 'SECCION DE FRENILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38116', 'SIALOLITOTOMIA PAROTIDA EXTRAORAL O INTRAORAL COMPLICADA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38121', 'SIALOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('38124', 'UVULORRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39004', 'ARTRECTOMIA ARTICULACION TEMPOROMANDIBULAR BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39010', 'CONDILECTOMIA DE LA MANDIBULA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39011', 'CORONOIDECTOMIA (PROC SEPARADO BILATERAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39014', 'ARTROSCOPIA DIAGNOSTICA DE ARTICULACION TEMPOROMANDIBULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39017', 'EXCISION TUMOR MALIGNO MANDIBULAR RESEC RADICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39019', 'EXTIRPACION LESION MALIGNA DE ENCIA CON VACIAMIENTO GANGLIONAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39026', 'MANDIBULECTOMIA PARCIAL CON RECONSTRUCCION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39029', 'MENISECTOMIA ARTICULACION TEMPOROMANDIBULAR BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39033', 'OSEOTOMIA MENTON' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39042', 'REDUCCION ABIERTA DE FRACTURA ALVEOLAR SUPERIOR INCLUYE: FRACTURA DE TUBEROSIDAD MAXILAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39043', 'REDUCCION ABIERTA FRACTURA DE ARCO CIGOMATICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39044', 'REDUCCION ABIERTA FRACTURA DE MALAR, INCLUYE: FRACTURA DEL PISO DE LA ORBITA (BLOW-OUT)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39048', 'REDUCCION ABIERTA FRACTURA MULTIPLES DE HUESOS FACIALES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39053', 'REEMPLAZO TOTAL DE ARTICULACION TEMPORO-MANDIBULAR, INCLUYE: INJERTO DE CARTILAGO DE CRECIMIENTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39054', 'RESECCION DE QUISTE NO ODONTOGENICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39055', 'RESECCION LESION BENIGNA DE MAXILARES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39060', 'TTO ABIERTO FRAC COMPLICADA ABIERTA O CERRADA MANDIBULAR POR MEDIO DE MULTIPLE DE ACCESO QUIRURGICAS INCLUYENDO FIJACION INTERNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39062', 'TTO ABIERTO LUXACION TEMPOROMANDIBULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39065', 'TTO FRACTURA CERRADA MANDIBULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('39066', 'TTO FRACTURA MANDIBULAR FIJACION EXTERNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41002', 'ARITENOIDECTOMIA, ARITENOPEXIA POR VIA EXTERNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41003', 'BIOPSIA CERRADA DE TRAQUEA [ENDOSCOPICA]' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41004', 'CIERRE DE FISTULA TRAQUEOESOFAGICA CON ANASTOMOSIS ESOFAGICA E INTERPOSICION DE TEJIDO MEDIASTINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41009', 'CRICOTIROIDOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41011', 'DILATACION DE LA LARINGE (POR SESION)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41012', 'EXTRACCION MOLDE LARINGEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41015', 'FISTULECTOMIA LARINGOTRAQUEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41016', 'LARINGECTOMIA PARCIAL ANTEROLATERAL PARCIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41022', 'LARINGOPLASTIA CON REDUCCION ABIERTA DE FRACTURA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41024', 'LARINGOSCOPIA DIRECTA CON INYECCION INTRACORDAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41040', 'RECONSTRUCCION TRAQUEAL O LARINGOTRAQUEAL TERMINOTERMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41041', 'REINERVACION LARINGE CON PEDICULO NEUROMUSCULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41042', 'RESECCION ABIERTA DE LESION DE TRAQUEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41050', 'SECCION DEL NERVIO LARINGEO RECURRENTE UNILATERAL (PROCED APARTE)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41052', 'SUTURA DE HERIDA TRAQUEAL (CUELLO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('41056', 'TRAQUEOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42001', 'ADENOIDECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42002', 'AMIGDALECTOMIA PRIMARIA O SECUNDARIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42004', 'BIOPSIA DE HIPOFARINGE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42005', 'BIOPSIA DE LESION VISIBLE SIMPLE EN NASOFARINGE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42006', 'BIOPSIA DE NASOFARINGE CON BUSQUEDA DE LESION PRIMARIA DESCONOCIDA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42007', 'BIOPSIA DE OROFARINGE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42008', 'CIERRE VELOFARINGEO CON COLGAJO FARINGEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42013', 'CORRECCION DE ATRESIA NASOFARINGEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('42019', 'FARINGOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('43002', 'DRENAJE DE ABSCESO TIROIDEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44008', 'ANEURISMA DE LA AORTA ABDOMINAL INFRARENAL CON INTERPOSICION Y CON INJERTO BIFORCADO AORTA BIFEMORAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44012', 'ANEURISMA DE LA OARTA ABDOMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44024', 'AORTOGRAMA TORACICO O ABDOMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44025', 'AORTOGRAMA Y ESTUDIO DE MIEMBROS INFERIORES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44027', 'ARTERIOGRAFIA ABDOMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44028', 'ARTERIOGRAFIA CAROTIDEA O VERTEBRAL (CADA VASO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44031', 'ARTERIOGRAFIA SELECTIVA DE AMBAS CAROTIDAS Y VERTEBRAL (PANANGIOGRAFIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44060', 'COLOCACION DE CATETER PERMANENTE DE QUIMIOTERAPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44062', 'TROMBOENDARTERECTOMIA COMBINADA AORTA-ILIACA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44064', 'CORRECCION DE COARTACION AORTICA CON ANASTOMOSIS T-T' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44065', 'CORRECCION DE COARTACION AORTICA CON FLAP DE SUBCLAVIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44077', 'DERIVACION AORTA RENAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44078', 'DERIVACION LINFOVENOSA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44079', 'DERIVACION MESENTERICO CAVA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44080', 'DERIVACION O PUENTE AORTO CELIACO MESENTERICO O RENAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44081', 'DERIVACION O PUENTE AORTO FEMORAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44083', 'DERIVACION O PUENTE AORTO ILIACO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44086', 'DERIVACION O PUENTE AORTO SUBCLAVIA O CAROTIDA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44087', 'DERIVACION O PUENTE AXILO AXILAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44088', 'DERIVACION O PUENTE AXILO FEMORAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44089', 'DERIVACION O PUENTE ESPLENO RENAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44090', 'DERIVACION O PUENTE FEMORA TIBIAL ANTERIOR, TIBIAL IN SITU' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44091', 'DERIVACION O PUENTE FEMORAL TIBIAL ANTERIOR, TIBIAL POSTERIOR O PERONEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44092', 'DERIVACION O PUENTE FEMORO FEMORAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44093', 'DERIVACION O PUENTE FEMORO POPLITEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44094', 'DERIVACION O PUENTE ILIO FEMORAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44097', 'DERIVACION O PUENTE SUBCLAVIO AXILAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44099', 'DERIVACION O PUENTE YUGULO ATERIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44100', 'DERIVACION O PUENTE YUGULO CAVA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44102', 'DISECCION VENOSA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44112', 'ENDARTARECTOMIA CAROTIDA CERVICAL UN SOLO LADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44113', 'ENDARTERECTOMIA CELIACA Y/O MESENTERICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44114', 'ENDARTERECTOMIA AXILAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44115', 'ENDARTERECTOMIA DE VASOS MIEMBROS SUPERIORES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44116', 'ENDARTERECTOMIA MIEMBROS INFERIORES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44117', 'ENDARTERECTOMIA RENAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44119', 'EXCISION DE TUMOR DEL CUERPO CAROTIDEO CON EXCISION DE LA ARTERIA CAROTIDA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44120', 'EXCISION TUMOR CUERPO CAROTIDEO SIN EXCISION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44121', 'EXPLORACION DE ARTERIA PERIFERICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44122', 'EXPLORACION DE LA ARTERIA CAROTIDA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44128', 'EXTRACCION DE CUERPO EXTRAÑO INTRAVASCULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44137', 'FLEBOEXTRACCION Y/O LIGADURA MULTIPLE VARICES TIPO I BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44143', 'FLEBOGRAFIA DE MIEMBROS SUPERIOR O INFERIOR (POR EXTREMIDAD)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44145', 'FLEBOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44150', 'TROMBOENDARTERECTOMIA DE LA AORTA ABDOMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44164', 'IMPLANTACION DE DISPOSITIVO EN VENA CAVA INFERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44173', 'INSERCION DE CANULA PARA HEMODIALISISY OTROS PROPOSITOS VENA A VENA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44174', 'INSERCION DE CATETER CENTRAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44175', 'INSERCION DE CATETER CENTRAL POR VIA PEREFERICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44176', 'INSERCION DE CATETER YUGULAR ASCENDENTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44178', 'INSERCION DE SCRIBNERSHUNT ARTERIOVENOSO EXTERNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44180', 'INTERRUPCION PARCIAL O COMPLETA DE LA VENA CAVA INFERIOR POR SUTURA, LIGADURA, APLICACION DE GRAPAS (EXTRAVASCULAR) O SOMBRILLAS INTRAVASCULARES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44181', 'INTERRUPCION PARCIAL O COMPLETA DE LA VENA COMUN ILIACA POR LIGADURA O PROCESO INTRAVASCULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44182', 'LIGADURA Y DIVISION DE LA VENA SAFENA MAYOR EN LA UNION SAFENAFEMORAL, CON INTERRUPCIONES DISTALES, UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44186', 'LIGADURA DE CAROTIDA COMUN' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44187', 'LIGADURA DE CAROTIDA EXTERNA O INTERNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44189', 'LIGADURA DE CONDUCTO TORACCICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44190', 'LIGADURA DE DUCTUS ARTERIOSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44191', 'LIGADURA DE LA ARTERIA CAROTIDA EXTERNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44201', 'LIGADURA Y DIVISION CON EMOCION COMPLETA DE LA VENA SAFENA MAYOR Y MENOR CON EXCISION RADICAL DE LA ULCERA E INJERTO CUTANEO BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44230', 'RESECCION DE TUMOR DE MALFORMACION VASCULAR EN CUERO CABELLUDO (EXTRACRANEAL) UN LADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44247', 'TROMBOEMBOLECTOMIA DE ARTERIAS EN MIEMBROS INFERIORES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44248', 'TROMBOEMBOLECTOMIA DE ARTERIAS ABDOMINALES VIA ABIERTA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44249', 'TROMBOEMBOLECTOMIA VENOSA DE BRAZO O ANTEBRAZO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44250', 'TROMBOEMBOLECTOMIA VENOSA SUPERFICIAL EN MIEMBROS INFERIORES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44253', 'TROMBOENDARTERECTOMIA DE LA ARTERIA SUBCLAVIA, INNOMINADA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44255', 'TROMBOENDARTERECTOMIA DE LA CAROTIDA, VERTEBRAL SUBCLAVIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('44256', 'TROMBOLISIS ARTERIAL SELECTIVA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45001', 'ANASTOMOSIS DE VASOS LINFATICOS DE GRUESO CALIBRE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45003', 'BIOPSIA GANGLIONAR PELVICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45008', 'EXTIRPACION DE GANGLIO LINFATICO REGIONNAL EXTENDIDO AL AREA DE DRENAJELINFATICO, INCLUSO PIEL Y TEJIDO CELULLAR SUBCUTANEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45010', 'EXTIRPACION DE LINFANGIOMA DE CUELLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45020', 'LINFADENECTOMIAO VACIAMIENTO INGUINOFEMORALO ILIACA; BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45022', 'LINFADENECTOMIA O VACIAMIENTO INGUINOILIACA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45023', 'LINFADENECTOMIA O VACIAMIENTO MODIFICADA DE CUELLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45024', 'LINFADENECTOMIAO VACIAMIENTO PELVICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45026', 'LINFADENECTOMIA RETROPERITONEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45027', 'LINFANGIOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45028', 'LINFANGIORRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45031', 'TRANSPLANTE DE LINFATICOS AUTOGENO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('45034', 'VACIAMIENTO LINFATICO RADICAL DE CUELLO; BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46012', 'ATRIOSEPTOSTOMIA CON CATETER BALON' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46019', 'CARDIOVERSION ELECTRICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46020', 'CATERISMO TRANS-SEPTAL, MAS CATETERISMO IZQUIERDO Y DERECHO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46021', 'CATETERISMO DERECHO, CON O SIN ANGIOGRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46022', 'CATETERISMO IZQUIERDO Y DERECHO, CON O SIN ANGIOGRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46023', 'CATETERISMO IZQUIERDO, CON O SIN ANGIOGRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46030', 'CIERRE DE DUCTUS POR DISPOSITIVO DE SOMBRILLA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46034', 'CORRECCION DEL TRUNCUS ARTERIOSO CON HOMOINJERTO O CON TEJIDO AUTOLOGO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46036', 'CORRECION DE COARTACION AORTICA CON INTERPOSICION DE INJERTO Y CIRCULACION EXTRA-CORPOREA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46043', 'ESTUDIO ELECTROFISIOLOGICO CON CATETERISMO DERECHO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46044', 'ESTUDIO ELECTROFISIOLOGICO CON CATETERISMO DERECHO E IZQUIERDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46045', 'ESTUDIO ELECTROFISIOLOGICO INTRACARDIACO CON CATETERISMO TRANSEPTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46050', 'IMPLANTACION DE CARDIODESFIBRILADOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46051', 'IMPLANTACION DE MARCAPASO DEFINITIVO BICAMERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46055', 'IMPLANTE Y/O EXPLANTE MARCAPASOS (INCLUYE REPOSICION DE ELECTRODO DE MARCAPASO DEFI UNI O BICAMERAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46060', 'PERICARDIECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46062', 'PERICARDIOCENTESIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46063', 'PERICARDIOCENTESIS MAS DRENAJE POR TUBO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46064', 'PERICARDIOTOMIA PARA EXTRAER COAGULOS O CUERPOS EXTRAÑOS Y VENTANA PERICARDICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46070', 'RECONSTRUCCION DE LA ARTERIA PULMONAR Y SUS RAMAS EN TROMBOEMBOLISMO CRONICO CON CIRCULACION EXTRACORPOREA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46071', 'REEMPLAZO VALVULAR AORTICO CON CORRECCION DE FISTULAS DEL SENO DE VALSALVA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46072', 'REEMPLAZO VALVULAR TRICUSPIDEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46074', 'REMPLAZO DE VALVULA AORTICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46075', 'REMPLAZO DE VALVULA MITRAL CON CIRCULACION EXTRACORPOREA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46076', 'REMPLAZO VALVULAR AORTICO CON HOMOINJERTOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46077', 'REPARACION DE CUERDAS TENDINOSAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46078', 'REPARACION DE MUSCULO PAPILAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46079', 'REPARACION DE TETRALOGIA DE FALLOT' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46080', 'REPARO COMPLETO DE DRENAJE TOTAL ANOMALO DE VENAS PULMONARES (SUPRACARDIACO INTRACARDIACOO INFRACARDIACO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46081', 'REPARO DE ATRESIA TRICUSPIDEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46087', 'REPARO DE LA DOBLE SALIDA DEL VENTRICULO IZQUIERDO Y CONEXION ATRIOVENTRICULAR CONCORDANTE O DISCORDANTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46088', 'REPARO DE VALVULA MITRAL POST-INFARTO CON O SIN PUENTES CORONARIOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46089', 'REPARO DEL CANAL ATRIO VENTRICULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46090', 'REPROGRAMACION DE CARDIODESFIBRILADOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46091', 'REPROGRAMACION DE MARCAPASO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46092', 'RESECCION AORTA ASCENDENTE CON RESECCION O RESUSPENCION VALVULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46094', 'RESECCION DE ANEURISMA VENTRICULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46098', 'RESECCION DE LA AORTA DESCENDENTE CON O SIN BY-PASS CARDIOPULMONAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46099', 'RESECCION DE QUISTE PERICARDICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46101', 'RESECCION DEL CAYADO AORTICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46107', 'TORACOTOMIA PARA MASAJE CARDIACO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46112', 'TRANSPLANTE CARDIACO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46113', 'TROMBOLISIS INTRACORONARIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46115', 'VALVULOPLASTIA MITRAL CON PLASTIA DE CUERDAS TENDINOSAS, LIBERACION DE MUSCULOS PAPILARES Y CON CIRCULACION EXTRACORPOREA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46116', 'VALVULOPLASTIA TRICUSPIDEA PARA CUALQUIER TIPO DE ENFERMEDAD CON CIRCULACION EXTRACORPOREA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46117', 'VALVULOPLASTIA Y/O VALVULOTOMIA AORTICA CON CIRCULACION EXTRACORPOREA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46123', 'ARTERIOGRAFIA CAROTIDEA EXTRACRANEANA BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46124', 'ARTERIOGRAFIA PULMONAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46125', 'ARTERIOGRAFIA SELECTIVA ABDOMINAL DE TRONCO PRIMARIO POR VASO (ARTERIAS: HEPATICAS, GASTRODUODENAL, MESENTERICA SUPERIORO INFERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46127', 'VENOGRAFIA DIAGNOSTICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46132', 'ESCLEROSIS PERCUTANEA DE LESIONES VASCULARES DE CABEZA, CUELLO, TORAX, ABDOMEN O EXTREMIDADES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46133', 'ARTERIOGRAFIA CORONARIA (INCLUYE: CATETERISMO IZQ. Y VENTRICULOGRAFIA IZQ)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46137', 'ANGIOPIASTIA CORONARIA 1 VASO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46138', 'ANGIOPLASTIA CORONARIA 2 O MAS VASOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46139', 'STENT CORONARIO 1 VASO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('46147', 'IMPLANTACION MARCAPASO DEFINITIVO UNICAMERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47001', 'BIOPSIA PULMON A CIELO ABIERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47002', 'BIOPSIA DE PLEURA CON AGUJA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47003', 'BIOPSIA DE PLEURA, ABIERTA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47004', 'BIOPSIA DE TUMOR POR MEDIASTINOSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47008', 'BIOPSIA TRUCUT' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47011', 'CIERRE DE TORACOSTOMIA ABIERTA (PROCEDIMIENTO DE CLAGETT)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47012', 'COLOCACION DE MARCAPASO DIAFRAGMATICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47013', 'CORRECCION HERNIA DIAFRAGMATICA TRANSTORACICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47014', 'CORRECCION DE ESTERNON BIFIDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47015', 'CORRECCION DE ESTERNON EN BIFIDO CON MALLA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47016', 'CORRECCION DE HERNIA PULMONAR A TRAVES DE PARED TORACICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47017', 'CORRECCION DE PECTUS EXCAVATUM CON BARRA METALICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47018', 'CORRECCION DE PECTUS EXCAVATUM O CARINATUM' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47021', 'CORRECCION HERNIA DIAFRAGMATICA COMBINADA, TORACOABDOMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47024', 'DECORTICACION PULMONAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47025', 'DECORTICACION Y PLEURECTOMIA PARIETAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47029', 'EXCISION DE TUMOR EN PARED DE TORAX INCLUYENDO COSTILLAS, CON RECOSTRUCCION PLASTICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47041', 'NEUMONECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47042', 'NEUMONOSTOMIA PARA DRENAJE DE ABSCESO O QUISTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47045', 'PLEURODESIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47046', 'PLEURODESIS POR TORACOSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47048', 'PLEURONEUMONECTOMIA CON DRENAJE DE EMPIEMA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47049', 'RECONSTRUCCION MAYOR DE PARED TORACICA (POST-TRAUMATICA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47050', 'REMOCION O REVISION DE MARCAPASO DIAFRAGMATICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47052', 'RESECCION DE TUMOR MALIGNO DEL MEDIASTINO POR ESTERNOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47053', 'RESECCION DE TUMOR MALIGNO DEL MEDIASTINO POR TORACOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47054', 'RESECCION DE COSTILLA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47059', 'RESECCION DE QUISTE O TUMOR BENIGNO DEL MEDIASTINO POR TORACOSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47060', 'RESECCION DE QUISTE O TUMOR BENIGNO DEL MEDIASTINO POR TORACOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47061', 'RESECCION DE QUISTE SIMPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47062', 'RESECCION DE TUMOR EN PARED COSTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47063', 'RESECCION DE TUMOR EN PARED COSTAL CON RECONSTRUCCION (MALLA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47064', 'RESECCION DE TUMOR EN PARED COSTAL CON RECONSTRUCCION Y COLGAJO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47071', 'RESECCION RADICAL DE ESTERNON POR TUMOR CON LINFADENECTOMI MEDIASTINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47073', 'RESECCION TUMOR BENIGNO DE LA PARED TORACICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47076', 'TORACENTESIS, PUNCION DE CAVIDAD PLEURAL PARA ASPIRACION, INICIAL O SUBSECUENTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47077', 'TORACOPLASTIA CON CIERRE DE FISTULA BRONCOPLEURAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47078', 'TORACOPLASTIA, TIPO EXTRAPLEURAL (TODOS LOS TIPOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47079', 'TORACOSCOPIA CON BIOPSIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47080', 'TORACOSCOPIA DIAGNOSTICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47082', 'TORACOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47087', 'TORACOTOMIA CON RETIRO DE CUERPO EXTRAÑO INTRAPULMONAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47095', 'VENTANA PLEURAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('47096', 'VENTANA PLEURAL MAS RESECCION COSTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48001', 'BIOPSIA CERRADA BRONQUIAL (ENDOSCOPICA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48002', 'BIOPSIA DE PULMON, PERCUTANEA, CON AGUJA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48020', 'LOBECTOMIA CON BRONCOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48021', 'LOBECTOMIA CON DECORTICACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48028', 'PNEUMONECTOMIA TOTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48030', 'REDUCCION QUIRUGICA DE VOLUMEN PULMONAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48031', 'REPARACION DE LACERACION PULMONAR CON CONTROL DE HEMORRAGIA POR TORACOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('48038', 'RESECCION O DESTRUCCION DE LESION O TEJIDO EN BRONQUIO CON BRONCOPLASTIA-VIA ABIERTA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49002', 'BIOPSIA ABIERTA DE ESOFAGO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49003', 'BIOPSIA DE ESOFAGO CERRADA (ENDOSCOPICA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49005', 'CIERRE DE ESOFAGOSTOMIA O FISTULA VIA TORACICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49008', 'CIRUGIA LAPAROSCOPICA ANTIREFLUJO (NISSEN)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49012', 'DILATACIÓN ESOFAGICA (SESIÓN)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49014', 'DILATACION ESOFAGICA NEUMATICA CON BALON' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49015', 'DILATACION ESOFAGICA SOBRE GUIA DE ALAMBRE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49017', 'DIVERTICULECTOMIA ENDOSCOPICA DE ESOFAGO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49018', 'DIVERTICULECTOMIA ESOFAGICA CON O SIN MIOTOMIA VIA TORACICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49022', 'DRENAJE DE ABSCESO DE ESOFAGO POR ESOFAGOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49023', 'ESOFAGOGASTRECTOMIA (TERCIO INFERIOR ), COMBINADO INCISION TORACOABDOMINAL CON O SIN PILOROPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49026', 'ESOFAGOMIOTOMIA DE HELLER CON O SIN PROCEDIMIENTO ANTIRREFLUJO VIA ABDOMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49027', 'ESOFAGOMIOTOMIA DE HELLER CON O SIN PROCEDIMIENTO ANTIRREFLUJO VIA TORACICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49034', 'ESOFAGOSCOPIA CON EXTRACCION DE CUERPO EXTRAÑO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49036', 'ESOFAGOSCOPIA CON INSERCION DE ALAMBRE PARA DILATACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49043', 'ESOFAGOSTOMIA CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49044', 'ESOFAGOSTOMIA CERVICAL MAS MIOTOMIA EN ESPIRAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49048', 'ESOFAGOTOMIA POR VIA TRANSTORACICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49051', 'ESOFAGUECTOMIA EN NIVEL DE LOS DOS TERCIOS SUPERIORES CON INTERPOSICION DE INTESTINO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49052', 'ESOFAGUECTOMIA EN NIVEL DE LOS DOS TERCIOS SUPERIORES Y ANASTOMOSIS GASTRICA, CON O SIN POLIROPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49053', 'ESOFAGUECTOMÍA CON DISECCION RADICAL DE CUELLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49054', 'ESOFOGOSCOPIA PARA CONTROL DE HEMORRAGIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49055', 'EXTRACCION CUERPO EXTRAÑO ESOFAGO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49056', 'INSERCION DE TUBO O PROTESIS (STENT) PERMANENTE EN ESOFAGO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49073', 'POLIPECTOMIA ENDOSCOPICA DE ESOFAGO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('49074', 'ABLACION ESOFAGICA CON RADIOFRECUENCIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50020', 'DRENAJE DE ABSCESO DE PARED' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50025', 'DRENAJE DE ABSCESO RETROPERITONEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50030', 'DRENAJE DE COLECCION (ABSCESO) EN PARED ABDOMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50036', 'EXPLORACION DEL ESPACIO RETROPERITONEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50037', 'EXPLORACION INGUINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50038', 'EXTIRPACION DE TUMOR BENIGNO EN LA PARED ABDOMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50044', 'HERNIORRAFIA EPIGASTRICA MULTIPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50046', 'HERNIORRAFIA ISQUIATICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50047', 'HERNIORRAFIA OBTURADORA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50051', 'LAPARATOMIA EXPLORATORIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50052', 'LAPAROSCOPIA DIAGNOSTICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50057', 'LAPAROTOMIA PARA CITOREDUCCION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50059', 'LAVADO PERITONEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50060', 'OMENTECTOMIA O RESECCION DEL OMENTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50061', 'ONFALECTOMIA, RESECCION DEL OMBLIGO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50076', 'RESECCION DE LESION BENIGNA O MALIGNA EN EPIPLON O EN MESENTERIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50079', 'RESECCION DE QUISTE VITELINO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50082', 'RESECCION DE TUMOR CON DISECCION DE GRANDES VASOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50083', 'RESECCION DE TUMOR MALIGNO DE LA PARED ABDOMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50086', 'RESECCION DE TUMOR MALIGNO EN PARED ABDOMINAL MAS ROTACION DE COLGAJO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50087', 'RESECCION DE TUMORES RETROPERITONEALES O INTRAABDOMINALES COMO QUISTE O ENDOMETRIOMAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50090', 'RETIRO DE CATETER PERMANENTE PARA HEMODIALISIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('50091', 'SECCION DE AHDERENCIAS A PARED ABDOMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51001', 'ANASTOMOSIS DE VESICULA BILIAR A CONDUCTOS HEPATICOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51006', 'BIOPSIA ABIERTA HIGADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51007', 'BIOPSIA ABIERTA DE VESICULA BILIAR O VIAS BILIARES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51008', 'BIOPSIA DE HIGADO ABIERTA O POR LAPAROSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51011', 'BIOPSIA PERCUTANEA DE HIGADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51014', 'CIERRE DE COLECISTOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51015', 'COLECISTECTOMIA CON EXPLORACION, DRENAJE O EXTRACCION DE CALCULOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51019', 'COLECISTOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51021', 'COLEDOCOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51027', 'DILATACION DEL ESFINTER DE ODDI' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51028', 'DILATACION ENDOSCOPICA DE AMPOLLA Y CONDUCTO BILIAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51034', 'DRENAJE DE ABSCESO VIA RETROPERITONEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51043', 'ESFINTERECTOMIA Y PAPILOTOMIA ENDOSCOPICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51044', 'ESFINTEROPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51045', 'EXCISION DE LA AMPOLLA DE VATER (AMPULECTOMIA); CON REIMPLANTACION DE COLEDOCO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51047', 'EXPLORACION DEL CONDUCTO BILIAR PRINCIPAL PARA EXTRACCION DE CUERPO EXTRAÑO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51049', 'EXTIRPACION ENDOSCOPICA DE LESION EN LAS VIAS BILIARES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51051', 'EXTRACCION ENDOSCOPICA DE CALCULOS DEL TRACTO BILIAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51054', 'HEPATECTOMIA TOTAL CON TRASPLANTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51055', 'HEPATECTOMIA, RESECCION DE HIGADO, LOBECTOMIA PARCIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51058', 'HEPATORRAFIA MULTIPLE (TRAUMA HEPATICO SEVERO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51059', 'HEPATORRAFIA SIMPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51060', 'INSERCCION DE TUBO COLEDOCOHEPATICO PARA DESCOMPRESION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51061', 'INSERCION DE CATETER BILIAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51062', 'INSERCION ENDOSCOPICA DE (STENT) EN VIA BILIAR CON GUIA IMAGENOLOGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51063', 'INSERCION ENDOSCOPICA DE TUBO DE DRENAJE NASOBILIAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51066', 'LOBECTOMIA DERECHA TOTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51071', 'RECONSTRUCCION DE VIA BILIAR EXTRAHEPATICA CON ANASTOMOSIS TERMINO TERMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51072', 'REPARACION DE LESION DE VESICULA BILIAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51073', 'RESECCION DE QUISTES DEL COLEDOCO CON/SIN DERIVACION BILIODIGESTIVA Y VALVULA ANTIARREFLUJO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51078', 'SUTURA SIMPLE DE COLEDOCO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('51079', 'TRANSPLANTE DE HIGADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52001', 'ANASTOMOSIS DEL PANCREAS POR LAPAROTOMIA O LAPAROSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52002', 'ANASTOMOSIS DEL PANCREAS; VIA PERCUTANEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52003', 'BIOPSIA POR ASPIRACION [AGUJA] CERRADA DE PANCREAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52005', 'DILATACION ENDOSCOPICA DE DUCTO PANCREATICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52006', 'DRENAJE PERCUTANEO DE SEUDOQUISTE PANCREATICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52007', 'DRENAJE TRANSAMPULAR ENDOSCOPICO DE SEUDOQUISTE DE PANCREAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52008', 'INSERCION ENDOSCOPICA DE TUBO DE DRENAJE NASOPANCREATICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52010', 'MARSUPIALIZACION DE QUISTE DEL PANCREAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52011', 'SUTURA SIMPLE DE PANCREAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52013', 'BIOPSIA DE PANCREAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52014', 'PANCREATECTOMIA TOTAL CON TRASPLANTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52016', 'DRENAJE DE PSEUDO QUISTE PANCREATICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52018', 'EXTRACCION DE CALCULOS PANCREATICOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52019', 'PANCREATECTOMIA DISTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52020', 'PANCREATECTOMIA DISTAL POR LAPAROSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52021', 'PANCREATECTOMIA DISTAL SUBTOTAL CON O SIN ESPLENECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52022', 'PANCREATECTOMIA PROXIMAL SUBTOTAL CON PANCREATICOYEYUNOSTOMIA O PANCREATICODUODENOSTOMIA (OPERACION DE WHIPPLE)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52026', 'PANCREATECTOMIA TOTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52032', 'RESECCION DE LESION DE PANCREAS COMO QUISTE O ADENOMA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('52034', 'RESECCION SIMPLE DE LA AMPOLLA DE VATER' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('54003', 'ESPLENECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('54004', 'ESPLENECTOMIA POR LAPAROSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('54005', 'ESPLENOPORTOGRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('54006', 'ESPLENORRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55001', 'BY-PASS GASTRICO POR OBESIDAD' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55002', 'BIOPSIA DE ESTOMAGO POR LAPARATOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55005', 'CIERRE DE GASTROSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55010', 'CON RECONSTRUCCION POR TRASPLANTE INTESTINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55016', 'DILATACION DE PILORO MEDIANTE INCISION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55017', 'DILATACION ENDOSCOPICA DE PILORO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55020', 'ESOFAGOGASTRO DUODENOSCOPIA Y CANULACION AMPOLLA DE VATER MAS COLANGIO O PANCREATOGRAFIA MAS SFINTEROPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55027', 'ESOFAGOGASTROPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55031', 'GASTRECTOMIA SUB- TOTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55033', 'GASTRODUODENOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55034', 'GASTROPEXIA ANTERIOR POR HERIDA HIATAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55035', 'GASTRORRAFIA, SUTURA DE ULCERA DUODENAL PERFORADA O ULCERA GASTRICA O HERIDA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55037', 'GASTROSTOMIA (PARA ALIMENTACION)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55038', 'GASTROSTOMIA EN RECIEN NACIDOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55039', 'GASTROSTOMIA ENDOSCOPICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55041', 'GASTROSTOMIA NEONATO PARA ALIMENTACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55042', 'GASTROSTOMIA PERMANENTE CON CONSTRUCCION DE TUBO GASTRICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55044', 'GASTROYEYUNOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55047', 'LIBERACION DE BANDAS DE LADD' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55049', 'PILOROMIOTOMIA (FREDET-REMSTEDT)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55050', 'PILOROPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55054', 'SUTURA GASTRICA POR OBESIDAD' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55055', 'TRANSECCION GASTRICA Y LIGADURA DE VARICES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55056', 'VAGOTOMIA Y PILOROPLASTIA CON O SIN GASTROSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55057', 'VAGOTOMIA MAS SEROMIOTOMIA O VAGOTOMIA MAS PILOROPLASTIA POR LAPAROSCOPIA **' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('55058', 'VAGOTOMIA ULTRASELECTIVA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56001', 'ANASTOMOSIS DE INTESTINO DELGADO A INTESTINO DELGADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56002', 'ANASTOMOSIS DE INTESTINO DELGADO AL MUÑON RECTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56003', 'ANASTOMOSIS ILEOCOLICA LATERO-LATERAL PARA HIRSCHSPRONG TOTAL REDUCCION DE VOLVULO EN SIGMOIDES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56004', 'ANOPLASTIA POR ESTENOSIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56005', 'ANOPLASTIA POR FISURA ANAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56006', 'APENDICECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56009', 'BIOPSIA ABIERTA DE RECTO O SIGMOIDE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56010', 'BIOPSIA CERRADA [ENDOSCOPICA] DE RECTO O SIGMOIDE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56013', 'BIOPSIA DE TEJIDO PERIANAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56015', 'BOLSA ILEAL PELVICA CON MUCOSECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56019', 'CIERRE DE FISTULA DE INTESTINO DELGADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56020', 'CIERRE DE FISTULA ENTERICA O ENTEROCOLICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56027', 'CIERRE FISTULA ENTEROCUTANEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56030', 'COLECTOMIA PARCIAL, ANASTOMOSIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56031', 'COLECTOMIA TOTAL MAS RESERVORIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56033', 'COLONOSCOPIA Y RESECCION DE POLIPO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56039', 'COLECTOMIA PARCIAL, ANASTOMOSIS CON CECOSTOMIA O COLOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56045', 'COLECTOMIA, TOTAL, ABDOMINAL, CON ILEOSTOMIA O ILEOPROTOSTOMIA, CON PROTECTOMIA CON PROTECTOMIA E ILEOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56049', 'PROCTOSIGMOIDOSCOPIA PARA DILATACIÓN INSTRUMENTAL CON RESECCIÓN DE POLIPO O PAILOMA UNICO O SIMPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56053', 'CONSTRUCCION DE ANO POR AUSENCIA CONGENITA CON REPARO DE FISTULA URINARIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56055', 'CORECCION DE FISTULA ANO VESTIBULAR (ASPL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56057', 'CORRECCION DE ANO IMPERFORADO Y FISTULA RECTO VESICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56058', 'CORRECCION DE ANO IMPERFORADO Y FISTULA RECTO URETRAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56060', 'CORRECCION DE ATRESIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56061', 'CORRECCION DE EXTROFIA DE CLOACA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56081', 'ENDOSCOPIA DEL INTESTINO DELGADO DESPUES DE LA SEGUNDA PORCIÓN DEL DUODENO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56095', 'ESFINTEROPLASTIA ANAL POR INCONTINENCIA EN EL ADULTO CON TRASPLANTE MUSCULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56101', 'FISTULECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56103', 'FISTULOTOMIA ANAL Y/O PERIANAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56105', 'HEMICOLECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56112', 'INCISION Y DRENAJE DE ABSCESO ISQUIORRECTAL O INTRAMURAL CON FISTULECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56115', 'INCISION Y DRENAJE DE ABSCESO PERIANAL SUPERFICIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56116', 'INCISION Y DRENAJE DE ABSCESO SUB-MUCOSO RECTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56117', 'INCISION Y DRENAJE DE ABSCESO SUBMUCOSO RECTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56125', 'MIOMECTOMIA ANORRECTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56127', 'ENTERORRAFIAS MULTIPLES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56130', 'PLICATURA INTESTINAL (OPERACION DE NOBLE)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56133', 'RESECCION DE PROCIDENCIA RECTAL POR VIA ABDOMINAL Y PERINEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56134', 'PROCTOPEXIA COMBINADA CON RESECCION SIGMOIDEA POR VIA ABDOMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56139', 'PROCTOSIGMOIDECTOMIA CON COLOSTOMIA CON ABORDAJE PERINEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56140', 'PROCTOSIGMOIDOSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56147', 'PROTOSIGMOIDOSCOPIA MAS RESECCION DE POLIPO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56151', 'REPARACION DE RECTO PROLAPSADO POR INFILTRACION PERIRRECTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56153', 'RESECCION DE CONDUCTO ONFALOMENSENTERICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56159', 'RESECCION DE INTESTINO Y QUISTES POR PERITONITIS MECONIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56162', 'RESECCION DE POLIPO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56165', 'RESECCION DE TUMOR RECTAL POR PROTECTOMIA TRANS-SACRA O TRANS-COXIGEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56168', 'RESECCION DE UNA O MAS LESIONES DE INTESTINOS DELGADO O GRUESO SIN ANASTOMOSIS, ESTERIORIZACION O FISTULA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56171', 'RESECCION INTESTINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56180', 'SUTURA DE INTESTINO(ENTERORRAFIA), INTESTINO GRUESO O DELGADO, ULCERA PERFORADA, DIVERTICULO, HERIDA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56181', 'SUTURA DE LACERACION DE RECTO (PROCTORRAFIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56184', 'CONSTRUCCION DE ANO VIA ABDOMINAL Y PERINEAL COMBINADA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56186', 'YUNOSTOMIA PARA ALIMENTACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56187', 'YUNOSTOMIA TERMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('56190', 'APENDICECTOMIA LAPAROSCOPICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57001', 'ANASTOMOSIS URETERO-CALICIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57005', 'AUTO-TRANSPLANTE RENAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57006', 'BIOPSIA ABIERTA RIÑON TRANSPLANTADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57007', 'BIOPSIA ENDOSCOPICA DE RIÑON' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57009', 'BIOPSIA RENAL PERCUTANEA CON TROCAR O AGUJA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57012', 'CIERRE DE FISTULA NEFROVISCERAL INCLUYENDO REPARO DE VISCERA, VIA DE ACCESO ABDOMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57014', 'CISTECTOMIA (QUISTE UNICO EN RIQON)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57015', 'DRENAJE ABSCESO RENAL Y/O PERIRRENAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57017', 'HEMINEFRECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57018', 'LITOTRIPSIA EXTRACORPOREA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57019', 'LUMBOTOMIA EXPLORADORA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57020', 'NEFRECTOMIA AL DONANTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57024', 'NEFRECTOMIA PARCIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57026', 'NEFRECTOMIA RADICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57027', 'NEFRECTOMIA RADICAL POR LAPAROSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57029', 'NEFROLITOMIA PERCUTANEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57032', 'NEFRORRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57033', 'NEFROSTOMIA A CIELO ABIETO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57034', 'NEFROSTOMIA, NEFROTOMIA CON DRENAJE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57036', 'NEFROTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57037', 'NEFROURECTERETOMIA CON RESECCION DE SEGMENTO VESICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57038', 'NEFROURETERECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57040', 'PERFUCION RENAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57041', 'PIELITOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57045', 'PIELOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57046', 'PIELOPLASTIA POR LAPAROSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57048', 'PIELOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57054', 'RESECCION Y/O MARSUPIALIZACION QUISTE RENAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57057', 'SINFISTOTOMIA DE RIQON EN HERRADURA CON O SIN PROCEDIMIENTO PLASTICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('57058', 'TRANSPLANTE RENAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58002', 'CAMBIO DE TUBO DE URETEROSTOMIA PERCUTANEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58004', 'COLOCACION DE CATETER URETERAL DE AUTO-RETENCION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58005', 'EXPLORACION DEL URETER' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58006', 'EXTIRPACION DE URETEROCELE Y REIMPLANTACION DE URETER' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58010', 'MEATOMIA URETRAL ABIERTA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58022', 'URETEROENTEROSTOMIA CUATANEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58023', 'URETEROLISIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58026', 'URETEROLITOMIA A CIELO ABIERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58027', 'URETEROLITOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58029', 'URETERONEOANASTOMOSIS RIQON TRASPALNTADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58030', 'URETEROPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58032', 'URETERORRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('58034', 'URETEROSTOMIA CUTANEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59001', 'APENDICO-VESICOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59004', 'ASPIRACION VEJIGA CON TROCAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59006', 'BIOPSIA VESICAL A CIELO ABIERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59007', 'CAMBIO DE CATETER URINARIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59008', 'CAMBIO DE TUBO DE CISTOSTOMIA SIMPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59010', 'CERCLAJE CUELLO VESICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59011', 'CIERRE DE CISTOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59012', 'CIERRE VESICOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59014', 'CISTECTOMIA RADICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59016', 'CISTOGRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59024', 'CISTOPEXIA VAGINAL+COLPORRAFIA POSTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59025', 'CISTORRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59026', 'CISTOSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59027', 'CISTOSTOMIA POR PUNCION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59028', 'CISTOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59036', 'DIVERTICULECTOMIA VESICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59037', 'DRENAJE COLECCION PERIVESICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59039', 'EXENTERACION PILVICA ANTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59040', 'EXTRACCION DE CUERPO EXTRAÑO EN VEJIGA (CIELO ABIERTO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59041', 'INSTALACION VESICAL DE BCC O QUIMIOTERAPICOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59043', 'LINFADENECTOMIA RETROPERITONEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59049', 'RESECCION QUISTE O FISTULA URACO CON O SIN REPARO DE HERNIA UMBILICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59050', 'RESECCION ENDOSCOPIA CUELLO VESICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59052', 'RESECCION PERSISTENCIA DE URACO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59053', 'RESECCION-FULGURACION TUMOR VESICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59056', 'URETOCISTOPEXIA POR LAPAROSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('59057', 'URETROCISTOGRAFIA RETROGADA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60001', 'BIOPSIA LESION PERIURETRAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60004', 'CIERRE DE URETROSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60005', 'CORRECCION EPISPADIAS DISTAL AL ESFINTER EXTERNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60009', 'DRENAJE ABSCESO PERIURETRAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60011', 'DRENAJE DE ABSCESO PERIURETRAL PROFUNDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60013', 'EXTIRPACION DE DIVERTICULO URETRAL FEMENINO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60014', 'EXTIRPACION DE DIVERTICULO URETRAL MASCULINO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60015', 'EXTRACCION ENDOSCOPICA DE CUERPO EXTRAÑO URETRAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60018', 'INYECCION PERIURETRAL PARA MANEJO DE INCONTINENCIA URINARIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60019', 'MAGPI (MEATOTOMIA-GLANDULOPLASTIA-AVANZAMIENTO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60020', 'MARZUPIALIZACION DE DIVERTICULO URETRAL MASCULINO O FEMENINO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60028', 'RESECCION ENDOSCOPICA DE VALVAS URETRALES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60031', 'URETRECTOMIA SIMPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60033', 'URETROLITOTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60034', 'URETROPLASTIA COMBINADA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60036', 'URETROPLASTIA MATHIEU' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60038', 'URETROPLASTIA PERINEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60042', 'URETRORRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60043', 'URETRORRAFIA EN URETRA FEMENINA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60044', 'URETRORRAFIA EN URETRA PENEANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60047', 'URETROSTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60048', 'URETROSTOMIA PERINEAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('60049', 'URETROTOMIA INTERNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('61001', 'BIOPSIA PROSTATICA PERINEAL O TRANSRECTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('61002', 'CONTROL ENDOSCOPICO HEMORRAGIA PROSTATICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('61004', 'DRENAJE ABSCESO PROSTATICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('61008', 'LINFADENECTOMIA PELVICA TERAPEUTICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('61010', 'PROSTATECTOMIA CON LASER' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('61012', 'PROSTATECTOMIA TRANSURETRAL Y/O VAPORIZACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('61018', 'VESICULECTOMIA SEMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62001', 'ASPIRACION DE HIDROCELE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62003', 'BIOPSIA LESION CORDON ESPERMATICO O TUNICA VAGINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62006', 'CIRUGIA DE GENITALES AMBIGUOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62013', 'EXTRACCION CUERPO EXTRAÑO EN ESCROTO O TESTMCULO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62014', 'FIJACION TESTICULAR PROFILACTICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62015', 'FISTULECTOMIA ESCROTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62016', 'FULGURACION DE LESION ESCROTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62017', 'HIDROCELECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62018', 'IMPLANTACION DEL TESTICULO EN EL MUSLO POR DESTRUCCION DEL ESCROTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62019', 'IMPLANTE PROTESIS TESTICULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62020', 'INCISION Y/O DRENAJE DEL CORDSN ESPERMATICO,ESCROTO O TESTICULO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62021', 'LIBERACION DE TORSION TESTICULAR (INCLUYE PEXIA CONTRALATERAL) SIN ORQUIDECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62029', 'ORQUIDOPEXIA CON RECONSTRUCCION DE CANAL INGUINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62030', 'ORQUIDOPEXIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62035', 'ORQUIDORRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62036', 'PEXIA TRANSESCROTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62039', 'RESECCION DE HEMATOCELE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62041', 'RESECCION PARCIAL DE ESCROTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62042', 'RESECCION QUISTES SEBACEOS ESCROTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62043', 'RESECCION TOTAL DEL ESCROTO Y RECONSTRUCCION DE PLASTIAS CUTANEAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62045', 'SUTURA LESION ESCROTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('62046', 'VARICOCELECTOMIA UNI O BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('63001', 'BIOPSIA EPIDIDIMO Y/O DEFERENTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('63002', 'EPIDIDEMECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('63006', 'EXTIRPACION DE LESION EN EPIDIDIMO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('63008', 'RESECCION QUISTE DE EPIDIDIMO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64001', 'BIOPSIA DE PENE, INCLUYE BIOPSIA DE CUERPOS CARVERNOSOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64003', 'CORRECCION ANGULACION DEL PENEANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64013', 'EXTIRPACION DE NODULOS DE LA ENFERMEDAD DE PEYRONIE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64014', 'EXTIRPACION DE NODULOS DE LA ENFERMEDAD DE PEYRONIE CON INJERTOS DE PIEL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64019', 'INCISION Y DRENAJE ABSCESO PENEANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64032', 'CIRCUNCISION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64033', 'RECONSTRUCCION PENE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64035', 'REIMPLANTE DE PENE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64036', 'REMODELACION DE AMPUTACION PENEANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64040', 'RETIRO DE CUERPO EXTRAÑO EN PENE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('64041', 'RETIRO PROTESIS PENEANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65002', 'BIOPSIA DE MAMA POR PUNCION ASPIRACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65004', 'BIOPSIA DE SENO POR INCISION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65005', 'BIOPSIA POR ASPIRACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65006', 'BIOPSIA POR INCISION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65014', 'ESCISION BIOPSIA DE NODULO MAMARIO UNICO O MULTIPLE UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65019', 'EXTRACCION DE CUERPO EXTRAÑO EN MAMA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65020', 'EXTRACCION DE IMPLANTE DE MAMA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65026', 'MAMOPLASTIA DE AUMENTO (PROTESIS) BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65027', 'MAMOPLASTIA DE REDUCCION BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65028', 'MAMOPLASTIA ONCOLOGICA BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65029', 'MAMOPLASTIA ONCOLOGICA UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65032', 'MASTECTOMIA RADICAL UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65033', 'MASTECTOMIA RADICAL MODIFICADA O SIMPLE AMPLIADA (CON EXTIRPACION DE GANGLIOS LINFATICOS REGIONALES)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65034', 'MASTECTOMIA SIMPLE, COMPLETA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65035', 'MASTECTOMIA SUBCUTANEA (LESION BENIGNA) UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65038', 'MASTOTOMIA (INCLUYE DRENAJE DE LA MAMA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65041', 'RECONSTRUCCION AREOLA PEZON UNILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65042', 'RECONSTRUCCION DE LA MAMA CON COLGAJO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('65043', 'RECONSTRUCCION DE LA MAMA CON PROTESIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('66002', 'CISTECTOMIA (QUISTE DE OVARIO O INTRALIGAMENTARIO UNILATERAL POR LAPAROSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('66006', 'FIMBRIOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('66010', 'OOFERECTOMIA LAPAROSCOPICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('66011', 'OOFORECTOMIA, PARCIAL O TOTAL, UNI O BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('66012', 'OOFORECTOMIA, PARCIAL O TOTAL, UNI O BILATERAL CON OMENTECTOMIA TOTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('66013', 'OOFOROPEXIA (ADICIONAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('66014', 'OOFOROPEXIA, PROCEDIMEINTO SEPARADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('66015', 'OOFORORRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('67005', 'SALPINGECTOMIA UNILATERAL POR LAPAROSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('67006', 'SALPINGECTOMIA, COMPLETA O PARCIAL UNI O BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('67007', 'SALPINGOLISIS UNILATERAL POR LAPAROSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('67010', 'SALPINGOPLASTIA POR LAPAROSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('67012', 'SALPINGOOFORECTOMIA UNI O BILATERAL POR LAPAROSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68009', 'BIOPSIA DE CERVIX, CIRCUFERENCIAL (CONIZACION), CON O SIN DILATACION O CURETAJE CON O SIN REPARACION TIPO STURMDORFF' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68010', 'BIOPSIA DE ENDOMETRIO, REALIZADA EN CONSULTORIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68012', 'CAUTERIZACION DEL CERVIX, ELECTRO, RADIO, TERMO, LASER O CRIO, INICIAL O REPETIDA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68017', 'CESAREA, EXTRAPERITONEAL, INCLUYENDO CONTROL POST-OPERATORIO INTRA-HOSPITALARIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68018', 'CESAREA, SEGMENTARIA TRANVERSAL, O CORPORAL(CLASICA) INCLUYENDO CONTROL POST-OPEREATORIO INTRA-HOSPITALARIO(PROCEDIMIENTO SEPARADO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68021', 'COLPOPEXIA POR LAPAROSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68022', 'COLPOSCOPIA CULDOSCOPIA (VAGINOSCOPIA)PROCEDIMIENTO SEPARADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68030', 'HISTERECTOMÍA TOTAL CON LINFADENECTOMIA PELVICA RADICAL BILATERAL INCLUYE LINFADENECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68031', 'ESCICION DE MUÑÓN CERVICAL CON REPARACION DEL PISO PELVICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68034', 'DILATACION Y CURETAJE DEL MUÑON CERVICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68038', 'ESCISION DEL MUÑON CERVICAL, VIA VAGINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68040', 'EXTRACCION HISTEROSCOPICA DE CUERPOS EXTRAÑOS INTRAUTERINOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68041', 'HISTEROSCOPIA DIAGNOSTICA CON BIOPSIA CON O SIN CURETAJE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68044', 'HISTERECTOMIA ADBOMINAL TOTAL, C/S REMOCION DE TROMPA(S), CON O SIN REMOCION DE OVARIO(S)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68045', 'HISTERECTOMIA CON COLPECTOMIA TOTAL O PARCIAL (TIPO SHAUTA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68051', 'HISTERECTOMIA TOTAL, AMPLIADA CANCER DEL CERVIX, CON LIFADECTOMIA PELVICA RADICALBILATERAL(OPERACION TIPO WERTHEIM)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68053', 'HISTERECTOMIA VAGINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68054', 'HISTEROPEXIA(SUSPENSION UTERINA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68055', 'HISTEROPLASTIA REPARACION DE ANOMALIA UTERINA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68056', 'HISTERORRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68061', 'HISTEROTOMIA, ABDOMINAL PARA REMOCION DE MOLA HIDATIDIFORME' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68062', 'HISTERRORAFIA POR RUPTURA UTERINA (PROCEDIMIENTO SEPARADO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68070', 'MIOMECTOMIA UNI O MULTIPLE CUALQUIER VIA (ABDOMINAL O VAGINAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68071', 'MIOMECTOMIA POR LAPAROSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68075', 'AMNIOCENTESIS PARA DRENAJES DE POLIHIDRAMNIOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68081', 'RESECCION HISTEROSCOPICA DE POLIPOS INTRAUTERINOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68084', 'SECCION DE LIGAMENTOS UTERO-SACROS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68087', 'SUSPENCION UTERINA CON SIMPATECTOMIA PRESACRA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68088', 'SUSPENCION UTERINA, C/S ACORTAMIENTO DE LIGAMENTOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('68092', 'TRAQUELORRAFIA, REPARACION PLASTICA DEL CERVIX UTERINO, VIA VAGINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69003', 'CIERRE DE FISTULA URETRO-VAGINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69004', 'CIERRE DE FISTULA VESICO-VAGINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69006', 'COLPOCENTESIS (PUNCION DOUGLAS) PROCEDIMIENTO SEPARADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69008', 'COLPOPEXIA VIA ABDOMINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69009', 'COLPOPEXIA VIA VAGINAL (FIJACION A MUQONES RESIDUALES)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69010', 'COLPORRAFIA ANTERIOR Y POSTERIOR COMBINADAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69012', 'COLPORRAFIA ANTERIOR, REPARACION DE DE CISTOCELE (PROCED SEPARADO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69013', 'COLPORRAFIA POSTERIOR, REPARACION DE RECTOCELE CON O SIN PERINEORRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69015', 'COLPOTOMIA CON DRENAJE DE ABSCESO O HEMATOMA DE CUPULA VAGINAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69016', 'COLPOTOMIA CON DRENAJE DE ABSCESO PELVICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69017', 'COLPOTOMIA CON EXPLORACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69021', 'CORRECCION DE SENO UROGENITAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69029', 'REPARCION DE DESGARRO VAGINAL GRADO 1 (MUCOSA VAGINA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69031', 'REPARCION DE DESGARRO VAGINALGRADO 3 (RECTOVAGINAL CON COMPROMISO DEL ESFINTER DEL ANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69032', 'REPARCION DE DESGARRO VAGINALGRADO 4 (ESTALLIDO DE VAGINA CON O SIN EVISCERACION)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69033', 'HIMENORRAFIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69035', 'OBLITERACION COMPLETA POR ESCISION DE VAGINA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69038', 'RECONSTRUCCION DE VAGINA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69039', 'REPARACION DE ENTEROCELE VIA ABDOMINAL (PROCEDIMIENTOS SEPARADO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('69045', 'VAGINOPLASTIA POR ANOMALIA CONGENITA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70001', 'BIOPSIA CON COLPOSCOPIA (VULVOSCOPIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70002', 'BIOPSIA DE PERINE, INCLUYE CAUTERIO Y/O SUTURA (PROCEDIMIENTO SEPARADO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70003', 'BIOPSIA DE VULVA(PROCEDIMIENTO SEPARADO Y UNICO EN CONSULTORIO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70004', 'BIOPSIA ESCISIONAL DE VULVA (SALAS DE CIRUGIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70007', 'CORRECCION DE DESGARRO EN VULVA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70010', 'DRENAJE DE ABSCESO DE BARTOLINO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70016', 'ESCISION DE LA GLANDULA DE SKENE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70017', 'ESCISION DE LA GLANDULA O QUISTE DE BARTHOLINO (BARTHOLINECTOMIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70019', 'HIMENECTOMIA PARCIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70022', 'INCISION Y DRENAJE ABSCESO DE VULVA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70028', 'MARSUPIALIZACION DE QUISTE O ABSCESO DE LA GLANDULA DE BARTHOLINO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70030', 'PERINEOPLASTIA POR HERIDA CAUSADA POR TRAUMA RECIENTE (POSTCOITO, ACCIDENTE)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70031', 'PERINEOPLASTIA, REPARACION DEL PERINE NO OBSTETRICO (PROCEDIMIENTO SEPARADO) DESGARRO PERINEAL ANTIGUO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70035', 'VULVECTOMIA RADICAL CON LINFADENECTOMIA INGUINOFEMORAL BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70040', 'VULVECTOMIA SIMPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('70041', 'VULVECTOMIA TOTAL, PIEL Y TEJIDO SUCUTANEO, BILATERAL CON INJERTO DE PIEL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71001', 'ACROMIOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71004', 'AMPUTACION DEL BRAZO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71006', 'APLICACION DE ESPICA EN HOMBRO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71007', 'APLICACION DE FERULA BRAZO (PINZA DE AZUCAR)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71009', 'APLICACION DE YESO TIPO VELPEAU' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71010', 'ARTRODESIS DE HOMBRO ARTROSCOPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71011', 'ARTRODESIS DEL HOMBRO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71017', 'ARTROTOMIA, EXPLORACION, SINOVECTOMIA, DRENAJE O REMOCION CUERPO EXTRAÑO, ARTICULACION GLENOHUMERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71018', 'ATROTOMIA, EXPLORACION, SINOVECTOMIA, DRENAJE O REMOCION CUERPO EXTRAÑO, ARTICULACION ESTERNOCLAVICULAR Y/O ACROMIO- CLAVICULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71021', 'BRIDA ACROMIO-HUMERAL, RESECCION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71024', 'CAPSULORRAFIA TIPO BANKART PARA LUXACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71045', 'EXCISION DEPOSITOS CALCAREOS, HOMBRO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71046', 'EXTRACCION DE CUERPOS LIBRES INTRA-ARTICULARES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71052', 'INMIVILIZACION POR ESGUINCE GRADO 2 DE HOMBRO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71075', 'OSTEOTOMIA DE CLAVICULA CON O SIN FIJACION INTERNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71076', 'OSTEOTOMIA DE HUMERO CON O SIN FIJACION INTERNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71077', 'OSTEOTOMIA DE HUMERO MULTIPLE (SOFFIELD) + CLAVO INTRAMEDULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71078', 'OSTEOTOMIA PARCIAL DE ESCAPULA (GLENOIDES)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71085', 'REDUCCION CERRADA DE FRACTURA TERCIO MEDIO DE HUMERO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71086', 'REDUCCION CERRADA, TRACCION ESQUELETICA DE FRACTURA DE ESCAPULA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71087', 'REDUCCION CON ANESTESIA DE LUXOFRACTURA ARTICULACION ESCAPULO-HUMERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71088', 'REDUCCION CON ANESTESIA GENERAL LUXACIONARTICULACION ESCAPULO-HUMERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71092', 'REDUCCION MAS INMOVILIZACION FRACTURA CLAVICULA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71093', 'REDUCCION MAS INMOVILIZACION FRACTURA CONDILO-TROCLEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71115', 'REIMPLANTE DE MIEMBRO SUPERIOR A NIVEL DEL BRAZO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71120', 'REMOCION EXOSTOSIS ARTROSCOPIA DE HOMBRO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71124', 'RESECCION DE LA CABEZA HUMERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71126', 'RESECCION O CURETAJE DE QUISTE O TUMOR BENIGNO DE CLAVICULA O ESCAPULA (CON AUTO INJERTO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71130', 'RESECCION RADICAL DE CLAVICULA POR TUMOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71132', 'RESECCION RADICAL DE TUMOR (MALIGNO) TEJIDOS BLANDOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71133', 'RESECCION RADICAL HUMERO PROXIMAL POR TUMOR CON AUTO INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71134', 'RESECCION RADICAL HUMERO PROXIMAL POR TUMOR CON REEMPLAZO PROTESICO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71135', 'RESECCION RADICAL HUMERO PROXIMAL POR TUMOR SIN INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71139', 'SECUESTRECTOMIA DE CLAVICULA POR OSTEOMIELITIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71140', 'SECUESTRECTOMIA DE OMOPLATO POR OSTEOMIELITIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71142', 'SINOVECTOMIA PARCIAL ARTROCOPIA DE HOMBRO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71143', 'SINOVECTOMIA TOTAL ARTROSCOPIA DE HOMBRO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71144', 'SUTURA DEL MANGUITO ROTADOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71145', 'SUTURA TENDON BICIPITAL (TENODESIS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71146', 'TENODESIS, RUPTURA PORCION LARGA DEL BICEPS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71149', 'TORTICOLIS CONGENITA, DIVISION ESTERNOCLEIDOMASTOIDEO, ABIERTA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('71150', 'TRACCION ESQUELETICA O CUTANEA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72001', 'ALARGAMIENTO DE CUBITO O RADIO CON AUTOINJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72002', 'ALARGAMIENTO DE CUBITO Y RADIO CON AUTOINJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72010', 'AMPUTACION DE ANTEBRAZO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72016', 'APLICACION YESO ANTEBRAQUIO-METACARPIANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72018', 'ARTRODESIS DEL CODO CON INJERTO (INCLUYE TOMA DEL INJERTO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72023', 'ARTROPLASTIA DE CODO CON REEMPLAZO PROTESICO HUMERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72026', 'ARTROTOMIA DE CODO CON ESCISION CAPSULAR PARA RELAJACION CAPSULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72036', 'DESARTICULACION DE CODO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72037', 'ARTROSCOPIA DIAGNOSTICA DE CODO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72044', 'EPIFISIODESIS RADIO O CUBITO DISTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72045', 'EPIFISIODESIS RADIO Y CUBITO DISTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72080', 'OSTEOTOMIA DE CUBITO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72082', 'OSTEOTOMIA DE RADIO, DISTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72090', 'REDUCCION ABIERTA DE LUXACION INVETERADA DE CODO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72092', 'REDUCCION BAG LUXACCION DE CODO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72095', 'REDUCCION BAG FRACTURA CUBITO Y RADIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72099', 'REDUCCION E INMOVILIZACION TERCIO MEDIO (DIAFISIS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72101', 'REDUCCION E INMOVILIZACION DISTAL (LUXOFRACTURA DE GALEAZZI)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72103', 'REDUCCION E INMOVILIZACION TERCIO DISTAL (COLLES, SMITH, BARTON, DESLIZAMIENTO EPIFISIARIO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72104', 'REDUCCION E INMOVILIZACION NIÑOS TERCIO MEDIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72125', 'REMOCION CUERPO EXTRAÑO PROFUNDO, BRAZO O CODO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72126', 'REMOCION CUERPO EXTRAÑO SUBCUTANEO, BRAZO O CODO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72127', 'REMOCION CUERPOS LIBRES ARTROSCOPIA DE CODO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72128', 'REMOCION PLICAS ARTROSCOPIA DE CODO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72131', 'REPARO DE LIGAMENTO MEDIAL O LATERAL CODO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72134', 'RESECCION PARCIAL (SAUCERIZACION, DIAFISECTOMIA) DE HUESO (PARA OSTEOMIELITIS):CUBITO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72135', 'RESECCION PARCIAL(SAUCERIZACION,DIAFISECTOMIA)DE HUESO (PARA OSTEOMIELITIS):RADIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72136', 'RESECCION RADICAL DE TUMOR (MALIGNO) DE TEJIDOS BLANDOS, BRAZO O CODO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72141', 'RESECCION RADICAL POR TUMOR DE RADIO O CUBITO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72155', 'SINOVECTOMIA PARCIAL ARTROSCOPIA DE CODO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('72156', 'SINOVECTOMIA TOTAL ARTROSCOPIA DE CODO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73003', 'ALARGAMIENTO DE TENDON (Z PLASTIA, OTRO) EXTENSOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73008', 'AMPUTACION DE LA MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73009', 'AMPUTACION DEDO MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73016', 'ARTOPLASTIA INTERFALANGICA (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73017', 'ARTOPLASTIA INTERFALANGICA (UNA A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73020', 'ARTOPLASTIA PUÑO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73021', 'ARTOPLASTIA TRAPECIO-METACARPIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73022', 'ARTRODESIS (UNA) INTERFALANGICAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73023', 'ARTRODESIS CARPOMETACARPIAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73024', 'ARTRODESIS DE MUQECA (RADIOCARPIANA Y CUBITOCARPIANA SIN INJERTO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73027', 'ARTRODESIS DE MUQECA CON INJERTO POR DESLIZAMIENTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73031', 'ARTRODESIS INTERCARPIANA, CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73032', 'ARTRODESIS INTERCARPIANA, SIN INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73033', 'ARTRODESIS INTERFALANGICAS CON INJERTO OSEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73034', 'ARTRODESIS METACARPO-FALANGICAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73054', 'ARTROTOMIA DE MUQECA CON EXPLORACION ARTICULAR, CON O SIN BIOPSIA, CON O SIN REMOCION DE CUERPO EXTRAÑO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73063', 'CAPSULODESIS METACARPO-FALANGICA (ZANCOLI) UNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73065', 'CAPSULORRAFIA ARTICULACIONES (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73069', 'CAPSULOTOMIA INTERFALANGICAS (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73071', 'CAPSULOTOMIA METACARPOFALANGICAS (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73073', 'CARPECTOMIA (TRES O MAS) HUESOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73078', 'SINOVECTOMIA CARPO-METACARPIANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73093', 'CONDROPLASTIA DE ABRASION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73094', 'CORRECCION POLIDACTILIA (DEDO SUPERNUMERARIO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73095', 'CORRECCION QUIRURGICA CAMPTODACTILIA (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73099', 'CORRECCION QUIRURGICA CLINODACTILIA (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73102', 'CORRECCION QUIRURGICA DEDO EN CUELLO DE CISNE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73105', 'CORRECCION SINDACTILIA ( DOS ESPACIOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73108', 'BIOPSIA DE HUESO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73116', 'DEBRIDAMIENTO DE FIBROCARTILAGO TRIANGULAR O EXTRACCION CUERPO LIBRE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73117', 'DEDO EN BOTONERA O MARTILLO , TRATAMIENTO CERRADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73119', 'DEDO EN GATILLO (RESORTE), LIBERACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73120', 'DEFORMIDAD DE MADELUNG' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73121', 'TRANSFERENCIA MUSCULO TENDINOSA DEL PRONADOR REDONDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73122', 'DESARTICULACION DE MUQECA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73124', 'ARTROSCOPIA DIAGNOSTICA DE MUÑECA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73125', 'ARTROSCOPIA DIAGNOSTICA DE FALANGES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73129', 'DRENAJE,CURETAJE,SECUESTRECTOMIA,FALANGES (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73130', 'DRENAJE,CURETAJE,SECUESTRECTOMIA,FALANGES (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73131', 'DRENAJE,CURETAJE,SECUESTRECTOMIA,HUESOS CARPO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73132', 'DRENAJE,CURETAJE,SECUESTRECTOMIA,METACARPIANOS (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73133', 'DRENAJE,CURETAJE,SECUESTRECTOMIA,METACARPIANOS (UNO A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73146', 'EXPLORACION PARA REMOCION CUERPO EXTRAÑO PROFUNDO, ANTEBRAZO O MUÑECA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73149', 'EXTRACCION CUERPO EXTRAÑO EN ANTEBRAZO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73150', 'EXTRACCION CUERPO EXTRAÑO EN BRAZO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73151', 'EXTRACCION CUERPO EXTRAÑO EN MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73152', 'EXTRACCION DE CUERPOS LIBRES INTRA-ARTICULARES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73158', 'FASCIOTOMIA DESCOMPRESIVA, ANTEBRAZO Y/O MUQECA, COMPARTIMIENTO FLEXOR O EXTENSOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73159', 'FASCIOTOMIA MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73160', 'FIJACION CON TUTOR EXTERNO - ARTRODESIS DEL CARPO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73162', 'FIJACION CON TUTOR EXTERNOOSTEOTOMIAS EN LA MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73163', 'FIJACION INTERNA DE FRACTURAS Y INESTABILIDADES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73165', 'GANGLION (QUISTE SINOVIAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73169', 'HEMIDIAFISECTOMIA FALANGES (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73173', 'INJERTO DE EXTENSOR EN DEDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73174', 'INJERTO DE FLEXOR EN DEDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73182', 'INJERTO LECHO UNGUEAL VASCULARIZADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73183', 'INJERTO OSEO EN ESCAFOIDES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73184', 'INJERTO OSEO EN FALANGES (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73186', 'INJERTO OSEO EN METACARPIOS (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73188', 'INJERTO OSEO VASCULARIZADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73190', 'INJERTOS OSEOS EN HUESOS CARPO (EXCEPTO ESCAFOIDES)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73191', 'INMOVILIZACION LUXO FRACTURA DE ARTICULACION RADIO CARPIANA E INTERCARPIANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73192', 'INMOVILIZACION CON YESO DE ESGUINCE DE ARTICULACION RADIO CARPIANA E INTERCARPIANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73203', 'SINOVECTOMIAS ARTICULARES INTERFALANGICA CADA DEDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73211', 'LIGAMENTORRAFIA O REINSERCION LIGAMENTOS (UNA A DOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73212', 'MACRODACTILIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73218', 'METACARPESTOMIA (TRES O MAS) HUESOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73220', 'SINOVECTOMIA METACARPO-FALANGICA LIBERACION INTRINSECOS CADA DEDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73221', 'MIORRAFIA EXTENSORES MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73222', 'MIORRAFIA FLEXORES MANO (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73224', 'MIOTOMIA MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73225', 'SINOVECTOMIA ARTICULAR MUÑECA (RADIOCARPIANA Y/O RADIO CUBITAL DISTAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73227', 'OSTEOSINTESIS CON CLAVO/ALAMBRE - OSTEOTOMIAS EN LA MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73228', 'OSTEOSINTESIS CON PLACA - OSTEOTOMIAS EN LA MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73229', 'OSTEOSINTESIS CON TORNILLO - OSTEOTOMIAS EN LA MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73230', 'OSTEOTOMIA EN FALANGE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73234', 'PARA CORREGIR 1 O 2 DEDOS EN GARRA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73235', 'PARA CORREGIR 3 O MAS DEDOS EN GARRA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73236', 'POLIDACTILIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73249', 'RECONSTRUCCION DE POLEAS PARA TENDON FLEXOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73250', 'RECONSTRUCCION DE POLEAS PARA TENDON FLEXOR CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73255', 'REDUCCION ABIERTA DE LUXACION RADIOCUBITAL DISTAL, CRONICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73256', 'REDUCCION ABIERTA FRACTURA INTRA-ARTICULAR MANO (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73261', 'REDUCCION ABIERTA FRACTURAS METACARPIOS (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73263', 'REDUCCION ABIERTA LUXACION CARPIANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73265', 'REDUCCION ABIERTA LUXACION INTERFALANGICA (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73269', 'REDUCCION ABIERTA O PERCUTANEA FRACTURA LUXO-FRACTURA DE BENNET' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73270', 'REDUCCION BAG FRACTURA METACARPIANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73271', 'REDUCCION CERRADA CON FIJACION PERCUTANEA DE LUXACIONARTICULACIONES CARPO-METACARPIANA Y METACARPOFALANGICAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73273', 'REDUCCION CERRADA FRACTURA INTRA-ARTICULAR MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73276', 'REDUCCION CERRADA LUXACION CARPIANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73278', 'REDUCCION CERRADA LUXACION INTERFALANGICA (TRES MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73280', 'REDUCCION CERRADA LUXACION METACARPOFALANGICA (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73282', 'REDUCCION CERRADA LUXO-FRACTURA DE BENNET' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73283', 'REDUCCION CERRADA MAS FIJACION PERCUTANEA - FX METACARPIANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73284', 'REDUCCION CERRADA MAS FIJACION PERCUTANEA (ESCAFOIDEA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73285', 'REDUCCION CERRADA MAS INMOVILIZACION - FX HUESOS DEL CARPO (EXCEPTO ESCAFOIDES)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73286', 'REDUCCION CERRADA MAS INMOVILIZACION - LUXAXION RADIO CARPIANA O INTERCARPIANA (UNO O MAS HUESOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73298', 'REDUCCION MAS FIJACION CON TUTOR LUXO FRACTURA Y ESGUINCE ARTICULACIONES CARPO METACARPIANA Y METACARPOFALANGICAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73303', 'REDUCCION MAS FIJACION PERCUTANEA FRACTURA FALANGES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73305', 'REDUCCION MAS INMOVILIZACION LUXO FRACTURA Y ESGUINCE ARTICULACIONES CARPO METACARPIANA Y METACARPOFALANGICAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73310', 'REDUCCION PINES PERCUTANEOS LUXOFRACTURAS ARTICULACION RADIO CARPIANA E INTERCARPIANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73341', 'REIMPLANTE DE DOS O MAS DEDOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73342', 'REIMPLANTE DE LA MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73343', 'REIMPLANTE DE MANO (NO VIABLE) (MICROCIRUGIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73344', 'REIMPLANTE DE UN DEDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73356', 'REPARACION DE DEFORMIDADES CONGENITAS DE LA MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73357', 'REPARACION DE LIGAMENTO TRIANGULAR RADIOCUBITAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73373', 'RESECCION CABEZA DE FALANGE DE PIE (UNA O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73375', 'RESECCION DE FALANGE O METACARPIANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73377', 'RESECCION DE TUMOR BENIGNO DE FASCIA, MUSCULO, TENDON O SINOVIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73382', 'RESECCION QUISTE VAINA TENDINOSA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73388', 'REVISION DE PROTESIS TOTAL DE MUQECA, INCLUYE REM DEL IMPLANTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73390', 'REVISION Y/O RECONSTRUCCION MUQSN DE AMPUTACION DEDOS MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73399', 'SINDACTILIA COMPLEJA-REPARACION ( COMPROMETE UÑA Y HUESO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73401', 'SINOSTOSIS RADIOCUBITAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73402', 'SINOVECTOMIA ARTROSCOPIA DE FALANGES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73406', 'SINOVECTOMIA PARCIAL ARTROSCOPIA DE MUÑECA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73407', 'SINOVECTOMIA TOTAL ARTROSCOPIA DE MUÑECA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73413', 'TENODESIS DE EXTENSORES DE LOS DEDOS EN LA MUÑECA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73419', 'TENOLISIS DE FLEXOR EN DEDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73420', 'TENOLISIS EXTENSOR MANO O DEDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73424', 'TENOLISIS FLEXORES MANO (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73427', 'TENORRAFIA DE EXTENSOR EN DEDOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73429', 'TENORRAFIA DE FLEXOR EN DEDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73432', 'TENORRAFIA EXTENSORES MANO (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73434', 'TENORRAFIA FLEXORES ANTEBRAZO (CINCO O MAS) CON NEURORRAFIAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73437', 'TENORRAFIA FLEXORES MANO (CINCO O MAS) CON NEURORRAFIAS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73440', 'TENOSINOVECTOMIA DE FLEXOR O EXTENSOR EN ANTEBRAZO PUÑO O MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73443', 'TENOSINOVECTOMIA FLEXORES MANO (TRES O MAS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73447', 'TENOTOMIA MANO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73449', 'TRANSFER TENDINOSA, FLEXOR O EXTENSOR, ANTEBRAZO Y/O MUQECA, C/U' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73464', 'APLICACION DE FERULA CORTA ANTEBRAQUI-METACARPIANA (DINAMICA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('73465', 'APLICACION DE FERULA CORTA ANTEBRAQUIO-METACARPIANA (ESTATICA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74013', 'ARTODESIS SACROILIACA UN LADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74014', 'ARTRODESIS DE CADERA (INCLUYE INJERTO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74019', 'ARTROPLASTIA - CONVERSION DE ARTRODESIS O REEMPLAZO TOTAL DE CADERA CON O SIN AUTO O ALOINJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74026', 'ARTROTOMIA DE ARTICULACION SACRAILIACA PARA BIOPSIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74037', 'CONDROPLASTIA POR ABRASION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74039', 'DESARTICULACION DE CADERA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74047', 'ARTROSCOPIA DIAGNOSTICA DE CADERA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74060', 'EXTRACCION PROTESIS PARCIAL DE CADERA (PROCEDIMIENTO SEPARADO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74065', 'HEMI-HEMIPELVECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74070', 'LIBERACION MUSCULAR DE CADERA (COLGANTE)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74073', 'OSTEOTOMIA BILATERAL PELVICA PARA MALFORMACION CONGENITA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74081', 'OSTEOTOMIA Y TRANSFERENCIA DEL TROCANTER MAYOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74082', 'PUNCION ARTICULAR DE CADERA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74083', 'REDUCCION ABIERTA LUXACION PROTESIS DE CADERA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74085', 'REDUCCION CERRADA LUXACION PROTESIS DE CADERA, CON ANESTESIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74118', 'SINOVECTOMIA PARCIAL ARTROSCOPIA DE CADERA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74120', 'TENOTOMIA DE ABDUCTORES, ABIERTA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74134', 'TRATAMIENTO BAG Y MANIPULACION LUXACION ARTICULACION SACROILIACA Y SINFISIS DEL PUBIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('74135', 'TRATAMIENTO CERRADO LUXACION O FRACTURA DEL COCCIX' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75008', 'ACORTAMIENTO DE FEMUR-OSTEOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75009', 'ALARGAMIENTO DE FEMUR-OSTEOPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75014', 'AMPUTACION ABIERTA EN GUILLOTINA O CON COLGAJOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75020', 'APLICACION DE INMOVILIZADOR EXTERNO EN FEMUR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75021', 'ARTRODESIS DE RODILLA, CUALQUIER TECNICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75035', 'ARTROTOMIA DE RODILLA CON EXPLORACION ARTICULAR, CON O SIN BIOPSIA, CON O SIN EXTRACCION CUERPOS LIBRES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75040', 'AVANCE DE PES ANSERINUS (SLOCUM)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75044', 'CONDROPLASTIA DE ABRASION MAS OSTEOTOMIA TIBIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75046', 'CUADRICEPLASTIA (THOMPSON, BENNET)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75047', 'DESARTICULACION DE RODILLA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75048', 'ARTROSCOPIA DIAGNOSTICA DE RODILLA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75051', 'EPIFISIODESIS CON INJERTO O GRAPA, FEMUR DISTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75055', 'ESCISION DE QUISTE SINOVIAL AREA POPLITEA(QUISTE DE BAKER)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75058', 'EXTRACCION CUERPO EXTRAÑO PROFUNDO, MUSLO O RODILLA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75062', 'EXTRACCION PROTESIS TOTAL DE RODILLA Y CEMENTO,CON O SIN COLOCACION DE ESPACIADOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75068', 'FRACTURAS OSTEOCONDRALES O DE LA ESPINA TIBIAL, OSTEOSINTESIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75069', 'FRACTURAS TERCIO PROXIMAL DE TIBIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75073', 'INMOVILIZACION EPIFISIS ABIERTAS (DESLIZAMIENTO EPIFISIARIO) TERCIO DISTAL FEMUR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75076', 'INMOVILIZACION CON FERULA ESGUINCE - RUPTURAS (LIGAMENTO, MENISCOPATIA Y TENDON)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75077', 'INMOVILIZACION CON YESO ESGUINCE - RUPTURAS (LIGAMENTO, MENISCOPATIA Y TENDON)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75082', 'LIBERACION DE ADHERENCIAS ARTROSCOPIA DE RODILLA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75084', 'MENISCECTOMIA MEDIAL O LATERAL INCLUYE CONDROPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75097', 'OSTEOTOMIA ANTERIORIZADORA Y/O MEDIALIZADORA, TUBERCULO TIBIAL (MAQUET, FULKERSON)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75098', 'OSTEOTOMIA DE FEMUR (DIAFISIS O SUPRACONDILEA), CON FIJACION' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75100', 'OSTEOTOMIA MULTIPLE (SOFFIELD)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75101', 'OSTEOTOMIA PROXIMAL TIBIA CON O SIN OSTEOTOMIA DE PERONE (CORRECCION GENU VARO O VALGO), ANTES DE CIERRE DE EPIFISIS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75103', 'PATELECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75108', 'RECONSTRUCCION DE LIGAMENTO CRUZADO ANTERIOR CON INJERTO AUTOLOGO O ALOINJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75109', 'RECONSTRUCCION DE LIGAMENTO CRUZADO POSTERIOR CON INJERTO AUTOLOGO O LATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75115', 'RECONSTRUCCION SECUNDARIA DE LCA CON AUTO O ALOINJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75119', 'RECONSTRUCCION SECUNDARIA DEL TENDON DEL CUADRICEPS CON INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75121', 'REDUCCION ABIERTA Y FIJACION INTERNA TROCANTER MAYOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75122', 'REDUCCION ABIERTA Y FIJACION INTERNA (CLAVO - PLACA) INTERTROCANTERICA, PERTROTANQUERICA, SUBTROCANTERICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75123', 'REDUCCION CERRADA MAS INMOVILIZACION EPIFISIS CERRADAS (SUPRACONDILEO O TRANSCONDILEO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75124', 'REDUCCION CERRADA SUPRA O TRANSCONDILEA (CON O SIN COMPROMISO INTERCONDILEO Y FIJACION DE PINES PERCUTANEOS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75126', 'REDUCCION E INMOVILIZACION TERCIO MEDIO (DIAFISIS)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75127', 'REDUCCION E INMOVILIZACION TERCIO DISTAL (DESLIZAMIENTO EPIFISIARIO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75128', 'REDUCCION E INMOVILIZACION CONDILOS (MEDIAL O LATERAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75129', 'REDUCCION E INMOVILIZACION (CON ANESTESIA) LUXACION RODILLA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75150', 'REMODELACION MENISCO ROTO (PICO DE LORO) INCLUYE CONDROPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75158', 'RESECCION PLICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75162', 'SINOVECTOMIA PARCIAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75163', 'SINOVECTOMIA TOTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75165', 'SUTURA MENISCO MEDIAL O LATERAL INCLUYE CONDROPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75166', 'SUTURA MENISCO MEDIAL Y LATERAL INCLUYE CONDROPLASTIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('75168', 'TENOTOMIA ABIERTA DE ISQUIOTIBIALES, MULTIPLE, BILATERAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76005', 'REIMPLANTE DE PIE PARCIAL POR AMPUTACION INCOMPLETA (EXTREMIDAD NO VIABLE)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76007', 'REIMPLANTE DE PIERNA PARCIAL POR AMPUTACION INCOMPLETA (EXTREMIDAD NO VIABLE)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76010', 'ALARGAMIENTO DE TENDON DE AQUILES ABIERTO SIMPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76013', 'ALARGAMIENTO DE TIBIA PROGRESIVO, INCLUYE OSTEOTOMIA Y COLOCACION DE ALARGADOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76014', 'ALARGAMIENTO DE TIBIA Y PERONE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76018', 'AMPUTACION ABIERTA PIERNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76019', 'AMPUTACION ARTEJO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76024', 'AMPUTACION DE TOBILLO (TIPO SYME O PIROGOFF)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76030', 'AMPUTACION TRANSMALEOLAR (SYME, PIROGOFF)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76031', 'AMPUTACION TRANSMETATARSIANA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76032', 'AMPUTACION, PIE, MEDIO TARSIANA (CHOPART)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76033', 'ARTRODESIS - ARTROSCOPIA DE TOBILLO Y PIE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76034', 'ARTRODESIS ARTICULACION INTER-FALANGICA HALLUX' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76036', 'ARTRODESIS DE TOBILLO (CUALQUIER METODO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76039', 'ARTRODESIS MEDIOTARSIANA O TARSOMETATARSIANA, MULTIPLE O TRANSVERSA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76043', 'ARTRODESIS SUBTALAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76047', 'ARTROPLASTIA DE TOBILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76054', 'ARTROTOMIA DE TOBILLO, CON EXPLORACION ARTICULAR, CON O SIN BIOPSIA, CON O SIN EXTRACCION CUERPO LIBRE O EXTRAÑO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76055', 'ARTROTOMIA DE TOBILLO, POR INFECCCION CON EXPLORACION, DRENAJE Y REMOCION CUERPO EXTRAÑO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76059', 'ASTRAGALECTOMIA (TALECTOMIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76061', 'BIOPSIA, PIERNA O TOBILLO, DE TEJIDOS BLANDOS, PROFUNDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76070', 'CONDILECTOMIA, OSTECTOMIA PARCIAL O EXOSTECTOMIA SIMPLE, CABEZA METATARSIANO (2-50), CADA UNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76071', 'DESARTICULACION DE TOBILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76074', 'DRENAJE POR INCISION PROFUNDA (SUBFASCIAL), INFECCION PROFUNDA DEL PIE, CON O SIN COMPROMISO DE VAINA TENDINOSA, MULTIPLE ESPACIOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76075', 'DRENAJE POR INCISION PROFUNDA (SUBFASCIAL), INFECCION PROFUNDA DEL PIE, CON O SIN COMPROMISO DE VAINA TENDINOSA, UN ESPACIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76078', 'EPIFISIODESIS CON INJERTO O GRAPA, PERONE DISTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76079', 'EPIFISIODESIS CON INJERTO O GRAPA, TIBIA DISTAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76086', 'ESCISION RADICAL DE TUMOR TEJIDOS BLANDOS (MALIGNO), PIE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76088', 'ESCISION TUMOR EN PIE, SUBCUTANEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76089', 'EXOSTECTOMIA ESPOLON CALCANEO, CON O SIN LIBERACION FASCIA PLANTAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76090', 'EXOSTECTOMIA PARCIAL DEL CALCANEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76091', 'EXTRACCION DE CUERPOS LIBRES INTRA-ARTICULARES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76096', 'EXTRACCION PROTESIS TOTAL DE TOBILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76097', 'FALANGECTOMIA SIMPLE, CADA UNA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76098', 'FASCIECTOMIA, ESCISION FASCIA PLANTAR, RADICAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76100', 'FASCIOTOMIA DESCOMPRESIVA, PIERNA, ANTERIOR Y/O LATERAL Y POSTERIOR, CON DEBRIDAMIENTO DE MUSCULO O NERVIO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76110', 'HALLUX VALGUS, CORRECCION EXOSTECTOMIA O BUNIECTOMIA SIMPLE (SILVER)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76111', 'HALLUX VALGUS, CORRECCION TIPO MCBRIDE, KELLER O MAYO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76121', 'HEMICONDILECTOMIA O CONDILECTOMIA IF DEDO, PARA CALLO IF, CADA UNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76144', 'LIBERACION FASCIA PLANTAR Y MUSCULAR (STEINDLER)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76163', 'OSTEOTOMIA DE TIBIA Y PERONE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76166', 'OSTEOTOMIA METATARSIANAS MULTIPLES, PARA PIE CAVO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76167', 'OSTEOTOMIA METATARSIANO, BASE O DIAFISIS, CON O SIN ALARGAMIENTO Y CORRECCION ANGULAR, OTROS METATARSIANOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76180', 'RECONSTRUCCION DE TENDON DE AQUILES CON O SIN INJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76185', 'REDUCCION ABIERTA CON O SIN FIJACION TARSO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76198', 'REDUCCION CERRADA HUESOS DEL TARSO CON ANESTESIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76200', 'REDUCCION CERRADA LUXACION MEDIOTARSIANA SIN ANESTESIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76207', 'REDUCCION CERRADA MAS YESO FRACTURA BIMALEOLAR TOBILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76208', 'REDUCCION CERRADA MAS YESO METATTRSIANOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76209', 'REDUCCION E INMOVILIZACION TERCIO MEDIO TIBIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76211', 'REDUCCION E INMOVILIZACION FRACTURA PROXIMAL PERONE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76214', 'REDUCCION E INMOVILIZACION LUXOFRACTURA ARTICULACION METATARSO FALANGICA E INTERFALANGICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76220', 'REDUCCION E INMOVILIZACION SIN ANESTESIA LUXACION ARTICULACION TIBIOPERONEA PROXIMAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76224', 'REDUCCION MAS YESO FRACTURA TRIMALEOLAR TOBILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76261', 'REMOCION CUERPO EXTRAÑO, PIE, COMPLICADO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76264', 'REPARACION DE LIGAMENTO PERONEO ASTRAGALINO ANTERIOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76265', 'REPARACION DE TEJIDOS BLANDOS, LUXACION TENDONES PERONEOS, SIN OSTEOTOMIA DE PERONE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76266', 'REPARACION DE TEJIDOS BLANDOS, LUXACION TENDONES PERONEOS, CON OSTEOTOMIA DE PERONE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76275', 'RESECCION DE METATARSIANO, UNO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76277', 'RESECCION EXOSTOSIS NO ARTICULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76278', 'RESECCION FALANGE PROXIMAL 50 DEDO, (RUIZ MORA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76285', 'RESECCION O CURETAJE DE QUISTE OSEO O TUMOR BENIGNO, TARSO O METATARSIANOS (EXCEPTO ASTRAGALO, CALCANEO)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76286', 'RESECCION O CURETAJE DE QUISTE OSEO O TUMOR BENIGNO, TARSO O METATARSIANOS (EXCEPTO ASTRAGALO, CALCANEO), CON AUTOINJERTO (INCLUYE OBTECION)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76287', 'RESECCION O CURETAJE DE QUISTE OSEO O TUMOR BENIGNO, TARSO O METATARSIANOS (EXCEPTO ASTRAGALO,CALCANEO), CON ALOINJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76288', 'RESECCION O CURETAJE DE QUISTE OSEO O TUMOR BENIGNO, TIBIA O PERONE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76289', 'RESECCION O CURETAJE DE QUISTE OSEO O TUMOR BENIGNO, TIBIA O PERONE, CON ALOINJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76290', 'RESECCION O CURETAJE DE QUISTE OSEO O TUMOR BENIGNO, TIBIA O PERONE, CON AUTOINJERTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76291', 'RESECCION OSTEOFITOS TIBIALES Y/O TALARES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76295', 'RESECCION RADICAL DE PERONE POR TUMOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76296', 'RESECCION RADICAL DE TIBIA POR TUMOR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76298', 'RESECCION RADICAL HUESO DEL TARSO POR TUMOR, EXCEPTO ASTRAGALO Y CALCANEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76314', 'SINOVECTOMIA DE TOBILLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76315', 'SINOVECTOMIA DE TOBILLO, INCLUYENDO TENOSINOVECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76324', 'TENOLISIS EXTENSOR PIE, MULTIPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76340', 'TENOTOMIA ABIERTA FLEXOR, PIE UNICA O MULTIPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76341', 'TENOTOMIA ABIERTA, EXTENSOR, PIE O DEDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76342', 'TENOTOMIA SUBCUTANEA DEDO PIE, UNICA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('76343', 'TENOTOMIA SUBCUTANEA DEDOS PIE, MULTIPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78015', 'REPARO DE ESPICA O CHAQUETA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78016', 'RETIRO DE INMOVILIZADOR EXTERNO BAG' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78019', 'VENDAJE CADERA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78033', 'ASPIRACION POR PUNCION DE ABSCESO, HEMATOMA O QUISTE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78036', 'BIOPSIA DE HUESO (ABIERTA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78040', 'BIOPSIA DE MUSCULO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78046', 'BURSECTOMIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78049', 'COLGAJO EN ISLA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78091', 'EXTIRPACION DE HIGROMA QUMSTICO DE CUELLO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78092', 'FASCIOTOMIA DE DESCOMPRESION DE COMPARTIMIENTO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78093', 'FASCIOTOMIA DE DESCOMPRESION DE COMPARTIMIENTO CON DEBRIDAMIENTO DE MUSCULO Y/O NERVIO NO VIABLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('78113', 'INJERTO TENDINOSO A DISTANCIA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79004', 'BIOPSIADE PIEL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79011', 'CIERRE DE DEHISCENCIA DE HERIDA SUPERFICIAL, SIMPLE' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79017', 'CIRUGIA DE MOHS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79018', 'COLGADO DE PIEL (ADICIONAL)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79019', 'COLGAJO DE CUERO CABELLUDO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79024', 'COLGAJO LIBRE (CON MICROCIRUGIA)' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79025', 'COLGAJO MUSCULAR,MIOCUTANEO Y FASCIOCUTANEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79027', 'COLGAJO SIMPLE DE PIEL DE MAS DIEZ CENTIMETROS CUADRADOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79028', 'COLGAJO SIMPLE DE PIEL ENTRE CINCO A DIEZ CENTIMETROS CUADRADOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79029', 'COLGAJO SIMPLE DE PIEL ENTRE DOS A CINCO CENTIMETROS CUADRADOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79030', 'COLGAJO SIMPLE DE PIEL HASTA DOS CENTIMETROS CUADRADOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79098', 'DRENAJE PIEL Y/O TEJIDO CELULAR SUBCUTANEO, INCLUYE:ABSCESO SUPERFICIAL HEMATOMA, PANADIZO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79100', 'ESCARECTOMÍA EN ÁREA ENTRE 11% Y 20% DE SUPERFICIE CORPORAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79101', 'ESCARECTOMÍA EN ÁREA HASTA 10% DE SUPERFICIE CORPORAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79103', 'ESCAROTOMÍA EN DOS O MÁS EXTREMIDADES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79134', 'EXTRACCION CUERPO EXTRAÑO TEJIDO CELULAR SUBCUTANEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79140', 'FISTULECTOMIA DE PIE Y TEJIDO CELULAR SUBCUTANEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79141', 'FOTOTERAPIA CA BASOCELULAR' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79156', 'INCISION Y REMOCION DE CUERPO EXTRAÑO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79161', 'INFILTRACION INTRALESIONAL HASTA 5 LESIONES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79162', 'INFILTRACION INTRALESIONAL DE 6 A 10 LESIONES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79163', 'INFILTRACIONES MAYORES DE 10 LESIONES' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79164', 'INJERTO CONDROCUTANEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79185', 'RESECCION DE TUMOR MALIGNO PIEL O ANEXOS O TEJIDO SUBCUTANEO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79186', 'PLASTIA EN Z O W EN AREA ESPECIAL (CARA, CUELLO, MANOS, PIES, PLIEGUES DE FLEXION, GENITALES) ENTRE TRES A CINCO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79187', 'PLASTIA EN Z O W EN AREA ESPECIAL (CARA, CUELLO, MANOS, PIES, PLIEGUES DE FLEXION, GENITALES) ENTRE UNA A DOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79188', 'PLASTIA EN Z O W EN AREA ESPECIAL (CARA, CUELLO, MANOS, PIES, PLIEGUES DE FLEXION, GENITALES) MAS DE CINCO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79189', 'PLASTIA EN Z O W EN AREA GENERAL ENTRE TRES A CINCO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79190', 'PLASTIA EN Z O W EN AREA GENERAL ENTRE UNA A DOS' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('79191', 'PLASTIA EN Z O W EN AREA GENERAL MAS DE CINCO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('80002', 'BIOPSIA UÑA, MATRIZ Y/O LECHO' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('80013', 'REPOSICION UÑA' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('81001', 'EXICISION DE QUISTE PILONIDAL' ,10, now(), now(), 'f', 1);
 INSERT INTO "rate_procedures"( "custom_code", "description", "rate_id", "created_at", "updated_at", "erased", "user_id") 
            VALUES ('81002', 'INCISION Y DRENAJE DE QUISTE PILONIDAL' ,10, now(), now(), 'f', 1);